
### Inter-core Interrupter or Doorbell

This folder contains device driver fragments for message passing using an inter-core interrupter (or doorbell).

For now, see 
  http://www.cl.cam.ac.uk/teaching/1011/SysOnChip/additional/doorbell