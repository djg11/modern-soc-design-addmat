#ifndef FOURPHASE_NETLEVEL_SRC_H
#define FOURPHASE_NETLEVEL_SRC_H

// Four-Phase Asynchronous Handshake using SystemC

// This folder contents (C) DJ Greaves 2021. You may use them as you
// wish, without waranty or license from the author, provided this
// copyright message is preserved.


#include <systemc.h>

// Four-phase data generator.
SC_MODULE(FOURPHASE_NETLEVEL_SRC)
{

  sc_out<bool> strobe;
  sc_in<bool> ack;
  sc_out<sc_uint<8> > data;

  void datagen()
  {
    strobe = 0;
    int words_generated = 0;
    while(words_generated++ < 50)
      {
	while (ack) wait ();	
	wait(3, SC_NS);
	data = 100 + words_generated;
	strobe = 1;
	wait(3, SC_NS);
	while (!ack) wait ();
	wait(3, SC_NS);
	strobe = 0;
      }
    std::cout << "Data generator finished\n";
    sc_stop();
  }

  SC_CTOR(FOURPHASE_NETLEVEL_SRC)
    {
      SC_THREAD(datagen);
      sensitive << ack;
    }
};

// eof
#endif

