// Four-Phase Asynchronous Handshake using SystemC 

// This folder contents (C) DJ Greaves 2021. You may use them as you
// wish, without waranty or license from the author, provided this
// copyright message is preserved.

// https://www.cl.cam.ac.uk/~djg11/wwwhpr/fourphase/fourphase.html

#include <systemc.h>
#include "fourphase_netlevel_src.h"
#include "fourphase_netlevel_sink.h"

#include "fourphase_tlm1_sink.h"

SC_MODULE(BASIC_NETLEVEL_TESTBENCH)
{

  sc_signal<bool> strobe, ack;
  sc_signal<sc_uint<8> > data;

  FOURPHASE_NETLEVEL_SRC *src0;
  FOURPHASE_NETLEVEL_SINK *sink0;


  FOURPHASE_TLM_SINK *need_for_exercise_ = new FOURPHASE_TLM_SINK("tlm_sink_for_exercise");
  
  SC_CTOR(BASIC_NETLEVEL_TESTBENCH): strobe("strobe"), ack("ack"), data("data")
    {
      src0 = new FOURPHASE_NETLEVEL_SRC("src0");
      src0->strobe(strobe);
      src0->ack(ack);
      src0->data(data);

      sink0 = new  FOURPHASE_NETLEVEL_SINK("sink0");
      sink0->strobe(strobe);
      sink0->ack(ack);
      sink0->data(data);
      
    }
};



int sc_main(int argc, char *argv[])
{

  BASIC_NETLEVEL_TESTBENCH *toplevel = new BASIC_NETLEVEL_TESTBENCH("toplevel");
  sc_trace_file *wf = sc_create_vcd_trace_file("tracefile");
  sc_trace(wf, toplevel->strobe, "strobe");
  sc_trace(wf, toplevel->ack, "ack");
  sc_trace(wf, toplevel->data, "data");    

  
  sc_start(100.0, SC_MS);
  sc_close_vcd_trace_file(wf); //  wf.close(); 
std::cout << "Testbench finished at " << sc_time_stamp() << "\n";
  return 0;
}

// eof

