#ifndef FOURPHASE_NETLEVEL_SINK_H
#define FOURPHASE_NETLEVEL_SINK_H

// Four-Phase Asynchronous Handshake using SystemC

// This folder contents (C) DJ Greaves 2021. You may use them as you
// wish, without waranty or license from the author, provided this
// copyright message is preserved.


#include <systemc.h>

// A data sink that prints out received bytes.
SC_MODULE(FOURPHASE_NETLEVEL_SINK)
{

  sc_in<bool> strobe;
  sc_out<bool> ack;
  sc_in<sc_uint<8> > data;

  void datasink()
  {
    ack = 0;
    while(1)
      {
	while (!strobe) wait();
	sc_uint<8> read_data = data;
	wait(3, SC_NS);
	std::cout << sc_time_stamp() << ": Sink received word " << read_data << "\n";
	ack = 1;
	wait(3, SC_NS);
	while (strobe) wait();
	wait(3, SC_NS);	  
	ack = 0;
      }
  }

  SC_CTOR(FOURPHASE_NETLEVEL_SINK)
    {
      SC_THREAD(datasink);
      sensitive << strobe;
    }
};

//eof
#endif
