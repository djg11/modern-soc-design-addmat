#ifndef SIMPLE_TLM1_BLOCKING_SINK_IF
#define SIMPLE_TLM1_BLOCKING_SINK_IF

// This folder contents (C) DJ Greaves 2021. You may use them as you
// wish, without waranty or license from the author, provided this
// copyright message is preserved.

#include <systemc.h>

class simple_tlm1_blocking_sink_if
{
  void putbyte(sc_uint<8> data);
};

//eof
#endif
