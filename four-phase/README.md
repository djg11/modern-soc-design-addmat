# Four-Phase Asynchronous Handshake using SystemC #

This folder contents (C) DJ Greaves 2021. You may use them as you
wish, without waranty or license from the author, provided this
copyright message is preserved.

The four-phase asynchronous handshake is referred to in a number of
places in the book Modern SoC design by David J Greaves.

This folder contains baseline implementations of the Four-Phase
Asynchronous Handshake using SystemC using net-level coding and TLM
coding.

## Folder Contents ##



-rw-rw-r--. 1 djg11 djg11      890 Jul 30 12:18 fourphase_netlevel_sink.h
-rw-rw-r--. 1 djg11 djg11      903 Jul 30 12:15 fourphase_netlevel_src.h
-rw-rw-r--. 1 djg11 djg11      786 Jul 30 12:27 fourphase_tlm1_sink.h
-rw-rw-r--. 1 djg11 djg11     1520 Jul 30 12:29 main.cpp
-rw-rw-r--. 1 djg11 djg11      434 Jul 30 12:29 Makefile
-rw-rw-r--. 1 djg11 djg11      979 Jul 30 11:20 README.md
-rw-rw-r--. 1 djg11 djg11      356 Jul 30 12:26 simple_tlm1_blocking_sink_if.h


## Exercise ##

Using the additional materials from the four-phase folder (i.e. _this_
folder), perform a net-level simulation of the source and sink. Then
code in SystemC a net-level FIFO to go between them and modify the
test bench to include it. Finally, write and deploy a transactor to
the TLM-1 style sink that is also provided.



Also mentioned is the two-phase variant.
This folder contains material for the exercise at the end of the ESL chapter.
