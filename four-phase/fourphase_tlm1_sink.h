// Four-Phase Asynchronous Handshake using SystemC #

// This folder contents (C) DJ Greaves 2021. You may use them as you
// wish, without waranty or license from the author, provided this
// copyright message is preserved.

#include "simple_tlm1_blocking_sink_if.h"

// A data sink that prints out received bytes.
class FOURPHASE_TLM_SINK: sc_module, public simple_tlm1_blocking_sink_if
{
 public:

  void datasink(sc_uint<8> read_data) // This implements the functionality of the sink.
  {
    std::cout << sc_time_stamp() << ": Sink received word " << read_data << "\n";
  }

  void putbyte(sc_uint<8> data)  // This method is part of (all of infact) the blocking_sink_if.
  {
    datasink(data);
  }

  
  SC_CTOR(FOURPHASE_TLM_SINK)
    {
      // nothing needed.
    }
};

//eof
