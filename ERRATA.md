


# Modern SoC Design First Edition Errata and Clarifications #


In Section 2.7.1, the text states that the additional pins on a D9 serial connector are
sometimes used for Xon/Xoff protocol.  This is later defined in section 3.4.4 as any
flow control mechanism that uses binary (bang-bang) feedback to turn the sender on or off.
But in the context of 2.7.1 this is a little misleading.  When the additional pins
are used, the Ready-to-Send/Clear-to-Send RTS/CTS protocol is in use, with the term Xon/Xoff
being reserved for when specific in-band characters are used.
  XON	Resume transmission	DC1   (Control-Q)
  XOFF	Pause transmission	DC3   (Control-S)


# End #