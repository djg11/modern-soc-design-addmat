
# Prazor Virtual Platform #

Here we present a snapshot of the PRAZOR virtual platform: a SystemC SoC model using TLM 2.0 blocking coding style.

This forms part of the additional material for Modern SoC design by DJ Greaves.

Documentation is being updated at the moment (August 2021) but meanwhile, perhaps see https://www.cl.cam.ac.uk/~djg11/prazor/