Copyright (C) 2015-20 XPARCH, Ltd. <info@xparch.com>

= Steps to build the PRAZOR/VHLS simulator =

== You need boost to be installed and a pointer provided to it ==

(1) Add boost library >= 1.48
Setup a path to BOOST using something like
export BOOST_ROOT=/usr/local/boost/boost_1_48_0

# If boost is not built you will need to cd to it and do something like
  $ chmod +x ./bootstrap.sh 
  $ chmod +x tools/build/v2/engine/*.sh
  $ ./boostrap.sh
  $ ./b2
# Note you may need to chmod +x some of the .sh files to make it build.

Alternatively, install it using your package manager:
  $ dnf install boost-devel
  

== Install SystemC ==

(2) Install SystemC library by downlaod from Accellera
(generally it is in usr/local or /opt), if it cannot be found when you get to ./configure then
use something like 

# export SYSTEMC=/usr/local/systemc/systemc-2.3.3
export   SYSTEMC=/opt/systemc
export   LDFLAGS="-L$SYSTEMC/lib-linux64   -L/usr/local/lib"
export   CXXFLAGS="-I$SYSTEMC/include"

If there are problems make sure you have compiled and installed SystemC for your architecture. You might need to make a symlink
as follows
 $ cd $SYSTEMC
 $ make install
 $ ln -s lib-linux64 lib-x86_64

You may find it helpful to have a debug verison of SystemC available and installed in
a different folder.   This can be enabled before compiling the SystemC library using something
like

$ ../configure --prefix=/usr/local/systemc-2.3.2-debug-version --enable-debug
$ cmake ../ -DCMAKE_CXX_STANDARD=14 -DCMAKE_BUILD_TYPE=Debug

You will need to compiler Prazor, SystemC and TLM POWER3 all with the same C++ abi, such as C++14 mentioned above.


== Creating a configure script to build the virtual platform. ==

Before make is invoked, you need to create configure script in order 
You can do this by changing directory to vhls and invoking autoreconf. Once
autoreconf has finished you should have configure script in vhls directory.
There may be one supplied already, but you should probably recreate it
with autoreconf.

$ autoreconf
$ automake --add-missing

To build simulator cd to vhls and get everything set up such that
./configure and make work ok. The steps to set everything up are as follows:


== TLM POWER3 (optional) ==

(3) Build tlm-power3 library (separate distribution) - if you dont want to install it in /use/local then adjust
the paths in step 4 accordingly. You do not need to 'install' it at all if you adjust your TLM_POWER3 setting for the following step to reflect where it resides (e.g. in your own folder).
	* autoreconf
	* LDFLAGS="-L/usr/local/lib-linux64" CXXFLAGS="-I/usr/local/include/tlm_core/tlm_2" ./configure --prefix=/usr/local
	* make
	* make install


The TLM POWER3 instructions need updating since tlm_2 is now part of SystemC. TODO

The prazor simulator can be compiled with and without TLM_POWER3 installed.


TLM_POWER3 adds overhead to the execution of the simulator which is not always wanted.  The hamming
distance tracking for mode 3 adds can add on a considerable overhead if the confidence switcher
is set to high accuracy. 
Without it you will need definitions of the following macros
#define  POWER3(X)            /* nothing */
typedef int pw_customer_acct; /*use a simple int as a placeholder */
#define PW_TLM3(X)            /* nothing */
typedef tlm::tlm_base_protocol_types PW_TLM_TYPES;
typedef tlm::tlm_generic_payload PW_TLM_PAYTYPE;


If TLM_POWER3 is not found or if "--with-tlm-power=no" is passed to ./configure it will be disabled. But you need
to make sure you unset TLM_POWER3 in your shell environment since the same identifier is used by the C++ preprocessor
to direct compilation.

== Build ==

(4) Build simulator in vhls directory just using make
$ make

There may be a 'make install' option, but given that modifying it is
part of the design exploration process, rather then installing it you
can simply implement your hardware configuration that you want to
simulate in the platform directy and the pass to the binary library
that was built in .libs directory of your implementation using the
-platform flag, as shown next.

== RUN ==

(5) Run any example from the images directory.  

You should find some example invocations in the Makefiles in the images folders. The DRAM simulator requires a pair of args for its configuration files and you need an ELF binary to load into the DRAM.  Ther emay be some precompiled binaries in the images folder.  The binary you want depends on the architecture you chose: mips, x86, openrisc, arm etc..

You will separately need binutils and gcc for the architecture you chose for when you recompile or make your own binary images. You will have the x86_64 standard binutils on your workstation/laptop most likely from building the simulator and general C++ programming.

$ cd images/hello-world/
$ file precompiled-hello-arm-zynq
    hello: ELF 32-bit MSB executable, OpenRISC, version 1 (SYSV), statically linked, not stripped
$ ls -l precompiled-hello-arm-zynq
-rwxrwxr-x. 1 djg11 djg11 343653 Sep  2 08:25 precompiled-hello-arm-zynq
$ $(VHLS)/src/vhls -platform $(VHLS)/src/platform/risc/or1k/.libs/libor1k.so -dram-system-ini /home/djg11/d320/prazor/trunk/vhls/src/dramsim2/dist/system.ini.example  -dram-device /home/djg11/d320/prazor/trunk/vhls/src/dramsim2/dist/ini/DDR3_micron_8M_8B_x16_sg15.ini -cores 1 -name vv -image ./precompiled-hello-arm-zynq -- red yellow green blue well done 
  ... an Xwindow console may pop up if X is enabled
$ cat vv.the_top.uart0.txt 
Hello World
$


END


