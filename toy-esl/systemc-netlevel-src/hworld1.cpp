//
// This folder contains additional material for the book Arm Modern SoC Design by DJ Greaves.
// For information on all Arm Education Media publications, visit our website at
// https://www.arm.com/resources/education/books


// Materials in this folder are provided (C) DJ Greaves 2021 with no expressed or implied
// warranty. You may use them as you wish, without warranty or license
// from the author, provided this copyright message is preserved.

// 
// Structural net list example:
// ----------------------------


#include "systemc.h"

SC_MODULE(dff)
{
  sc_in <bool> clk, d, reset;
  sc_out <bool> q;
  SC_CTOR(dff)
  {
    void runme(); 
    SC_METHOD(runme); sensitive << clk.pos() << reset;
  }
  void runme() 
  { if (reset.read()) q = 0; 
    else
      {
	q = d.read(); 
	printf("DFF pending set to %i\n", d.read()); 
      }
  }
};



SC_MODULE(shiftreg)  // Two-bit shift register 
{   sc_in  <bool>  clk, reset, din;
    sc_out <bool> dout;
 
    sc_signal <bool> q1_s;
    dff dff1, dff2;      // Instantiate FFs
 
    SC_CTOR(shiftreg) : dff1("dff1"), dff2("dff2")
    {   dff1.clk(clk);
        dff1.reset(reset);
        dff1.d(din);
        dff1.q(q1_s);
 
        dff2.clk(clk);
        dff2.reset(reset);
        dff2.d(q1_s);
        dff2.q(dout);
    }
};

SC_MODULE(clkgen)
{
  sc_out <bool> clk;

  SC_CTOR(clkgen)
  {
    SC_THREAD(runme);
  }
  
  void runme()
  {
    clk = 0;
    while (1)
      {
	cout << "Toggle at " << sc_time_stamp() << "\n";
	wait(50, SC_NS);
	clk = !clk.read();
      }
  }
}; 


int sc_main(int argc, char *argv[])
{
  std::cout << "Hello world: shift register example:\n";


  sc_trace_file *tf = sc_create_vcd_trace_file("trace");
  tf->set_time_unit(1, SC_NS);

  sc_signal <bool> clk, serialin, serialout, reset;


  clkgen u_clkgen("u_clkgen");
  u_clkgen.clk(clk);


  shiftreg u_shiftreg("u_shiftreg");
  u_shiftreg.clk(clk);
  u_shiftreg.din(serialin);
  u_shiftreg.dout(serialout);
  u_shiftreg.reset(reset);

  sc_trace(tf, serialin, "serialin");
  sc_trace(tf, serialout, "serialout");
  sc_trace(tf, clk, "clk");
  sc_trace(tf, reset, "reset");


  // Here's a useful but crude way of generating input stimulus.
  reset = 1; serialin =0;
  sc_start(1000, SC_NS);
  reset = 0;
  cout << "Exiting from reset at " << sc_time_stamp() << "\n";

  sc_start(2000, SC_NS);
  serialin = 1;
  cout << "Set input high at " << sc_time_stamp() << "\n";
  sc_start(2000, SC_NS);

  cout << "Finished at " << sc_time_stamp() << "\n";
  return 0;

}

// eof
