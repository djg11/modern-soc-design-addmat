

# SystemC and SystemC toy-esl #

This folder contains additional material for the book Arm Modern SoC Design by DJ Greaves.
For information on all Arm Education Media publications, visit our website at
https://www.arm.com/resources/education/books


Materials are provided (C) DJ Greaves 2021 with no expressed or implied
warranty. You may use them as you wish, without warranty or license
from the author, provided this copyright message is preserved.


The toy ESL folder contains a set of exercises for familiarity with blocking TLM coding using TLM 1 coding style.


END
