
# Environment  configuration or set up.


#// This folder contents (C) DJ Greaves 2021. You may use them as you
#// wish, without waranty or license from the author, provided this
#// copyright message is preserved.



SYSTEMC?=/opt/systemc
INCLUDES=-I$(SYSTEMC)/include
SCLIB=$(SYSTEMC)/lib-linux64
LDFLAGS=-L$(SCLIB) -lsystemc
CXX=g++
CXXFLAGS=-g

# eof
