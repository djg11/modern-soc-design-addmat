#ifndef TLM_BUSARB_H
#define TLM_BUSARB_H

// (C) 2009 DJ Greaves.  
//
// TLM-style bus arbitrator for blocking transport.
//
// Showing need for some sort of tagging with multiple instances of the same port.
//

// Alternative approaches to what's shown here:
//   SystemC TLM 1.0 put the tag inside the generic payload.
//   TLM 2.0 puts all the TLM ports instances inside a convenience 'sockets' class.

#include "../djip/nominal_tlm.h"


class tlm_busarb : public tagged_memport_if <named_class0>, public tagged_memport_if<named_class1>, public sc_module
{
public:
  uint32 threshold;
  sc_port <memport_if> output_port;


  // Both interfaces invoke this single implementation of b_transact.
  void b_transact(MEMPAYLOAD *p, sc_time &delta)
  {
    //std::cout << "b_transact invoked\n";
    output_port->b_transact(p, delta);
  }; 


  SC_CTOR(tlm_busarb)
  {


  };

  // Undesirable need to implement the virtual methods that only serve as markers.
  void tag(named_class0 *a) {  };
  void tag(named_class1 *a)  {  };


};

// eof
#endif
