// Toy ESL Nominal Processor:   (C) DJ Greaves, 2009.  
// 
//
// This testbench file contains two TLM-style CPU masters and a bus arbiter, connected to a single RAM.
//

#define uint32 unsigned int // Perhaps instead defined as sc_uint<32> ?



#include <stdlib.h>
#include <string.h>

#include "systemc.h"
#include "../djip/nominalproc_tlm.h"
#include "../djip/clock100.h"
#include "../djip/ram32.h"
#include "../djip/tlmram32.h"
#include "../djip/tlm_busmux.h"
#include "../djip/nominal_tlm.h"

#include "tlm_busarb.h"



ASM test_program[] = 
  {
    { "start", ORG,  0, 0, "Reset to here" },
    { "io_base", EQU,  0x20000, 0, "Output device" },
    { 0,      JMP,  L(enter), 0, "" },
    { "unity", DEFW,  1, 0, "" },


    { "enter", LOAD,  R1, L(unity), "" },
    { "loop",  ADD,  R1, R1, "Double until wraps to zero" },
    { "",      STORE,  R1, L(io_base), "" },
    { "",      BNZ,  R1, L(loop), "" },
    { "",      HALT, 0, 0, "" },
    { "",      END,  0, 0, "" } // Terminate end of program

  }
;


//
//
//
int sc_main(int argc, char *argv[])
{
  assembler *A;
  uint32 MAX = 128;
  uint32 *code = new uint32 [MAX];
  A = new assembler(code, MAX); 
  A->assemble("test_program", test_program);

  //std::cout << "Debug: code[0] is " << code[0] << "\n";

  sc_trace_file *tf = sc_create_vcd_trace_file("trace");
  tf->set_time_unit(1, SC_NS);

  sc_signal <bool> rst, clk;

  clock100 u_clkgen("u_clkgen");
  u_clkgen.clk(clk);


  // TLM-style components now follow
  tlmram32 code_memory_0("code_memory_0");
  code_memory_0.contents(code, MAX);

  tlmram32 data_memory_0("data_memory_0");
  data_memory_0.contents(0, MAX);


  tlm_busmux tlm_busmux_0("tlm_busmux_0");
  tlm_busmux_0.threshold = 0x10000;
  tlm_busmux_0.port0(code_memory_0);
  tlm_busmux_0.port1(data_memory_0);



  // First processor core
  nominalproc_tlm nominal_0("nominal_0");
  sc_signal <bool> irq0;
  irq0 = 0; 
  nominal_0.rst(rst);
  nominal_0.irq(irq0);

  // Second processor core
  nominalproc_tlm nominal_1("nominal_1");
  sc_signal <bool> irq1;
  irq1 = 0; 
  nominal_1.rst(rst);
  nominal_1.irq(irq1);


  // Bus arbiter
  tlm_busarb tlm_busarb_0("tlm_busarb_0");

  tagged_memport_if<named_class0> &k0 = tlm_busarb_0;
  tagged_memport_if<named_class1> &k1 = tlm_busarb_0;
  nominal_0.port0(k0);
  nominal_1.port0(k1);
  tlm_busarb_0.output_port(tlm_busmux_0);


  // Alternate syntax:
  //  nominal_0.port0(reinterpret_cast<tagged_memport_if<int> >(tlm_busarb_0));

  // Testbench now follows
  rst = 1; 
  sc_start(100, SC_NS);
  rst = 0;
  cout << "End of reset\n";
  sc_start(10, SC_US);

  cout << "TLM-style dual CPU SystemC nominal proc finished at " << sc_time_stamp() << "\n";
  sc_close_vcd_trace_file(tf);
  return 0;
}


// eof
