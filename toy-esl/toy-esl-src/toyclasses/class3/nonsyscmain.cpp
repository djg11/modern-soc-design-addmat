// Toy ESL Nominal Processor:   (C) DJ Greaves, 2009.  


// Here is a testwrapper for nominalproc that does not use SystemC.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#include "../djip/nominalproc_iss.h"



ASM test_program[] = 
  {
    { "start", ORG,  0, 0, "Reset to here" },
    { "io_base", EQU,  0x20000, 0, "Output device" },
    { 0,      JMP,  L(enter), 0, "" },
    { "unity", DEFW,  1, 0, "" },


    { "enter", LOAD,  R1, L(unity), "" },
    { "loop",  ADD,  R1, R1, "Double until wraps to zero" },
    { "",      STORE,  R1, L(io_base), "" },
    { "",      BNZ,  R1, L(loop), "" },
    { "",      HALT, 0, 0, "" },
    { "",      END,  0, 0, "" } // Terminate end of program

  }
;


class sw_nominalproc_iss : public nominalproc_iss
{
  public:
  uint32 *code, codelen;  



  int codefetch(int a)
  {
    return code[a/4];
  }

  int datafetch(int a)
  {
    return 0x55aa55aa; 
  }


  void datawrite(int a, int d)
  {
    printf("Datawrite A=0x%08X  D=0x%08x\n", a, d);
  }


  sw_nominalproc_iss(uint32 *code0, uint32 codelen0) : code(code0), codelen(codelen0)
  {

  }



};


int main()
{

  assembler *A;
  uint32 MAX = 100;
  uint32 *code = (uint32 *) malloc(MAX * sizeof(uint32));

  A = new assembler(code, MAX); 
  A->assemble("test_program", test_program);

  sw_nominalproc_iss *ISS  = new sw_nominalproc_iss(code, MAX); 

  ISS->reset();


  while (ISS->runhaltb) ISS->step();
  printf("Nominal proc Instruction Set Simulator (ran without SystemC)\n");

  return 0;
}


// eof
