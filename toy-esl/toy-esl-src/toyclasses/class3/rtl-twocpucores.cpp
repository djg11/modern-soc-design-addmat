// Toy ESL Nominal Processor:   (C) DJ Greaves, 2009.  
// 
// 
//
// This file contains a SystemC testbench with two CPUs, each with local resources connected in RTL style. 
// This is needed for toyclass 5.

#define uint32 unsigned int
// Perhaps instead defined as sc_uint<32> ?


#include <stdio.h>
#include <string.h>

#include "systemc.h"
#include "../djip/nominalproc_rtl.h"
#include "../djip/clock100.h"
#include "../djip/ram32.h"
#include "../djip/busmux.h"
#include "../djip/addr_decode.h"
#include "../djip/dma_controller.h"
#include "../djip/bus_bridge.h"




ASM test_program[] = 
  {
    { "start", ORG,  0, 0, "Reset to here" },
    { "io_base", EQU,  0x20000, 0, "Output device" },
    { 0,      JMP,  L(enter), 0, "" },
    { "unity", DEFW,  1, 0, "" },


    { "enter", LOAD,  R1, L(unity), "" },
    { "loop",  ADD,  R1, R1, "Double until wraps to zero" },
    { "",      STORE,  R1, L(io_base), "" },
    { "",      BNZ,  R1, L(loop), "" },
    { "",      HALT, 0, 0, "" },
    { "",      END,  0, 0, "" } // Terminate end of program

  }
;
 

//
// A processor nominal unit with local instruction RAM.
//
SC_MODULE(nominalunit)
{
  assembler *A;
  uint32 MAX, *code;


  sc_in <bool> rst, clk, irq;

  // Bus master port
  sc_in <bool>   opack1;
  sc_in <uint32> rdata1;
  sc_out  <uint32> addr, wdata;
  sc_out <bool>  opreq1, rwbar;

  // Local signals

  sc_signal <uint32> rdata, rdata0;
  sc_signal <bool> opreq, opreq0;
  sc_signal <bool> opack, opack0;


  void traceme(sc_trace_file *tf)
  {
#define VCD(X)  sc_trace(tf, X, #X);
    VCD(clk);  VCD(rst);
    VCD(irq);  VCD(addr); VCD(rwbar);
    VCD(wdata);   VCD(rdata);
    VCD(opreq1);  VCD(opreq0);
    VCD(opack1);  VCD(opack0);
  }

  nominalproc_rtl *nominal_0;
           busmux *busmux_0;
      addr_decode *addr_decode_0;
            ram32 *code_memory_0;


  SC_CTOR(nominalunit)
  {

            nominal_0 = new nominalproc_rtl("nominal_0");
             busmux_0 = new busmux("busmux_0");
        addr_decode_0 = new addr_decode("addr_decode_0");
        code_memory_0 = new ram32("code_memory_0");
    
    nominal_0->rst(rst);
    nominal_0->clk(clk);
    nominal_0->irq(irq);
    nominal_0->opreq(opreq);
    nominal_0->opack(opack);
    nominal_0->rdata(rdata);
    nominal_0->wdata(wdata);
    nominal_0->addr(addr);
    nominal_0->rwbar(rwbar);
  
  
    busmux_0->threshold = 0x10000;
    busmux_0->in0(rdata0);
    busmux_0->in1(rdata1);
    busmux_0->opack0(opack0);
    busmux_0->opack1(opack1);
    busmux_0->y(rdata);
    busmux_0->opack(opack);
    busmux_0->addr(addr);
    


    addr_decode_0->threshold = 0x10000;
    addr_decode_0->y0(opreq0);
    addr_decode_0->y1(opreq1);
    addr_decode_0->g(opreq);
    addr_decode_0->addr(addr);
    

    MAX = 128;
    uint32 *code = (uint32 *) malloc(MAX * sizeof(uint32));
    A = new assembler(code, MAX); 
    A->assemble(name(), test_program);
    
    code_memory_0->contents(code, MAX);
    
    code_memory_0->opack(opack0);
    code_memory_0->rdata(rdata0);
    code_memory_0->opreq(opreq0);
    code_memory_0->rst(rst);
    code_memory_0->clk(clk);
    code_memory_0->wdata(wdata);
    code_memory_0->addr(addr);
    code_memory_0->rwbar(rwbar);
    

  }

};



int sc_main(int argc, char *argv[])
{
  sc_trace_file *tf = sc_create_vcd_trace_file("trace");
  tf->set_time_unit(1, SC_NS);

  sc_signal<bool> clk, rst;
  clock100 u_clkgen("u_clkgen");
  u_clkgen.clk(clk);


  // First processor unit (A): 
  sc_signal <bool>  opreq1_A, rwbar_A, irq_A;
  sc_signal <bool>   opack1_A;
  sc_signal <uint32> rdata1_A;
  sc_signal <uint32> addr_A, wdata_A;

  VCD(opreq1_A); VCD(rwbar_A); VCD(opack1_A); VCD(irq_A);
  VCD(rdata1_A); VCD(addr_A); VCD(wdata_A); VCD(rwbar_A);

  nominalunit *nominalunit_A = new nominalunit("nominalunit_A");
  nominalunit_A->clk(clk);
  nominalunit_A->rst(rst);
  nominalunit_A->irq(irq_A);
  nominalunit_A->rwbar(rwbar_A);
  nominalunit_A->opreq1(opreq1_A);
  nominalunit_A->opack1(opack1_A);
  nominalunit_A->wdata(wdata_A);
  nominalunit_A->addr(addr_A);
  nominalunit_A->rdata1(rdata1_A);


#if 1
  // Second processor unit (B): 
  sc_signal <bool>  opreq1_B, rwbar_B, irq_B;
  sc_signal <bool>   opack1_B;
  sc_signal <uint32> rdata1_B;
  sc_signal <uint32> addr_B, wdata_B;

  VCD(opreq1_B); VCD(rwbar_B); VCD(opack1_B); VCD(irq_B);
  VCD(rdata1_B); VCD(addr_B); VCD(wdata_B); VCD(rwbar_B);

  nominalunit *nominalunit_B = new nominalunit("nominalunit_B");
  nominalunit_B->clk(clk);
  nominalunit_B->rst(rst);
  nominalunit_B->irq(irq_B);
  nominalunit_B->rwbar(rwbar_B);
  nominalunit_B->opreq1(opreq1_B);
  nominalunit_B->opack1(opack1_B);
  nominalunit_B->wdata(wdata_B);
  nominalunit_B->addr(addr_B);
  nominalunit_B->rdata1(rdata1_B);
  nominalunit_B->traceme(tf);
#endif



  irq_A  = 0;
  irq_B  = 0;

  rst = 1; 
  sc_start(100, SC_NS);
  rst = 0;
  printf("End of reset\n");
  sc_start(5, SC_US);
  irq_B = 1;
  cout << ("Interrupt to B\n") << sc_time_stamp() << "\n";
  sc_start(10, SC_US);

  cout << "Dual CPU SystemC nominal proc finished at " << sc_time_stamp() << "\n";
  sc_close_vcd_trace_file(tf);
  return 0;
}


// eof
