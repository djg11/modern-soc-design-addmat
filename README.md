# README #

This repository contains various folders of worked examples and additional material
for the book Modern SoC design by David J Greaves et al.

The content will be uploaded here over the next few months (July/August 2021).

The official URL is http://bitbucket.org/djg11/modern-soc-design-djg
which currently redirects to here, which is https://bitbucket.org/djg11/modern-soc-design-addmat/src/master

Each folder is specifically referenced at some point in the book.

Currently, many of the sub-folders are missing or just contain links to old exercise sheets at the University of Cambridge. These
are being revisited, in turn, by the author and converted to more modern or tidier form.

## Errata ##

Clarifications or corrections for the published content will be posted in the ERRATA.md file.

## Folder Structure ##
------------------------

### bit-blasting ###

Code and examples for converting vector arithmetic into bit-level arithmetic.  Most is straightforward, but division
by constants is interesting.

### bus-checker ###

For the Formal chapter of the book, this folder contains an exercise.

### cacti22 ###

For the ESL chapter, this folder contains the HP CACTI memory design
tool, as modified by DJG to generate the data tables for 22nm SRAM,
as presented in the book.

### doorbell ###

For the Processors and IP Blocks chapter, this folder contains device
driver fragments for message passing using an inter-core interrupter
(or doorbell).

### UART-device-driver ###

Chapter 2 presents the basic code for an interrupt-driven device driver. The full code is given here.


### ethercrc ###

For the Design Exploration chapter, design ideas for a CRC computation accelerator along with a TLM/ESL model.

### four-phase ###

The four-phase asynchronous handshake is referred to in a number of places in the book. Also mentioned is the two-phase variant.
This folder contains material for the exercise at the end of the ESL chapter.

### framestore ###

For the Processors and IP Blocks chapter, this folder contains the RTL for a framestore that drives an HDMI output socket.

### kiwi-hls-examples ###

For the Design Exploration chapter, this folder contains some simple HLS examples where various forms of loop demonstate
different amounts of parallel speedup.

### locallink ###

For the Interconnect chapter, this folder contains examples of the standard synchronous handshake and the Xilinx LocalLink protocol
implemented in RTL and SystemC.

### multiaccess-noc ###

For the Design Exploration chapter, this folder contains hardware construction language (HCL) code that generates RTL implementations
of on-chip folded busses, rings and NoCs so that typical gate counts can be estimated.

The spreadsheet that plotted the results should also be included.

### prazor folder ###

For the Design Exploration and ESL chapters, the Prazor virtual
platform (ESL simulator) is implemented using SystemC and supports
inter-working between RTL-level SystemC and high-level SystemC models
using TLM 2.0 sockets.  Any mix of heterogeneous cores can be included
in the system model. The ISAs provided are x86\_64, Arm-32 (including
Thumb2), MIPS and OpenRISC-32, but others can be added.  Prazor uses
the TLM_POWER3 library to generate energy profiles for an overall
system.

This folder is also referred to as the vhls folder in the book.

### mp3-example ###

For the Design Exploration chapter, the full experimental setup for the MP3 example used to estimate the ALU and memory
requirement for basic audio decoding.


### mpeg/vlc, mpeg/dct, mpeg/motion-estimation folders ###

For the Design Exploration chapter, code fragments for MPEG compression that have been annotated with
to estimate ALU requirements etc..


### toy-eslfolder ###

For the ESL chapter, the Toy ESL folder contains a set of exercises for familiarity with blocking TLM coding using TLM 1 coding style.

### biquad-filter ###

This example was removed from the first edition of the book.


Licenses
========

All examples are released with one form of open source license or another. Please read the
license details in each folder or source file.