# cacti 22 #

This folder contains material for the first edition of the book Modern SoC Design on Arm by DJ Greaves.

For the ESL chapter, this folder contains spreadsheets, data results
and the HP CACTI memory design tool, as modified by DJG to generate
the data tables for 22nm SRAM, as presented in the book.


The input to CACTI, the output and the spreadsheet are in the folder
djg-cacti-run-for-msd-book:
  -rw-rw-r--. 1 djg11 djg11  57590 Feb 22 10:19 cactii-sram.eps
  -rw-rw-r--. 1 djg11 djg11 249970 Feb 22 10:19 cactii-sram.svg
  -rw-rw-r--. 1 djg11 djg11  33268 Feb 22 11:21 plots-cacti70.ods
  -rw-rw-r--. 1 djg11 djg11   8700 Feb 22 10:22 sram-run22.cfg
  -rw-rw-r--. 1 djg11 djg11   2576 Feb 22 10:22 sram-run22.cfg.out
  -rw-rw-r--. 1 djg11 djg11  63135 Feb 22 10:22 sram-run22.cf.spool


Several generations of the cacti tool have been released over the years.  For this
work I used the latest copy I could easily find, which is version 7.0 from

   https://github.com/HewlettPackard/cacti


This copy has some new features for die stacking and so on, this copy
appeared to have a few bugs that prevented generation of the simple
RAM models that worked in the past.  So I made a few modifications and
also added some print statements and a control loop to emit the lines
I wanted to capture for my spreadsheet and plots.


There license files for the tool are collected in CACTI-LICENSE.txt

The patches I made to the tool are in djg-cacti7.0-patch-jan-2021.txt

There is a complete zip of my src folder here too: cacti70.zip

  -rw-rw-r--.  1 djg11 djg11 4335129 Aug  4 16:24 cacti70.zip
  -rw-rw-r--.  1 djg11 djg11    1623 Aug  4 16:23 README.md
  -rw-rw-r--.  1 djg11 djg11    4172 Aug  4 16:23 CACTI-LICENSE.txt
  -rw-rw-r--.  1 djg11 djg11   10931 Aug  4 16:19 djg-cacti7.0-patch-jan-2021.txt

END





