/*
#
# Contents for the mp3-example additional material folder for "Modern SoC Design, First Edition" by DJ Greaves. 2021.
#
# LICENSE NOTICE:
# No waranties are implied or expressed.  The original material in this folder is taken from https://github.com/dr-soft/miniaudio
# which was released with two alternative licenses: 1 - Public Domain (www.unlicense.org) and 2 - MIT No Attribution.
# The additional Makefile, djgmain.c and ancillary instrumentation files are licensed to you by DJ Greaves under the
# same conditions.
#
*/
#define MINIAUDIO_IMPLEMENTATION

#include "hacked_audio.h"

//#include "bimp.h"

#include <stdio.h>
#include <math.h>
#include "c89atomic.h"
#include <wchar.h>

ma_result ma_device_init__null(ma_context* pContext, const ma_device_config* pConfig, ma_device* pDevice)
{
    ma_result result;
    ma_uint32 periodSizeInFrames;

    MA_ASSERT(pDevice != NULL);

    MA_ZERO_OBJECT(&pDevice->null_device);

    if (pConfig->deviceType == ma_device_type_loopback) {
        return MA_DEVICE_TYPE_NOT_SUPPORTED;
    }

    periodSizeInFrames = pConfig->periodSizeInFrames;
    if (periodSizeInFrames == 0) {
        periodSizeInFrames = ma_calculate_buffer_size_in_frames_from_milliseconds(pConfig->periodSizeInMilliseconds, pConfig->sampleRate);
    }

    if (pConfig->deviceType == ma_device_type_capture || pConfig->deviceType == ma_device_type_duplex) {
      (pDevice->capture.name,  sizeof(pDevice->capture.name),  "NULL Capture Device",  (size_t)-1);
        pDevice->capture.internalFormat             = pConfig->capture.format;
        pDevice->capture.internalChannels           = pConfig->capture.channels;
        ma_channel_map_copy(pDevice->capture.internalChannelMap, pConfig->capture.channelMap, pConfig->capture.channels);
        pDevice->capture.internalPeriodSizeInFrames = periodSizeInFrames;
        pDevice->capture.internalPeriods            = pConfig->periods;
    }
    if (pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) {
        ma_strncpy_s(pDevice->playback.name, sizeof(pDevice->playback.name), "NULL Playback Device", (size_t)-1);
        pDevice->playback.internalFormat             = pConfig->playback.format;
        pDevice->playback.internalChannels           = pConfig->playback.channels;
        ma_channel_map_copy(pDevice->playback.internalChannelMap, pConfig->playback.channelMap, pConfig->playback.channels);
        pDevice->playback.internalPeriodSizeInFrames = periodSizeInFrames;
        pDevice->playback.internalPeriods            = pConfig->periods;
    }

    /*
    In order to get timing right, we need to create a thread that does nothing but keeps track of the timer. This timer is started when the
    first period is "written" to it, and then stopped in ma_device_stop__null().
    */
    result = ma_event_init(&pDevice->null_device.operationEvent);
    if (result != MA_SUCCESS) {
        return result;
    }

    result = ma_event_init(&pDevice->null_device.operationCompletionEvent);
    if (result != MA_SUCCESS) {
        return result;
    }

    result = ma_thread_create(&pDevice->thread, pContext->threadPriority, 0, ma_device_thread__null, pDevice);
    if (result != MA_SUCCESS) {
        return result;
    }

    return MA_SUCCESS;
}

void data_callback(ma_device* pDevice, void* pOutput, const void* pInput, ma_uint32 frameCount)
{
    ma_decoder* pDecoder = (ma_decoder*)pDevice->pUserData;
    if (pDecoder == NULL) {
        return;
    }

    ma_decoder_read_pcm_frames(pDecoder, pOutput, frameCount);

    (void)pInput;
}



int main(int argc, char** argv)
{
    ma_result result;
    ma_decoder decoder;
    ma_device_config deviceConfig;
    ma_device device;
      if (argc < 2) {
        printf("No input file.\n");
        return -1;
    }

    result = ma_decoder_init_file(argv[1], NULL, &decoder);
    if (result != MA_SUCCESS) {
      return -2;
    }

    deviceConfig = ma_device_config_init(ma_device_type_playback);
    deviceConfig.playback.format   = decoder.outputFormat;
    deviceConfig.playback.channels = decoder.outputChannels;
    deviceConfig.sampleRate        = decoder.outputSampleRate;
    deviceConfig.dataCallback      = data_callback;
    deviceConfig.pUserData         = &decoder;

    if (ma_device_init(NULL, &deviceConfig, &device) != MA_SUCCESS) {
        printf("Failed to open playback device.\n");
        ma_decoder_uninit(&decoder);
        return -3;
    }

    if (ma_device_start(&device) != MA_SUCCESS)
      {
        printf("Failed to start playback device.\n");
        ma_device_uninit(&device);
        ma_decoder_uninit(&decoder);
        return -4;
    }

    printf("Press Enter to quit...");
    getchar();

    ma_device_uninit(&device);
    ma_decoder_uninit(&decoder);

    return 0;
}


MA_API int ma_strncpy_s(char* dst, size_t dstSizeInBytes, const char* src, size_t count)
{
    size_t maxcount;
    size_t i;

    if (dst == 0) {
        return 22;
    }
    if (dstSizeInBytes == 0) {
        return 34;
    }
    if (src == 0) {
        dst[0] = '\0';
        return 22;
    }

    maxcount = count;
    if (count == ((size_t)-1) || count >= dstSizeInBytes) {        /* -1 = _TRUNCATE */
        maxcount = dstSizeInBytes - 1;
    }

    for (i = 0; i < maxcount && src[i] != '\0'; ++i) {
        dst[i] = src[i];
    }

    if (src[i] == '\0' || i == count || count == ((size_t)-1)) {
        dst[i] = '\0';
        return 0;
    }

    dst[0] = '\0';
    return 34;
}

MA_API int ma_strcat_s(char* dst, size_t dstSizeInBytes, const char* src)
{
    char* dstorig;

    if (dst == 0) {
        return 22;
    }
    if (dstSizeInBytes == 0) {
        return 34;
    }
    if (src == 0) {
        dst[0] = '\0';
        return 22;
    }

    dstorig = dst;

    while (dstSizeInBytes > 0 && dst[0] != '\0') {
        dst += 1;
        dstSizeInBytes -= 1;
    }

    if (dstSizeInBytes == 0) {
        return 22;  /* Unterminated. */
    }


    while (dstSizeInBytes > 0 && src[0] != '\0') {
        *dst++ = *src++;
        dstSizeInBytes -= 1;
    }

    if (dstSizeInBytes > 0) {
        dst[0] = '\0';
    } else {
        dstorig[0] = '\0';
        return 34;
    }

    return 0;
}
static ma_result ma_thread_create__posix(ma_thread* pThread, ma_thread_priority priority, size_t stackSize, ma_thread_entry_proc entryProc, void* pData)
{
    int result;
    pthread_attr_t* pAttr = NULL;

#if !defined(__EMSCRIPTEN__)
    /* Try setting the thread priority. It's not critical if anything fails here. */
    pthread_attr_t attr;
    if (pthread_attr_init(&attr) == 0) {
        int scheduler = -1;
        if (priority == ma_thread_priority_idle) {
#ifdef SCHED_IDLE
            if (pthread_attr_setschedpolicy(&attr, SCHED_IDLE) == 0) {
                scheduler = SCHED_IDLE;
            }
#endif
        } else if (priority == ma_thread_priority_realtime) {
#ifdef SCHED_FIFO
            if (pthread_attr_setschedpolicy(&attr, SCHED_FIFO) == 0) {
                scheduler = SCHED_FIFO;
            }
#endif
#ifdef MA_LINUX
        } else {
            scheduler = sched_getscheduler(0);
#endif
        }

        if (stackSize > 0) {
            pthread_attr_setstacksize(&attr, stackSize);
        }

        if (scheduler != -1) {
            int priorityMin = sched_get_priority_min(scheduler);
            int priorityMax = sched_get_priority_max(scheduler);
            int priorityStep = (priorityMax - priorityMin) / 7;  /* 7 = number of priorities supported by miniaudio. */

            struct sched_param sched;
            if (pthread_attr_getschedparam(&attr, &sched) == 0) {
                if (priority == ma_thread_priority_idle) {
                    sched.sched_priority = priorityMin;
                } else if (priority == ma_thread_priority_realtime) {
                    sched.sched_priority = priorityMax;
                } else {
                    sched.sched_priority += ((int)priority + 5) * priorityStep;  /* +5 because the lowest priority is -5. */
                    if (sched.sched_priority < priorityMin) {
                        sched.sched_priority = priorityMin;
                    }
                    if (sched.sched_priority > priorityMax) {
                        sched.sched_priority = priorityMax;
                    }
                }

                if (pthread_attr_setschedparam(&attr, &sched) == 0) {
                    pAttr = &attr;
                }
            }
        }

        pthread_attr_destroy(&attr);
    }
#endif

    result = pthread_create(pThread, pAttr, entryProc, pData);
    if (result != 0) {
        return ma_result_from_errno(result);
    }

    return MA_SUCCESS;
}

ma_result ma_thread_create(ma_thread* pThread, ma_thread_priority priority, size_t stackSize, ma_thread_entry_proc entryProc, void* pData)
{
    if (pThread == NULL || entryProc == NULL) {
        return MA_FALSE;
    }
    return ma_thread_create__posix(pThread, priority, stackSize, entryProc, pData);
}


void ma_thread_wait__posix(ma_thread* pThread)
{
    pthread_join(*pThread, NULL);
}

/*******************************************************************************

Null Backend

*******************************************************************************/


ma_thread_result MA_THREADCALL ma_device_thread__null(void* pData)
{
    ma_device* pDevice = (ma_device*)pData;
    MA_ASSERT(pDevice != NULL);

    for (;;) {  /* Keep the thread alive until the device is uninitialized. */
        /* Wait for an operation to be requested. */
        ma_event_wait(&pDevice->null_device.operationEvent);

        /* At this point an event should have been triggered. */

        /* Starting the device needs to put the thread into a loop. */
        if (pDevice->null_device.operation == MA_DEVICE_OP_START__NULL) {
            c89atomic_exchange_32(&pDevice->null_device.operation, MA_DEVICE_OP_NONE__NULL);

            /* Reset the timer just in case. */
            ma_timer_init(&pDevice->null_device.timer);

            /* Keep looping until an operation has been requested. */
            while (pDevice->null_device.operation != MA_DEVICE_OP_NONE__NULL && pDevice->null_device.operation != MA_DEVICE_OP_START__NULL) {
                ma_sleep(10); /* Don't hog the CPU. */
            }

            /* Getting here means a suspend or kill operation has been requested. */
            c89atomic_exchange_32(&pDevice->null_device.operationResult, MA_SUCCESS);
            ma_event_signal(&pDevice->null_device.operationCompletionEvent);
            continue;
        }

        /* Suspending the device means we need to stop the timer and just continue the loop. */
        if (pDevice->null_device.operation == MA_DEVICE_OP_SUSPEND__NULL) {
            c89atomic_exchange_32(&pDevice->null_device.operation, MA_DEVICE_OP_NONE__NULL);

            /* We need to add the current run time to the prior run time, then reset the timer. */
            pDevice->null_device.priorRunTime += ma_timer_get_time_in_seconds(&pDevice->null_device.timer);
            ma_timer_init(&pDevice->null_device.timer);

            /* We're done. */
            c89atomic_exchange_32(&pDevice->null_device.operationResult, MA_SUCCESS);
            ma_event_signal(&pDevice->null_device.operationCompletionEvent);
            continue;
        }

        /* Killing the device means we need to get out of this loop so that this thread can terminate. */
        if (pDevice->null_device.operation == MA_DEVICE_OP_KILL__NULL) {
            c89atomic_exchange_32(&pDevice->null_device.operation, MA_DEVICE_OP_NONE__NULL);
            c89atomic_exchange_32(&pDevice->null_device.operationResult, MA_SUCCESS);
            ma_event_signal(&pDevice->null_device.operationCompletionEvent);
            break;
        }

        /* Getting a signal on a "none" operation probably means an error. Return invalid operation. */
        if (pDevice->null_device.operation == MA_DEVICE_OP_NONE__NULL) {
            MA_ASSERT(MA_FALSE);  /* <-- Trigger this in debug mode to ensure developers are aware they're doing something wrong (or there's a bug in a miniaudio). */
            c89atomic_exchange_32(&pDevice->null_device.operationResult, MA_INVALID_OPERATION);
            ma_event_signal(&pDevice->null_device.operationCompletionEvent);
            continue;   /* Continue the loop. Don't terminate. */
        }
    }

    return (ma_thread_result)0;
}

#include <errno.h>
ma_result ma_result_from_errno(int e)
{
    switch (e)
    {
        case 0: return MA_SUCCESS;
    #ifdef EPERM
        case EPERM: return MA_INVALID_OPERATION;
    #endif
    #ifdef ENOENT
        case ENOENT: return MA_DOES_NOT_EXIST;
    #endif
    #ifdef ESRCH
        case ESRCH: return MA_DOES_NOT_EXIST;
    #endif
    #ifdef EINTR
        case EINTR: return MA_INTERRUPT;
    #endif
    #ifdef EIO
        case EIO: return MA_IO_ERROR;
    #endif
    #ifdef ENXIO
        case ENXIO: return MA_DOES_NOT_EXIST;
    #endif
    #ifdef E2BIG
        case E2BIG: return MA_INVALID_ARGS;
    #endif
    #ifdef ENOEXEC
        case ENOEXEC: return MA_INVALID_FILE;
    #endif
    #ifdef EBADF
        case EBADF: return MA_INVALID_FILE;
    #endif
    #ifdef ECHILD
        case ECHILD: return MA_ERROR;
    #endif
    #ifdef EAGAIN
        case EAGAIN: return MA_UNAVAILABLE;
    #endif
    #ifdef ENOMEM
        case ENOMEM: return MA_OUT_OF_MEMORY;
    #endif
    #ifdef EACCES
        case EACCES: return MA_ACCESS_DENIED;
    #endif
    #ifdef EFAULT
        case EFAULT: return MA_BAD_ADDRESS;
    #endif
    #ifdef ENOTBLK
        case ENOTBLK: return MA_ERROR;
    #endif
    #ifdef EBUSY
        case EBUSY: return MA_BUSY;
    #endif
    #ifdef EEXIST
        case EEXIST: return MA_ALREADY_EXISTS;
    #endif
    #ifdef EXDEV
        case EXDEV: return MA_ERROR;
    #endif
    #ifdef ENODEV
        case ENODEV: return MA_DOES_NOT_EXIST;
    #endif
    #ifdef ENOTDIR
        case ENOTDIR: return MA_NOT_DIRECTORY;
    #endif
    #ifdef EISDIR
        case EISDIR: return MA_IS_DIRECTORY;
    #endif
    #ifdef EINVAL
        case EINVAL: return MA_INVALID_ARGS;
    #endif
    #ifdef ENFILE
        case ENFILE: return MA_TOO_MANY_OPEN_FILES;
    #endif
    #ifdef EMFILE
        case EMFILE: return MA_TOO_MANY_OPEN_FILES;
    #endif
    #ifdef ENOTTY
        case ENOTTY: return MA_INVALID_OPERATION;
    #endif
    #ifdef ETXTBSY
        case ETXTBSY: return MA_BUSY;
    #endif
    #ifdef EFBIG
        case EFBIG: return MA_TOO_BIG;
    #endif
    #ifdef ENOSPC
        case ENOSPC: return MA_NO_SPACE;
    #endif
    #ifdef ESPIPE
        case ESPIPE: return MA_BAD_SEEK;
    #endif
    #ifdef EROFS
        case EROFS: return MA_ACCESS_DENIED;
    #endif
    #ifdef EMLINK
        case EMLINK: return MA_TOO_MANY_LINKS;
    #endif
    #ifdef EPIPE
        case EPIPE: return MA_BAD_PIPE;
    #endif
    #ifdef EDOM
        case EDOM: return MA_OUT_OF_RANGE;
    #endif
    #ifdef ERANGE
        case ERANGE: return MA_OUT_OF_RANGE;
    #endif
    #ifdef EDEADLK
        case EDEADLK: return MA_DEADLOCK;
    #endif
    #ifdef ENAMETOOLONG
        case ENAMETOOLONG: return MA_PATH_TOO_LONG;
    #endif
    #ifdef ENOLCK
        case ENOLCK: return MA_ERROR;
    #endif
    #ifdef ENOSYS
        case ENOSYS: return MA_NOT_IMPLEMENTED;
    #endif
    #ifdef ENOTEMPTY
        case ENOTEMPTY: return MA_DIRECTORY_NOT_EMPTY;
    #endif
    #ifdef ELOOP
        case ELOOP: return MA_TOO_MANY_LINKS;
    #endif
    #ifdef ENOMSG
        case ENOMSG: return MA_NO_MESSAGE;
    #endif
    #ifdef EIDRM
        case EIDRM: return MA_ERROR;
    #endif
    #ifdef ECHRNG
        case ECHRNG: return MA_ERROR;
    #endif
    #ifdef EL2NSYNC
        case EL2NSYNC: return MA_ERROR;
    #endif
    #ifdef EL3HLT
        case EL3HLT: return MA_ERROR;
    #endif
    #ifdef EL3RST
        case EL3RST: return MA_ERROR;
    #endif
    #ifdef ELNRNG
        case ELNRNG: return MA_OUT_OF_RANGE;
    #endif
    #ifdef EUNATCH
        case EUNATCH: return MA_ERROR;
    #endif
    #ifdef ENOCSI
        case ENOCSI: return MA_ERROR;
    #endif
    #ifdef EL2HLT
        case EL2HLT: return MA_ERROR;
    #endif
    #ifdef EBADE
        case EBADE: return MA_ERROR;
    #endif
    #ifdef EBADR
        case EBADR: return MA_ERROR;
    #endif
    #ifdef EXFULL
        case EXFULL: return MA_ERROR;
    #endif
    #ifdef ENOANO
        case ENOANO: return MA_ERROR;
    #endif
    #ifdef EBADRQC
        case EBADRQC: return MA_ERROR;
    #endif
    #ifdef EBADSLT
        case EBADSLT: return MA_ERROR;
    #endif
    #ifdef EBFONT
        case EBFONT: return MA_INVALID_FILE;
    #endif
    #ifdef ENOSTR
        case ENOSTR: return MA_ERROR;
    #endif
    #ifdef ENODATA
        case ENODATA: return MA_NO_DATA_AVAILABLE;
    #endif
    #ifdef ETIME
        case ETIME: return MA_TIMEOUT;
    #endif
    #ifdef ENOSR
        case ENOSR: return MA_NO_DATA_AVAILABLE;
    #endif
    #ifdef ENONET
        case ENONET: return MA_NO_NETWORK;
    #endif
    #ifdef ENOPKG
        case ENOPKG: return MA_ERROR;
    #endif
    #ifdef EREMOTE
        case EREMOTE: return MA_ERROR;
    #endif
    #ifdef ENOLINK
        case ENOLINK: return MA_ERROR;
    #endif
    #ifdef EADV
        case EADV: return MA_ERROR;
    #endif
    #ifdef ESRMNT
        case ESRMNT: return MA_ERROR;
    #endif
    #ifdef ECOMM
        case ECOMM: return MA_ERROR;
    #endif
    #ifdef EPROTO
        case EPROTO: return MA_ERROR;
    #endif
    #ifdef EMULTIHOP
        case EMULTIHOP: return MA_ERROR;
    #endif
    #ifdef EDOTDOT
        case EDOTDOT: return MA_ERROR;
    #endif
    #ifdef EBADMSG
        case EBADMSG: return MA_BAD_MESSAGE;
    #endif
    #ifdef EOVERFLOW
        case EOVERFLOW: return MA_TOO_BIG;
    #endif
    #ifdef ENOTUNIQ
        case ENOTUNIQ: return MA_NOT_UNIQUE;
    #endif
    #ifdef EBADFD
        case EBADFD: return MA_ERROR;
    #endif
    #ifdef EREMCHG
        case EREMCHG: return MA_ERROR;
    #endif
    #ifdef ELIBACC
        case ELIBACC: return MA_ACCESS_DENIED;
    #endif
    #ifdef ELIBBAD
        case ELIBBAD: return MA_INVALID_FILE;
    #endif
    #ifdef ELIBSCN
        case ELIBSCN: return MA_INVALID_FILE;
    #endif
    #ifdef ELIBMAX
        case ELIBMAX: return MA_ERROR;
    #endif
    #ifdef ELIBEXEC
        case ELIBEXEC: return MA_ERROR;
    #endif
    #ifdef EILSEQ
        case EILSEQ: return MA_INVALID_DATA;
    #endif
    #ifdef ERESTART
        case ERESTART: return MA_ERROR;
    #endif
    #ifdef ESTRPIPE
        case ESTRPIPE: return MA_ERROR;
    #endif
    #ifdef EUSERS
        case EUSERS: return MA_ERROR;
    #endif
    #ifdef ENOTSOCK
        case ENOTSOCK: return MA_NOT_SOCKET;
    #endif
    #ifdef EDESTADDRREQ
        case EDESTADDRREQ: return MA_NO_ADDRESS;
    #endif
    #ifdef EMSGSIZE
        case EMSGSIZE: return MA_TOO_BIG;
    #endif
    #ifdef EPROTOTYPE
        case EPROTOTYPE: return MA_BAD_PROTOCOL;
    #endif
    #ifdef ENOPROTOOPT
        case ENOPROTOOPT: return MA_PROTOCOL_UNAVAILABLE;
    #endif
    #ifdef EPROTONOSUPPORT
        case EPROTONOSUPPORT: return MA_PROTOCOL_NOT_SUPPORTED;
    #endif
    #ifdef ESOCKTNOSUPPORT
        case ESOCKTNOSUPPORT: return MA_SOCKET_NOT_SUPPORTED;
    #endif
    #ifdef EOPNOTSUPP
        case EOPNOTSUPP: return MA_INVALID_OPERATION;
    #endif
    #ifdef EPFNOSUPPORT
        case EPFNOSUPPORT: return MA_PROTOCOL_FAMILY_NOT_SUPPORTED;
    #endif
    #ifdef EAFNOSUPPORT
        case EAFNOSUPPORT: return MA_ADDRESS_FAMILY_NOT_SUPPORTED;
    #endif
    #ifdef EADDRINUSE
        case EADDRINUSE: return MA_ALREADY_IN_USE;
    #endif
    #ifdef EADDRNOTAVAIL
        case EADDRNOTAVAIL: return MA_ERROR;
    #endif
    #ifdef ENETDOWN
        case ENETDOWN: return MA_NO_NETWORK;
    #endif
    #ifdef ENETUNREACH
        case ENETUNREACH: return MA_NO_NETWORK;
    #endif
    #ifdef ENETRESET
        case ENETRESET: return MA_NO_NETWORK;
    #endif
    #ifdef ECONNABORTED
        case ECONNABORTED: return MA_NO_NETWORK;
    #endif
    #ifdef ECONNRESET
        case ECONNRESET: return MA_CONNECTION_RESET;
    #endif
    #ifdef ENOBUFS
        case ENOBUFS: return MA_NO_SPACE;
    #endif
    #ifdef EISCONN
        case EISCONN: return MA_ALREADY_CONNECTED;
    #endif
    #ifdef ENOTCONN
        case ENOTCONN: return MA_NOT_CONNECTED;
    #endif
    #ifdef ESHUTDOWN
        case ESHUTDOWN: return MA_ERROR;
    #endif
    #ifdef ETOOMANYREFS
        case ETOOMANYREFS: return MA_ERROR;
    #endif
    #ifdef ETIMEDOUT
        case ETIMEDOUT: return MA_TIMEOUT;
    #endif
    #ifdef ECONNREFUSED
        case ECONNREFUSED: return MA_CONNECTION_REFUSED;
    #endif
    #ifdef EHOSTDOWN
        case EHOSTDOWN: return MA_NO_HOST;
    #endif
    #ifdef EHOSTUNREACH
        case EHOSTUNREACH: return MA_NO_HOST;
    #endif
    #ifdef EALREADY
        case EALREADY: return MA_IN_PROGRESS;
    #endif
    #ifdef EINPROGRESS
        case EINPROGRESS: return MA_IN_PROGRESS;
    #endif
    #ifdef ESTALE
        case ESTALE: return MA_INVALID_FILE;
    #endif
    #ifdef EUCLEAN
        case EUCLEAN: return MA_ERROR;
    #endif
    #ifdef ENOTNAM
        case ENOTNAM: return MA_ERROR;
    #endif
    #ifdef ENAVAIL
        case ENAVAIL: return MA_ERROR;
    #endif
    #ifdef EISNAM
        case EISNAM: return MA_ERROR;
    #endif
    #ifdef EREMOTEIO
        case EREMOTEIO: return MA_IO_ERROR;
    #endif
    #ifdef EDQUOT
        case EDQUOT: return MA_NO_SPACE;
    #endif
    #ifdef ENOMEDIUM
        case ENOMEDIUM: return MA_DOES_NOT_EXIST;
    #endif
    #ifdef EMEDIUMTYPE
        case EMEDIUMTYPE: return MA_ERROR;
    #endif
    #ifdef ECANCELED
        case ECANCELED: return MA_CANCELLED;
    #endif
    #ifdef ENOKEY
        case ENOKEY: return MA_ERROR;
    #endif
    #ifdef EKEYEXPIRED
        case EKEYEXPIRED: return MA_ERROR;
    #endif
    #ifdef EKEYREVOKED
        case EKEYREVOKED: return MA_ERROR;
    #endif
    #ifdef EKEYREJECTED
        case EKEYREJECTED: return MA_ERROR;
    #endif
    #ifdef EOWNERDEAD
        case EOWNERDEAD: return MA_ERROR;
    #endif
    #ifdef ENOTRECOVERABLE
        case ENOTRECOVERABLE: return MA_ERROR;
    #endif
    #ifdef ERFKILL
        case ERFKILL: return MA_ERROR;
    #endif
    #ifdef EHWPOISON
        case EHWPOISON: return MA_ERROR;
    #endif
        default: return MA_ERROR;
    }
}

void ma_timer_init(ma_timer* pTimer)
{
  struct timeval newTime;
  gettimeofday(&newTime, NULL);
  
  pTimer->counter = (newTime.tv_sec * 1000000) + newTime.tv_usec;
}


#if !defined(MA_EMSCRIPTEN)
void ma_sleep__posix(ma_uint32 milliseconds)
{
#ifdef MA_EMSCRIPTEN
    (void)milliseconds;
    MA_ASSERT(MA_FALSE);  /* The Emscripten build should never sleep. */
#else
    #if _POSIX_C_SOURCE >= 199309L
        struct timespec ts;
        ts.tv_sec  = milliseconds / 1000;
        ts.tv_nsec = milliseconds % 1000 * 1000000;
        nanosleep(&ts, NULL);
    #else
        struct timeval tv;
        tv.tv_sec  = milliseconds / 1000;
        tv.tv_usec = milliseconds % 1000 * 1000;
        select(0, NULL, NULL, NULL, &tv);
    #endif
#endif
}
#endif  /* MA_EMSCRIPTEN */

double ma_timer_get_time_in_seconds(ma_timer* pTimer)
{
  ma_uint64 newTimeCounter;
  ma_uint64 oldTimeCounter;
  
  struct timeval newTime;
  gettimeofday(&newTime, NULL);
  
  newTimeCounter = (newTime.tv_sec * 1000000) + newTime.tv_usec;
  oldTimeCounter = pTimer->counter;
  
  return (newTimeCounter - oldTimeCounter) / 1000000.0;
}

static void* ma__malloc_default(size_t sz, void* pUserData)
{
    (void)pUserData;
    return MA_MALLOC(sz);
}

static void* ma__realloc_default(void* p, size_t sz, void* pUserData)
{
    (void)pUserData;
    return MA_REALLOC(p, sz);
}

static void ma__free_default(void* p, void* pUserData)
{
    (void)pUserData;
    MA_FREE(p);
}


void* ma__malloc_from_callbacks(size_t sz, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks == NULL) {
        return NULL;
    }

    if (pAllocationCallbacks->onMalloc != NULL) {
        return pAllocationCallbacks->onMalloc(sz, pAllocationCallbacks->pUserData);
    }

    /* Try using realloc(). */
    if (pAllocationCallbacks->onRealloc != NULL) {
        return pAllocationCallbacks->onRealloc(NULL, sz, pAllocationCallbacks->pUserData);
    }

    return NULL;
}

static void* ma__realloc_from_callbacks(void* p, size_t szNew, size_t szOld, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks == NULL) {
        return NULL;
    }

    if (pAllocationCallbacks->onRealloc != NULL) {
        return pAllocationCallbacks->onRealloc(p, szNew, pAllocationCallbacks->pUserData);
    }

    /* Try emulating realloc() in terms of malloc()/free(). */
    if (pAllocationCallbacks->onMalloc != NULL && pAllocationCallbacks->onFree != NULL) {
        void* p2;

        p2 = pAllocationCallbacks->onMalloc(szNew, pAllocationCallbacks->pUserData);
        if (p2 == NULL) {
            return NULL;
        }

        if (p != NULL) {
            MA_COPY_MEMORY(p2, p, szOld);
            pAllocationCallbacks->onFree(p, pAllocationCallbacks->pUserData);
        }

        return p2;
    }

    return NULL;
}

static MA_INLINE void* ma__calloc_from_callbacks(size_t sz, const ma_allocation_callbacks* pAllocationCallbacks)
{
    void* p = ma__malloc_from_callbacks(sz, pAllocationCallbacks);
    if (p != NULL) {
        MA_ZERO_MEMORY(p, sz);
    }

    return p;
}

void ma__free_from_callbacks(void* p, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (p == NULL || pAllocationCallbacks == NULL) {
        return;
    }

    if (pAllocationCallbacks->onFree != NULL) {
        pAllocationCallbacks->onFree(p, pAllocationCallbacks->pUserData);
    }
}



static ma_allocation_callbacks ma_allocation_callbacks_init_default(void)
{
    ma_allocation_callbacks callbacks;
    callbacks.pUserData = NULL;
    callbacks.onMalloc  = ma__malloc_default;
    callbacks.onRealloc = ma__realloc_default;
    callbacks.onFree    = ma__free_default;

    return callbacks;
}

static ma_result ma_allocation_callbacks_init_copy(ma_allocation_callbacks* pDst, const ma_allocation_callbacks* pSrc)
{
    if (pDst == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pSrc == NULL) {
        *pDst = ma_allocation_callbacks_init_default();
    } else {
        if (pSrc->pUserData == NULL && pSrc->onFree == NULL && pSrc->onMalloc == NULL && pSrc->onRealloc == NULL) {
            *pDst = ma_allocation_callbacks_init_default();
        } else {
            if (pSrc->onFree == NULL || (pSrc->onMalloc == NULL && pSrc->onRealloc == NULL)) {
                return MA_INVALID_ARGS;    /* Invalid allocation callbacks. */
            } else {
                *pDst = *pSrc;
            }
        }
    }

    return MA_SUCCESS;
}

MA_API ma_result ma_device_init_ex(const ma_backend backends[], ma_uint32 backendCount, const ma_context_config* pContextConfig, const ma_device_config* pConfig, ma_device* pDevice)
{
    ma_result result;
    ma_context* pContext;
    ma_backend defaultBackends[ma_backend_null+1];
    ma_uint32 iBackend;
    ma_backend* pBackendsToIterate;
    ma_uint32 backendsToIterateCount;
    ma_allocation_callbacks allocationCallbacks;

    if (pConfig == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pContextConfig != NULL) {
        result = ma_allocation_callbacks_init_copy(&allocationCallbacks, &pContextConfig->allocationCallbacks);
        if (result != MA_SUCCESS) {
            return result;
        }
    } else {
        allocationCallbacks = ma_allocation_callbacks_init_default();
    }
    

    pContext = (ma_context*)ma__malloc_from_callbacks(sizeof(*pContext), &allocationCallbacks);
    if (pContext == NULL) {
        return MA_OUT_OF_MEMORY;
    }

    for (iBackend = 0; iBackend <= ma_backend_null; ++iBackend) {
        defaultBackends[iBackend] = (ma_backend)iBackend;
    }

    pBackendsToIterate = (ma_backend*)backends;
    backendsToIterateCount = backendCount;
    if (pBackendsToIterate == NULL) {
        pBackendsToIterate = (ma_backend*)defaultBackends;
        backendsToIterateCount = ma_countof(defaultBackends);
    }

    result = MA_NO_BACKEND;

    for (iBackend = 0; iBackend < backendsToIterateCount; ++iBackend) {
        result = ma_context_init(&pBackendsToIterate[iBackend], 1, pContextConfig, pContext);
        if (result == MA_SUCCESS) {
            result = ma_device_init(pContext, pConfig, pDevice);
            if (result == MA_SUCCESS) {
                break;  /* Success. */
            } else {
                ma_context_uninit(pContext);   /* Failure. */
            }
        }
    }

    if (result != MA_SUCCESS) {
        ma__free_from_callbacks(pContext, &allocationCallbacks);
        return result;
    }

    pDevice->isOwnerOfContext = MA_TRUE;
    return result;
}

/* A helper for changing the state of the device. */
MA_INLINE void ma_device__set_state(ma_device* pDevice, ma_uint32 newState)
{
    c89atomic_exchange_32(&pDevice->state, newState);
}

/* A helper for getting the state of the device. */
ma_uint32 ma_device__get_state(ma_device* pDevice)
{
    return pDevice->state;
}


/* Helper for determining whether or not the given device is initialized. */
ma_bool32 ma_device__is_initialized(ma_device* pDevice)
{
    if (pDevice == NULL) {
        return MA_FALSE;
    }

    return ma_device__get_state(pDevice) != MA_STATE_UNINITIALIZED;
}

ma_bool32 ma_context_is_backend_asynchronous(ma_context* pContext)
{
    return pContext->isBackendAsynchronous;
}


void ma_thread_wait(ma_thread* pThread)
{
    if (pThread == NULL) {
        return;
    }
#ifdef MA_WIN32
    ma_thread_wait__win32(pThread);
#endif
#ifdef MA_POSIX
    ma_thread_wait__posix(pThread);
#endif
}

MA_API void ma_device_uninit(ma_device* pDevice)
{
    if (!ma_device__is_initialized(pDevice)) {
        return;
    }

    /* Make sure the device is stopped first. The backends will probably handle this naturally, but I like to do it explicitly for my own sanity. */
    if (ma_device_is_started(pDevice)) {
        ma_device_stop(pDevice);
    }

    /* Putting the device into an uninitialized state will make the worker thread return. */
    ma_device__set_state(pDevice, MA_STATE_UNINITIALIZED);

    /* Wake up the worker thread and wait for it to properly terminate. */
    if (!ma_context_is_backend_asynchronous(pDevice->pContext)) {
        ma_event_signal(&pDevice->wakeupEvent);
        ma_thread_wait(&pDevice->thread);
    }

    pDevice->pContext->onDeviceUninit(pDevice);

    ma_event_uninit(&pDevice->stopEvent);
    ma_event_uninit(&pDevice->startEvent);
    ma_event_uninit(&pDevice->wakeupEvent);
    ma_mutex_uninit(&pDevice->lock);

    if (pDevice->isOwnerOfContext) {
        ma_allocation_callbacks allocationCallbacks = pDevice->pContext->allocationCallbacks;

        ma_context_uninit(pDevice->pContext);
        ma__free_from_callbacks(pDevice->pContext, &allocationCallbacks);
    }

    MA_ZERO_OBJECT(pDevice);
}

static void ma_post_log_message(ma_context* pContext, ma_device* pDevice, ma_uint32 logLevel, const char* message)
{
    if (pContext == NULL) {
        if (pDevice != NULL) {
            pContext = pDevice->pContext;
        }
    }

    /* All logs must be output when debug output is enabled. */
#if defined(MA_DEBUG_OUTPUT)
    printf("%s: %s\n", ma_log_level_to_string(logLevel), message);
#endif

    if (pContext == NULL) {
        return;
    }
    
#if defined(MA_LOG_LEVEL)
    if (logLevel <= MA_LOG_LEVEL) {
        ma_log_proc onLog;

        onLog = pContext->logCallback;
        if (onLog) {
            onLog(pContext, pDevice, logLevel, message);
        }
    }
#endif
}

void ma_post_log_messagev(ma_context* pContext, ma_device* pDevice, ma_uint32 logLevel, const char* pFormat, va_list args)
{
  char pFormattedMessage[1024];
  vsnprintf(pFormattedMessage, sizeof(pFormattedMessage), pFormat, args);

  printf("DJG message %s\n", pFormattedMessage);
}

void ma_post_log_messagef(ma_context* pContext, ma_device* pDevice, ma_uint32 logLevel, const char* pFormat, ...)
{
    va_list args;
    va_start(args, pFormat);
    {
        ma_post_log_messagev(pContext, pDevice, logLevel, pFormat, args);
    }
    va_end(args);
}


/* Posts an log message. Throw a breakpoint in here if you're needing to debug. The return value is always "resultCode". */
ma_result ma_context_post_error(ma_context* pContext, ma_device* pDevice, ma_uint32 logLevel, const char* message, ma_result resultCode)
{
    ma_post_log_message(pContext, pDevice, logLevel, message);
    return resultCode;
}



ma_result ma_post_error(ma_device* pDevice, ma_uint32 logLevel, const char* message, ma_result resultCode)
{
    return ma_context_post_error(NULL, pDevice, logLevel, message, resultCode);
}

MA_API ma_result ma_device_start(ma_device* pDevice)
{
    ma_result result;

    if (pDevice == NULL) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "ma_device_start() called with invalid arguments (pDevice == NULL).", MA_INVALID_ARGS);
    }

    if (ma_device__get_state(pDevice) == MA_STATE_UNINITIALIZED) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "ma_device_start() called for an uninitialized device.", MA_DEVICE_NOT_INITIALIZED);
    }

    if (ma_device__get_state(pDevice) == MA_STATE_STARTED) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_WARNING, "ma_device_start() called when the device is already started.", MA_INVALID_OPERATION);  /* Already started. Returning an error to let the application know because it probably means they're doing something wrong. */
    }

    result = MA_ERROR;
    ma_mutex_lock(&pDevice->lock);
    {
        /* Starting and stopping are wrapped in a mutex which means we can assert that the device is in a stopped or paused state. */
        MA_ASSERT(ma_device__get_state(pDevice) == MA_STATE_STOPPED);

        ma_device__set_state(pDevice, MA_STATE_STARTING);

        /* Asynchronous backends need to be handled differently. */
        if (ma_context_is_backend_asynchronous(pDevice->pContext)) {
            result = pDevice->pContext->onDeviceStart(pDevice);
            if (result == MA_SUCCESS) {
                ma_device__set_state(pDevice, MA_STATE_STARTED);
            }
        } else {
            /*
            Synchronous backends are started by signaling an event that's being waited on in the worker thread. We first wake up the
            thread and then wait for the start event.
            */
            ma_event_signal(&pDevice->wakeupEvent);

            /*
            Wait for the worker thread to finish starting the device. Note that the worker thread will be the one who puts the device
            into the started state. Don't call ma_device__set_state() here.
            */
            ma_event_wait(&pDevice->startEvent);
            result = pDevice->workResult;
        }
    }
    ma_mutex_unlock(&pDevice->lock);

    return result;
}

MA_API ma_result ma_device_stop(ma_device* pDevice)
{
    ma_result result;

    if (pDevice == NULL) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "ma_device_stop() called with invalid arguments (pDevice == NULL).", MA_INVALID_ARGS);
    }

    if (ma_device__get_state(pDevice) == MA_STATE_UNINITIALIZED) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "ma_device_stop() called for an uninitialized device.", MA_DEVICE_NOT_INITIALIZED);
    }

    if (ma_device__get_state(pDevice) == MA_STATE_STOPPED) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_WARNING, "ma_device_stop() called when the device is already stopped.", MA_INVALID_OPERATION);   /* Already stopped. Returning an error to let the application know because it probably means they're doing something wrong. */
    }

    result = MA_ERROR;
    ma_mutex_lock(&pDevice->lock);
    {
        /* Starting and stopping are wrapped in a mutex which means we can assert that the device is in a started or paused state. */
        MA_ASSERT(ma_device__get_state(pDevice) == MA_STATE_STARTED);

        ma_device__set_state(pDevice, MA_STATE_STOPPING);

        /* There's no need to wake up the thread like we do when starting. */

        if (pDevice->pContext->onDeviceStop) {
            result = pDevice->pContext->onDeviceStop(pDevice);
        } else {
            result = MA_SUCCESS;
        }

        /* Asynchronous backends need to be handled differently. */
        if (ma_context_is_backend_asynchronous(pDevice->pContext)) {
            ma_device__set_state(pDevice, MA_STATE_STOPPED);
        } else {
            /* Synchronous backends. */

            /*
            We need to wait for the worker thread to become available for work before returning. Note that the worker thread will be
            the one who puts the device into the stopped state. Don't call ma_device__set_state() here.
            */
            ma_event_wait(&pDevice->stopEvent);
            result = MA_SUCCESS;
        }
    }
    ma_mutex_unlock(&pDevice->lock);

    return result;
}

MA_API ma_bool32 ma_device_is_started(ma_device* pDevice)
{
    if (pDevice == NULL) {
        return MA_FALSE;
    }

    return ma_device__get_state(pDevice) == MA_STATE_STARTED;
}

MA_API ma_result ma_device_set_master_volume(ma_device* pDevice, float volume)
{
    if (pDevice == NULL) {
        return MA_INVALID_ARGS;
    }

    if (volume < 0.0f || volume > 1.0f) {
        return MA_INVALID_ARGS;
    }

    pDevice->masterVolumeFactor = volume;

    return MA_SUCCESS;
}

MA_API ma_result ma_device_get_master_volume(ma_device* pDevice, float* pVolume)
{
    if (pVolume == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pDevice == NULL) {
        *pVolume = 0;
        return MA_INVALID_ARGS;
    }

    *pVolume = pDevice->masterVolumeFactor;

    return MA_SUCCESS;
}

MA_API ma_result ma_device_set_master_gain_db(ma_device* pDevice, float gainDB)
{
    if (gainDB > 0) {
        return MA_INVALID_ARGS;
    }

    return ma_device_set_master_volume(pDevice, ma_gain_db_to_factor(gainDB));
}

MA_API ma_result ma_device_get_master_gain_db(ma_device* pDevice, float* pGainDB)
{
    float factor;
    ma_result result;

    if (pGainDB == NULL) {
        return MA_INVALID_ARGS;
    }

    result = ma_device_get_master_volume(pDevice, &factor);
    if (result != MA_SUCCESS) {
        *pGainDB = 0;
        return result;
    }

    *pGainDB = ma_factor_to_gain_db(factor);

    return MA_SUCCESS;
}

ma_bool32 ma__is_channel_map_valid(const ma_channel* channelMap, ma_uint32 channels)
{
    /* A blank channel map should be allowed, in which case it should use an appropriate default which will depend on context. */
    if (channelMap[0] != MA_CHANNEL_NONE) {
        ma_uint32 iChannel;

        if (channels == 0) {
            return MA_FALSE;   /* No channels. */
        }

        /* A channel cannot be present in the channel map more than once. */
        for (iChannel = 0; iChannel < channels; ++iChannel) {
            ma_uint32 jChannel;
            for (jChannel = iChannel + 1; jChannel < channels; ++jChannel) {
                if (channelMap[iChannel] == channelMap[jChannel]) {
                    return MA_FALSE;
                }
            }
        }
    }

    return MA_TRUE;
}

MA_API void ma_channel_map_copy(ma_channel* pOut, const ma_channel* pIn, ma_uint32 channels)
{
    if (pOut != NULL && pIn != NULL && channels > 0) {
        MA_COPY_MEMORY(pOut, pIn, sizeof(*pOut) * channels);
    }
}

static ma_result ma_mutex_init__posix(ma_mutex* pMutex)
{
    int result = pthread_mutex_init((pthread_mutex_t*)pMutex, NULL);
    if (result != 0) {
        return ma_result_from_errno(result);
    }
    return MA_SUCCESS;
}

MA_API ma_result ma_mutex_init(ma_mutex* pMutex)
{
    if (pMutex == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return MA_INVALID_ARGS;
    }
    return ma_mutex_init__posix(pMutex);
}


static ma_result ma_event_init__posix(ma_event* pEvent)
{
    int result;

    result = pthread_mutex_init(&pEvent->lock, NULL);
    if (result != 0) {
        return ma_result_from_errno(result);
    }

    result = pthread_cond_init(&pEvent->cond, NULL);
    if (result != 0) {
        pthread_mutex_destroy(&pEvent->lock);
        return ma_result_from_errno(result);
    }

    pEvent->value = 0;
    return MA_SUCCESS;
}


MA_API ma_result ma_event_init(ma_event* pEvent)
{
    if (pEvent == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return MA_INVALID_ARGS;
    }
    return ma_event_init__posix(pEvent);
}

static void ma_event_uninit__posix(ma_event* pEvent)
{
    pthread_cond_destroy(&pEvent->cond);
    pthread_mutex_destroy(&pEvent->lock);
}

MA_API void ma_event_uninit(ma_event* pEvent)
{
    if (pEvent == NULL) {
        return;
    }

#ifdef MA_WIN32
    ma_event_uninit__win32(pEvent);
#endif
#ifdef MA_POSIX
    ma_event_uninit__posix(pEvent);
#endif
}


static ma_result ma_event_wait__posix(ma_event* pEvent)
{
    pthread_mutex_lock(&pEvent->lock);
    {
        while (pEvent->value == 0) {
            pthread_cond_wait(&pEvent->cond, &pEvent->lock);
        }
        pEvent->value = 0;  /* Auto-reset. */
    }
    pthread_mutex_unlock(&pEvent->lock);

    return MA_SUCCESS;
}

ma_result ma_event_signal__posix(ma_event* pEvent)
{
    pthread_mutex_lock(&pEvent->lock);
    {
        pEvent->value = 1;
        pthread_cond_signal(&pEvent->cond);
    }
    pthread_mutex_unlock(&pEvent->lock);

    return MA_SUCCESS;
}

static void ma_mutex_uninit__posix(ma_mutex* pMutex)
{
    pthread_mutex_destroy((pthread_mutex_t*)pMutex);
}

MA_API void ma_mutex_uninit(ma_mutex* pMutex)
{
    if (pMutex == NULL) {
        return;
    }

#ifdef MA_WIN32
    ma_mutex_uninit__win32(pMutex);
#endif
#ifdef MA_POSIX
    ma_mutex_uninit__posix(pMutex);
#endif
}

static void ma_mutex_lock__posix(ma_mutex* pMutex)
{
    pthread_mutex_lock((pthread_mutex_t*)pMutex);
}

static void ma_mutex_unlock__posix(ma_mutex* pMutex)
{
    pthread_mutex_unlock((pthread_mutex_t*)pMutex);
}



static ma_result ma_semaphore_init__posix(int initialValue, ma_semaphore* pSemaphore)
{
    int result;

    if (pSemaphore == NULL) {
        return MA_INVALID_ARGS;
    }

    pSemaphore->value = initialValue;

    result = pthread_mutex_init(&pSemaphore->lock, NULL);
    if (result != 0) {
        return ma_result_from_errno(result);  /* Failed to create mutex. */
    }

    result = pthread_cond_init(&pSemaphore->cond, NULL);
    if (result != 0) {
        pthread_mutex_destroy(&pSemaphore->lock);
        return ma_result_from_errno(result);  /* Failed to create condition variable. */
    }

    return MA_SUCCESS;
}

static void ma_semaphore_uninit__posix(ma_semaphore* pSemaphore)
{
    if (pSemaphore == NULL) {
        return;
    }

    pthread_cond_destroy(&pSemaphore->cond);
    pthread_mutex_destroy(&pSemaphore->lock);
}

static ma_result ma_semaphore_wait__posix(ma_semaphore* pSemaphore)
{
    if (pSemaphore == NULL) {
        return MA_INVALID_ARGS;
    }

    pthread_mutex_lock(&pSemaphore->lock);
    {
        /* We need to wait on a condition variable before escaping. We can't return from this function until the semaphore has been signaled. */
        while (pSemaphore->value == 0) {
            pthread_cond_wait(&pSemaphore->cond, &pSemaphore->lock);
        }

        pSemaphore->value -= 1;
    }
    pthread_mutex_unlock(&pSemaphore->lock);

    return MA_SUCCESS;
}

static ma_result ma_semaphore_release__posix(ma_semaphore* pSemaphore)
{
    if (pSemaphore == NULL) {
        return MA_INVALID_ARGS;
    }

    pthread_mutex_lock(&pSemaphore->lock);
    {
        pSemaphore->value += 1;
        pthread_cond_signal(&pSemaphore->cond);
    }
    pthread_mutex_unlock(&pSemaphore->lock);

    return MA_SUCCESS;
}



typedef struct
{
    ma_device_type deviceType;
    const ma_device_id* pDeviceID;
    char* pName;
    size_t nameBufferSize;
    ma_bool32 foundDevice;
} ma_context__try_get_device_name_by_id__enum_callback_data;

ma_bool32 ma_context__try_get_device_name_by_id__enum_callback(ma_context* pContext, ma_device_type deviceType, const ma_device_info* pDeviceInfo, void* pUserData)
{
    ma_context__try_get_device_name_by_id__enum_callback_data* pData = (ma_context__try_get_device_name_by_id__enum_callback_data*)pUserData;
    MA_ASSERT(pData != NULL);

    if (pData->deviceType == deviceType) {
        if (pContext->onDeviceIDEqual(pContext, pData->pDeviceID, &pDeviceInfo->id)) {
            ma_strncpy_s(pData->pName, pData->nameBufferSize, pDeviceInfo->name, (size_t)-1);
            pData->foundDevice = MA_TRUE;
        }
    }

    return !pData->foundDevice;
}

/*
Generic function for retrieving the name of a device by it's ID.

This function simply enumerates every device and then retrieves the name of the first device that has the same ID.
*/
ma_result ma_context__try_get_device_name_by_id(ma_context* pContext, ma_device_type deviceType, const ma_device_id* pDeviceID, char* pName, size_t nameBufferSize)
{
    ma_result result;
    ma_context__try_get_device_name_by_id__enum_callback_data data;

    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pName != NULL);

    if (pDeviceID == NULL) {
        return MA_NO_DEVICE;
    }

    data.deviceType = deviceType;
    data.pDeviceID = pDeviceID;
    data.pName = pName;
    data.nameBufferSize = nameBufferSize;
    data.foundDevice = MA_FALSE;
    result = ma_context_enumerate_devices(pContext, ma_context__try_get_device_name_by_id__enum_callback, &data);
    if (result != MA_SUCCESS) {
        return result;
    }

    if (!data.foundDevice) {
        return MA_NO_DEVICE;
    } else {
        return MA_SUCCESS;
    }
}

MA_API const char* ma_get_backend_name(ma_backend backend)
{
    switch (backend)
    {
        case ma_backend_wasapi:     return "WASAPI";
        case ma_backend_dsound:     return "DirectSound";
        case ma_backend_winmm:      return "WinMM";
        case ma_backend_coreaudio:  return "Core Audio";
        case ma_backend_sndio:      return "sndio";
        case ma_backend_audio4:     return "audio(4)";
        case ma_backend_oss:        return "OSS";
        case ma_backend_pulseaudio: return "PulseAudio";
        case ma_backend_alsa:       return "ALSA";
        case ma_backend_jack:       return "JACK";
        case ma_backend_aaudio:     return "AAudio";
        case ma_backend_opensl:     return "OpenSL|ES";
        case ma_backend_webaudio:   return "Web Audio";
        case ma_backend_null:       return "Null";
        default:                    return "Unknown";
    }
}


MA_API const char* ma_get_format_name(ma_format format)
{
    switch (format)
    {
        case ma_format_unknown: return "Unknown";
        case ma_format_u8:      return "8-bit Unsigned Integer";
        case ma_format_s16:     return "16-bit Signed Integer";
        case ma_format_s24:     return "24-bit Signed Integer (Tightly Packed)";
        case ma_format_s32:     return "32-bit Signed Integer";
        case ma_format_f32:     return "32-bit IEEE Floating Point";
        default:                return "Invalid";
    }
}


MA_API ma_uint32 ma_scale_buffer_size(ma_uint32 baseBufferSize, float scale)
{
    return ma_max(1, (ma_uint32)(baseBufferSize*scale));
}

MA_API ma_uint32 ma_calculate_buffer_size_in_milliseconds_from_frames(ma_uint32 bufferSizeInFrames, ma_uint32 sampleRate)
{
    return bufferSizeInFrames / (sampleRate/1000);
}

MA_API ma_uint32 ma_calculate_buffer_size_in_frames_from_milliseconds(ma_uint32 bufferSizeInMilliseconds, ma_uint32 sampleRate)
{
    return bufferSizeInMilliseconds * (sampleRate/1000); 
}

void ma_copy_memory_64(void* dst, const void* src, ma_uint64 sizeInBytes)
{
#if 0xFFFFFFFFFFFFFFFF <= MA_SIZE_MAX
    MA_COPY_MEMORY(dst, src, (size_t)sizeInBytes);
#else
    while (sizeInBytes > 0) {
        ma_uint64 bytesToCopyNow = sizeInBytes;
        if (bytesToCopyNow > MA_SIZE_MAX) {
            bytesToCopyNow = MA_SIZE_MAX;
        }

        MA_COPY_MEMORY(dst, src, (size_t)bytesToCopyNow);  /* Safe cast to size_t. */

        sizeInBytes -= bytesToCopyNow;
        dst = (      void*)((      ma_uint8*)dst + bytesToCopyNow);
        src = (const void*)((const ma_uint8*)src + bytesToCopyNow);
    }
#endif
}

void ma_zero_memory_64(void* dst, ma_uint64 sizeInBytes)
{
#if 0xFFFFFFFFFFFFFFFF <= MA_SIZE_MAX
    MA_ZERO_MEMORY(dst, (size_t)sizeInBytes);
#else
    while (sizeInBytes > 0) {
        ma_uint64 bytesToZeroNow = sizeInBytes;
        if (bytesToZeroNow > MA_SIZE_MAX) {
            bytesToZeroNow = MA_SIZE_MAX;
        }

        MA_ZERO_MEMORY(dst, (size_t)bytesToZeroNow);  /* Safe cast to size_t. */

        sizeInBytes -= bytesToZeroNow;
        dst = (void*)((ma_uint8*)dst + bytesToZeroNow);
    }
#endif
}


MA_API void ma_copy_pcm_frames(void* dst, const void* src, ma_uint64 frameCount, ma_format format, ma_uint32 channels)
{
    if (dst == src) {
        return; /* No-op. */
    }

    ma_copy_memory_64(dst, src, frameCount * ma_get_bytes_per_frame(format, channels));
}

MA_API void ma_silence_pcm_frames(void* p, ma_uint64 frameCount, ma_format format, ma_uint32 channels)
{
    if (format == ma_format_u8) {
        ma_uint64 sampleCount = frameCount * channels;
        ma_uint64 iSample;
        for (iSample = 0; iSample < sampleCount; iSample += 1) {
            ((ma_uint8*)p)[iSample] = 128;
        }
    } else {
        ma_zero_memory_64(p, frameCount * ma_get_bytes_per_frame(format, channels));
    }
}

MA_API void* ma_offset_pcm_frames_ptr(void* p, ma_uint64 offsetInFrames, ma_format format, ma_uint32 channels)
{
    return ma_offset_ptr(p, offsetInFrames * ma_get_bytes_per_frame(format, channels));
}

MA_API const void* ma_offset_pcm_frames_const_ptr(const void* p, ma_uint64 offsetInFrames, ma_format format, ma_uint32 channels)
{
    return ma_offset_ptr(p, offsetInFrames * ma_get_bytes_per_frame(format, channels));
}


MA_API void ma_clip_samples_f32(float* p, ma_uint64 sampleCount)
{
    ma_uint32 iSample;

    /* TODO: Research a branchless SSE implementation. */
    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        p[iSample] = ma_clip_f32(p[iSample]);
    }
}


MA_API void ma_copy_and_apply_volume_factor_u8(ma_uint8* pSamplesOut, const ma_uint8* pSamplesIn, ma_uint32 sampleCount, float factor)
{
    ma_uint32 iSample;

    if (pSamplesOut == NULL || pSamplesIn == NULL) {
        return;
    }

    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        pSamplesOut[iSample] = (ma_uint8)(pSamplesIn[iSample] * factor);
    }
}

MA_API void ma_copy_and_apply_volume_factor_s16(ma_int16* pSamplesOut, const ma_int16* pSamplesIn, ma_uint32 sampleCount, float factor)
{
    ma_uint32 iSample;

    if (pSamplesOut == NULL || pSamplesIn == NULL) {
        return;
    }

    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        pSamplesOut[iSample] = (ma_int16)(pSamplesIn[iSample] * factor);
    }
}

MA_API void ma_copy_and_apply_volume_factor_s24(void* pSamplesOut, const void* pSamplesIn, ma_uint32 sampleCount, float factor)
{
    ma_uint32 iSample;
    ma_uint8* pSamplesOut8;
    ma_uint8* pSamplesIn8;

    if (pSamplesOut == NULL || pSamplesIn == NULL) {
        return;
    }

    pSamplesOut8 = (ma_uint8*)pSamplesOut;
    pSamplesIn8  = (ma_uint8*)pSamplesIn;

    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        ma_int32 sampleS32;

        sampleS32 = (ma_int32)(((ma_uint32)(pSamplesIn8[iSample*3+0]) << 8) | ((ma_uint32)(pSamplesIn8[iSample*3+1]) << 16) | ((ma_uint32)(pSamplesIn8[iSample*3+2])) << 24);
        sampleS32 = (ma_int32)(sampleS32 * factor);

        pSamplesOut8[iSample*3+0] = (ma_uint8)(((ma_uint32)sampleS32 & 0x0000FF00) >>  8);
        pSamplesOut8[iSample*3+1] = (ma_uint8)(((ma_uint32)sampleS32 & 0x00FF0000) >> 16);
        pSamplesOut8[iSample*3+2] = (ma_uint8)(((ma_uint32)sampleS32 & 0xFF000000) >> 24);
    }
}

MA_API void ma_copy_and_apply_volume_factor_s32(ma_int32* pSamplesOut, const ma_int32* pSamplesIn, ma_uint32 sampleCount, float factor)
{
    ma_uint32 iSample;

    if (pSamplesOut == NULL || pSamplesIn == NULL) {
        return;
    }

    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        pSamplesOut[iSample] = (ma_int32)(pSamplesIn[iSample] * factor);
    }
}

MA_API void ma_copy_and_apply_volume_factor_f32(float* pSamplesOut, const float* pSamplesIn, ma_uint32 sampleCount, float factor)
{
    ma_uint32 iSample;

    if (pSamplesOut == NULL || pSamplesIn == NULL) {
        return;
    }

    for (iSample = 0; iSample < sampleCount; iSample += 1) {
        pSamplesOut[iSample] = pSamplesIn[iSample] * factor;
    }
}

MA_API void ma_apply_volume_factor_u8(ma_uint8* pSamples, ma_uint32 sampleCount, float factor)
{
    ma_copy_and_apply_volume_factor_u8(pSamples, pSamples, sampleCount, factor);
}

MA_API void ma_apply_volume_factor_s16(ma_int16* pSamples, ma_uint32 sampleCount, float factor)
{
    ma_copy_and_apply_volume_factor_s16(pSamples, pSamples, sampleCount, factor);
}

MA_API void ma_apply_volume_factor_s24(void* pSamples, ma_uint32 sampleCount, float factor)
{
    ma_copy_and_apply_volume_factor_s24(pSamples, pSamples, sampleCount, factor);
}

MA_API void ma_apply_volume_factor_s32(ma_int32* pSamples, ma_uint32 sampleCount, float factor)
{
    ma_copy_and_apply_volume_factor_s32(pSamples, pSamples, sampleCount, factor);
}

MA_API void ma_apply_volume_factor_f32(float* pSamples, ma_uint32 sampleCount, float factor)
{
    ma_copy_and_apply_volume_factor_f32(pSamples, pSamples, sampleCount, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames_u8(ma_uint8* pPCMFramesOut, const ma_uint8* pPCMFramesIn, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_u8(pPCMFramesOut, pPCMFramesIn, frameCount*channels, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames_s16(ma_int16* pPCMFramesOut, const ma_int16* pPCMFramesIn, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_s16(pPCMFramesOut, pPCMFramesIn, frameCount*channels, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames_s24(void* pPCMFramesOut, const void* pPCMFramesIn, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_s24(pPCMFramesOut, pPCMFramesIn, frameCount*channels, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames_s32(ma_int32* pPCMFramesOut, const ma_int32* pPCMFramesIn, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_s32(pPCMFramesOut, pPCMFramesIn, frameCount*channels, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames_f32(float* pPCMFramesOut, const float* pPCMFramesIn, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_f32(pPCMFramesOut, pPCMFramesIn, frameCount*channels, factor);
}

MA_API void ma_copy_and_apply_volume_factor_pcm_frames(void* pPCMFramesOut, const void* pPCMFramesIn, ma_uint32 frameCount, ma_format format, ma_uint32 channels, float factor)
{
    switch (format)
    {
    case ma_format_u8:  ma_copy_and_apply_volume_factor_pcm_frames_u8 ((ma_uint8*)pPCMFramesOut, (const ma_uint8*)pPCMFramesIn, frameCount, channels, factor); return;
    case ma_format_s16: ma_copy_and_apply_volume_factor_pcm_frames_s16((ma_int16*)pPCMFramesOut, (const ma_int16*)pPCMFramesIn, frameCount, channels, factor); return;
    case ma_format_s24: ma_copy_and_apply_volume_factor_pcm_frames_s24(           pPCMFramesOut,                  pPCMFramesIn, frameCount, channels, factor); return;
    case ma_format_s32: ma_copy_and_apply_volume_factor_pcm_frames_s32((ma_int32*)pPCMFramesOut, (const ma_int32*)pPCMFramesIn, frameCount, channels, factor); return;
    case ma_format_f32: ma_copy_and_apply_volume_factor_pcm_frames_f32(   (float*)pPCMFramesOut,    (const float*)pPCMFramesIn, frameCount, channels, factor); return;
    default: return;    /* Do nothing. */
    }
}

MA_API void ma_apply_volume_factor_pcm_frames_u8(ma_uint8* pPCMFrames, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames_u8(pPCMFrames, pPCMFrames, frameCount, channels, factor);
}

MA_API void ma_apply_volume_factor_pcm_frames_s16(ma_int16* pPCMFrames, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames_s16(pPCMFrames, pPCMFrames, frameCount, channels, factor);
}

MA_API void ma_apply_volume_factor_pcm_frames_s24(void* pPCMFrames, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames_s24(pPCMFrames, pPCMFrames, frameCount, channels, factor);
}

MA_API void ma_apply_volume_factor_pcm_frames_s32(ma_int32* pPCMFrames, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames_s32(pPCMFrames, pPCMFrames, frameCount, channels, factor);
}

MA_API void ma_apply_volume_factor_pcm_frames_f32(float* pPCMFrames, ma_uint32 frameCount, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames_f32(pPCMFrames, pPCMFrames, frameCount, channels, factor);
}

MA_API void ma_apply_volume_factor_pcm_frames(void* pPCMFrames, ma_uint32 frameCount, ma_format format, ma_uint32 channels, float factor)
{
    ma_copy_and_apply_volume_factor_pcm_frames(pPCMFrames, pPCMFrames, frameCount, format, channels, factor);
}



MA_INLINE double ma_log(double x)
{
    /* TODO: Implement custom log(x). */
    return log(x);
}

MA_INLINE double ma_pow(double x, double y)
{
    /* TODO: Implement custom pow(x, y). */
    return pow(x, y);
}

MA_INLINE float ma_powf(float x, float y)
{
    return (float)ma_pow((double)x, (double)y);
}

MA_INLINE float ma_log10f(float x)
{
    return (float)log10((double)x);
}

MA_API float ma_factor_to_gain_db(float factor)
{
    return (float)(20*ma_log10f(factor));
}

MA_API float ma_gain_db_to_factor(float gain)
{
    return (float)ma_powf(10, gain/20.0f);
}


float ma_clip_f32(float x)
{
    if (x < -1) return -1;
    if (x > +1) return +1;
    return x;
}

float ma_mix_f32(float x, float y, float a)
{
    return x*(1-a) + y*a;
}

float ma_mix_f32_fast(float x, float y, float a)
{
    float r0 = (y - x);
    float r1 = r0*a;
    return x + r1;
    /*return x + (y - x)*a;*/
}

static ma_result ma_default_vfs_read__stdio(ma_vfs* pVFS, ma_vfs_file file, void* pDst, size_t sizeInBytes, size_t* pBytesRead)
{
    size_t result;

    MA_ASSERT(file != NULL);
    MA_ASSERT(pDst != NULL);

    (void)pVFS;
    
    result = fread(pDst, 1, sizeInBytes, (FILE*)file);

    printf("Read %i bytes\n", result);
    if (pBytesRead != NULL) {
        *pBytesRead = result;
    }
    
    if (result != sizeInBytes) {
        if (feof((FILE*)file)) {
            return MA_END_OF_FILE;
        } else {
            return ma_result_from_errno(ferror((FILE*)file));
        }
    }

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_write__stdio(ma_vfs* pVFS, ma_vfs_file file, const void* pSrc, size_t sizeInBytes, size_t* pBytesWritten)
{
    size_t result;

    MA_ASSERT(file != NULL);
    MA_ASSERT(pSrc != NULL);

    (void)pVFS;

    result = fwrite(pSrc, 1, sizeInBytes, (FILE*)file);

    if (pBytesWritten != NULL) {
        *pBytesWritten = result;
    }

    if (result != sizeInBytes) {
        return ma_result_from_errno(ferror((FILE*)file));
    }

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_seek__stdio(ma_vfs* pVFS, ma_vfs_file file, ma_int64 offset, ma_seek_origin origin)
{
    int result;

    MA_ASSERT(file != NULL);

    (void)pVFS;
    
#if defined(_WIN32)
    #if defined(_MSC_VER) && _MSC_VER > 1200
        result = _fseeki64((FILE*)file, offset, origin);
    #else
        /* No _fseeki64() so restrict to 31 bits. */
        if (origin > 0x7FFFFFFF) {
            return MA_OUT_OF_RANGE;
        }

        result = fseek((FILE*)file, (int)offset, origin);
    #endif
#else
    result = fseek((FILE*)file, (long int)offset, origin);
#endif
    if (result != 0) {
        return MA_ERROR;
    }

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_tell__stdio(ma_vfs* pVFS, ma_vfs_file file, ma_int64* pCursor)
{
    ma_int64 result;

    MA_ASSERT(file    != NULL);
    MA_ASSERT(pCursor != NULL);

    (void)pVFS;

#if defined(_WIN32)
    #if defined(_MSC_VER) && _MSC_VER > 1200
        result = _ftelli64((FILE*)file);
    #else
        result = ftell((FILE*)file);
    #endif
#else
    result = ftell((FILE*)file);
#endif

    *pCursor = result;

    return MA_SUCCESS;
}

#if !((defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE >= 1) || defined(_XOPEN_SOURCE) || defined(_POSIX_SOURCE))
int fileno(FILE *stream);
#endif

#include <sys/stat.h>
static ma_result ma_default_vfs_info__stdio(ma_vfs* pVFS, ma_vfs_file file, ma_file_info* pInfo)
{
    int fd;
    struct stat info;

    MA_ASSERT(file  != NULL);
    MA_ASSERT(pInfo != NULL);

    (void)pVFS;

#if defined(_MSC_VER)
    fd = _fileno((FILE*)file);
#else
    fd =  fileno((FILE*)file);
#endif

    if (fstat(fd, &info) != 0) {
        return ma_result_from_errno(errno);
    }

    pInfo->sizeInBytes = info.st_size;

    return MA_SUCCESS;
}

static MA_INLINE double ma_sin(double x)
{
    /* TODO: Implement custom sin(x). */
    return sin(x);
}

static MA_INLINE double ma_exp(double x)
{
    /* TODO: Implement custom exp(x). */
    return exp(x);
}

static MA_INLINE double ma_sqrt(double x)
{
    /* TODO: Implement custom sqrt(x). */
    return sqrt(x);
}


static MA_INLINE double ma_cos(double x)
{
    return ma_sin((MA_PI_D*0.5) - x);
}




/*
Return Values:
  0:  Success
  22: EINVAL
  34: ERANGE

Not using symbolic constants for errors because I want to avoid #including errno.h
*/
MA_API int ma_strcpy_s(char* dst, size_t dstSizeInBytes, const char* src)
{
    size_t i;

    if (dst == 0) {
        return 22;
    }
    if (dstSizeInBytes == 0) {
        return 34;
    }
    if (src == 0) {
        dst[0] = '\0';
        return 22;
    }

    for (i = 0; i < dstSizeInBytes && src[i] != '\0'; ++i) {
        dst[i] = src[i];
    }

    if (i < dstSizeInBytes) {
        dst[i] = '\0';
        return 0;
    }

    dst[0] = '\0';
    return 34;
}


MA_API int ma_strncat_s(char* dst, size_t dstSizeInBytes, const char* src, size_t count)
{
    char* dstorig;

    if (dst == 0) {
        return 22;
    }
    if (dstSizeInBytes == 0) {
        return 34;
    }
    if (src == 0) {
        return 22;
    }

    dstorig = dst;

    while (dstSizeInBytes > 0 && dst[0] != '\0') {
        dst += 1;
        dstSizeInBytes -= 1;
    }

    if (dstSizeInBytes == 0) {
        return 22;  /* Unterminated. */
    }


    if (count == ((size_t)-1)) {        /* _TRUNCATE */
        count = dstSizeInBytes - 1;
    }

    while (dstSizeInBytes > 0 && src[0] != '\0' && count > 0) {
        *dst++ = *src++;
        dstSizeInBytes -= 1;
        count -= 1;
    }

    if (dstSizeInBytes > 0) {
        dst[0] = '\0';
    } else {
        dstorig[0] = '\0';
        return 34;
    }

    return 0;
}

MA_API int ma_itoa_s(int value, char* dst, size_t dstSizeInBytes, int radix)
{
    int sign;
    unsigned int valueU;
    char* dstEnd;

    if (dst == NULL || dstSizeInBytes == 0) {
        return 22;
    }
    if (radix < 2 || radix > 36) {
        dst[0] = '\0';
        return 22;
    }

    sign = (value < 0 && radix == 10) ? -1 : 1;     /* The negative sign is only used when the base is 10. */

    if (value < 0) {
        valueU = -value;
    } else {
        valueU = value;
    }

    dstEnd = dst;
    do
    {
        int remainder = valueU % radix;
        if (remainder > 9) {
            *dstEnd = (char)((remainder - 10) + 'a');
        } else {
            *dstEnd = (char)(remainder + '0');
        }

        dstEnd += 1;
        dstSizeInBytes -= 1;
        valueU /= radix;
    } while (dstSizeInBytes > 0 && valueU > 0);

    if (dstSizeInBytes == 0) {
        dst[0] = '\0';
        return 22;  /* Ran out of room in the output buffer. */
    }

    if (sign < 0) {
        *dstEnd++ = '-';
        dstSizeInBytes -= 1;
    }

    if (dstSizeInBytes == 0) {
        dst[0] = '\0';
        return 22;  /* Ran out of room in the output buffer. */
    }

    *dstEnd = '\0';


    /* At this point the string will be reversed. */
    dstEnd -= 1;
    while (dst < dstEnd) {
        char temp = *dst;
        *dst = *dstEnd;
        *dstEnd = temp;

        dst += 1;
        dstEnd -= 1;
    }

    return 0;
}

MA_API int ma_strcmp(const char* str1, const char* str2)
{
    if (str1 == str2) return  0;

    /* These checks differ from the standard implementation. It's not important, but I prefer it just for sanity. */
    if (str1 == NULL) return -1;
    if (str2 == NULL) return  1;

    for (;;) {
        if (str1[0] == '\0') {
            break;
        }
        if (str1[0] != str2[0]) {
            break;
        }

        str1 += 1;
        str2 += 1;
    }

    return ((unsigned char*)str1)[0] - ((unsigned char*)str2)[0];
}

MA_API int ma_strappend(char* dst, size_t dstSize, const char* srcA, const char* srcB)
{
    int result;

    result = ma_strncpy_s(dst, dstSize, srcA, (size_t)-1);
    if (result != 0) {
        return result;
    }

    result = ma_strncat_s(dst, dstSize, srcB, (size_t)-1);
    if (result != 0) {
        return result;
    }

    return result;
}




/* Thanks to good old Bit Twiddling Hacks for this one: http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2 */
static MA_INLINE unsigned int ma_next_power_of_2(unsigned int x)
{
    x--;
    x |= x >> 1;
    x |= x >> 2;
    x |= x >> 4;
    x |= x >> 8;
    x |= x >> 16;
    x++;

    return x;
}

static MA_INLINE unsigned int ma_prev_power_of_2(unsigned int x)
{
    return ma_next_power_of_2(x) >> 1;
}

static MA_INLINE unsigned int ma_round_to_power_of_2(unsigned int x)
{
    unsigned int prev = ma_prev_power_of_2(x);
    unsigned int next = ma_next_power_of_2(x);
    if ((next - x) > (x - prev)) {
        return prev;
    } else {
        return next;
    }
}

static MA_INLINE unsigned int ma_count_set_bits(unsigned int x)
{
    unsigned int count = 0;
    while (x != 0) {
        if (x & 1) {
            count += 1;
        }
        
        x = x >> 1;
    }
    
    return count;
}

MA_API char* ma_copy_string(const char* src, const ma_allocation_callbacks* pAllocationCallbacks)
{
    size_t sz = strlen(src)+1;
    char* dst = (char*)ma_malloc(sz, pAllocationCallbacks);
    if (dst == NULL) {
        return NULL;
    }

    ma_strcpy_s(dst, sz, src);

    return dst;
}



MA_API ma_result ma_fopen(FILE** ppFile, const char* pFilePath, const char* pOpenMode)
{
#if _MSC_VER && _MSC_VER >= 1400
    errno_t err;
#endif

    if (ppFile != NULL) {
        *ppFile = NULL;  /* Safety. */
    }

    if (pFilePath == NULL || pOpenMode == NULL || ppFile == NULL) {
        return MA_INVALID_ARGS;
    }

#if _MSC_VER && _MSC_VER >= 1400
    err = fopen_s(ppFile, pFilePath, pOpenMode);
    if (err != 0) {
        return ma_result_from_errno(err);
    }
#else
#if defined(_WIN32) || defined(__APPLE__)
    *ppFile = fopen(pFilePath, pOpenMode);
#else
    #if defined(_FILE_OFFSET_BITS) && _FILE_OFFSET_BITS == 64 && defined(_LARGEFILE64_SOURCE)
        *ppFile = fopen64(pFilePath, pOpenMode);
    #else
        *ppFile = fopen(pFilePath, pOpenMode);
    #endif
	printf("Open %s %s %p\n", pFilePath, pOpenMode, ppFile);
#endif
    if (*ppFile == NULL) {
        ma_result result = ma_result_from_errno(errno);
        if (result == MA_SUCCESS) {
            result = MA_ERROR;   /* Just a safety check to make sure we never ever return success when pFile == NULL. */
        }

        return result;
    }
#endif

    return MA_SUCCESS;
}



/*
_wfopen() isn't always available in all compilation environments.

    * Windows only.
    * MSVC seems to support it universally as far back as VC6 from what I can tell (haven't checked further back).
    * MinGW-64 (both 32- and 64-bit) seems to support it.
    * MinGW wraps it in !defined(__STRICT_ANSI__).

This can be reviewed as compatibility issues arise. The preference is to use _wfopen_s() and _wfopen() as opposed to the wcsrtombs()
fallback, so if you notice your compiler not detecting this properly I'm happy to look at adding support.
*/
#if defined(_WIN32)
    #if defined(_MSC_VER) || defined(__MINGW64__) || !defined(__STRICT_ANSI__)
        #define MA_HAS_WFOPEN
    #endif
#endif

MA_API ma_result ma_wfopen(FILE** ppFile, const wchar_t* pFilePath, const wchar_t* pOpenMode, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (ppFile != NULL) {
        *ppFile = NULL;  /* Safety. */
    }

    if (pFilePath == NULL || pOpenMode == NULL || ppFile == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_HAS_WFOPEN)
    {
        /* Use _wfopen() on Windows. */
    #if defined(_MSC_VER) && _MSC_VER >= 1400
        errno_t err = _wfopen_s(ppFile, pFilePath, pOpenMode);
        if (err != 0) {
            return ma_result_from_errno(err);
        }
    #else
        *ppFile = _wfopen(pFilePath, pOpenMode);
        if (*ppFile == NULL) {
            return ma_result_from_errno(errno);
        }
    #endif
        (void)pAllocationCallbacks;
    }
#else
    /*
    Use fopen() on anything other than Windows. Requires a conversion. This is annoying because fopen() is locale specific. The only real way I can
    think of to do this is with wcsrtombs(). Note that wcstombs() is apparently not thread-safe because it uses a static global mbstate_t object for
    maintaining state. I've checked this with -std=c89 and it works, but if somebody get's a compiler error I'll look into improving compatibility.
    */
    {
        mbstate_t mbs;
        size_t lenMB;
        const wchar_t* pFilePathTemp = pFilePath;
        char* pFilePathMB = NULL;
        char pOpenModeMB[32] = {0};

        /* Get the length first. */
        MA_ZERO_OBJECT(&mbs);
        lenMB = wcsrtombs(NULL, &pFilePathTemp, 0, &mbs);
        if (lenMB == (size_t)-1) {
            return ma_result_from_errno(errno);
        }

        pFilePathMB = (char*)ma_malloc(lenMB + 1, pAllocationCallbacks);
        if (pFilePathMB == NULL) {
            return MA_OUT_OF_MEMORY;
        }

        pFilePathTemp = pFilePath;
        MA_ZERO_OBJECT(&mbs);
        wcsrtombs(pFilePathMB, &pFilePathTemp, lenMB + 1, &mbs);

        /* The open mode should always consist of ASCII characters so we should be able to do a trivial conversion. */
        {
            size_t i = 0;
            for (;;) {
                if (pOpenMode[i] == 0) {
                    pOpenModeMB[i] = '\0';
                    break;
                }

                pOpenModeMB[i] = (char)pOpenMode[i];
                i += 1;
            }
        }

        *ppFile = fopen(pFilePathMB, pOpenModeMB);

        ma_free(pFilePathMB, pAllocationCallbacks);
    }

    if (*ppFile == NULL) {
        return MA_ERROR;
    }
#endif

    return MA_SUCCESS;
}



static ma_result ma_default_vfs_open__stdio(ma_vfs* pVFS, const char* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    ma_result result;
    FILE* pFileStd;
    const char* pOpenModeStr;

    MA_ASSERT(pFilePath != NULL);
    MA_ASSERT(openMode  != 0);
    MA_ASSERT(pFile     != NULL);

    (void)pVFS;

    if ((openMode & MA_OPEN_MODE_READ) != 0) {
        if ((openMode & MA_OPEN_MODE_WRITE) != 0) {
            pOpenModeStr = "r+";
        } else {
            pOpenModeStr = "rb";
        }
    } else {
        pOpenModeStr = "wb";
    }

    result = ma_fopen(&pFileStd, pFilePath, pOpenModeStr);
    if (result != MA_SUCCESS) {
        return result;
    }

    *pFile = pFileStd;

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_open_w__stdio(ma_vfs* pVFS, const wchar_t* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    ma_result result;
    FILE* pFileStd;
    const wchar_t* pOpenModeStr;

    MA_ASSERT(pFilePath != NULL);
    MA_ASSERT(openMode  != 0);
    MA_ASSERT(pFile     != NULL);

    (void)pVFS;

    if ((openMode & MA_OPEN_MODE_READ) != 0) {
        if ((openMode & MA_OPEN_MODE_WRITE) != 0) {
            pOpenModeStr = L"r+";
        } else {
            pOpenModeStr = L"rb";
        }
    } else {
        pOpenModeStr = L"wb";
    }

    result = ma_wfopen(&pFileStd, pFilePath, pOpenModeStr, (pVFS != NULL) ? &((ma_default_vfs*)pVFS)->allocationCallbacks : NULL);
    if (result != MA_SUCCESS) {
        return result;
    }

    *pFile = pFileStd;

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_close__stdio(ma_vfs* pVFS, ma_vfs_file file)
{
    MA_ASSERT(file != NULL);

    (void)pVFS;

    fclose((FILE*)file);

    return MA_SUCCESS;
}

static ma_result ma_default_vfs_open(ma_vfs* pVFS, const char* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    if (pFile == NULL) {
        return MA_INVALID_ARGS;
    }

    *pFile = NULL;

    if (pFilePath == NULL || openMode == 0) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_open__win32(pVFS, pFilePath, openMode, pFile);
#else
    return ma_default_vfs_open__stdio(pVFS, pFilePath, openMode, pFile);
#endif
}

static ma_result ma_default_vfs_open_w(ma_vfs* pVFS, const wchar_t* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    if (pFile == NULL) {
        return MA_INVALID_ARGS;
    }

    *pFile = NULL;

    if (pFilePath == NULL || openMode == 0) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_open_w__win32(pVFS, pFilePath, openMode, pFile);
#else
    return ma_default_vfs_open_w__stdio(pVFS, pFilePath, openMode, pFile);
#endif
}

static ma_result ma_default_vfs_close(ma_vfs* pVFS, ma_vfs_file file)
{
    if (file == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_close__win32(pVFS, file);
#else
    return ma_default_vfs_close__stdio(pVFS, file);
#endif
}


static ma_result ma_default_vfs_write(ma_vfs* pVFS, ma_vfs_file file, const void* pSrc, size_t sizeInBytes, size_t* pBytesWritten)
{
    if (file == NULL || pSrc == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_write__win32(pVFS, file, pSrc, sizeInBytes, pBytesWritten);
#else
    return ma_default_vfs_write__stdio(pVFS, file, pSrc, sizeInBytes, pBytesWritten);
#endif
}

static ma_result ma_default_vfs_seek(ma_vfs* pVFS, ma_vfs_file file, ma_int64 offset, ma_seek_origin origin)
{
    if (file == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_seek__win32(pVFS, file, offset, origin);
#else
    return ma_default_vfs_seek__stdio(pVFS, file, offset, origin);
#endif
}

static ma_result ma_default_vfs_tell(ma_vfs* pVFS, ma_vfs_file file, ma_int64* pCursor)
{
    if (pCursor == NULL) {
        return MA_INVALID_ARGS;
    }

    *pCursor = 0;

    if (file == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_tell__win32(pVFS, file, pCursor);
#else
    return ma_default_vfs_tell__stdio(pVFS, file, pCursor);
#endif
}

static ma_result ma_default_vfs_info(ma_vfs* pVFS, ma_vfs_file file, ma_file_info* pInfo)
{
    if (pInfo == NULL) {
        return MA_INVALID_ARGS;
    }

    MA_ZERO_OBJECT(pInfo);

    if (file == NULL) {
        return MA_INVALID_ARGS;
    }

#if defined(MA_WIN32) && defined(MA_WIN32_DESKTOP)
    return ma_default_vfs_info__win32(pVFS, file, pInfo);
#else
    return ma_default_vfs_info__stdio(pVFS, file, pInfo);
#endif
}



static ma_result ma_default_vfs_read(ma_vfs* pVFS, ma_vfs_file file, void* pDst, size_t sizeInBytes, size_t* pBytesRead)
{
    if (file == NULL || pDst == NULL) {
        return MA_INVALID_ARGS;
    }

    return ma_default_vfs_read__stdio(pVFS, file, pDst, sizeInBytes, pBytesRead);
}

size_t ma_decoder__on_read_vfs(ma_decoder* pDecoder, void* pBufferOut, size_t bytesToRead)
{
    size_t bytesRead;

    MA_ASSERT(pDecoder   != NULL);
    MA_ASSERT(pBufferOut != NULL);

    if (pDecoder->backend.vfs.pVFS == NULL) {
        ma_default_vfs_read(NULL, pDecoder->backend.vfs.file, pBufferOut, bytesToRead, &bytesRead);
    } else {
        ma_vfs_read(pDecoder->backend.vfs.pVFS, pDecoder->backend.vfs.file, pBufferOut, bytesToRead, &bytesRead);
    }
    
    return bytesRead;
}

MA_API ma_result ma_default_vfs_init(ma_default_vfs* pVFS, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pVFS == NULL) {
        return MA_INVALID_ARGS;
    }

    pVFS->cb.onOpen  = ma_default_vfs_open;
    pVFS->cb.onOpenW = ma_default_vfs_open_w;
    pVFS->cb.onClose = ma_default_vfs_close;
    pVFS->cb.onRead  = ma_default_vfs_read;
    pVFS->cb.onWrite = ma_default_vfs_write;
    pVFS->cb.onSeek  = ma_default_vfs_seek;
    pVFS->cb.onTell  = ma_default_vfs_tell;
    pVFS->cb.onInfo  = ma_default_vfs_info;
    ma_allocation_callbacks_init_copy(&pVFS->allocationCallbacks, pAllocationCallbacks);

    return MA_SUCCESS;
}

MA_API ma_result ma_event_wait(ma_event* pEvent)
{
    if (pEvent == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return MA_INVALID_ARGS;
    }

#ifdef MA_WIN32
    return ma_event_wait__win32(pEvent);
#endif
#ifdef MA_POSIX
    return ma_event_wait__posix(pEvent);
#endif
}

MA_API ma_result ma_event_signal(ma_event* pEvent)
{
    if (pEvent == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return MA_INVALID_ARGS;
    }

#ifdef MA_WIN32
    return ma_event_signal__win32(pEvent);
#endif
#ifdef MA_POSIX
    return ma_event_signal__posix(pEvent);
#endif
}


void ma_sleep(ma_uint32 milliseconds)
{
    ma_sleep__posix(milliseconds);
}

MA_API void ma_mutex_lock(ma_mutex* pMutex)
{
    if (pMutex == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return;
    }

#ifdef MA_WIN32
    ma_mutex_lock__win32(pMutex);
#endif
#ifdef MA_POSIX
    ma_mutex_lock__posix(pMutex);
#endif
}

MA_API void ma_mutex_unlock(ma_mutex* pMutex)
{
    if (pMutex == NULL) {
        MA_ASSERT(MA_FALSE);    /* Fire an assert to the caller is aware of this bug. */
        return;
}

#ifdef MA_WIN32
    ma_mutex_unlock__win32(pMutex);
#endif
#ifdef MA_POSIX
    ma_mutex_unlock__posix(pMutex);
#endif
}

MA_API ma_result ma_decoder_uninit(ma_decoder* pDecoder)
{
    if (pDecoder == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pDecoder->onUninit) {
        pDecoder->onUninit(pDecoder);
    }

    if (pDecoder->onRead == ma_decoder__on_read_vfs) {
        if (pDecoder->backend.vfs.pVFS == NULL) {
            ma_default_vfs_close(NULL, pDecoder->backend.vfs.file);
        } else {
            ma_vfs_close(pDecoder->backend.vfs.pVFS, pDecoder->backend.vfs.file);
        }
    }

    ma_data_converter_uninit(&pDecoder->converter);

    return MA_SUCCESS;
}
MA_API void* ma_malloc(size_t sz, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks != NULL) {
        return ma__malloc_from_callbacks(sz, pAllocationCallbacks);
    } else {
        return ma__malloc_default(sz, NULL);
    }
}

MA_API void* ma_realloc(void* p, size_t sz, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks != NULL) {
        if (pAllocationCallbacks->onRealloc != NULL) {
            return pAllocationCallbacks->onRealloc(p, sz, pAllocationCallbacks->pUserData);
        } else {
            return NULL;    /* This requires a native implementation of realloc(). */
        }
    } else {
        return ma__realloc_default(p, sz, NULL);
    }
}

MA_API void ma_free(void* p, const ma_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks != NULL) {
        ma__free_from_callbacks(p, pAllocationCallbacks);
    } else {
        ma__free_default(p, NULL);
    }
}

MA_API void* ma_aligned_malloc(size_t sz, size_t alignment, const ma_allocation_callbacks* pAllocationCallbacks)
{
    size_t extraBytes;
    void* pUnaligned;
    void* pAligned;

    if (alignment == 0) {
        return 0;
    }

    extraBytes = alignment-1 + sizeof(void*);

    pUnaligned = ma_malloc(sz + extraBytes, pAllocationCallbacks);
    if (pUnaligned == NULL) {
        return NULL;
    }

    pAligned = (void*)(((ma_uintptr)pUnaligned + extraBytes) & ~((ma_uintptr)(alignment-1)));
    ((void**)pAligned)[-1] = pUnaligned;

    return pAligned;
}

MA_API void ma_aligned_free(void* p, const ma_allocation_callbacks* pAllocationCallbacks)
{
    ma_free(((void**)p)[-1], pAllocationCallbacks);
}


MA_API ma_result ma_context_enumerate_devices(ma_context* pContext, ma_enum_devices_callback_proc callback, void* pUserData)
{
    ma_result result;

    if (pContext == NULL || pContext->onEnumDevices == NULL || callback == NULL) {
        return MA_INVALID_ARGS;
    }

    ma_mutex_lock(&pContext->deviceEnumLock);
    {
        result = pContext->onEnumDevices(pContext, callback, pUserData);
    }
    ma_mutex_unlock(&pContext->deviceEnumLock);

    return result;
}


MA_API ma_result ma_vfs_close(ma_vfs* pVFS, ma_vfs_file file)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pVFS == NULL || file == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onClose == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onClose(pVFS, file);
}

MA_API ma_result ma_vfs_read(ma_vfs* pVFS, ma_vfs_file file, void* pDst, size_t sizeInBytes, size_t* pBytesRead)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pBytesRead != NULL) {
        *pBytesRead = 0;
    }

    if (pVFS == NULL || file == NULL || pDst == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onRead == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onRead(pVFS, file, pDst, sizeInBytes, pBytesRead);
}

MA_API ma_result ma_vfs_write(ma_vfs* pVFS, ma_vfs_file file, const void* pSrc, size_t sizeInBytes, size_t* pBytesWritten)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pBytesWritten == NULL) {
        *pBytesWritten = 0;
    }

    if (pVFS == NULL || file == NULL || pSrc == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onWrite == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onWrite(pVFS, file, pSrc, sizeInBytes, pBytesWritten);
}

MA_API ma_result ma_vfs_seek(ma_vfs* pVFS, ma_vfs_file file, ma_int64 offset, ma_seek_origin origin)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pVFS == NULL || file == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onSeek == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onSeek(pVFS, file, offset, origin);
}

MA_API ma_result ma_vfs_tell(ma_vfs* pVFS, ma_vfs_file file, ma_int64* pCursor)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pCursor == NULL) {
        return MA_INVALID_ARGS;
    }

    *pCursor = 0;

    if (pVFS == NULL || file == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onTell == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onTell(pVFS, file, pCursor);
}

MA_API ma_result ma_vfs_info(ma_vfs* pVFS, ma_vfs_file file, ma_file_info* pInfo)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pInfo == NULL) {
        return MA_INVALID_ARGS;
    }

    MA_ZERO_OBJECT(pInfo);

    if (pVFS == NULL || file == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onInfo == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onInfo(pVFS, file, pInfo);
}


static ma_result ma_vfs_open_and_read_file_ex(ma_vfs* pVFS, const char* pFilePath, void** ppData, size_t* pSize, const ma_allocation_callbacks* pAllocationCallbacks, ma_uint32 allocationType)
{
    ma_result result;
    ma_vfs_file file;
    ma_file_info info;
    void* pData;
    size_t bytesRead;

    (void)allocationType;

    if (ppData != NULL) {
        *ppData = NULL;
    }
    if (pSize != NULL) {
        *pSize = 0;
    }

    if (ppData == NULL) {
        return MA_INVALID_ARGS;
    }

    result = ma_vfs_open(pVFS, pFilePath, MA_OPEN_MODE_READ, &file);
    if (result != MA_SUCCESS) {
        return result;
    }

    result = ma_vfs_info(pVFS, file, &info);
    if (result != MA_SUCCESS) {
        ma_vfs_close(pVFS, file);
        return result;
    }

    if (info.sizeInBytes > MA_SIZE_MAX) {
        ma_vfs_close(pVFS, file);
        return MA_TOO_BIG;
    }

    pData = ma__malloc_from_callbacks((size_t)info.sizeInBytes, pAllocationCallbacks);  /* Safe cast. */
    if (pData == NULL) {
        ma_vfs_close(pVFS, file);
        return result;
    }

    result = ma_vfs_read(pVFS, file, pData, (size_t)info.sizeInBytes, &bytesRead);  /* Safe cast. */
    ma_vfs_close(pVFS, file);

    if (result != MA_SUCCESS) {
        ma__free_from_callbacks(pData, pAllocationCallbacks);
        return result;
    }

    if (pSize != NULL) {
        *pSize = bytesRead;
    }

    MA_ASSERT(ppData != NULL);
    *ppData = pData;

    return MA_SUCCESS;
}

ma_result ma_vfs_open_and_read_file(ma_vfs* pVFS, const char* pFilePath, void** ppData, size_t* pSize, const ma_allocation_callbacks* pAllocationCallbacks)
{
    return ma_vfs_open_and_read_file_ex(pVFS, pFilePath, ppData, pSize, pAllocationCallbacks, 0 /*MA_ALLOCATION_TYPE_GENERAL*/);
}


MA_API ma_result ma_vfs_open(ma_vfs* pVFS, const char* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pFile == NULL) {
        return MA_INVALID_ARGS;
    }

    *pFile = NULL;

    if (pVFS == NULL || pFilePath == NULL || openMode == 0) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onOpen == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onOpen(pVFS, pFilePath, openMode, pFile);
}

MA_API ma_result ma_vfs_open_w(ma_vfs* pVFS, const wchar_t* pFilePath, ma_uint32 openMode, ma_vfs_file* pFile)
{
    ma_vfs_callbacks* pCallbacks = (ma_vfs_callbacks*)pVFS;

    if (pFile == NULL) {
        return MA_INVALID_ARGS;
    }

    *pFile = NULL;

    if (pVFS == NULL || pFilePath == NULL || openMode == 0) {
        return MA_INVALID_ARGS;
    }

    if (pCallbacks->onOpenW == NULL) {
        return MA_NOT_IMPLEMENTED;
    }

    return pCallbacks->onOpenW(pVFS, pFilePath, openMode, pFile);
}


void ma_get_standard_channel_map_alsa(ma_uint32 channels, ma_channel channelMap[MA_MAX_CHANNELS])
{
    switch (channels)
    {
        case 1:
        {
            channelMap[0] = MA_CHANNEL_MONO;
        } break;

        case 2:
        {
            channelMap[0] = MA_CHANNEL_LEFT;
            channelMap[1] = MA_CHANNEL_RIGHT;
        } break;

        case 3:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
        } break;

        case 4:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
        } break;

        case 5:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
            channelMap[4] = MA_CHANNEL_FRONT_CENTER;
        } break;

        case 6:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
            channelMap[4] = MA_CHANNEL_FRONT_CENTER;
            channelMap[5] = MA_CHANNEL_LFE;
        } break;

        case 7:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
            channelMap[4] = MA_CHANNEL_FRONT_CENTER;
            channelMap[5] = MA_CHANNEL_LFE;
            channelMap[6] = MA_CHANNEL_BACK_CENTER;
        } break;

        case 8:
        default:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
            channelMap[4] = MA_CHANNEL_FRONT_CENTER;
            channelMap[5] = MA_CHANNEL_LFE;
            channelMap[6] = MA_CHANNEL_SIDE_LEFT;
            channelMap[7] = MA_CHANNEL_SIDE_RIGHT;
        } break;
    }

    /* Remainder. */
    if (channels > 8) {
        ma_uint32 iChannel;
        for (iChannel = 8; iChannel < MA_MAX_CHANNELS; ++iChannel) {
            channelMap[iChannel] = (ma_channel)(MA_CHANNEL_AUX_0 + (iChannel-8));
        }
    }
}

static void ma_get_standard_channel_map_microsoft(ma_uint32 channels, ma_channel channelMap[MA_MAX_CHANNELS])
{
    /* Based off the speaker configurations mentioned here: https://docs.microsoft.com/en-us/windows-hardware/drivers/ddi/content/ksmedia/ns-ksmedia-ksaudio_channel_config */
    switch (channels)
    {
        case 1:
        {
            channelMap[0] = MA_CHANNEL_MONO;
        } break;

        case 2:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
        } break;

        case 3: /* Not defined, but best guess. */
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
        } break;

        case 4:
        {
#ifndef MA_USE_QUAD_MICROSOFT_CHANNEL_MAP
            /* Surround. Using the Surround profile has the advantage of the 3rd channel (MA_CHANNEL_FRONT_CENTER) mapping nicely with higher channel counts. */
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
            channelMap[3] = MA_CHANNEL_BACK_CENTER;
#else
            /* Quad. */
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_BACK_LEFT;
            channelMap[3] = MA_CHANNEL_BACK_RIGHT;
#endif
        } break;

        case 5: /* Not defined, but best guess. */
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
            channelMap[3] = MA_CHANNEL_BACK_LEFT;
            channelMap[4] = MA_CHANNEL_BACK_RIGHT;
        } break;

        case 6:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
            channelMap[3] = MA_CHANNEL_LFE;
            channelMap[4] = MA_CHANNEL_SIDE_LEFT;
            channelMap[5] = MA_CHANNEL_SIDE_RIGHT;
        } break;

        case 7: /* Not defined, but best guess. */
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
            channelMap[3] = MA_CHANNEL_LFE;
            channelMap[4] = MA_CHANNEL_BACK_CENTER;
            channelMap[5] = MA_CHANNEL_SIDE_LEFT;
            channelMap[6] = MA_CHANNEL_SIDE_RIGHT;
        } break;

        case 8:
        default:
        {
            channelMap[0] = MA_CHANNEL_FRONT_LEFT;
            channelMap[1] = MA_CHANNEL_FRONT_RIGHT;
            channelMap[2] = MA_CHANNEL_FRONT_CENTER;
            channelMap[3] = MA_CHANNEL_LFE;
            channelMap[4] = MA_CHANNEL_BACK_LEFT;
            channelMap[5] = MA_CHANNEL_BACK_RIGHT;
            channelMap[6] = MA_CHANNEL_SIDE_LEFT;
            channelMap[7] = MA_CHANNEL_SIDE_RIGHT;
        } break;
    }

    /* Remainder. */
    if (channels > 8) {
        ma_uint32 iChannel;
        for (iChannel = 8; iChannel < MA_MAX_CHANNELS; ++iChannel) {
            channelMap[iChannel] = (ma_channel)(MA_CHANNEL_AUX_0 + (iChannel-8));
        }
    }
}


MA_API void ma_get_standard_channel_map(ma_standard_channel_map standardChannelMap, ma_uint32 channels, ma_channel channelMap[MA_MAX_CHANNELS])
{
    switch (standardChannelMap)
    {
        case ma_standard_channel_map_alsa:
        {
	  printf("Get alsa map\n");
	  ma_get_standard_channel_map_alsa(channels, channelMap);
        } break;
#if 0
        case ma_standard_channel_map_rfc3551:
        {
            ma_get_standard_channel_map_rfc3551(channels, channelMap);
        } break;

        case ma_standard_channel_map_flac:
        {
            ma_get_standard_channel_map_flac(channels, channelMap);
        } break;

        case ma_standard_channel_map_vorbis:
        {
            ma_get_standard_channel_map_vorbis(channels, channelMap);
        } break;

        case ma_standard_channel_map_sound4:
        {
            ma_get_standard_channel_map_sound4(channels, channelMap);
        } break;
        
        case ma_standard_channel_map_sndio:
        {
            ma_get_standard_channel_map_sndio(channels, channelMap);
        } break;

        case ma_standard_channel_map_microsoft:
#endif
    default:
      {
	printf("Get standard map\n");
	ma_get_standard_channel_map_microsoft(channels, channelMap);
      } break;

    }
}

ma_uint32 ma_get_bytes_per_frame(ma_format format, ma_uint32 channels) { return ma_get_bytes_per_sample(format) * channels; }
MA_API ma_uint32 ma_get_bytes_per_sample(ma_format format)
{
    ma_uint32 sizes[] = {
        0,  /* unknown */
        1,  /* u8 */
        2,  /* s16 */
        3,  /* s24 */
        4,  /* s32 */
        4,  /* f32 */
    };
    return sizes[format];
}

MA_API void ma_data_converter_uninit(ma_data_converter* pConverter)
{
    if (pConverter == NULL) {
        return;
    }

    if (pConverter->hasResampler) {
        ma_resampler_uninit(&pConverter->resampler);
    }
}

MA_API void ma_resampler_uninit(ma_resampler* pResampler)
{
    if (pResampler == NULL) {
        return;
    }

    assert(0);
#if 0
    if (pResampler->config.algorithm == ma_resample_algorithm_linear) {
        ma_linear_resampler_uninit(&pResampler->state.linear);
    }

#if defined(MA_HAS_SPEEX_RESAMPLER)
    if (pResampler->config.algorithm == ma_resample_algorithm_speex) {
        speex_resampler_destroy((SpeexResamplerState*)pResampler->state.speex.pSpeexResamplerState);
    }
#endif
#endif
}

float g_maChannelPlaneRatios[MA_CHANNEL_POSITION_COUNT][6] = {
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_NONE */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_MONO */
    { 0.5f,  0.0f,  0.5f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_FRONT_LEFT */
    { 0.0f,  0.5f,  0.5f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_FRONT_RIGHT */
    { 0.0f,  0.0f,  1.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_FRONT_CENTER */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_LFE */
    { 0.5f,  0.0f,  0.0f,  0.5f,  0.0f,  0.0f},  /* MA_CHANNEL_BACK_LEFT */
    { 0.0f,  0.5f,  0.0f,  0.5f,  0.0f,  0.0f},  /* MA_CHANNEL_BACK_RIGHT */
    { 0.25f, 0.0f,  0.75f, 0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_FRONT_LEFT_CENTER */
    { 0.0f,  0.25f, 0.75f, 0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_FRONT_RIGHT_CENTER */
    { 0.0f,  0.0f,  0.0f,  1.0f,  0.0f,  0.0f},  /* MA_CHANNEL_BACK_CENTER */
    { 1.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_SIDE_LEFT */
    { 0.0f,  1.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_SIDE_RIGHT */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  1.0f},  /* MA_CHANNEL_TOP_CENTER */
    { 0.33f, 0.0f,  0.33f, 0.0f,  0.0f,  0.34f}, /* MA_CHANNEL_TOP_FRONT_LEFT */
    { 0.0f,  0.0f,  0.5f,  0.0f,  0.0f,  0.5f},  /* MA_CHANNEL_TOP_FRONT_CENTER */
    { 0.0f,  0.33f, 0.33f, 0.0f,  0.0f,  0.34f}, /* MA_CHANNEL_TOP_FRONT_RIGHT */
    { 0.33f, 0.0f,  0.0f,  0.33f, 0.0f,  0.34f}, /* MA_CHANNEL_TOP_BACK_LEFT */
    { 0.0f,  0.0f,  0.0f,  0.5f,  0.0f,  0.5f},  /* MA_CHANNEL_TOP_BACK_CENTER */
    { 0.0f,  0.33f, 0.0f,  0.33f, 0.0f,  0.34f}, /* MA_CHANNEL_TOP_BACK_RIGHT */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_0 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_1 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_2 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_3 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_4 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_5 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_6 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_7 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_8 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_9 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_10 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_11 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_12 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_13 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_14 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_15 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_16 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_17 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_18 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_19 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_20 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_21 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_22 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_23 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_24 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_25 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_26 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_27 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_28 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_29 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_30 */
    { 0.0f,  0.0f,  0.0f,  0.0f,  0.0f,  0.0f},  /* MA_CHANNEL_AUX_31 */
};

float ma_calculate_channel_position_rectangular_weight(ma_channel channelPositionA, ma_channel channelPositionB)
{
    /*
    Imagine the following simplified example: You have a single input speaker which is the front/left speaker which you want to convert to
    the following output configuration:
    
     - front/left
     - side/left
     - back/left
    
    The front/left output is easy - it the same speaker position so it receives the full contribution of the front/left input. The amount
    of contribution to apply to the side/left and back/left speakers, however, is a bit more complicated.
    
    Imagine the front/left speaker as emitting audio from two planes - the front plane and the left plane. You can think of the front/left
    speaker emitting half of it's total volume from the front, and the other half from the left. Since part of it's volume is being emitted
    from the left side, and the side/left and back/left channels also emit audio from the left plane, one would expect that they would
    receive some amount of contribution from front/left speaker. The amount of contribution depends on how many planes are shared between
    the two speakers. Note that in the examples below I've added a top/front/left speaker as an example just to show how the math works
    across 3 spatial dimensions.
    
    The first thing to do is figure out how each speaker's volume is spread over each of plane:
     - front/left:     2 planes (front and left)      = 1/2 = half it's total volume on each plane
     - side/left:      1 plane (left only)            = 1/1 = entire volume from left plane
     - back/left:      2 planes (back and left)       = 1/2 = half it's total volume on each plane
     - top/front/left: 3 planes (top, front and left) = 1/3 = one third it's total volume on each plane
    
    The amount of volume each channel contributes to each of it's planes is what controls how much it is willing to given and take to other
    channels on the same plane. The volume that is willing to the given by one channel is multiplied by the volume that is willing to be
    taken by the other to produce the final contribution.
    */

    /* Contribution = Sum(Volume to Give * Volume to Take) */
    float contribution = 
        g_maChannelPlaneRatios[channelPositionA][0] * g_maChannelPlaneRatios[channelPositionB][0] +
        g_maChannelPlaneRatios[channelPositionA][1] * g_maChannelPlaneRatios[channelPositionB][1] +
        g_maChannelPlaneRatios[channelPositionA][2] * g_maChannelPlaneRatios[channelPositionB][2] +
        g_maChannelPlaneRatios[channelPositionA][3] * g_maChannelPlaneRatios[channelPositionB][3] +
        g_maChannelPlaneRatios[channelPositionA][4] * g_maChannelPlaneRatios[channelPositionB][4] +
        g_maChannelPlaneRatios[channelPositionA][5] * g_maChannelPlaneRatios[channelPositionB][5];

    return contribution;
}

MA_API ma_channel_converter_config ma_channel_converter_config_init(ma_format format, ma_uint32 channelsIn, const ma_channel channelMapIn[MA_MAX_CHANNELS], ma_uint32 channelsOut, const ma_channel channelMapOut[MA_MAX_CHANNELS], ma_channel_mix_mode mixingMode)
{
    ma_channel_converter_config config;
    MA_ZERO_OBJECT(&config);
    config.format      = format;
    config.channelsIn  = channelsIn;
    config.channelsOut = channelsOut;
    ma_channel_map_copy(config.channelMapIn,  channelMapIn,  channelsIn);
    ma_channel_map_copy(config.channelMapOut, channelMapOut, channelsOut);
    config.mixingMode  = mixingMode;

    return config;
}

ma_bool32 ma_is_spatial_channel_position(ma_channel channelPosition)
{
    int i;

    if (channelPosition == MA_CHANNEL_NONE || channelPosition == MA_CHANNEL_MONO || channelPosition == MA_CHANNEL_LFE) {
        return MA_FALSE;
    }

    for (i = 0; i < 6; ++i) {   /* Each side of a cube. */
        if (g_maChannelPlaneRatios[channelPosition][i] != 0) {
            return MA_TRUE;
        }
    }

    return MA_FALSE;
}

ma_bool32 ma_channel_map_blank(ma_uint32 channels, const ma_channel channelMap[MA_MAX_CHANNELS])
{
    ma_uint32 iChannel;

    for (iChannel = 0; iChannel < channels; ++iChannel) {
        if (channelMap[iChannel] != MA_CHANNEL_NONE) {
            return MA_FALSE;
        }
    }

    return MA_TRUE;
}

ma_result ma_decoder__init_data_converter(ma_decoder* pDecoder, const ma_decoder_config* pConfig)
{
    ma_data_converter_config converterConfig;

    MA_ASSERT(pDecoder != NULL);

    /* Output format. */
    if (pConfig->format == ma_format_unknown) {
        pDecoder->outputFormat = pDecoder->internalFormat;
    } else {
        pDecoder->outputFormat = pConfig->format;
    }

    if (pConfig->channels == 0) {
        pDecoder->outputChannels = pDecoder->internalChannels;
    } else {
        pDecoder->outputChannels = pConfig->channels;
    }

    if (pConfig->sampleRate == 0) {
        pDecoder->outputSampleRate = pDecoder->internalSampleRate;
    } else {
        pDecoder->outputSampleRate = pConfig->sampleRate;
    }

    if (ma_channel_map_blank(pDecoder->outputChannels, pConfig->channelMap)) {
        ma_get_standard_channel_map(ma_standard_channel_map_default, pDecoder->outputChannels, pDecoder->outputChannelMap);
    } else {
        MA_COPY_MEMORY(pDecoder->outputChannelMap, pConfig->channelMap, sizeof(pConfig->channelMap));
    }

    
    converterConfig = ma_data_converter_config_init(
        pDecoder->internalFormat,     pDecoder->outputFormat, 
        pDecoder->internalChannels,   pDecoder->outputChannels,
        pDecoder->internalSampleRate, pDecoder->outputSampleRate
    );
    ma_channel_map_copy(converterConfig.channelMapIn,  pDecoder->internalChannelMap, pDecoder->internalChannels);
    ma_channel_map_copy(converterConfig.channelMapOut, pDecoder->outputChannelMap,   pDecoder->outputChannels);
    converterConfig.channelMixMode             = pConfig->channelMixMode;
    converterConfig.ditherMode                 = pConfig->ditherMode;
    converterConfig.resampling.allowDynamicSampleRate = MA_FALSE;   /* Never allow dynamic sample rate conversion. Setting this to true will disable passthrough optimizations. */
    converterConfig.resampling.algorithm       = pConfig->resampling.algorithm;
    converterConfig.resampling.linear.lpfOrder = pConfig->resampling.linear.lpfOrder;
    converterConfig.resampling.speex.quality   = pConfig->resampling.speex.quality;

    return ma_data_converter_init(&converterConfig, &pDecoder->converter);
}



static ma_result ma_decoder__postinit(const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_result result = MA_SUCCESS;

    /* Basic validation in case the internal decoder supports different limits to miniaudio. */
    if (pDecoder->internalChannels < MA_MIN_CHANNELS || pDecoder->internalChannels > MA_MAX_CHANNELS) {
        result = MA_INVALID_DATA;
    }

    if (result == MA_SUCCESS) {
        result = ma_decoder__init_data_converter(pDecoder, pConfig);
    }

    /* If we failed post initialization we need to uninitialize the decoder before returning to prevent a memory leak. */
    if (result != MA_SUCCESS) {
        ma_decoder_uninit(pDecoder);
        return result;
    }

    return result;
}



MA_API ma_decoder_config ma_decoder_config_init_copy(const ma_decoder_config* pConfig)
{
    ma_decoder_config config;
    if (pConfig != NULL) {
        config = *pConfig;
    } else {
        MA_ZERO_OBJECT(&config);
    }

    return config;
}

static ma_result ma_decoder__preinit(ma_decoder_read_proc onRead, ma_decoder_seek_proc onSeek, void* pUserData, const ma_decoder_config* pConfig, ma_decoder* pDecoder);
static ma_bool32 ma_decoder__on_seek_vfs(ma_decoder* pDecoder, int offset, ma_seek_origin origin);
  
static ma_result ma_decoder__preinit_vfs(ma_vfs* pVFS, const char* pFilePath, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_result result;
    ma_vfs_file file;

    result = ma_decoder__preinit(ma_decoder__on_read_vfs, ma_decoder__on_seek_vfs, NULL, pConfig, pDecoder);
    if (result != MA_SUCCESS) {
        return result;
    }

    if (pFilePath == NULL || pFilePath[0] == '\0') {
        return MA_INVALID_ARGS;
    }

    if (pVFS == NULL) {
        result = ma_default_vfs_open(NULL, pFilePath, MA_OPEN_MODE_READ, &file);
    } else {
        result = ma_vfs_open(pVFS, pFilePath, MA_OPEN_MODE_READ, &file);
    }
    
    if (result != MA_SUCCESS) {
        return result;
    }

    pDecoder->backend.vfs.pVFS = pVFS;
    pDecoder->backend.vfs.file = file;

    return MA_SUCCESS;
}

static ma_result ma_decoder_init__internal(ma_decoder_read_proc onRead, ma_decoder_seek_proc onSeek, void* pUserData, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_result result = MA_NO_BACKEND;

    MA_ASSERT(pConfig != NULL);
    MA_ASSERT(pDecoder != NULL);

    /* Silence some warnings in the case that we don't have any decoder backends enabled. */
    (void)onRead;
    (void)onSeek;
    (void)pUserData;
    (void)pConfig;
    (void)pDecoder;

    /* We use trial and error to open a decoder. */

#ifdef MA_HAS_WAV
    if (result != MA_SUCCESS) {
        result = ma_decoder_init_wav__internal(pConfig, pDecoder);
        if (result != MA_SUCCESS) {
            onSeek(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif
#ifdef MA_HAS_FLAC
    if (result != MA_SUCCESS) {
        result = ma_decoder_init_flac__internal(pConfig, pDecoder);
        if (result != MA_SUCCESS) {
            onSeek(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif
#ifdef MA_HAS_MP3
    if (result != MA_SUCCESS) {
        result = ma_decoder_init_mp3__internal(pConfig, pDecoder);
        if (result != MA_SUCCESS) {
            onSeek(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif
#ifdef MA_HAS_VORBIS
    if (result != MA_SUCCESS) {
        result = ma_decoder_init_vorbis__internal(pConfig, pDecoder);
        if (result != MA_SUCCESS) {
            onSeek(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif

    if (result != MA_SUCCESS) {
        return result;
    }

    return ma_decoder__postinit(pConfig, pDecoder);
}


static ma_result ma_decoder__data_source_on_seek(ma_data_source* pDataSource, ma_uint64 frameIndex)
{
    return ma_decoder_seek_to_pcm_frame((ma_decoder*)pDataSource, frameIndex);
}

static ma_result ma_decoder__data_source_on_get_data_format(ma_data_source* pDataSource, ma_format* pFormat, ma_uint32* pChannels)
{
    ma_decoder* pDecoder = (ma_decoder*)pDataSource;

    *pFormat   = pDecoder->outputFormat;
    *pChannels = pDecoder->outputChannels;

    return MA_SUCCESS;
}


static ma_result ma_decoder__data_source_on_read(ma_data_source* pDataSource, void* pFramesOut, ma_uint64 frameCount, ma_uint64* pFramesRead)
{
    ma_uint64 framesRead = ma_decoder_read_pcm_frames((ma_decoder*)pDataSource, pFramesOut, frameCount);

    if (pFramesRead != NULL) {
        *pFramesRead = framesRead;
    }

    if (framesRead < frameCount) {
        return MA_AT_END;
    }

    return MA_SUCCESS;
}

ma_result ma_decoder__init_allocation_callbacks(const ma_decoder_config* pConfig, ma_decoder* pDecoder);

ma_result ma_decoder__preinit(ma_decoder_read_proc onRead, ma_decoder_seek_proc onSeek, void* pUserData, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_result result;

    MA_ASSERT(pConfig != NULL);

    if (pDecoder == NULL) {
        return MA_INVALID_ARGS;
    }

    MA_ZERO_OBJECT(pDecoder);

    if (onRead == NULL || onSeek == NULL) {
        return MA_INVALID_ARGS;
    }

    pDecoder->ds.onRead          = ma_decoder__data_source_on_read;
    pDecoder->ds.onSeek          = ma_decoder__data_source_on_seek;
    pDecoder->ds.onGetDataFormat = ma_decoder__data_source_on_get_data_format;

    pDecoder->onRead    = onRead;
    pDecoder->onSeek    = onSeek;
    pDecoder->pUserData = pUserData;

    result = ma_decoder__init_allocation_callbacks(pConfig, pDecoder);
    if (result != MA_SUCCESS) {
        return result;
    }

    return MA_SUCCESS;
}

static ma_bool32 ma_decoder__on_seek_vfs(ma_decoder* pDecoder, int offset, ma_seek_origin origin)
{
    ma_result result;

    MA_ASSERT(pDecoder != NULL);

    if (pDecoder->backend.vfs.pVFS == NULL) {
        result = ma_default_vfs_seek(NULL, pDecoder->backend.vfs.file, offset, origin);
    } else {
        result = ma_vfs_seek(pDecoder->backend.vfs.pVFS, pDecoder->backend.vfs.file, offset, origin);
    }
    
    if (result != MA_SUCCESS) {
        return MA_FALSE;
    }

    return MA_TRUE;
}


ma_result ma_decoder__init_allocation_callbacks(const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    MA_ASSERT(pDecoder != NULL);

    if (pConfig != NULL) {
        return ma_allocation_callbacks_init_copy(&pDecoder->allocationCallbacks, &pConfig->allocationCallbacks);
    } else {
        pDecoder->allocationCallbacks = ma_allocation_callbacks_init_default();
        return MA_SUCCESS;
    }
}

MA_API ma_result ma_decoder_init(ma_decoder_read_proc onRead, ma_decoder_seek_proc onSeek, void* pUserData, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_decoder_config config;
    ma_result result;

    config = ma_decoder_config_init_copy(pConfig);

    result = ma_decoder__preinit(onRead, onSeek, pUserData, &config, pDecoder);
    if (result != MA_SUCCESS) {
        return result;
    }

    return ma_decoder_init__internal(onRead, onSeek, pUserData, &config, pDecoder);
}

static ma_bool32 ma_path_extension_equal(const char* path, const char* extension)
{
    const char* ext1;
    const char* ext2;

    if (path == NULL || extension == NULL) {
        return MA_FALSE;
    }

    ext1 = extension;
    ext2 = ma_path_extension(path);

#if defined(_MSC_VER) || defined(__DMC__)
    return _stricmp(ext1, ext2) == 0;
#else
    return strcasecmp(ext1, ext2) == 0;
#endif
}

const char* ma_path_extension(const char* path)
{
    const char* extension;
    const char* lastOccurance;

    if (path == NULL) {
        path = "";
    }

    extension = ma_path_file_name(path);
    lastOccurance = NULL;

    /* Just find the last '.' and return. */
    while (extension[0] != '\0') {
        if (extension[0] == '.') {
            extension += 1;
            lastOccurance = extension;
        }

        extension += 1;
    }

    return (lastOccurance != NULL) ? lastOccurance : extension;
}



MA_API ma_result ma_decoder_init_vfs(ma_vfs* pVFS, const char* pFilePath, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    ma_result result;
    ma_decoder_config config;


    config = ma_decoder_config_init_copy(pConfig);
    result = ma_decoder__preinit_vfs(pVFS, pFilePath, &config, pDecoder);

    if (result != MA_SUCCESS) {
        return result;
    }

    result = MA_NO_BACKEND;

#ifdef MA_HAS_WAV
    if (result != MA_SUCCESS && ma_path_extension_equal(pFilePath, "wav")) {
        result = ma_decoder_init_wav__internal(&config, pDecoder);
        if (result != MA_SUCCESS) {
            ma_decoder__on_seek_vfs(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif
#ifdef MA_HAS_FLAC
    if (result != MA_SUCCESS && ma_path_extension_equal(pFilePath, "flac")) {
        result = ma_decoder_init_flac__internal(&config, pDecoder);
        if (result != MA_SUCCESS) {
            ma_decoder__on_seek_vfs(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif
#ifdef MA_HAS_MP3
    if (result != MA_SUCCESS && ma_path_extension_equal(pFilePath, "mp3")) {
        result = ma_decoder_init_mp3__internal(&config, pDecoder);
        if (result != MA_SUCCESS) {
            ma_decoder__on_seek_vfs(pDecoder, 0, ma_seek_origin_start);
        }
    }
#endif

    /* If we still haven't got a result just use trial and error. Otherwise we can finish up. */
    if (result != MA_SUCCESS) {
        result = ma_decoder_init__internal(ma_decoder__on_read_vfs, ma_decoder__on_seek_vfs, NULL, &config, pDecoder);
    } else {
        result = ma_decoder__postinit(&config, pDecoder);
    }

    if (result != MA_SUCCESS) {
        ma_vfs_close(pVFS, pDecoder->backend.vfs.file);
        return result;
    }

    return MA_SUCCESS;
}


size_t ma_decoder_read_bytes(ma_decoder* pDecoder, void* pBufferOut, size_t bytesToRead)
{
    size_t bytesRead;

    MA_ASSERT(pDecoder != NULL);
    MA_ASSERT(pBufferOut != NULL);

    bytesRead = pDecoder->onRead(pDecoder, pBufferOut, bytesToRead);
    pDecoder->readPointer += bytesRead;

    return bytesRead;
}

const char* ma_path_file_name(const char* path)
{
    const char* fileName;

    if (path == NULL) {
        return NULL;
    }

    fileName = path;

    /* We just loop through the path until we find the last slash. */
    while (path[0] != '\0') {
        if (path[0] == '/' || path[0] == '\\') {
            fileName = path;
        }

        path += 1;
    }

    /* At this point the file name is sitting on a slash, so just move forward. */
    while (fileName[0] != '\0' && (fileName[0] == '/' || fileName[0] == '\\')) {
        fileName += 1;
    }

    return fileName;
}

void* drmp3__malloc_default(size_t sz, void* pUserData)
{
    (void)pUserData;
    return DRMP3_MALLOC(sz);
}

void* drmp3__realloc_default(void* p, size_t sz, void* pUserData)
{
    (void)pUserData;
    return DRMP3_REALLOC(p, sz);
}

void drmp3__free_default(void* p, void* pUserData)
{
    (void)pUserData;
    DRMP3_FREE(p);
}

float drmp3_mix_f32(float x, float y, float a)
{
    return x*(1-a) + y*a;
}


float drmp3_mix_f32_fast(float x, float y, float a)
{
    float r0 = (y - x);
    float r1 = r0*a;
    return x + r1;
}


drmp3_uint32 drmp3_gcf_u32(drmp3_uint32 a, drmp3_uint32 b)
{
    for (;;) {
        if (b == 0) {
            break;
        } else {
            drmp3_uint32 t = a;
            a = b;
            b = t % a;
        }
    }
    return a;
}

double drmp3_sin(double x)
{
    return sin(x);
}

double drmp3_exp(double x)
{
    return exp(x);
}
double drmp3_cos(double x)
{
    return drmp3_sin((DRMP3_PI_D*0.5) - x);
}


void ma_device_uninit__null(ma_device* pDevice)
{
    MA_ASSERT(pDevice != NULL);

    /* Keep it clean and wait for the device thread to finish before returning. */
    ma_device_do_operation__null(pDevice, MA_DEVICE_OP_KILL__NULL);

    /* At this point the loop in the device thread is as good as terminated so we can uninitialize our events. */
    ma_event_uninit(&pDevice->null_device.operationCompletionEvent);
    ma_event_uninit(&pDevice->null_device.operationEvent);
}


/* A helper for sending sample data to the client. */
void ma_device__send_frames_to_client(ma_device* pDevice, ma_uint32 frameCountInDeviceFormat, const void* pFramesInDeviceFormat)
{
    MA_ASSERT(pDevice != NULL);
    MA_ASSERT(frameCountInDeviceFormat > 0);
    MA_ASSERT(pFramesInDeviceFormat != NULL);

    if (pDevice->capture.converter.isPassthrough) {
        ma_device__on_data(pDevice, NULL, pFramesInDeviceFormat, frameCountInDeviceFormat);
    } else {
        ma_result result;
        ma_uint8 pFramesInClientFormat[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
        ma_uint64 framesInClientFormatCap = sizeof(pFramesInClientFormat) / ma_get_bytes_per_frame(pDevice->capture.format, pDevice->capture.channels);
        ma_uint64 totalDeviceFramesProcessed = 0;
        ma_uint64 totalClientFramesProcessed = 0;
        const void* pRunningFramesInDeviceFormat = pFramesInDeviceFormat;

        /* We just keep going until we've exhaused all of our input frames and cannot generate any more output frames. */
        for (;;) {
            ma_uint64 deviceFramesProcessedThisIteration;
            ma_uint64 clientFramesProcessedThisIteration;

            deviceFramesProcessedThisIteration = (frameCountInDeviceFormat - totalDeviceFramesProcessed);
            clientFramesProcessedThisIteration = framesInClientFormatCap;

            result = ma_data_converter_process_pcm_frames(&pDevice->capture.converter, pRunningFramesInDeviceFormat, &deviceFramesProcessedThisIteration, pFramesInClientFormat, &clientFramesProcessedThisIteration);
            if (result != MA_SUCCESS) {
                break;
            }

            if (clientFramesProcessedThisIteration > 0) {
                ma_device__on_data(pDevice, NULL, pFramesInClientFormat, (ma_uint32)clientFramesProcessedThisIteration);    /* Safe cast. */
            }

            pRunningFramesInDeviceFormat = ma_offset_ptr(pRunningFramesInDeviceFormat, deviceFramesProcessedThisIteration * ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels));
            totalDeviceFramesProcessed  += deviceFramesProcessedThisIteration;
            totalClientFramesProcessed  += clientFramesProcessedThisIteration;

            if (deviceFramesProcessedThisIteration == 0 && clientFramesProcessedThisIteration == 0) {
                break;  /* We're done. */
            }
        }
    }
}


MA_INLINE void ma_clip_pcm_frames_f32(float* p, ma_uint64 frameCount, ma_uint32 channels) { ma_clip_samples_f32(p, frameCount*channels); }


/* A helper function for reading sample data from the client. */
void ma_device__read_frames_from_client(ma_device* pDevice, ma_uint32 frameCount, void* pFramesOut)
{
    MA_ASSERT(pDevice != NULL);
    MA_ASSERT(frameCount > 0);
    MA_ASSERT(pFramesOut != NULL);

    if (pDevice->playback.converter.isPassthrough) {
        ma_device__on_data(pDevice, pFramesOut, NULL, frameCount);
    } else {
        ma_result result;
        ma_uint64 totalFramesReadOut;
        ma_uint64 totalFramesReadIn;
        void* pRunningFramesOut;

        totalFramesReadOut = 0;
        totalFramesReadIn  = 0;
        pRunningFramesOut  = pFramesOut;

        while (totalFramesReadOut < frameCount) {
            ma_uint8 pIntermediaryBuffer[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];  /* In client format. */
            ma_uint64 intermediaryBufferCap = sizeof(pIntermediaryBuffer) / ma_get_bytes_per_frame(pDevice->playback.format, pDevice->playback.channels);
            ma_uint64 framesToReadThisIterationIn;
            ma_uint64 framesReadThisIterationIn;
            ma_uint64 framesToReadThisIterationOut;
            ma_uint64 framesReadThisIterationOut;
            ma_uint64 requiredInputFrameCount;

            framesToReadThisIterationOut = (frameCount - totalFramesReadOut);
            framesToReadThisIterationIn = framesToReadThisIterationOut;
            if (framesToReadThisIterationIn > intermediaryBufferCap) {
                framesToReadThisIterationIn = intermediaryBufferCap;
            }

            requiredInputFrameCount = ma_data_converter_get_required_input_frame_count(&pDevice->playback.converter, framesToReadThisIterationOut);
            if (framesToReadThisIterationIn > requiredInputFrameCount) {
                framesToReadThisIterationIn = requiredInputFrameCount;
            }

            if (framesToReadThisIterationIn > 0) {
                ma_device__on_data(pDevice, pIntermediaryBuffer, NULL, (ma_uint32)framesToReadThisIterationIn);
                totalFramesReadIn += framesToReadThisIterationIn;
            }

            /*
            At this point we have our decoded data in input format and now we need to convert to output format. Note that even if we didn't read any
            input frames, we still want to try processing frames because there may some output frames generated from cached input data.
            */
            framesReadThisIterationIn  = framesToReadThisIterationIn;
            framesReadThisIterationOut = framesToReadThisIterationOut;
            result = ma_data_converter_process_pcm_frames(&pDevice->playback.converter, pIntermediaryBuffer, &framesReadThisIterationIn, pRunningFramesOut, &framesReadThisIterationOut);
            if (result != MA_SUCCESS) {
                break;
            }

            totalFramesReadOut += framesReadThisIterationOut;
            pRunningFramesOut   = ma_offset_ptr(pRunningFramesOut, framesReadThisIterationOut * ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels));

            if (framesReadThisIterationIn == 0 && framesReadThisIterationOut == 0) {
                break;  /* We're done. */
            }
        }
    }
}


ma_result ma_context_get_device_info__null(ma_context* pContext, ma_device_type deviceType, const ma_device_id* pDeviceID, ma_share_mode shareMode, ma_device_info* pDeviceInfo)
{
    ma_uint32 iFormat;

    MA_ASSERT(pContext != NULL);

    if (pDeviceID != NULL && pDeviceID->nullbackend != 0) {
        return MA_NO_DEVICE;   /* Don't know the device. */
    }

    /* Name / Description */
    if (deviceType == ma_device_type_playback) {
        ma_strncpy_s(pDeviceInfo->name, sizeof(pDeviceInfo->name), "NULL Playback Device", (size_t)-1);
    } else {
        ma_strncpy_s(pDeviceInfo->name, sizeof(pDeviceInfo->name), "NULL Capture Device", (size_t)-1);
    }

    /* Support everything on the null backend. */
    pDeviceInfo->formatCount = ma_format_count - 1;    /* Minus one because we don't want to include ma_format_unknown. */
    for (iFormat = 0; iFormat < pDeviceInfo->formatCount; ++iFormat) {
        pDeviceInfo->formats[iFormat] = (ma_format)(iFormat + 1);  /* +1 to skip over ma_format_unknown. */
    }

    pDeviceInfo->minChannels   = 1;
    pDeviceInfo->maxChannels   = MA_MAX_CHANNELS;
    pDeviceInfo->minSampleRate = MA_SAMPLE_RATE_8000;
    pDeviceInfo->maxSampleRate = MA_SAMPLE_RATE_384000;

    (void)pContext;
    (void)shareMode;
    return MA_SUCCESS;
}


/*******************************************************************************

Dynamic Linking

*******************************************************************************/

#include <dlfcn.h>

MA_API ma_handle ma_dlopen(ma_context* pContext, const char* filename)
{
    ma_handle handle;

#if MA_LOG_LEVEL >= MA_LOG_LEVEL_VERBOSE
    if (pContext != NULL) {
        char message[256];
        ma_strappend(message, sizeof(message), "Loading library: ", filename);
        ma_post_log_message(pContext, NULL, MA_LOG_LEVEL_VERBOSE, message);
    }
#endif

#ifdef _WIN32
#ifdef MA_WIN32_DESKTOP
    handle = (ma_handle)LoadLibraryA(filename);
#else
    /* *sigh* It appears there is no ANSI version of LoadPackagedLibrary()... */
    WCHAR filenameW[4096];
    if (MultiByteToWideChar(CP_UTF8, 0, filename, -1, filenameW, sizeof(filenameW)) == 0) {
        handle = NULL;
    } else {
        handle = (ma_handle)LoadPackagedLibrary(filenameW, 0);
    }
#endif
#else
    handle = (ma_handle)dlopen(filename, RTLD_NOW);
#endif

    /*
    I'm not considering failure to load a library an error nor a warning because seamlessly falling through to a lower-priority
    backend is a deliberate design choice. Instead I'm logging it as an informational message.
    */
#if MA_LOG_LEVEL >= MA_LOG_LEVEL_INFO
    if (handle == NULL) {
        char message[256];
        ma_strappend(message, sizeof(message), "Failed to load library: ", filename);
        ma_post_log_message(pContext, NULL, MA_LOG_LEVEL_INFO, message);
    }
#endif

    (void)pContext; /* It's possible for pContext to be unused. */
    return handle;
}

MA_API void ma_dlclose(ma_context* pContext, ma_handle handle)
{
#ifdef _WIN32
    FreeLibrary((HMODULE)handle);
#else
    dlclose((void*)handle);
#endif

    (void)pContext;
}

MA_API ma_proc ma_dlsym(ma_context* pContext, ma_handle handle, const char* symbol)
{
    ma_proc proc;

#if MA_LOG_LEVEL >= MA_LOG_LEVEL_VERBOSE
    if (pContext != NULL) {
        char message[256];
        ma_strappend(message, sizeof(message), "Loading symbol: ", symbol);
        ma_post_log_message(pContext, NULL, MA_LOG_LEVEL_VERBOSE, message);
    }
#endif

#ifdef _WIN32
    proc = (ma_proc)GetProcAddress((HMODULE)handle, symbol);
#else
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wpedantic"
#endif
    proc = (ma_proc)dlsym((void*)handle, symbol);
#if defined(__GNUC__) && (__GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ >= 6))
    #pragma GCC diagnostic pop
#endif
#endif

#if MA_LOG_LEVEL >= MA_LOG_LEVEL_WARNING
    if (handle == NULL) {
        char message[256];
        ma_strappend(message, sizeof(message), "Failed to load symbol: ", symbol);
        ma_post_log_message(pContext, NULL, MA_LOG_LEVEL_WARNING, message);
    }
#endif

    (void)pContext; /* It's possible for pContext to be unused. */
    return proc;
}

ma_bool32 ma_is_little_endian(void)
{
#if defined(MA_X86) || defined(MA_X64)
    return MA_TRUE;
#else
    int n = 1;
    return (*(char*)&n) == 1;
#endif
}

MA_INLINE ma_bool32 ma_is_big_endian(void)
{
    return !ma_is_little_endian();
}


MA_INLINE ma_uint32 ma_swap_endian_uint32(ma_uint32 n)
{
#ifdef MA_HAS_BYTESWAP32_INTRINSIC
    #if defined(_MSC_VER)
        return _byteswap_ulong(n);
    #elif defined(__GNUC__) || defined(__clang__)
        #if defined(MA_ARM) && (defined(__ARM_ARCH) && __ARM_ARCH >= 6) && !defined(MA_64BIT)   /* <-- 64-bit inline assembly has not been tested, so disabling for now. */
            /* Inline assembly optimized implementation for ARM. In my testing, GCC does not generate optimized code with __builtin_bswap32(). */
            ma_uint32 r;
            __asm__ __volatile__ (
            #if defined(MA_64BIT)
                "rev %w[out], %w[in]" : [out]"=r"(r) : [in]"r"(n)   /* <-- This is untested. If someone in the community could test this, that would be appreciated! */
            #else
                "rev %[out], %[in]" : [out]"=r"(r) : [in]"r"(n)
            #endif
            );
            return r;
        #else
            return __builtin_bswap32(n);
        #endif
    #else
        #error "This compiler does not support the byte swap intrinsic."
    #endif
#else
    return ((n & 0xFF000000) >> 24) |
           ((n & 0x00FF0000) >>  8) |
           ((n & 0x0000FF00) <<  8) |
           ((n & 0x000000FF) << 24);
#endif
}

#include "pulsedjg.h"

ma_result ma_context_init__pulse(const ma_context_config* pConfig, ma_context* pContext)
{
#ifndef MA_NO_RUNTIME_LINKING
    const char* libpulseNames[] = {
        "libpulse.so",
        "libpulse.so.0"
    };
    size_t i;

    for (i = 0; i < ma_countof(libpulseNames); ++i) {
        pContext->pulse.pulseSO = ma_dlopen(pContext, libpulseNames[i]);
        if (pContext->pulse.pulseSO != NULL) {
            break;
        }
    }

    if (pContext->pulse.pulseSO == NULL) {
        return MA_NO_BACKEND;
    }

    pContext->pulse.pa_mainloop_new                    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_mainloop_new");
    pContext->pulse.pa_mainloop_free                   = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_mainloop_free");
    pContext->pulse.pa_mainloop_get_api                = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_mainloop_get_api");
    pContext->pulse.pa_mainloop_iterate                = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_mainloop_iterate");
    pContext->pulse.pa_mainloop_wakeup                 = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_mainloop_wakeup");
    pContext->pulse.pa_context_new                     = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_new");
    pContext->pulse.pa_context_unref                   = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_unref");
    pContext->pulse.pa_context_connect                 = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_connect");
    pContext->pulse.pa_context_disconnect              = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_disconnect");
    pContext->pulse.pa_context_set_state_callback      = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_set_state_callback");
    pContext->pulse.pa_context_get_state               = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_get_state");
    pContext->pulse.pa_context_get_sink_info_list      = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_get_sink_info_list");
    pContext->pulse.pa_context_get_source_info_list    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_get_source_info_list");
    pContext->pulse.pa_context_get_sink_info_by_name   = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_get_sink_info_by_name");
    pContext->pulse.pa_context_get_source_info_by_name = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_context_get_source_info_by_name");
    pContext->pulse.pa_operation_unref                 = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_operation_unref");
    pContext->pulse.pa_operation_get_state             = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_operation_get_state");
    pContext->pulse.pa_channel_map_init_extend         = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_channel_map_init_extend");
    pContext->pulse.pa_channel_map_valid               = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_channel_map_valid");
    pContext->pulse.pa_channel_map_compatible          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_channel_map_compatible");
    pContext->pulse.pa_stream_new                      = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_new");
    pContext->pulse.pa_stream_unref                    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_unref");
    pContext->pulse.pa_stream_connect_playback         = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_connect_playback");
    pContext->pulse.pa_stream_connect_record           = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_connect_record");
    pContext->pulse.pa_stream_disconnect               = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_disconnect");
    pContext->pulse.pa_stream_get_state                = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_get_state");
    pContext->pulse.pa_stream_get_sample_spec          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_get_sample_spec");
    pContext->pulse.pa_stream_get_channel_map          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_get_channel_map");
    pContext->pulse.pa_stream_get_buffer_attr          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_get_buffer_attr");
    pContext->pulse.pa_stream_set_buffer_attr          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_set_buffer_attr");
    pContext->pulse.pa_stream_get_device_name          = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_get_device_name");
    pContext->pulse.pa_stream_set_write_callback       = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_set_write_callback");
    pContext->pulse.pa_stream_set_read_callback        = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_set_read_callback");
    pContext->pulse.pa_stream_flush                    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_flush");
    pContext->pulse.pa_stream_drain                    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_drain");
    pContext->pulse.pa_stream_is_corked                = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_is_corked");
    pContext->pulse.pa_stream_cork                     = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_cork");
    pContext->pulse.pa_stream_trigger                  = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_trigger");
    pContext->pulse.pa_stream_begin_write              = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_begin_write");
    pContext->pulse.pa_stream_write                    = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_write");
    pContext->pulse.pa_stream_peek                     = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_peek");
    pContext->pulse.pa_stream_drop                     = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_drop");
    pContext->pulse.pa_stream_writable_size            = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_writable_size");
    pContext->pulse.pa_stream_readable_size            = (ma_proc)ma_dlsym(pContext, pContext->pulse.pulseSO, "pa_stream_readable_size");
#else
    /* This strange assignment system is just for type safety. */
    ma_pa_mainloop_new_proc                    _pa_mainloop_new                   = pa_mainloop_new;
    ma_pa_mainloop_free_proc                   _pa_mainloop_free                  = pa_mainloop_free;
    ma_pa_mainloop_get_api_proc                _pa_mainloop_get_api               = pa_mainloop_get_api;
    ma_pa_mainloop_iterate_proc                _pa_mainloop_iterate               = pa_mainloop_iterate;
    ma_pa_mainloop_wakeup_proc                 _pa_mainloop_wakeup                = pa_mainloop_wakeup;
    ma_pa_context_new_proc                     _pa_context_new                    = pa_context_new;
    ma_pa_context_unref_proc                   _pa_context_unref                  = pa_context_unref;
    ma_pa_context_connect_proc                 _pa_context_connect                = pa_context_connect;
    ma_pa_context_disconnect_proc              _pa_context_disconnect             = pa_context_disconnect;
    ma_pa_context_set_state_callback_proc      _pa_context_set_state_callback     = pa_context_set_state_callback;
    ma_pa_context_get_state_proc               _pa_context_get_state              = pa_context_get_state;
    ma_pa_context_get_sink_info_list_proc      _pa_context_get_sink_info_list     = pa_context_get_sink_info_list;
    ma_pa_context_get_source_info_list_proc    _pa_context_get_source_info_list   = pa_context_get_source_info_list;
    ma_pa_context_get_sink_info_by_name_proc   _pa_context_get_sink_info_by_name  = pa_context_get_sink_info_by_name;
    ma_pa_context_get_source_info_by_name_proc _pa_context_get_source_info_by_name= pa_context_get_source_info_by_name;
    ma_pa_operation_unref_proc                 _pa_operation_unref                = pa_operation_unref;
    ma_pa_operation_get_state_proc             _pa_operation_get_state            = pa_operation_get_state;
    ma_pa_channel_map_init_extend_proc         _pa_channel_map_init_extend        = pa_channel_map_init_extend;
    ma_pa_channel_map_valid_proc               _pa_channel_map_valid              = pa_channel_map_valid;
    ma_pa_channel_map_compatible_proc          _pa_channel_map_compatible         = pa_channel_map_compatible;
    ma_pa_stream_new_proc                      _pa_stream_new                     = pa_stream_new;
    ma_pa_stream_unref_proc                    _pa_stream_unref                   = pa_stream_unref;
    ma_pa_stream_connect_playback_proc         _pa_stream_connect_playback        = pa_stream_connect_playback;
    ma_pa_stream_connect_record_proc           _pa_stream_connect_record          = pa_stream_connect_record;
    ma_pa_stream_disconnect_proc               _pa_stream_disconnect              = pa_stream_disconnect;
    ma_pa_stream_get_state_proc                _pa_stream_get_state               = pa_stream_get_state;
    ma_pa_stream_get_sample_spec_proc          _pa_stream_get_sample_spec         = pa_stream_get_sample_spec;
    ma_pa_stream_get_channel_map_proc          _pa_stream_get_channel_map         = pa_stream_get_channel_map;
    ma_pa_stream_get_buffer_attr_proc          _pa_stream_get_buffer_attr         = pa_stream_get_buffer_attr;
    ma_pa_stream_set_buffer_attr_proc          _pa_stream_set_buffer_attr         = pa_stream_set_buffer_attr;
    ma_pa_stream_get_device_name_proc          _pa_stream_get_device_name         = pa_stream_get_device_name;
    ma_pa_stream_set_write_callback_proc       _pa_stream_set_write_callback      = pa_stream_set_write_callback;
    ma_pa_stream_set_read_callback_proc        _pa_stream_set_read_callback       = pa_stream_set_read_callback;
    ma_pa_stream_flush_proc                    _pa_stream_flush                   = pa_stream_flush;
    ma_pa_stream_drain_proc                    _pa_stream_drain                   = pa_stream_drain;
    ma_pa_stream_is_corked_proc                _pa_stream_is_corked               = pa_stream_is_corked;
    ma_pa_stream_cork_proc                     _pa_stream_cork                    = pa_stream_cork;
    ma_pa_stream_trigger_proc                  _pa_stream_trigger                 = pa_stream_trigger;
    ma_pa_stream_begin_write_proc              _pa_stream_begin_write             = pa_stream_begin_write;
    ma_pa_stream_write_proc                    _pa_stream_write                   = pa_stream_write;
    ma_pa_stream_peek_proc                     _pa_stream_peek                    = pa_stream_peek;
    ma_pa_stream_drop_proc                     _pa_stream_drop                    = pa_stream_drop;
    ma_pa_stream_writable_size_proc            _pa_stream_writable_size           = pa_stream_writable_size;
    ma_pa_stream_readable_size_proc            _pa_stream_readable_size           = pa_stream_readable_size;

    pContext->pulse.pa_mainloop_new                    = (ma_proc)_pa_mainloop_new;
    pContext->pulse.pa_mainloop_free                   = (ma_proc)_pa_mainloop_free;
    pContext->pulse.pa_mainloop_get_api                = (ma_proc)_pa_mainloop_get_api;
    pContext->pulse.pa_mainloop_iterate                = (ma_proc)_pa_mainloop_iterate;
    pContext->pulse.pa_mainloop_wakeup                 = (ma_proc)_pa_mainloop_wakeup;
    pContext->pulse.pa_context_new                     = (ma_proc)_pa_context_new;
    pContext->pulse.pa_context_unref                   = (ma_proc)_pa_context_unref;
    pContext->pulse.pa_context_connect                 = (ma_proc)_pa_context_connect;
    pContext->pulse.pa_context_disconnect              = (ma_proc)_pa_context_disconnect;
    pContext->pulse.pa_context_set_state_callback      = (ma_proc)_pa_context_set_state_callback;
    pContext->pulse.pa_context_get_state               = (ma_proc)_pa_context_get_state;
    pContext->pulse.pa_context_get_sink_info_list      = (ma_proc)_pa_context_get_sink_info_list;
    pContext->pulse.pa_context_get_source_info_list    = (ma_proc)_pa_context_get_source_info_list;
    pContext->pulse.pa_context_get_sink_info_by_name   = (ma_proc)_pa_context_get_sink_info_by_name;
    pContext->pulse.pa_context_get_source_info_by_name = (ma_proc)_pa_context_get_source_info_by_name;
    pContext->pulse.pa_operation_unref                 = (ma_proc)_pa_operation_unref;
    pContext->pulse.pa_operation_get_state             = (ma_proc)_pa_operation_get_state;
    pContext->pulse.pa_channel_map_init_extend         = (ma_proc)_pa_channel_map_init_extend;
    pContext->pulse.pa_channel_map_valid               = (ma_proc)_pa_channel_map_valid;
    pContext->pulse.pa_channel_map_compatible          = (ma_proc)_pa_channel_map_compatible;
    pContext->pulse.pa_stream_new                      = (ma_proc)_pa_stream_new;
    pContext->pulse.pa_stream_unref                    = (ma_proc)_pa_stream_unref;
    pContext->pulse.pa_stream_connect_playback         = (ma_proc)_pa_stream_connect_playback;
    pContext->pulse.pa_stream_connect_record           = (ma_proc)_pa_stream_connect_record;
    pContext->pulse.pa_stream_disconnect               = (ma_proc)_pa_stream_disconnect;
    pContext->pulse.pa_stream_get_state                = (ma_proc)_pa_stream_get_state;
    pContext->pulse.pa_stream_get_sample_spec          = (ma_proc)_pa_stream_get_sample_spec;
    pContext->pulse.pa_stream_get_channel_map          = (ma_proc)_pa_stream_get_channel_map;
    pContext->pulse.pa_stream_get_buffer_attr          = (ma_proc)_pa_stream_get_buffer_attr;
    pContext->pulse.pa_stream_set_buffer_attr          = (ma_proc)_pa_stream_set_buffer_attr;
    pContext->pulse.pa_stream_get_device_name          = (ma_proc)_pa_stream_get_device_name;
    pContext->pulse.pa_stream_set_write_callback       = (ma_proc)_pa_stream_set_write_callback;
    pContext->pulse.pa_stream_set_read_callback        = (ma_proc)_pa_stream_set_read_callback;
    pContext->pulse.pa_stream_flush                    = (ma_proc)_pa_stream_flush;
    pContext->pulse.pa_stream_drain                    = (ma_proc)_pa_stream_drain;
    pContext->pulse.pa_stream_is_corked                = (ma_proc)_pa_stream_is_corked;
    pContext->pulse.pa_stream_cork                     = (ma_proc)_pa_stream_cork;
    pContext->pulse.pa_stream_trigger                  = (ma_proc)_pa_stream_trigger;
    pContext->pulse.pa_stream_begin_write              = (ma_proc)_pa_stream_begin_write;
    pContext->pulse.pa_stream_write                    = (ma_proc)_pa_stream_write;
    pContext->pulse.pa_stream_peek                     = (ma_proc)_pa_stream_peek;
    pContext->pulse.pa_stream_drop                     = (ma_proc)_pa_stream_drop;
    pContext->pulse.pa_stream_writable_size            = (ma_proc)_pa_stream_writable_size;
    pContext->pulse.pa_stream_readable_size            = (ma_proc)_pa_stream_readable_size;
#endif

    pContext->onUninit         = ma_context_uninit__pulse;
    pContext->onDeviceIDEqual  = ma_context_is_device_id_equal__pulse;
    pContext->onEnumDevices    = ma_context_enumerate_devices__pulse;
    pContext->onGetDeviceInfo  = ma_context_get_device_info__pulse;
    pContext->onDeviceInit     = ma_device_init__pulse;
    pContext->onDeviceUninit   = ma_device_uninit__pulse;
    pContext->onDeviceStart    = NULL;
    pContext->onDeviceStop     = NULL;
    pContext->onDeviceMainLoop = ma_device_main_loop__pulse;

    if (pConfig->pulse.pApplicationName) {
        pContext->pulse.pApplicationName = ma_copy_string(pConfig->pulse.pApplicationName, &pContext->allocationCallbacks);
    }
    if (pConfig->pulse.pServerName) {
        pContext->pulse.pServerName = ma_copy_string(pConfig->pulse.pServerName, &pContext->allocationCallbacks);
    }
    pContext->pulse.tryAutoSpawn = pConfig->pulse.tryAutoSpawn;
    
    /*
    Although we have found the libpulse library, it doesn't necessarily mean PulseAudio is useable. We need to initialize
    and connect a dummy PulseAudio context to test PulseAudio's usability.
    */
    {
        ma_pa_mainloop* pMainLoop;
        ma_pa_mainloop_api* pAPI;
        ma_pa_context* pPulseContext;
        int error;

        pMainLoop = ((ma_pa_mainloop_new_proc)pContext->pulse.pa_mainloop_new)();
        if (pMainLoop == NULL) {
            ma_free(pContext->pulse.pServerName, &pContext->allocationCallbacks);
            ma_free(pContext->pulse.pApplicationName, &pContext->allocationCallbacks);
        #ifndef MA_NO_RUNTIME_LINKING
            ma_dlclose(pContext, pContext->pulse.pulseSO);
        #endif
            return MA_NO_BACKEND;
        }

        pAPI = ((ma_pa_mainloop_get_api_proc)pContext->pulse.pa_mainloop_get_api)(pMainLoop);
        if (pAPI == NULL) {
            ma_free(pContext->pulse.pServerName, &pContext->allocationCallbacks);
            ma_free(pContext->pulse.pApplicationName, &pContext->allocationCallbacks);
            ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        #ifndef MA_NO_RUNTIME_LINKING
            ma_dlclose(pContext, pContext->pulse.pulseSO);
        #endif
            return MA_NO_BACKEND;
        }

        pPulseContext = ((ma_pa_context_new_proc)pContext->pulse.pa_context_new)(pAPI, pContext->pulse.pApplicationName);
        if (pPulseContext == NULL) {
            ma_free(pContext->pulse.pServerName, &pContext->allocationCallbacks);
            ma_free(pContext->pulse.pApplicationName, &pContext->allocationCallbacks);
            ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        #ifndef MA_NO_RUNTIME_LINKING
            ma_dlclose(pContext, pContext->pulse.pulseSO);
        #endif
            return MA_NO_BACKEND;
        }

        error = ((ma_pa_context_connect_proc)pContext->pulse.pa_context_connect)(pPulseContext, pContext->pulse.pServerName, 0, NULL);
        if (error != MA_PA_OK) {
            ma_free(pContext->pulse.pServerName, &pContext->allocationCallbacks);
            ma_free(pContext->pulse.pApplicationName, &pContext->allocationCallbacks);
            ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
            ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        #ifndef MA_NO_RUNTIME_LINKING
            ma_dlclose(pContext, pContext->pulse.pulseSO);
        #endif
            return MA_NO_BACKEND;
        }

        ((ma_pa_context_disconnect_proc)pContext->pulse.pa_context_disconnect)(pPulseContext);
        ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
    }

    return MA_SUCCESS;
}



// eof
