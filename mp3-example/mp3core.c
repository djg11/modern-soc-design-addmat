/*
#
# Contents for the mp3-example additional material folder for "Modern SoC Design, First Edition" by DJ Greaves. 2021.
#
# LICENSE NOTICE:
# No waranties are implied or expressed.  The original material in this folder is taken from https://github.com/dr-soft/miniaudio
# which was released with two alternative licenses: 1 - Public Domain (www.unlicense.org) and 2 - MIT No Attribution.
# The additional Makefile, djgmain.c and ancillary instrumentation files are licensed to you by DJ Greaves under the
# same conditions.
#
*/

// callgraph  layers 1 and 2 OR layer 3.
/*
decode_frame->L12_dequantize_granule(float *grbuf, drmp3_bs *bs, drmp3_L12_scale_info *sci, int group_size)
decode_frame->L12_apply_scf_384(sci, sci->scf + igr, scratch.grbuf[0]);
decode_frame->L3_decode
decode_frame->L3_decode->drmp3_L3_decode_scalefactors
decode_frame->L3_decode->drmp3_L3_huffman
decode_frame->L3_decode->drmp3_L3_intensity_stereo
decode_frame->L3_decode->drmp3_L3_reorder
decode_frame->L3_decode->drmp3_L3_antialias(s->grbuf[ch], aa_bands);
// The two main control flows within L3.
decode_frame->L3_decode->L3_imdct_gr->L3_imdct_short->drmp3_L3_imdct12->L3_idct3(float x0, float x1, float x2, float *dst)
decode_frame->L3_decode->L3_imdct_gr->drmp3_L3_imdct36
decode_frame->L3_decode->drmp3_L3_change_sign(s->grbuf[ch]);
// Then synth.
decode_frame->synth_granule(dec->qmf_state, scratch.grbuf[0], 12, info->channels, (drmp3d_sample_t*)pcm, scratch.syn[0]);
decode_frame->synth_granule->drmp3d_DCT_II
decode_frame->synth_granule->drmp3d_synth
decode_frame->synth_granule->drmp3d_synth->drmp3d_synth_pair


*/

#include <stdio.h>
#include <time.h>
#include "mytop.h"
#include <assert.h>
#include <limits.h> /* For INT_MAX */

#include "mp3bimp.h"
#include "c89atomic.h"
#include "dither.h"
#include "formats.h"

int n_FDIV = 0;
int n_FCONV = 0;

void FDIV(int line, int no)
{
  n_FDIV += 1;
}

void FCONV(int line, int no)
{
  n_FCONV += 1;
}

// Profile counts added by DJG.
// Subroutine call counts
int n_frames = 0;
int n_drmp3d = 0;

// ALU counts for integer (excluding loop control/array subscription).
int n_IADD = 0;
int n_ITIMES = 0;

// ALU counts for floating-point.
int n_FADD = 0;
int n_FTIMES = 0;  



void IADD(int line, int no)  // Integer addition/subtraction.
{
  n_IADD += 1;
}

void ITIMES(int line, int no) // Integer multiplication.
{
  n_ITIMES += 1;
}


void FADD(int line, int no)
{
  n_FADD += 1;
}

void FTIMES(int line, int no)
{
  n_FTIMES += 1;
}


void timestamp()
{
  time_t now;
  clock_t milli;
  int waitMillSec = 2800, seconds, milliseconds = 0;
  struct tm * ptm;
  
  now = time(NULL);
  ptm = gmtime ( &now );
  printf("%d:%d:%d ", ptm->tm_min,ptm->tm_sec, milliseconds);
}


void djg_stats()
{
  static int p;
  if (p == 0)
    {
      timestamp();
      printf("iadd %i, imul %i, fadd %i, fmul %i, fconv %i, fdiv %i, frames %i, n_mp3d=%i\n", n_IADD, n_ITIMES,  n_FADD, n_FTIMES, n_FDIV, n_FCONV, n_frames, n_drmp3d);
    }
  p = (p+1)%32;
}

typedef struct
{
    float scf[3*64];
    drmp3_uint8 total_bands, stereo_bands, bitalloc[64], scfcod[64];
} drmp3_L12_scale_info;
typedef struct
{
    drmp3_uint8 tab_offset, code_tab_width, band_count;
} drmp3_L12_subband_alloc;

typedef struct
{
    const drmp3_uint8 *sfbtab;
    drmp3_uint16 part_23_length, big_values, scalefac_compress;
    drmp3_uint8 global_gain, block_type, mixed_block_flag, n_long_sfb, n_short_sfb;
    drmp3_uint8 table_select[3], region_count[3], subblock_gain[3];
    drmp3_uint8 preflag, scalefac_scale, count1_table, scfsi;
} drmp3_L3_gr_info;


typedef struct
{
    const drmp3_uint8 *buf;
    int pos, limit;
} drmp3_bs;

typedef struct
{
    drmp3_bs bs;
    drmp3_uint8 maindata[DRMP3_MAX_BITRESERVOIR_BYTES + DRMP3_MAX_L3_FRAME_PAYLOAD_BYTES];
    drmp3_L3_gr_info gr_info[4];
    float grbuf[2][576], scf[40], syn[18 + 15][2*32];
    drmp3_uint8 ist_pos[2][39];
} drmp3dec_scratch;


drmp3_uint32 drmp3_bs_get_bits(drmp3_bs *bs, int n);
void drmp3_bs_init(drmp3_bs *bs, const drmp3_uint8 *data, int bytes);
int drmp3_L3_read_side_info(drmp3_bs *bs, drmp3_L3_gr_info *gr, const drmp3_uint8 *hdr);
MA_API ma_uint64 ma_decoder_read_pcm_frames(ma_decoder* pDecoder, void* pFramesOut, ma_uint64 frameCount);

static void drmp3_L3_save_reservoir(drmp3dec *h, drmp3dec_scratch *s)
{
    int pos = (s->bs.pos + 7)/8u;
    int remains = s->bs.limit/8u - pos;
    if (remains > DRMP3_MAX_BITRESERVOIR_BYTES)
    {
        pos += remains - DRMP3_MAX_BITRESERVOIR_BYTES;
        remains = DRMP3_MAX_BITRESERVOIR_BYTES;
    }
    if (remains > 0)
    {
        memmove(h->reserv_buf, s->maindata + pos, remains);
    }
    h->reserv = remains;
}


static int drmp3_L3_restore_reservoir(drmp3dec *h, drmp3_bs *bs, drmp3dec_scratch *s, int main_data_begin)
{
    int frame_bytes = (bs->limit - bs->pos)/8;
    int bytes_have = DRMP3_MIN(h->reserv, main_data_begin);
    memcpy(s->maindata, h->reserv_buf + DRMP3_MAX(0, h->reserv - main_data_begin), DRMP3_MIN(h->reserv, main_data_begin));
    memcpy(s->maindata + bytes_have, bs->buf + bs->pos/8, frame_bytes);
    drmp3_bs_init(&s->bs, s->maindata, bytes_have + frame_bytes);
    return h->reserv >= main_data_begin;
}



MA_INLINE double ma_mix_f64(double x, double y, double a)
{
    return x*(1-a) + y*a;
}

MA_INLINE double ma_mix_f64_fast(double x, double y, double a)
{
    return x + (y - x)*a;
}

MA_INLINE float ma_scale_to_range_f32(float x, float lo, float hi)
{
    return lo + x*(hi-lo);
}

static void drmp3_L3_read_scalefactors(drmp3_uint8 *scf, drmp3_uint8 *ist_pos, const drmp3_uint8 *scf_size, const drmp3_uint8 *scf_count, drmp3_bs *bitbuf, int scfsi)
{
    int i, k;
    for (i = 0; i < 4 && scf_count[i]; i++, scfsi *= 2)
    {
        int cnt = scf_count[i];
        if (scfsi & 8)
        {
            memcpy(scf, ist_pos, cnt);
        } else
        {
            int bits = scf_size[i];
            if (!bits)
            {
                memset(scf, 0, cnt);
                memset(ist_pos, 0, cnt);
            } else
            {
                int max_scf = (scfsi < 0) ? (1 << bits) - 1 : -1;
                for (k = 0; k < cnt; k++)
                {
                    int s = drmp3_bs_get_bits(bitbuf, bits);
                    ist_pos[k] = (drmp3_uint8)(s == max_scf ? -1 : s);
                    scf[k] = (drmp3_uint8)s;
                }
            }
        }
	IADD(__LINE__, 2);
        ist_pos += cnt;
        scf += cnt;
    }
    scf[0] = scf[1] = scf[2] = 0;
}

static float drmp3_L3_ldexp_q2(float y, int exp_q2)
{
  static const float g_expfrac[4] = { 9.31322575e-10f,7.83145814e-10f,6.58544508e-10f,5.53767716e-10f };
    int e;
    do
    {
        e = DRMP3_MIN(30*4, exp_q2);
        y *= g_expfrac[e & 3]*(1 << 30 >> (e >> 2));
	FTIMES(__LINE__, 1);
	IADD(__LINE__, 1);
    } while ((exp_q2 -= e) > 0);
    return y;
}



static void drmp3_L3_decode_scalefactors(const drmp3_uint8 *hdr, drmp3_uint8 *ist_pos, drmp3_bs *bs, const drmp3_L3_gr_info *gr, float *scf, int ch)
{

  static const drmp3_uint8 g_scf_partitions[3][28] = {
        { 6,5,5, 5,6,5,5,5,6,5, 7,3,11,10,0,0, 7, 7, 7,0, 6, 6,6,3, 8, 8,5,0 },
        { 8,9,6,12,6,9,9,9,6,9,12,6,15,18,0,0, 6,15,12,0, 6,12,9,6, 6,18,9,0 },
        { 9,9,6,12,9,9,9,9,9,9,12,6,18,18,0,0,12,12,12,0,12, 9,9,6,15,12,9,0 }
    };
    const drmp3_uint8 *scf_partition = g_scf_partitions[!!gr->n_short_sfb + !gr->n_long_sfb];
    drmp3_uint8 scf_size[4], iscf[40];
    int i, scf_shift = gr->scalefac_scale + 1, gain_exp, scfsi = gr->scfsi;
    float gain;
    if (DRMP3_HDR_TEST_MPEG1(hdr))
    {
        static const drmp3_uint8 g_scfc_decode[16] = { 0,1,2,3, 12,5,6,7, 9,10,11,13, 14,15,18,19 };
        int part = g_scfc_decode[gr->scalefac_compress];
        scf_size[1] = scf_size[0] = (drmp3_uint8)(part >> 2);
        scf_size[3] = scf_size[2] = (drmp3_uint8)(part & 3);
    } else
    {
        static const drmp3_uint8 g_mod[6*4] = { 5,5,4,4,5,5,4,1,4,3,1,1,5,6,6,1,4,4,4,1,4,3,1,1 };
        int k, modprod, sfc, ist = DRMP3_HDR_TEST_I_STEREO(hdr) && ch;
        sfc = gr->scalefac_compress >> ist;
        for (k = ist*3*4; sfc >= 0; sfc -= modprod, k += 4)
        {
            for (modprod = 1, i = 3; i >= 0; i--)
            {
                scf_size[i] = (drmp3_uint8)(sfc / modprod % g_mod[k + i]);
                modprod *= g_mod[k + i];
		ITIMES(__LINE__, 1);
            }
        }
        scf_partition += k;
        scfsi = -16;
    }
    drmp3_L3_read_scalefactors(iscf, ist_pos, scf_size, scf_partition, bs, scfsi);
    if (gr->n_short_sfb)
    {
        int sh = 3 - scf_shift;
        for (i = 0; i < gr->n_short_sfb; i += 3)
        {
            iscf[gr->n_long_sfb + i + 0] = (drmp3_uint8)(iscf[gr->n_long_sfb + i + 0] + (gr->subblock_gain[0] << sh));
            iscf[gr->n_long_sfb + i + 1] = (drmp3_uint8)(iscf[gr->n_long_sfb + i + 1] + (gr->subblock_gain[1] << sh));
            iscf[gr->n_long_sfb + i + 2] = (drmp3_uint8)(iscf[gr->n_long_sfb + i + 2] + (gr->subblock_gain[2] << sh));
        }
    } else if (gr->preflag)
    {
        static const drmp3_uint8 g_preamp[10] = { 1,1,1,1,2,2,3,3,3,2 };
        for (i = 0; i < 10; i++)
        {
	  IADD(__LINE__, 1);
	  iscf[11 + i] = (drmp3_uint8)(iscf[11 + i] + g_preamp[i]);
        }
    }
    gain_exp = gr->global_gain + DRMP3_BITS_DEQUANTIZER_OUT*4 - 210 - (DRMP3_HDR_IS_MS_STEREO(hdr) ? 2 : 0);
    gain = drmp3_L3_ldexp_q2(1 << (DRMP3_MAX_SCFI/4),  DRMP3_MAX_SCFI - gain_exp);
    for (i = 0; i < (int)(gr->n_long_sfb + gr->n_short_sfb); i++)
    {
        scf[i] = drmp3_L3_ldexp_q2(gain, iscf[i] << scf_shift);
    }
}


static const float g_drmp3_pow43[129 + 16] = {
    0,-1,-2.519842f,-4.326749f,-6.349604f,-8.549880f,-10.902724f,-13.390518f,-16.000000f,-18.720754f,-21.544347f,-24.463781f,-27.473142f,-30.567351f,-33.741992f,-36.993181f,
    0,1,2.519842f,4.326749f,6.349604f,8.549880f,10.902724f,13.390518f,16.000000f,18.720754f,21.544347f,24.463781f,27.473142f,30.567351f,33.741992f,36.993181f,40.317474f,43.711787f,47.173345f,50.699631f,54.288352f,57.937408f,61.644865f,65.408941f,69.227979f,73.100443f,77.024898f,81.000000f,85.024491f,89.097188f,93.216975f,97.382800f,101.593667f,105.848633f,110.146801f,114.487321f,118.869381f,123.292209f,127.755065f,132.257246f,136.798076f,141.376907f,145.993119f,150.646117f,155.335327f,160.060199f,164.820202f,169.614826f,174.443577f,179.305980f,184.201575f,189.129918f,194.090580f,199.083145f,204.107210f,209.162385f,214.248292f,219.364564f,224.510845f,229.686789f,234.892058f,240.126328f,245.389280f,250.680604f,256.000000f,261.347174f,266.721841f,272.123723f,277.552547f,283.008049f,288.489971f,293.998060f,299.532071f,305.091761f,310.676898f,316.287249f,321.922592f,327.582707f,333.267377f,338.976394f,344.709550f,350.466646f,356.247482f,362.051866f,367.879608f,373.730522f,379.604427f,385.501143f,391.420496f,397.362314f,403.326427f,409.312672f,415.320884f,421.350905f,427.402579f,433.475750f,439.570269f,445.685987f,451.822757f,457.980436f,464.158883f,470.357960f,476.577530f,482.817459f,489.077615f,495.357868f,501.658090f,507.978156f,514.317941f,520.677324f,527.056184f,533.454404f,539.871867f,546.308458f,552.764065f,559.238575f,565.731879f,572.243870f,578.774440f,585.323483f,591.890898f,598.476581f,605.080431f,611.702349f,618.342238f,625.000000f,631.675540f,638.368763f,645.079578f
};



static float drmp3_L3_pow_43(int x)
{

  float frac;
    int sign, mult = 256;
    if (x < 129)
    {
      IADD(__LINE__, 1);
      return g_drmp3_pow43[16 + x];  
    }
    if (x < 1024)
    {
        mult = 16;
        x <<= 3;
    }
    sign = 2*x & 64;  IADD(__LINE__, 1);
    frac = (float)((x & 63) - sign) / ((x & ~63) + sign);   FDIV(__LINE__, 1); FCONV(__LINE__, 2);
    ITIMES(__LINE__, 3); IADD(__LINE__, 3);
    return g_drmp3_pow43[16 + ((x + sign) >> 6)]*(1.f + frac*((4.f/3) + frac*(2.f/9)))*mult;   
}

static void drmp3_L3_huffman(float *dst, drmp3_bs *bs, const drmp3_L3_gr_info *gr_info, const float *scf, int layer3gr_limit)
{
  static const drmp3_int16 tabs[] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        785,785,785,785,784,784,784,784,513,513,513,513,513,513,513,513,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,
        -255,1313,1298,1282,785,785,785,785,784,784,784,784,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,290,288,
        -255,1313,1298,1282,769,769,769,769,529,529,529,529,529,529,529,529,528,528,528,528,528,528,528,528,512,512,512,512,512,512,512,512,290,288,
        -253,-318,-351,-367,785,785,785,785,784,784,784,784,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,819,818,547,547,275,275,275,275,561,560,515,546,289,274,288,258,
        -254,-287,1329,1299,1314,1312,1057,1057,1042,1042,1026,1026,784,784,784,784,529,529,529,529,529,529,529,529,769,769,769,769,768,768,768,768,563,560,306,306,291,259,
        -252,-413,-477,-542,1298,-575,1041,1041,784,784,784,784,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,-383,-399,1107,1092,1106,1061,849,849,789,789,1104,1091,773,773,1076,1075,341,340,325,309,834,804,577,577,532,532,516,516,832,818,803,816,561,561,531,531,515,546,289,289,288,258,
        -252,-429,-493,-559,1057,1057,1042,1042,529,529,529,529,529,529,529,529,784,784,784,784,769,769,769,769,512,512,512,512,512,512,512,512,-382,1077,-415,1106,1061,1104,849,849,789,789,1091,1076,1029,1075,834,834,597,581,340,340,339,324,804,833,532,532,832,772,818,803,817,787,816,771,290,290,290,290,288,258,
        -253,-349,-414,-447,-463,1329,1299,-479,1314,1312,1057,1057,1042,1042,1026,1026,785,785,785,785,784,784,784,784,769,769,769,769,768,768,768,768,-319,851,821,-335,836,850,805,849,341,340,325,336,533,533,579,579,564,564,773,832,578,548,563,516,321,276,306,291,304,259,
        -251,-572,-733,-830,-863,-879,1041,1041,784,784,784,784,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,-511,-527,-543,1396,1351,1381,1366,1395,1335,1380,-559,1334,1138,1138,1063,1063,1350,1392,1031,1031,1062,1062,1364,1363,1120,1120,1333,1348,881,881,881,881,375,374,359,373,343,358,341,325,791,791,1123,1122,-703,1105,1045,-719,865,865,790,790,774,774,1104,1029,338,293,323,308,-799,-815,833,788,772,818,803,816,322,292,307,320,561,531,515,546,289,274,288,258,
        -251,-525,-605,-685,-765,-831,-846,1298,1057,1057,1312,1282,785,785,785,785,784,784,784,784,769,769,769,769,512,512,512,512,512,512,512,512,1399,1398,1383,1367,1382,1396,1351,-511,1381,1366,1139,1139,1079,1079,1124,1124,1364,1349,1363,1333,882,882,882,882,807,807,807,807,1094,1094,1136,1136,373,341,535,535,881,775,867,822,774,-591,324,338,-671,849,550,550,866,864,609,609,293,336,534,534,789,835,773,-751,834,804,308,307,833,788,832,772,562,562,547,547,305,275,560,515,290,290,
        -252,-397,-477,-557,-622,-653,-719,-735,-750,1329,1299,1314,1057,1057,1042,1042,1312,1282,1024,1024,785,785,785,785,784,784,784,784,769,769,769,769,-383,1127,1141,1111,1126,1140,1095,1110,869,869,883,883,1079,1109,882,882,375,374,807,868,838,881,791,-463,867,822,368,263,852,837,836,-543,610,610,550,550,352,336,534,534,865,774,851,821,850,805,593,533,579,564,773,832,578,578,548,548,577,577,307,276,306,291,516,560,259,259,
        -250,-2107,-2507,-2764,-2909,-2974,-3007,-3023,1041,1041,1040,1040,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,-767,-1052,-1213,-1277,-1358,-1405,-1469,-1535,-1550,-1582,-1614,-1647,-1662,-1694,-1726,-1759,-1774,-1807,-1822,-1854,-1886,1565,-1919,-1935,-1951,-1967,1731,1730,1580,1717,-1983,1729,1564,-1999,1548,-2015,-2031,1715,1595,-2047,1714,-2063,1610,-2079,1609,-2095,1323,1323,1457,1457,1307,1307,1712,1547,1641,1700,1699,1594,1685,1625,1442,1442,1322,1322,-780,-973,-910,1279,1278,1277,1262,1276,1261,1275,1215,1260,1229,-959,974,974,989,989,-943,735,478,478,495,463,506,414,-1039,1003,958,1017,927,942,987,957,431,476,1272,1167,1228,-1183,1256,-1199,895,895,941,941,1242,1227,1212,1135,1014,1014,490,489,503,487,910,1013,985,925,863,894,970,955,1012,847,-1343,831,755,755,984,909,428,366,754,559,-1391,752,486,457,924,997,698,698,983,893,740,740,908,877,739,739,667,667,953,938,497,287,271,271,683,606,590,712,726,574,302,302,738,736,481,286,526,725,605,711,636,724,696,651,589,681,666,710,364,467,573,695,466,466,301,465,379,379,709,604,665,679,316,316,634,633,436,436,464,269,424,394,452,332,438,363,347,408,393,448,331,422,362,407,392,421,346,406,391,376,375,359,1441,1306,-2367,1290,-2383,1337,-2399,-2415,1426,1321,-2431,1411,1336,-2447,-2463,-2479,1169,1169,1049,1049,1424,1289,1412,1352,1319,-2495,1154,1154,1064,1064,1153,1153,416,390,360,404,403,389,344,374,373,343,358,372,327,357,342,311,356,326,1395,1394,1137,1137,1047,1047,1365,1392,1287,1379,1334,1364,1349,1378,1318,1363,792,792,792,792,1152,1152,1032,1032,1121,1121,1046,1046,1120,1120,1030,1030,-2895,1106,1061,1104,849,849,789,789,1091,1076,1029,1090,1060,1075,833,833,309,324,532,532,832,772,818,803,561,561,531,560,515,546,289,274,288,258,
        -250,-1179,-1579,-1836,-1996,-2124,-2253,-2333,-2413,-2477,-2542,-2574,-2607,-2622,-2655,1314,1313,1298,1312,1282,785,785,785,785,1040,1040,1025,1025,768,768,768,768,-766,-798,-830,-862,-895,-911,-927,-943,-959,-975,-991,-1007,-1023,-1039,-1055,-1070,1724,1647,-1103,-1119,1631,1767,1662,1738,1708,1723,-1135,1780,1615,1779,1599,1677,1646,1778,1583,-1151,1777,1567,1737,1692,1765,1722,1707,1630,1751,1661,1764,1614,1736,1676,1763,1750,1645,1598,1721,1691,1762,1706,1582,1761,1566,-1167,1749,1629,767,766,751,765,494,494,735,764,719,749,734,763,447,447,748,718,477,506,431,491,446,476,461,505,415,430,475,445,504,399,460,489,414,503,383,474,429,459,502,502,746,752,488,398,501,473,413,472,486,271,480,270,-1439,-1455,1357,-1471,-1487,-1503,1341,1325,-1519,1489,1463,1403,1309,-1535,1372,1448,1418,1476,1356,1462,1387,-1551,1475,1340,1447,1402,1386,-1567,1068,1068,1474,1461,455,380,468,440,395,425,410,454,364,467,466,464,453,269,409,448,268,432,1371,1473,1432,1417,1308,1460,1355,1446,1459,1431,1083,1083,1401,1416,1458,1445,1067,1067,1370,1457,1051,1051,1291,1430,1385,1444,1354,1415,1400,1443,1082,1082,1173,1113,1186,1066,1185,1050,-1967,1158,1128,1172,1097,1171,1081,-1983,1157,1112,416,266,375,400,1170,1142,1127,1065,793,793,1169,1033,1156,1096,1141,1111,1155,1080,1126,1140,898,898,808,808,897,897,792,792,1095,1152,1032,1125,1110,1139,1079,1124,882,807,838,881,853,791,-2319,867,368,263,822,852,837,866,806,865,-2399,851,352,262,534,534,821,836,594,594,549,549,593,593,533,533,848,773,579,579,564,578,548,563,276,276,577,576,306,291,516,560,305,305,275,259,
        -251,-892,-2058,-2620,-2828,-2957,-3023,-3039,1041,1041,1040,1040,769,769,769,769,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,256,-511,-527,-543,-559,1530,-575,-591,1528,1527,1407,1526,1391,1023,1023,1023,1023,1525,1375,1268,1268,1103,1103,1087,1087,1039,1039,1523,-604,815,815,815,815,510,495,509,479,508,463,507,447,431,505,415,399,-734,-782,1262,-815,1259,1244,-831,1258,1228,-847,-863,1196,-879,1253,987,987,748,-767,493,493,462,477,414,414,686,669,478,446,461,445,474,429,487,458,412,471,1266,1264,1009,1009,799,799,-1019,-1276,-1452,-1581,-1677,-1757,-1821,-1886,-1933,-1997,1257,1257,1483,1468,1512,1422,1497,1406,1467,1496,1421,1510,1134,1134,1225,1225,1466,1451,1374,1405,1252,1252,1358,1480,1164,1164,1251,1251,1238,1238,1389,1465,-1407,1054,1101,-1423,1207,-1439,830,830,1248,1038,1237,1117,1223,1148,1236,1208,411,426,395,410,379,269,1193,1222,1132,1235,1221,1116,976,976,1192,1162,1177,1220,1131,1191,963,963,-1647,961,780,-1663,558,558,994,993,437,408,393,407,829,978,813,797,947,-1743,721,721,377,392,844,950,828,890,706,706,812,859,796,960,948,843,934,874,571,571,-1919,690,555,689,421,346,539,539,944,779,918,873,932,842,903,888,570,570,931,917,674,674,-2575,1562,-2591,1609,-2607,1654,1322,1322,1441,1441,1696,1546,1683,1593,1669,1624,1426,1426,1321,1321,1639,1680,1425,1425,1305,1305,1545,1668,1608,1623,1667,1592,1638,1666,1320,1320,1652,1607,1409,1409,1304,1304,1288,1288,1664,1637,1395,1395,1335,1335,1622,1636,1394,1394,1319,1319,1606,1621,1392,1392,1137,1137,1137,1137,345,390,360,375,404,373,1047,-2751,-2767,-2783,1062,1121,1046,-2799,1077,-2815,1106,1061,789,789,1105,1104,263,355,310,340,325,354,352,262,339,324,1091,1076,1029,1090,1060,1075,833,833,788,788,1088,1028,818,818,803,803,561,561,531,531,816,771,546,546,289,274,288,258,
        -253,-317,-381,-446,-478,-509,1279,1279,-811,-1179,-1451,-1756,-1900,-2028,-2189,-2253,-2333,-2414,-2445,-2511,-2526,1313,1298,-2559,1041,1041,1040,1040,1025,1025,1024,1024,1022,1007,1021,991,1020,975,1019,959,687,687,1018,1017,671,671,655,655,1016,1015,639,639,758,758,623,623,757,607,756,591,755,575,754,559,543,543,1009,783,-575,-621,-685,-749,496,-590,750,749,734,748,974,989,1003,958,988,973,1002,942,987,957,972,1001,926,986,941,971,956,1000,910,985,925,999,894,970,-1071,-1087,-1102,1390,-1135,1436,1509,1451,1374,-1151,1405,1358,1480,1420,-1167,1507,1494,1389,1342,1465,1435,1450,1326,1505,1310,1493,1373,1479,1404,1492,1464,1419,428,443,472,397,736,526,464,464,486,457,442,471,484,482,1357,1449,1434,1478,1388,1491,1341,1490,1325,1489,1463,1403,1309,1477,1372,1448,1418,1433,1476,1356,1462,1387,-1439,1475,1340,1447,1402,1474,1324,1461,1371,1473,269,448,1432,1417,1308,1460,-1711,1459,-1727,1441,1099,1099,1446,1386,1431,1401,-1743,1289,1083,1083,1160,1160,1458,1445,1067,1067,1370,1457,1307,1430,1129,1129,1098,1098,268,432,267,416,266,400,-1887,1144,1187,1082,1173,1113,1186,1066,1050,1158,1128,1143,1172,1097,1171,1081,420,391,1157,1112,1170,1142,1127,1065,1169,1049,1156,1096,1141,1111,1155,1080,1126,1154,1064,1153,1140,1095,1048,-2159,1125,1110,1137,-2175,823,823,1139,1138,807,807,384,264,368,263,868,838,853,791,867,822,852,837,866,806,865,790,-2319,851,821,836,352,262,850,805,849,-2399,533,533,835,820,336,261,578,548,563,577,532,532,832,772,562,562,547,547,305,275,560,515,290,290,288,258 };
    static const drmp3_uint8 tab32[] = { 130,162,193,209,44,28,76,140,9,9,9,9,9,9,9,9,190,254,222,238,126,94,157,157,109,61,173,205};
    static const drmp3_uint8 tab33[] = { 252,236,220,204,188,172,156,140,124,108,92,76,60,44,28,12 };
    static const drmp3_int16 tabindex[2*16] = { 0,32,64,98,0,132,180,218,292,364,426,538,648,746,0,1126,1460,1460,1460,1460,1460,1460,1460,1460,1842,1842,1842,1842,1842,1842,1842,1842 };
    static const drmp3_uint8 g_linbits[] =  { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,2,3,4,6,8,10,13,4,5,6,7,8,9,11,13 };
#define DRMP3_PEEK_BITS(n)    (bs_cache >> (32 - n))
#define DRMP3_FLUSH_BITS(n)   { bs_cache <<= (n); bs_sh += (n); }
#define DRMP3_CHECK_BITS      while (bs_sh >= 0) { bs_cache |= (drmp3_uint32)*bs_next_ptr++ << bs_sh; bs_sh -= 8; }
#define DRMP3_BSPOS           ((bs_next_ptr - bs->buf)*8 - 24 + bs_sh)
    float one = 0.0f;
    int ireg = 0, big_val_cnt = gr_info->big_values;
    const drmp3_uint8 *sfb = gr_info->sfbtab;
    const drmp3_uint8 *bs_next_ptr = bs->buf + bs->pos/8;
    drmp3_uint32 bs_cache = (((bs_next_ptr[0]*256u + bs_next_ptr[1])*256u + bs_next_ptr[2])*256u + bs_next_ptr[3]) << (bs->pos & 7);
    int pairs_to_decode, np, bs_sh = (bs->pos & 7) - 8;
	  IADD(__LINE__, 12);
    bs_next_ptr += 4;
    while (big_val_cnt > 0)
    {
        int tab_num = gr_info->table_select[ireg];
        int sfb_cnt = gr_info->region_count[ireg++];
        const drmp3_int16 *codebook = tabs + tabindex[tab_num];
        int linbits = g_linbits[tab_num];
        if (linbits)
        {
            do
            {
                np = *sfb++ / 2;
                pairs_to_decode = DRMP3_MIN(big_val_cnt, np);
	  IADD(__LINE__, 4);
                one = *scf++;
                do
                {
                    int j, w = 5;
                    int leaf = codebook[DRMP3_PEEK_BITS(w)];
                    while (leaf < 0)
                    {
                        DRMP3_FLUSH_BITS(w);
                        w = leaf & 7;
                        leaf = codebook[DRMP3_PEEK_BITS(w) - (leaf >> 3)];
			IADD(__LINE__, 2);
                    }
                    DRMP3_FLUSH_BITS(leaf >> 8);
                    for (j = 0; j < 2; j++, dst++, leaf >>= 4)
                    {
                        int lsb = leaf & 0x0F;
                        if (lsb == 15)
                        {
			  IADD(__LINE__, 2);
			  lsb += DRMP3_PEEK_BITS(linbits);
			  DRMP3_FLUSH_BITS(linbits);
			  DRMP3_CHECK_BITS;
                            *dst = one*drmp3_L3_pow_43(lsb)*((drmp3_int32)bs_cache < 0 ? -1: 1);
                        } else
                        {
                            *dst = g_drmp3_pow43[16 + lsb - 16*(bs_cache >> 31)]*one;
                        }
                        DRMP3_FLUSH_BITS(lsb ? 1 : 0);
                    }
                    DRMP3_CHECK_BITS;
	  IADD(__LINE__, 1);
                } while (--pairs_to_decode);
            } while ((big_val_cnt -= np) > 0 && --sfb_cnt >= 0);
        } else
        {
            do
            {
                np = *sfb++ / 2;
                pairs_to_decode = DRMP3_MIN(big_val_cnt, np);
                one = *scf++;
                do
                {
		  	  IADD(__LINE__, 3);
                    int j, w = 5;
                    int leaf = codebook[DRMP3_PEEK_BITS(w)];
                    while (leaf < 0)
                    {
                        DRMP3_FLUSH_BITS(w);
                        w = leaf & 7;
                        leaf = codebook[DRMP3_PEEK_BITS(w) - (leaf >> 3)];
                    }
                    DRMP3_FLUSH_BITS(leaf >> 8);
                    for (j = 0; j < 2; j++, dst++, leaf >>= 4)
                    {
		  	  IADD(__LINE__, 3);
		      int lsb = leaf & 0x0F;
                        *dst = g_drmp3_pow43[16 + lsb - 16*(bs_cache >> 31)]*one;
                        DRMP3_FLUSH_BITS(lsb ? 1 : 0);
                    }
                    DRMP3_CHECK_BITS;
		    		  	  IADD(__LINE__, 1);
                } while (--pairs_to_decode);
            } while ((big_val_cnt -= np) > 0 && --sfb_cnt >= 0);
        }
    }
    for (np = 1 - big_val_cnt;; dst += 4)
    {
        const drmp3_uint8 *codebook_count1 = (gr_info->count1_table) ? tab33 : tab32;
        int leaf = codebook_count1[DRMP3_PEEK_BITS(4)];
        if (!(leaf & 8))
        {
		  	  IADD(__LINE__, 3);
	  leaf = codebook_count1[(leaf >> 3) + (bs_cache << 4 >> (32 - (leaf & 3)))];
        }
        DRMP3_FLUSH_BITS(leaf & 7);
        if (DRMP3_BSPOS > layer3gr_limit)
        {
            break;
        }
#define DRMP3_RELOAD_SCALEFACTOR  if (!--np) { np = *sfb++/2; if (!np) break; one = *scf++; }
#define DRMP3_DEQ_COUNT1(s) if (leaf & (128 >> s)) { dst[s] = ((drmp3_int32)bs_cache < 0) ? -one : one; DRMP3_FLUSH_BITS(1) }
        DRMP3_RELOAD_SCALEFACTOR;
        DRMP3_DEQ_COUNT1(0);
        DRMP3_DEQ_COUNT1(1);
        DRMP3_RELOAD_SCALEFACTOR;
        DRMP3_DEQ_COUNT1(2);
        DRMP3_DEQ_COUNT1(3);
        DRMP3_CHECK_BITS;
		  	  IADD(__LINE__, 13);
    }
    bs->pos = layer3gr_limit;
}


void drmp3_L3_stereo_top_band(const float *right, const drmp3_uint8 *sfb, int nbands, int max_band[3])
{
  assert(0);
  int i, k;
    max_band[0] = max_band[1] = max_band[2] = -1;
    for (i = 0; i < nbands; i++)
    {
        for (k = 0; k < sfb[i]; k += 2)
        {
            if (right[k] != 0 || right[k + 1] != 0)
            {
                max_band[i % 3] = i;
                break;
            }
        }
        right += sfb[i];
    }
}



void drmp3_L3_intensity_stereo_band(float *left, int n, float kl, float kr)
{
  assert(0);
  int i;
    for (i = 0; i < n; i++)
    {
        left[i + 576] = left[i]*kr;
        left[i] = left[i]*kl;
    }
}


void drmp3_L3_midside_stereo(float *left, int n)
{

  int i = 0;
    float *right = left + 576;
#if DRMP3_HAVE_SIMD
    if (drmp3_have_simd()) for (; i < n - 3; i += 4)
    {
      assert(0);
      drmp3_f4 vl = DRMP3_VLD(left + i);
        drmp3_f4 vr = DRMP3_VLD(right + i);
        DRMP3_VSTORE(left + i, DRMP3_VADD(vl, vr));
        DRMP3_VSTORE(right + i, DRMP3_VSUB(vl, vr));
    }
#endif
    for (; i < n; i++)
    {
        float a = left[i];
        float b = right[i];
        left[i] = a + b;
        right[i] = a - b;
	FADD(__LINE__, 2);
    }
}

void drmp3_L3_stereo_process(float *left, const drmp3_uint8 *ist_pos, const drmp3_uint8 *sfb, const drmp3_uint8 *hdr, int max_band[3], int mpeg2_sh)
{
  assert(0);
  static const float g_pan[7*2] = { 0,1,0.21132487f,0.78867513f,0.36602540f,0.63397460f,0.5f,0.5f,0.63397460f,0.36602540f,0.78867513f,0.21132487f,1,0 };
    unsigned i, max_pos = DRMP3_HDR_TEST_MPEG1(hdr) ? 7 : 64;
    for (i = 0; sfb[i]; i++)
    {
        unsigned ipos = ist_pos[i];
        if ((int)i > max_band[i % 3] && ipos < max_pos)
        {
            float kl, kr, s = DRMP3_HDR_TEST_MS_STEREO(hdr) ? 1.41421356f : 1;
            if (DRMP3_HDR_TEST_MPEG1(hdr))
            {
                kl = g_pan[2*ipos];
                kr = g_pan[2*ipos + 1];
            } else
            {
                kl = 1;
                kr = drmp3_L3_ldexp_q2(1, (ipos + 1) >> 1 << mpeg2_sh);
                if (ipos & 1)
                {
                    kl = kr;
                    kr = 1;
                }
            }
            drmp3_L3_intensity_stereo_band(left, sfb[i], kl*s, kr*s);
        } else if (DRMP3_HDR_TEST_MS_STEREO(hdr))
        {
            drmp3_L3_midside_stereo(left, sfb[i]);
        }
        left += sfb[i];
    }
}


void drmp3_L3_intensity_stereo(float *left, drmp3_uint8 *ist_pos, const drmp3_L3_gr_info *gr, const drmp3_uint8 *hdr)
{
    assert(0);
    int max_band[3], n_sfb = gr->n_long_sfb + gr->n_short_sfb;
    int i, max_blocks = gr->n_short_sfb ? 3 : 1;
    drmp3_L3_stereo_top_band(left + 576, gr->sfbtab, n_sfb, max_band);
    if (gr->n_long_sfb)
    {
        max_band[0] = max_band[1] = max_band[2] = DRMP3_MAX(DRMP3_MAX(max_band[0], max_band[1]), max_band[2]);
    }
    for (i = 0; i < max_blocks; i++)
    {
        int default_pos = DRMP3_HDR_TEST_MPEG1(hdr) ? 3 : 0;
        int itop = n_sfb - max_blocks + i;
        int prev = itop - max_blocks;
        ist_pos[itop] = (drmp3_uint8)(max_band[i] >= prev ? default_pos : ist_pos[prev]);
    }
    drmp3_L3_stereo_process(left, ist_pos, gr->sfbtab, hdr, max_band, gr[1].scalefac_compress & 1);
}

static void drmp3_L3_reorder(float *grbuf, float *scratch, const drmp3_uint8 *sfb)
{
  int i, len;
    float *src = grbuf, *dst = scratch;
    for (;0 != (len = *sfb); sfb += 3, src += 2*len)
    {
      IADD(__LINE__,3);
      for (i = 0; i < len; i++, src++)
        {
	  *dst++ = src[0*len];  
	  *dst++ = src[1*len];
	  *dst++ = src[2*len];
        }
    }
    memcpy(grbuf, scratch, (dst - scratch)*sizeof(float));
}


static void drmp3_L3_antialias(float *grbuf, int nbands)
{
  static const float g_aa[2][8] = {
        {0.85749293f,0.88174200f,0.94962865f,0.98331459f,0.99551782f,0.99916056f,0.99989920f,0.99999316f},
        {0.51449576f,0.47173197f,0.31337745f,0.18191320f,0.09457419f,0.04096558f,0.01419856f,0.00369997f}
    };
    for (; nbands > 0; nbands--, grbuf += 18)
    {
        int i = 0;
#if DRMP3_HAVE_SIMD
        if (drmp3_have_simd()) for (; i < 8; i += 4)
        {
	  assert(0);
	  drmp3_f4 vu = DRMP3_VLD(grbuf + 18 + i);
            drmp3_f4 vd = DRMP3_VLD(grbuf + 14 - i);
            drmp3_f4 vc0 = DRMP3_VLD(g_aa[0] + i);
            drmp3_f4 vc1 = DRMP3_VLD(g_aa[1] + i);
            vd = DRMP3_VREV(vd);
            DRMP3_VSTORE(grbuf + 18 + i, DRMP3_VSUB(DRMP3_VMUL(vu, vc0), DRMP3_VMUL(vd, vc1)));
            vd = DRMP3_VADD(DRMP3_VMUL(vu, vc1), DRMP3_VMUL(vd, vc0));
            DRMP3_VSTORE(grbuf + 14 - i, DRMP3_VREV(vd));
        }
#endif
#ifndef DR_MP3_ONLY_SIMD
        for(; i < 8; i++)
        {
	  FADD(__LINE__, 2);	  FTIMES(__LINE__, 4); // Butterfly!
  
	  float u = grbuf[18 + i];
	  float d = grbuf[17 - i];
	  grbuf[18 + i] = u*g_aa[0][i] - d*g_aa[1][i];
	  grbuf[17 - i] = u*g_aa[1][i] + d*g_aa[0][i];
        }
#endif
    }
}


void drmp3_L3_dct3_9(float *y)
{
    float s0, s1, s2, s3, s4, s5, s6, s7, s8, t0, t2, t4;
    s0 = y[0]; s2 = y[2]; s4 = y[4]; s6 = y[6]; s8 = y[8];
    t0 = s0 + s6*0.5f; 	FADD(__LINE__, 1);
    s0 -= s6;
    t4 = (s4 + s2)*0.93969262f; 	FADD(__LINE__, 1); 	FTIMES(__LINE__, 1);
    t2 = (s8 + s2)*0.76604444f;	FADD(__LINE__, 1); 	FTIMES(__LINE__, 1);
    s6 = (s4 - s8)*0.17364818f;	  FADD(__LINE__, 1); 	FTIMES(__LINE__, 1);
    s4 += s8 - s2;	          FADD(__LINE__, 2);
    s2 = s0 - s4*0.5f;            FADD(__LINE__, 1); 	FTIMES(__LINE__, 1);
    y[4] = s4 + s0;                 FADD(__LINE__, 1);
    s8 = t0 - t2 + s6;  FADD(__LINE__, 2);
    s0 = t0 - t4 + t2;    FADD(__LINE__, 2);
    s4 = t0 + t4 - s6;  FADD(__LINE__, 2);
    s1 = y[1]; s3 = y[3]; s5 = y[5]; s7 = y[7];
    s3 *= 0.86602540f;    FTIMES(__LINE__, 1);
    t0 = (s5 + s1)*0.98480775f;
    t4 = (s5 - s7)*0.34202014f;
    t2 = (s1 + s7)*0.64278761f;
    s1 = (s1 - s5 - s7)*0.86602540f;
    s5 = t0 - s3 - t2;   FADD(__LINE__, 2);  
    s7 = t4 - s3 - t0; FADD(__LINE__, 2);  
    s3 = t4 + s3 - t2; FADD(__LINE__, 2);  
    y[0] = s4 - s7;  FADD(__LINE__, 1);  
    y[1] = s2 + s1;FADD(__LINE__, 1);  
    y[2] = s0 - s3;FADD(__LINE__, 1);  
    y[3] = s8 + s5;FADD(__LINE__, 1);  
    y[5] = s8 - s5;FADD(__LINE__, 1);  
    y[6] = s0 + s3;FADD(__LINE__, 1);  
    y[7] = s2 - s1;FADD(__LINE__, 1);  
    y[8] = s4 + s7;FADD(__LINE__, 1);  
}

void drmp3_L3_idct3(float x0, float x1, float x2, float *dst)
{
  float m1 = x1*0.86602540f;  FTIMES(__LINE__, 1);
  float a1 = x0 - x2*0.5f;   FTIMES(__LINE__, 1);  FADD(__LINE__, 1);
  dst[1] = x0 + x2; FADD(__LINE__, 3);
  dst[0] = a1 + m1;
  dst[2] = a1 - m1;
}

static void drmp3_L3_imdct12(float *x, float *dst, float *overlap)
{
    static const float g_twid3[6] = { 0.79335334f,0.92387953f,0.99144486f, 0.60876143f,0.38268343f,0.13052619f };
    float co[3], si[3];
    int i;
    drmp3_L3_idct3(-x[0], x[6] + x[3], x[12] + x[9], co);
    drmp3_L3_idct3(x[15], x[12] - x[9], x[6] - x[3], si);
    si[1] = -si[1];
    for (i = 0; i < 3; i++)
    {
        float ovl  = overlap[i];
        float sum  = co[i]*g_twid3[3 + i] + si[i]*g_twid3[0 + i];  FADD(__LINE__, 2);	  FTIMES(__LINE__, 4); // Spline
	overlap[i] = co[i]*g_twid3[0 + i] - si[i]*g_twid3[3 + i]; 
        dst[i]     = ovl*g_twid3[2 - i] - sum*g_twid3[5 - i];  FADD(__LINE__, 2);	  FTIMES(__LINE__, 4); // Spline
        dst[5 - i] = ovl*g_twid3[5 - i] + sum*g_twid3[2 - i];
    }
}


// Radix 36 ? 576 samples per frame.  576=16*36
static void drmp3_L3_imdct36(float *grbuf, float *overlap, const float *window, int nbands)
{
  printf("drmp3_L3_imdct36  overlap=%f  window=%f  %i\n", *overlap, *window, nbands);
  int i, j;
    static const float g_twid9[18] = {
        0.73727734f,0.79335334f,0.84339145f,0.88701083f,0.92387953f,0.95371695f,0.97629601f,0.99144486f,0.99904822f,0.67559021f,0.60876143f,0.53729961f,0.46174861f,0.38268343f,0.30070580f,0.21643961f,0.13052619f,0.04361938f
    };
    for (j = 0; j < nbands; j++, grbuf += 18, overlap += 9)
    {
      float co[9], si[9];
      FADD(__LINE__, 1);
      co[0] = -grbuf[0];
        si[0] = grbuf[17];
        for (i = 0; i < 4; i++)
        {
	  FADD(__LINE__, 4);	  
	  si[8 - 2*i] =   grbuf[4*i + 1] - grbuf[4*i + 2];
	  co[1 + 2*i] =   grbuf[4*i + 1] + grbuf[4*i + 2];
	  si[7 - 2*i] =   grbuf[4*i + 4] - grbuf[4*i + 3];
            co[2 + 2*i] = -(grbuf[4*i + 3] + grbuf[4*i + 4]);
        }
        drmp3_L3_dct3_9(co);
        drmp3_L3_dct3_9(si);
        si[1] = -si[1];
        si[3] = -si[3];
        si[5] = -si[5];
        si[7] = -si[7];
        i = 0;
#if DRMP3_HAVE_SIMD
        if (drmp3_have_simd()) for (; i < 8; i += 4)
        {
	  assert(0);
	  drmp3_f4 vovl = DRMP3_VLD(overlap + i);
            drmp3_f4 vc = DRMP3_VLD(co + i);
            drmp3_f4 vs = DRMP3_VLD(si + i);
            drmp3_f4 vr0 = DRMP3_VLD(g_twid9 + i);
            drmp3_f4 vr1 = DRMP3_VLD(g_twid9 + 9 + i);
            drmp3_f4 vw0 = DRMP3_VLD(window + i);
            drmp3_f4 vw1 = DRMP3_VLD(window + 9 + i);
            drmp3_f4 vsum = DRMP3_VADD(DRMP3_VMUL(vc, vr1), DRMP3_VMUL(vs, vr0));
            DRMP3_VSTORE(overlap + i, DRMP3_VSUB(DRMP3_VMUL(vc, vr0), DRMP3_VMUL(vs, vr1)));
            DRMP3_VSTORE(grbuf + i, DRMP3_VSUB(DRMP3_VMUL(vovl, vw0), DRMP3_VMUL(vsum, vw1)));
            vsum = DRMP3_VADD(DRMP3_VMUL(vovl, vw1), DRMP3_VMUL(vsum, vw0));
            DRMP3_VSTORE(grbuf + 14 - i, DRMP3_VREV(vsum));
        }
#endif
        for (; i < 9; i++)
        {
	  float ovl  = overlap[i];
	  float sum  = co[i]*g_twid9[9 + i] + si[i]*g_twid9[0 + i];   	  FADD(__LINE__, 2);	  FTIMES(__LINE__, 4); // Butterfly!
	  overlap[i] = co[i]*g_twid9[0 + i] - si[i]*g_twid9[9 + i];
	  grbuf[i]      = ovl*window[0 + i] - sum*window[9 + i];   	  FADD(__LINE__, 2);	  FTIMES(__LINE__, 4); // Butterfly!
	  grbuf[17 - i] = ovl*window[9 + i] + sum*window[0 + i];
        }
    }
}


static void drmp3_L3_imdct_short(float *grbuf, float *overlap, int nbands)
{
  printf("drmp3_L3_imdct_short  %f  %i\n", *overlap, nbands);
  for (;nbands > 0; nbands--, overlap += 9, grbuf += 18)
    {
        float tmp[18];
        memcpy(tmp, grbuf, sizeof(tmp));
        memcpy(grbuf, overlap, 6*sizeof(float));
        drmp3_L3_imdct12(tmp, grbuf + 6, overlap + 6);
        drmp3_L3_imdct12(tmp + 1, grbuf + 12, overlap + 6);
        drmp3_L3_imdct12(tmp + 2, overlap, overlap + 6);
    }
}

static void drmp3_L3_change_sign(float *grbuf)
{
    int b, i;
    for (b = 0, grbuf += 18; b < 32; b += 2, grbuf += 36)
        for (i = 1; i < 18; i += 2)
            grbuf[i] = -grbuf[i];
}


static void drmp3_L3_imdct_gr(float *grbuf, float *overlap, unsigned block_type, unsigned n_long_bands)
{
    static const float g_mdct_window[2][18] = {
        { 0.99904822f,0.99144486f,0.97629601f,0.95371695f,0.92387953f,0.88701083f,0.84339145f,0.79335334f,0.73727734f,0.04361938f,0.13052619f,0.21643961f,0.30070580f,0.38268343f,0.46174861f,0.53729961f,0.60876143f,0.67559021f },
        { 1,1,1,1,1,1,0.99144486f,0.92387953f,0.79335334f,0,0,0,0,0,0,0.13052619f,0.38268343f,0.60876143f }
    };


    if (n_long_bands)
    {
      IADD(__LINE__, 4);
      ITIMES(__LINE__, 2);      
      drmp3_L3_imdct36(grbuf, overlap, g_mdct_window[0], n_long_bands);
      grbuf += 18*n_long_bands;
      overlap += 9*n_long_bands;
    }
    if (block_type == DRMP3_SHORT_BLOCK_TYPE)
        drmp3_L3_imdct_short(grbuf, overlap, 32 - n_long_bands);
    else
        drmp3_L3_imdct36(grbuf, overlap, g_mdct_window[block_type == DRMP3_STOP_BLOCK_TYPE], 32 - n_long_bands);
      IADD(__LINE__, 2);
    }


void drmp3_L3_decode(drmp3dec *h, drmp3dec_scratch *s, drmp3_L3_gr_info *gr_info, int nch)
{
    int ch;
    for (ch = 0; ch < nch; ch++)
    {
        int layer3gr_limit = s->bs.pos + gr_info[ch].part_23_length;
        drmp3_L3_decode_scalefactors(h->header, s->ist_pos[ch], &s->bs, gr_info + ch, s->scf, ch);
        drmp3_L3_huffman(s->grbuf[ch], &s->bs, gr_info + ch, s->scf, layer3gr_limit);
    }
    if (DRMP3_HDR_TEST_I_STEREO(h->header))
    {
      assert(0);
      drmp3_L3_intensity_stereo(s->grbuf[0], s->ist_pos[1], gr_info, h->header);
    } else if (DRMP3_HDR_IS_MS_STEREO(h->header))
    {
        drmp3_L3_midside_stereo(s->grbuf[0], 576);
    }
    for (ch = 0; ch < nch; ch++, gr_info++)
    {
        int aa_bands = 31;
        int n_long_bands = (gr_info->mixed_block_flag ? 2 : 0) << (int)(DRMP3_HDR_GET_MY_SAMPLE_RATE(h->header) == 2);

        if (gr_info->n_short_sfb)
        {
            aa_bands = n_long_bands - 1;
            drmp3_L3_reorder(s->grbuf[ch] + n_long_bands*18, s->syn[0], gr_info->sfbtab + gr_info->n_long_sfb);
        }
        drmp3_L3_antialias(s->grbuf[ch], aa_bands);
        drmp3_L3_imdct_gr(s->grbuf[ch], h->mdct_overlap[ch], gr_info->block_type, n_long_bands);
        drmp3_L3_change_sign(s->grbuf[ch]);
    }
}


// n = 18
void drmp3d_DCT_II(float *grbuf, int n, int ch)
{
  n_drmp3d += 1;
  printf("DCT %i  %i  ch=%i\n", n, n_drmp3d, ch);
  static const float g_sec[24] = {
        10.19000816f,0.50060302f,0.50241929f,3.40760851f,0.50547093f,0.52249861f,2.05778098f,0.51544732f,0.56694406f,1.48416460f,0.53104258f,0.64682180f,1.16943991f,0.55310392f,0.78815460f,0.97256821f,0.58293498f,1.06067765f,0.83934963f,0.62250412f,1.72244716f,0.74453628f,0.67480832f,5.10114861f
    };
    int i, k = 0;
#if DRMP3_HAVE_SIMD
    if (drmp3_have_simd()) for (; k < n; k += 4)
    {
      assert(0);
      drmp3_f4 t[4][8], *x;
        float *y = grbuf + k;
        for (x = t[0], i = 0; i < 8; i++, x++)
        {
            drmp3_f4 x0 = DRMP3_VLD(&y[i*18]);
            drmp3_f4 x1 = DRMP3_VLD(&y[(15 - i)*18]);
            drmp3_f4 x2 = DRMP3_VLD(&y[(16 + i)*18]);
            drmp3_f4 x3 = DRMP3_VLD(&y[(31 - i)*18]);
            drmp3_f4 t0 = DRMP3_VADD(x0, x3);
            drmp3_f4 t1 = DRMP3_VADD(x1, x2);
            drmp3_f4 t2 = DRMP3_VMUL_S(DRMP3_VSUB(x1, x2), g_sec[3*i + 0]);
            drmp3_f4 t3 = DRMP3_VMUL_S(DRMP3_VSUB(x0, x3), g_sec[3*i + 1]);
            x[0] = DRMP3_VADD(t0, t1);
            x[8] = DRMP3_VMUL_S(DRMP3_VSUB(t0, t1), g_sec[3*i + 2]);
            x[16] = DRMP3_VADD(t3, t2);
            x[24] = DRMP3_VMUL_S(DRMP3_VSUB(t3, t2), g_sec[3*i + 2]);
        }
        for (x = t[0], i = 0; i < 4; i++, x += 8)
        {
            drmp3_f4 x0 = x[0], x1 = x[1], x2 = x[2], x3 = x[3], x4 = x[4], x5 = x[5], x6 = x[6], x7 = x[7], xt;
            xt = DRMP3_VSUB(x0, x7); x0 = DRMP3_VADD(x0, x7);
            x7 = DRMP3_VSUB(x1, x6); x1 = DRMP3_VADD(x1, x6);
            x6 = DRMP3_VSUB(x2, x5); x2 = DRMP3_VADD(x2, x5);
            x5 = DRMP3_VSUB(x3, x4); x3 = DRMP3_VADD(x3, x4);
            x4 = DRMP3_VSUB(x0, x3); x0 = DRMP3_VADD(x0, x3);
            x3 = DRMP3_VSUB(x1, x2); x1 = DRMP3_VADD(x1, x2);
            x[0] = DRMP3_VADD(x0, x1);
            x[4] = DRMP3_VMUL_S(DRMP3_VSUB(x0, x1), 0.70710677f);
            x5 = DRMP3_VADD(x5, x6);
            x6 = DRMP3_VMUL_S(DRMP3_VADD(x6, x7), 0.70710677f);
            x7 = DRMP3_VADD(x7, xt);
            x3 = DRMP3_VMUL_S(DRMP3_VADD(x3, x4), 0.70710677f);
            x5 = DRMP3_VSUB(x5, DRMP3_VMUL_S(x7, 0.198912367f));
            x7 = DRMP3_VADD(x7, DRMP3_VMUL_S(x5, 0.382683432f));
            x5 = DRMP3_VSUB(x5, DRMP3_VMUL_S(x7, 0.198912367f));
            x0 = DRMP3_VSUB(xt, x6); xt = DRMP3_VADD(xt, x6);
            x[1] = DRMP3_VMUL_S(DRMP3_VADD(xt, x7), 0.50979561f);
            x[2] = DRMP3_VMUL_S(DRMP3_VADD(x4, x3), 0.54119611f);
            x[3] = DRMP3_VMUL_S(DRMP3_VSUB(x0, x5), 0.60134488f);
            x[5] = DRMP3_VMUL_S(DRMP3_VADD(x0, x5), 0.89997619f);
            x[6] = DRMP3_VMUL_S(DRMP3_VSUB(x4, x3), 1.30656302f);
            x[7] = DRMP3_VMUL_S(DRMP3_VSUB(xt, x7), 2.56291556f);
        }
        if (k > n - 3)
        {
#if DRMP3_HAVE_SSE
#define DRMP3_VSAVE2(i, v) _mm_storel_pi((__m64 *)(void*)&y[i*18], v)
#else
#define DRMP3_VSAVE2(i, v) vst1_f32((float32_t *)&y[i*18],  vget_low_f32(v))
#endif
	  assert(0);
	  for (i = 0; i < 7; i++, y += 4*18)
            {
                drmp3_f4 s = DRMP3_VADD(t[3][i], t[3][i + 1]);
                DRMP3_VSAVE2(0, t[0][i]);
                DRMP3_VSAVE2(1, DRMP3_VADD(t[2][i], s));
                DRMP3_VSAVE2(2, DRMP3_VADD(t[1][i], t[1][i + 1]));
                DRMP3_VSAVE2(3, DRMP3_VADD(t[2][1 + i], s));
            }
            DRMP3_VSAVE2(0, t[0][7]);
            DRMP3_VSAVE2(1, DRMP3_VADD(t[2][7], t[3][7]));
            DRMP3_VSAVE2(2, t[1][7]);
            DRMP3_VSAVE2(3, t[3][7]);
        } else
        {
#define DRMP3_VSAVE4(i, v) DRMP3_VSTORE(&y[i*18], v)
  assert(0);
	  for (i = 0; i < 7; i++, y += 4*18)
            {
                drmp3_f4 s = DRMP3_VADD(t[3][i], t[3][i + 1]);
                DRMP3_VSAVE4(0, t[0][i]);
                DRMP3_VSAVE4(1, DRMP3_VADD(t[2][i], s));
                DRMP3_VSAVE4(2, DRMP3_VADD(t[1][i], t[1][i + 1]));
                DRMP3_VSAVE4(3, DRMP3_VADD(t[2][1 + i], s));
            }
            DRMP3_VSAVE4(0, t[0][7]);
            DRMP3_VSAVE4(1, DRMP3_VADD(t[2][7], t[3][7]));
            DRMP3_VSAVE4(2, t[1][7]);
            DRMP3_VSAVE4(3, t[3][7]);
        }
    } else
#endif
#ifdef DR_MP3_ONLY_SIMD
    {}
#else
    for (; k < n; k++)
    {

        float t[4][8], *x, *y = grbuf + k; 	FADD(__LINE__, 1);
        for (x = t[0], i = 0; i < 8; i++, x++)        	// 8
        {
	  IADD(__LINE__, 1);
	  float x0 = y[i*18];  ITIMES(__LINE__, 1);   
	  float x1 = y[(15 - i)*18];   IADD(__LINE__, 1); ITIMES(__LINE__, 1);
	  float x2 = y[(16 + i)*18]; IADD(__LINE__, 1); ITIMES(__LINE__, 1);
	  float x3 = y[(31 - i)*18]; IADD(__LINE__, 1); ITIMES(__LINE__, 1);
	  float t0 = x0 + x3;  FADD(__LINE__,1);
	  float t1 = x1 + x2; FADD(__LINE__,1);
	  float t2 = (x1 - x2)*g_sec[3*i + 0];  FADD(__LINE__,1);  FTIMES(__LINE__,1);
	  float t3 = (x0 - x3)*g_sec[3*i + 1];  FADD(__LINE__,1);  FTIMES(__LINE__,1);
	  x[0] = t0 + t1;  FADD(__LINE__,1);
	  x[8] = (t0 - t1)*g_sec[3*i + 2];      FADD(__LINE__,1);  FTIMES(__LINE__,1);
	  x[16] = t3 + t2; FADD(__LINE__,1); 
	  x[24] = (t3 - t2)*g_sec[3*i + 2];  FADD(__LINE__,1);  FTIMES(__LINE__,1);
        }
        for (x = t[0], i = 0; i < 4; i++, x += 8) // Four
        {

            float x0 = x[0], x1 = x[1], x2 = x[2], x3 = x[3], x4 = x[4], x5 = x[5], x6 = x[6], x7 = x[7], xt;
            xt = x0 - x7; x0 += x7;     FADD(__LINE__, 2);
            x7 = x1 - x6; x1 += x6;     FADD(__LINE__, 2);
            x6 = x2 - x5; x2 += x5;      FADD(__LINE__, 2);
            x5 = x3 - x4; x3 += x4;    FADD(__LINE__, 2);
            x4 = x0 - x3; x0 += x3;    FADD(__LINE__, 2);
            x3 = x1 - x2; x1 += x2;     FADD(__LINE__, 2);
            x[0] = x0 + x1;              FADD(__LINE__, 1);
            x[4] = (x0 - x1)*0.70710677f;     FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x5 =  x5 + x6;                       FADD(__LINE__, 1);
            x6 = (x6 + x7)*0.70710677f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x7 =  x7 + xt;                  FADD(__LINE__, 1);
            x3 = (x3 + x4)*0.70710677f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x5 -= x7*0.198912367f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x7 += x5*0.382683432f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x5 -= x7*0.198912367f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x0 = xt - x6; xt += x6;   FADD(__LINE__, 2); 
            x[1] = (xt + x7)*0.50979561f;   FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x[2] = (x4 + x3)*0.54119611f; FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x[3] = (x0 - x5)*0.60134488f; FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x[5] = (x0 + x5)*0.89997619f; FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x[6] = (x4 - x3)*1.30656302f; FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
            x[7] = (xt - x7)*2.56291556f; FADD(__LINE__, 1);    FTIMES(__LINE__, 1);
        }
        for (i = 0; i < 7; i++, y += 4*18) // 7
        {
	  y[0*18] = t[0][i];  
	  y[1*18] = t[2][i] + t[3][i] + t[3][i + 1];  FADD(__LINE__, 2);
	  y[2*18] = t[1][i] + t[1][i + 1];   FADD(__LINE__, 2);
	  y[3*18] = t[2][i + 1] + t[3][i] + t[3][i + 1];  FADD(__LINE__, 2);
        }
        y[0*18] = t[0][7];
        y[1*18] = t[2][7] + t[3][7]; 	  FADD(__LINE__, 1);
        y[2*18] = t[1][7];
        y[3*18] = t[3][7];
    }
#endif
}



MA_API ma_device_config ma_device_config_init(ma_device_type deviceType)
{
    ma_device_config config;
    MA_ZERO_OBJECT(&config);
    config.deviceType = deviceType;

    /* Resampling defaults. We must never use the Speex backend by default because it uses licensed third party code. */
    config.resampling.algorithm       = ma_resample_algorithm_linear;
    config.resampling.linear.lpfOrder = ma_min(MA_DEFAULT_RESAMPLER_LPF_ORDER, MA_MAX_FILTER_ORDER);
    config.resampling.speex.quality   = 3;

    return config;
}


static ma_thread_result MA_THREADCALL ma_worker_thread(void* pData)
{
    ma_device* pDevice = (ma_device*)pData;
    MA_ASSERT(pDevice != NULL);

#ifdef MA_WIN32
    ma_CoInitializeEx(pDevice->pContext, NULL, MA_COINIT_VALUE);
#endif

    /*
    When the device is being initialized it's initial state is set to MA_STATE_UNINITIALIZED. Before returning from
    ma_device_init(), the state needs to be set to something valid. In miniaudio the device's default state immediately
    after initialization is stopped, so therefore we need to mark the device as such. miniaudio will wait on the worker
    thread to signal an event to know when the worker thread is ready for action.
    */
    ma_device__set_state(pDevice, MA_STATE_STOPPED);
    ma_event_signal(&pDevice->stopEvent);

    for (;;) {  /* <-- This loop just keeps the thread alive. The main audio loop is inside. */
        ma_stop_proc onStop;

        /* We wait on an event to know when something has requested that the device be started and the main loop entered. */
        ma_event_wait(&pDevice->wakeupEvent);

        /* Default result code. */
        pDevice->workResult = MA_SUCCESS;

        /* If the reason for the wake up is that we are terminating, just break from the loop. */
        if (ma_device__get_state(pDevice) == MA_STATE_UNINITIALIZED) {
            break;
        }

        /*
        Getting to this point means the device is wanting to get started. The function that has requested that the device
        be started will be waiting on an event (pDevice->startEvent) which means we need to make sure we signal the event
        in both the success and error case. It's important that the state of the device is set _before_ signaling the event.
        */
        MA_ASSERT(ma_device__get_state(pDevice) == MA_STATE_STARTING);

        /* Make sure the state is set appropriately. */
        ma_device__set_state(pDevice, MA_STATE_STARTED);
        ma_event_signal(&pDevice->startEvent);

        if (pDevice->pContext->onDeviceMainLoop != NULL) {
            pDevice->pContext->onDeviceMainLoop(pDevice);
        } else {
            ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "No main loop implementation.", MA_API_NOT_FOUND);
        }

        /*
        Getting here means we have broken from the main loop which happens the application has requested that device be stopped. Note that this
        may have actually already happened above if the device was lost and miniaudio has attempted to re-initialize the device. In this case we
        don't want to be doing this a second time.
        */
        if (ma_device__get_state(pDevice) != MA_STATE_UNINITIALIZED) {
            if (pDevice->pContext->onDeviceStop) {
                pDevice->pContext->onDeviceStop(pDevice);
            }
        }

        /* After the device has stopped, make sure an event is posted. */
        onStop = pDevice->onStop;
        if (onStop) {
            onStop(pDevice);
        }

        /*
        A function somewhere is waiting for the device to have stopped for real so we need to signal an event to allow it to continue. Note that
        it's possible that the device has been uninitialized which means we need to _not_ change the status to stopped. We cannot go from an
        uninitialized state to stopped state.
        */
        if (ma_device__get_state(pDevice) != MA_STATE_UNINITIALIZED) {
            ma_device__set_state(pDevice, MA_STATE_STOPPED);
            ma_event_signal(&pDevice->stopEvent);
        }
    }

    /* Make sure we aren't continuously waiting on a stop event. */
    ma_event_signal(&pDevice->stopEvent);  /* <-- Is this still needed? */

#ifdef MA_WIN32
    ma_CoUninitialize(pDevice->pContext);
#endif

    return (ma_thread_result)0;
}


MA_API ma_result ma_device_init(ma_context* pContext, const ma_device_config* pConfig, ma_device* pDevice)
{
    ma_result result;
    ma_device_config config;

    if (pContext == NULL) {
        return ma_device_init_ex(NULL, 0, NULL, pConfig, pDevice);
    }
    if (pDevice == NULL) {
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with invalid arguments (pDevice == NULL).", MA_INVALID_ARGS);
    }
    if (pConfig == NULL) {
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with invalid arguments (pConfig == NULL).", MA_INVALID_ARGS);
    }

    /* We need to make a copy of the config so we can set default values if they were left unset in the input config. */
    config = *pConfig;

    /* Basic config validation. */
    if (config.deviceType != ma_device_type_playback && config.deviceType != ma_device_type_capture && config.deviceType != ma_device_type_duplex && config.deviceType != ma_device_type_loopback) {
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with an invalid config. Device type is invalid. Make sure the device type has been set in the config.", MA_INVALID_DEVICE_CONFIG);
    }

    if (config.deviceType == ma_device_type_capture || config.deviceType == ma_device_type_duplex) {
        if (config.capture.channels > MA_MAX_CHANNELS) {
            return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with an invalid config. Capture channel count cannot exceed 32.", MA_INVALID_DEVICE_CONFIG);
        }
        if (!ma__is_channel_map_valid(config.capture.channelMap, config.capture.channels)) {
            return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with invalid config. Capture channel map is invalid.", MA_INVALID_DEVICE_CONFIG);
        }
    }

    if (config.deviceType == ma_device_type_playback || config.deviceType == ma_device_type_duplex || config.deviceType == ma_device_type_loopback) {
        if (config.playback.channels > MA_MAX_CHANNELS) {
            return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with an invalid config. Playback channel count cannot exceed 32.", MA_INVALID_DEVICE_CONFIG);
        }
        if (!ma__is_channel_map_valid(config.playback.channelMap, config.playback.channels)) {
            return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "ma_device_init() called with invalid config. Playback channel map is invalid.", MA_INVALID_DEVICE_CONFIG);
        }
    }


    MA_ZERO_OBJECT(pDevice);
    pDevice->pContext = pContext;

    /* Set the user data and log callback ASAP to ensure it is available for the entire initialization process. */
    pDevice->pUserData = config.pUserData;
    pDevice->onData = config.dataCallback;
    pDevice->onStop = config.stopCallback;

    if (((ma_uintptr)pDevice % sizeof(pDevice)) != 0) {
        if (pContext->logCallback) {
            pContext->logCallback(pContext, pDevice, MA_LOG_LEVEL_WARNING, "WARNING: ma_device_init() called for a device that is not properly aligned. Thread safety is not supported.");
        }
    }

    pDevice->noPreZeroedOutputBuffer = config.noPreZeroedOutputBuffer;
    pDevice->noClip = config.noClip;
    pDevice->masterVolumeFactor = 1;

    /*
    When passing in 0 for the format/channels/rate/chmap it means the device will be using whatever is chosen by the backend. If everything is set
    to defaults it means the format conversion pipeline will run on a fast path where data transfer is just passed straight through to the backend.
    */
    if (config.sampleRate == 0) {
        config.sampleRate = MA_DEFAULT_SAMPLE_RATE;
        pDevice->usingDefaultSampleRate = MA_TRUE;
    }

    if (config.capture.format == ma_format_unknown) {
        config.capture.format = MA_DEFAULT_FORMAT;
        pDevice->capture.usingDefaultFormat = MA_TRUE;
    }
    if (config.capture.channels == 0) {
        config.capture.channels = MA_DEFAULT_CHANNELS;
        pDevice->capture.usingDefaultChannels = MA_TRUE;
    }
    if (config.capture.channelMap[0] == MA_CHANNEL_NONE) {
        pDevice->capture.usingDefaultChannelMap = MA_TRUE;
    }

    if (config.playback.format == ma_format_unknown) {
        config.playback.format = MA_DEFAULT_FORMAT;
        pDevice->playback.usingDefaultFormat = MA_TRUE;
    }
    if (config.playback.channels == 0) {
        config.playback.channels = MA_DEFAULT_CHANNELS;
        pDevice->playback.usingDefaultChannels = MA_TRUE;
    }
    if (config.playback.channelMap[0] == MA_CHANNEL_NONE) {
        pDevice->playback.usingDefaultChannelMap = MA_TRUE;
    }


    /* Default periods. */
    if (config.periods == 0) {
        config.periods = MA_DEFAULT_PERIODS;
        pDevice->usingDefaultPeriods = MA_TRUE;
    }

    /*
    Must have at least 3 periods for full-duplex mode. The idea is that the playback and capture positions hang out in the middle period, with the surrounding
    periods acting as a buffer in case the capture and playback devices get's slightly out of sync.
    */
    if (config.deviceType == ma_device_type_duplex && config.periods < 3) {
        config.periods = 3;
    }

    /* Default buffer size. */
    if (config.periodSizeInMilliseconds == 0 && config.periodSizeInFrames == 0) {
        config.periodSizeInMilliseconds = (config.performanceProfile == ma_performance_profile_low_latency) ? MA_DEFAULT_PERIOD_SIZE_IN_MILLISECONDS_LOW_LATENCY : MA_DEFAULT_PERIOD_SIZE_IN_MILLISECONDS_CONSERVATIVE;
        pDevice->usingDefaultBufferSize = MA_TRUE;
    }
    


    pDevice->type = config.deviceType;
    pDevice->sampleRate = config.sampleRate;
    pDevice->resampling.algorithm       = config.resampling.algorithm;
    pDevice->resampling.linear.lpfOrder = config.resampling.linear.lpfOrder;
    pDevice->resampling.speex.quality   = config.resampling.speex.quality;

    pDevice->capture.shareMode   = config.capture.shareMode;
    pDevice->capture.format      = config.capture.format;
    pDevice->capture.channels    = config.capture.channels;
    ma_channel_map_copy(pDevice->capture.channelMap, config.capture.channelMap, config.capture.channels);

    pDevice->playback.shareMode  = config.playback.shareMode;
    pDevice->playback.format     = config.playback.format;
    pDevice->playback.channels   = config.playback.channels;
    ma_channel_map_copy(pDevice->playback.channelMap, config.playback.channelMap, config.playback.channels);


    /* The internal format, channel count and sample rate can be modified by the backend. */
    pDevice->capture.internalFormat      = pDevice->capture.format;
    pDevice->capture.internalChannels    = pDevice->capture.channels;
    pDevice->capture.internalSampleRate  = pDevice->sampleRate;
    ma_channel_map_copy(pDevice->capture.internalChannelMap, pDevice->capture.channelMap, pDevice->capture.channels);

    pDevice->playback.internalFormat     = pDevice->playback.format;
    pDevice->playback.internalChannels   = pDevice->playback.channels;
    pDevice->playback.internalSampleRate = pDevice->sampleRate;
    ma_channel_map_copy(pDevice->playback.internalChannelMap, pDevice->playback.channelMap, pDevice->playback.channels);
    
    result = ma_mutex_init(&pDevice->lock);
    if (result != MA_SUCCESS) {
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "Failed to create mutex.", result);
    }

    /*
    When the device is started, the worker thread is the one that does the actual startup of the backend device. We
    use a semaphore to wait for the background thread to finish the work. The same applies for stopping the device.
    
    Each of these semaphores is released internally by the worker thread when the work is completed. The start
    semaphore is also used to wake up the worker thread.
    */
    result = ma_event_init(&pDevice->wakeupEvent);
    if (result != MA_SUCCESS) {
        ma_mutex_uninit(&pDevice->lock);
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "Failed to create worker thread wakeup event.", result);
    }

    result = ma_event_init(&pDevice->startEvent);
    if (result != MA_SUCCESS) {
        ma_event_uninit(&pDevice->wakeupEvent);
        ma_mutex_uninit(&pDevice->lock);
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "Failed to create worker thread start event.", result);
    }

    result = ma_event_init(&pDevice->stopEvent);
    if (result != MA_SUCCESS) {
        ma_event_uninit(&pDevice->startEvent);
        ma_event_uninit(&pDevice->wakeupEvent);
        ma_mutex_uninit(&pDevice->lock);
        return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "Failed to create worker thread stop event.", result);
    }

    assert(pContext->onDeviceInit);
    result = pContext->onDeviceInit(pContext, &config, pDevice);
    if (result != MA_SUCCESS) {
        return result;
    }

    ma_device__post_init_setup(pDevice, pConfig->deviceType);


    /* If the backend did not fill out a name for the device, try a generic method. */
    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        if (pDevice->capture.name[0] == '\0') {
            if (ma_context__try_get_device_name_by_id(pContext, ma_device_type_capture, config.capture.pDeviceID, pDevice->capture.name, sizeof(pDevice->capture.name)) != MA_SUCCESS) {
	      ma_strncpy_s(pDevice->capture.name, sizeof(pDevice->capture.name), (config.capture.pDeviceID == NULL)  ? MA_DEFAULT_CAPTURE_DEVICE_NAME : "Capture Device", (size_t)-1);
            }
        }
    }
    if (pDevice->type == ma_device_type_playback || pDevice->type == ma_device_type_duplex || pDevice->type == ma_device_type_loopback) {
        if (pDevice->playback.name[0] == '\0') {
            if (ma_context__try_get_device_name_by_id(pContext, ma_device_type_playback, config.playback.pDeviceID, pDevice->playback.name, sizeof(pDevice->playback.name)) != MA_SUCCESS) {
	      ma_strncpy_s(pDevice->playback.name, sizeof(pDevice->playback.name), (config.playback.pDeviceID == NULL)  ? MA_DEFAULT_PLAYBACK_DEVICE_NAME : "Playback Device", (size_t)-1);
            }
        }
    }


    /* Some backends don't require the worker thread. */
    if (!ma_context_is_backend_asynchronous(pContext)) {
        /* The worker thread. */
        result = ma_thread_create(&pDevice->thread, pContext->threadPriority, pContext->threadStackSize, ma_worker_thread, pDevice);
        if (result != MA_SUCCESS) {
            ma_device_uninit(pDevice);
            return ma_context_post_error(pContext, NULL, MA_LOG_LEVEL_ERROR, "Failed to create worker thread.", result);
        }

        /* Wait for the worker thread to put the device into it's stopped state for real. */
        ma_event_wait(&pDevice->stopEvent);
    } else {
        ma_device__set_state(pDevice, MA_STATE_STOPPED);
    }


    ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "[%s]", ma_get_backend_name(pDevice->pContext->backend));
    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "  %s (%s)", pDevice->capture.name, "Capture");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Format:      %s -> %s", ma_get_format_name(pDevice->capture.format), ma_get_format_name(pDevice->capture.internalFormat));
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Channels:    %d -> %d", pDevice->capture.channels, pDevice->capture.internalChannels);
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Sample Rate: %d -> %d", pDevice->sampleRate, pDevice->capture.internalSampleRate);
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Buffer Size: %d*%d (%d)", pDevice->capture.internalPeriodSizeInFrames, pDevice->capture.internalPeriods, (pDevice->capture.internalPeriodSizeInFrames * pDevice->capture.internalPeriods));
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Conversion:");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Pre Format Conversion:    %s", pDevice->capture.converter.hasPreFormatConversion  ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Post Format Conversion:   %s", pDevice->capture.converter.hasPostFormatConversion ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Channel Routing:          %s", pDevice->capture.converter.hasChannelConverter     ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Resampling:               %s", pDevice->capture.converter.hasResampler            ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Passthrough:              %s", pDevice->capture.converter.isPassthrough           ? "YES" : "NO");
    }
    if (pDevice->type == ma_device_type_playback || pDevice->type == ma_device_type_duplex) {
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "  %s (%s)", pDevice->playback.name, "Playback");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Format:      %s -> %s", ma_get_format_name(pDevice->playback.format), ma_get_format_name(pDevice->playback.internalFormat));
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Channels:    %d -> %d", pDevice->playback.channels, pDevice->playback.internalChannels);
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Sample Rate: %d -> %d", pDevice->sampleRate, pDevice->playback.internalSampleRate);
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Buffer Size: %d*%d (%d)", pDevice->playback.internalPeriodSizeInFrames, pDevice->playback.internalPeriods, (pDevice->playback.internalPeriodSizeInFrames * pDevice->playback.internalPeriods));
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "    Conversion:");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Pre Format Conversion:    %s", pDevice->playback.converter.hasPreFormatConversion  ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Post Format Conversion:   %s", pDevice->playback.converter.hasPostFormatConversion ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Channel Routing:          %s", pDevice->playback.converter.hasChannelConverter     ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Resampling:               %s", pDevice->playback.converter.hasResampler            ? "YES" : "NO");
        ma_post_log_messagef(pContext, pDevice, MA_LOG_LEVEL_INFO, "      Passthrough:              %s", pDevice->playback.converter.isPassthrough           ? "YES" : "NO");
    }

    MA_ASSERT(ma_device__get_state(pDevice) == MA_STATE_STOPPED);
    return MA_SUCCESS;
}


MA_API ma_uint64 ma_decoder_read_pcm_frames(ma_decoder* pDecoder, void* pFramesOut, ma_uint64 frameCount)
{
    ma_result result;
    ma_uint64 totalFramesReadOut;
    ma_uint64 totalFramesReadIn;
    void* pRunningFramesOut;
    
    if (pDecoder == NULL) {
        return 0;
    }

    if (pDecoder->onReadPCMFrames == NULL) {
        return 0;
    }

    /* Fast path. */
    if (pDecoder->converter.isPassthrough) {
      djg_stats();
      return pDecoder->onReadPCMFrames(pDecoder, pFramesOut, frameCount);
    }

    /*
    Getting here means we need to do data conversion. If we're seeking forward and are _not_ doing resampling we can run this in a fast path. If we're doing resampling we
    need to run through each sample because we need to ensure it's internal cache is updated.
    */
    if (pFramesOut == NULL && pDecoder->converter.hasResampler == MA_FALSE)
      {
	assert(0);
        return pDecoder->onReadPCMFrames(pDecoder, NULL, frameCount);   /* All decoder backends must support passing in NULL for the output buffer. */
      }

    /* Slow path. Need to run everything through the data converter. */
    totalFramesReadOut = 0;
    totalFramesReadIn  = 0;
    pRunningFramesOut  = pFramesOut;
    
    while (totalFramesReadOut < frameCount)
      {

        ma_uint8 pIntermediaryBuffer[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];  /* In internal format. */
        ma_uint64 intermediaryBufferCap = sizeof(pIntermediaryBuffer) / ma_get_bytes_per_frame(pDecoder->internalFormat, pDecoder->internalChannels);
        ma_uint64 framesToReadThisIterationIn;
        ma_uint64 framesReadThisIterationIn;
        ma_uint64 framesToReadThisIterationOut;
        ma_uint64 framesReadThisIterationOut;
        ma_uint64 requiredInputFrameCount;

        framesToReadThisIterationOut = (frameCount - totalFramesReadOut);
        framesToReadThisIterationIn = framesToReadThisIterationOut;
        if (framesToReadThisIterationIn > intermediaryBufferCap) {
            framesToReadThisIterationIn = intermediaryBufferCap;
        }

        requiredInputFrameCount = ma_data_converter_get_required_input_frame_count(&pDecoder->converter, framesToReadThisIterationOut);
        if (framesToReadThisIterationIn > requiredInputFrameCount) {
            framesToReadThisIterationIn = requiredInputFrameCount;
        }

        if (requiredInputFrameCount > 0) {
            framesReadThisIterationIn = pDecoder->onReadPCMFrames(pDecoder, pIntermediaryBuffer, framesToReadThisIterationIn);
            totalFramesReadIn += framesReadThisIterationIn;
        }

        /*
        At this point we have our decoded data in input format and now we need to convert to output format. Note that even if we didn't read any
        input frames, we still want to try processing frames because there may some output frames generated from cached input data.
        */
        framesReadThisIterationOut = framesToReadThisIterationOut;
        result = ma_data_converter_process_pcm_frames(&pDecoder->converter, pIntermediaryBuffer, &framesReadThisIterationIn, pRunningFramesOut, &framesReadThisIterationOut);
        if (result != MA_SUCCESS) {
            break;
        }

        totalFramesReadOut += framesReadThisIterationOut;

        if (pRunningFramesOut != NULL) {
            pRunningFramesOut = ma_offset_ptr(pRunningFramesOut, framesReadThisIterationOut * ma_get_bytes_per_frame(pDecoder->outputFormat, pDecoder->outputChannels));
        }

        if (framesReadThisIterationIn == 0 && framesReadThisIterationOut == 0) {
            break;  /* We're done. */
        }
    }

    return totalFramesReadOut;
}


ma_result ma_device__post_init_setup(ma_device* pDevice, ma_device_type deviceType)
{
    ma_result result;

    MA_ASSERT(pDevice != NULL);

    if (deviceType == ma_device_type_capture || deviceType == ma_device_type_duplex) {
        if (pDevice->capture.usingDefaultFormat) {
            pDevice->capture.format = pDevice->capture.internalFormat;
        }
        if (pDevice->capture.usingDefaultChannels) {
            pDevice->capture.channels = pDevice->capture.internalChannels;
        }
        if (pDevice->capture.usingDefaultChannelMap) {
            if (pDevice->capture.internalChannels == pDevice->capture.channels) {
                ma_channel_map_copy(pDevice->capture.channelMap, pDevice->capture.internalChannelMap, pDevice->capture.channels);
            } else {
                ma_get_standard_channel_map(ma_standard_channel_map_default, pDevice->capture.channels, pDevice->capture.channelMap);
            }
        }
    }

    if (deviceType == ma_device_type_playback || deviceType == ma_device_type_duplex) {
        if (pDevice->playback.usingDefaultFormat) {
            pDevice->playback.format = pDevice->playback.internalFormat;
        }
        if (pDevice->playback.usingDefaultChannels) {
            pDevice->playback.channels = pDevice->playback.internalChannels;
        }
        if (pDevice->playback.usingDefaultChannelMap) {
            if (pDevice->playback.internalChannels == pDevice->playback.channels) {
                ma_channel_map_copy(pDevice->playback.channelMap, pDevice->playback.internalChannelMap, pDevice->playback.channels);
            } else {
                ma_get_standard_channel_map(ma_standard_channel_map_default, pDevice->playback.channels, pDevice->playback.channelMap);
            }
        }
    }

    if (pDevice->usingDefaultSampleRate) {
        if (deviceType == ma_device_type_capture || deviceType == ma_device_type_duplex) {
            pDevice->sampleRate = pDevice->capture.internalSampleRate;
        } else {
            pDevice->sampleRate = pDevice->playback.internalSampleRate;
        }
    }

    /* PCM converters. */
    if (deviceType == ma_device_type_capture || deviceType == ma_device_type_duplex || deviceType == ma_device_type_loopback) {
        /* Converting from internal device format to client format. */
        ma_data_converter_config converterConfig = ma_data_converter_config_init_default();
        converterConfig.formatIn                   = pDevice->capture.internalFormat;
        converterConfig.channelsIn                 = pDevice->capture.internalChannels;
        converterConfig.sampleRateIn               = pDevice->capture.internalSampleRate;
        ma_channel_map_copy(converterConfig.channelMapIn, pDevice->capture.internalChannelMap, pDevice->capture.internalChannels);
        converterConfig.formatOut                  = pDevice->capture.format;
        converterConfig.channelsOut                = pDevice->capture.channels;
        converterConfig.sampleRateOut              = pDevice->sampleRate;
        ma_channel_map_copy(converterConfig.channelMapOut, pDevice->capture.channelMap, pDevice->capture.channels);
        converterConfig.resampling.allowDynamicSampleRate = MA_FALSE;
        converterConfig.resampling.algorithm       = pDevice->resampling.algorithm;
        converterConfig.resampling.linear.lpfOrder = pDevice->resampling.linear.lpfOrder;
        converterConfig.resampling.speex.quality   = pDevice->resampling.speex.quality;

        result = ma_data_converter_init(&converterConfig, &pDevice->capture.converter);
        if (result != MA_SUCCESS) {
            return result;
        }
    }

    if (deviceType == ma_device_type_playback || deviceType == ma_device_type_duplex) {
        /* Converting from client format to device format. */
        ma_data_converter_config converterConfig = ma_data_converter_config_init_default();
        converterConfig.formatIn                   = pDevice->playback.format;
        converterConfig.channelsIn                 = pDevice->playback.channels;
        converterConfig.sampleRateIn               = pDevice->sampleRate;
        ma_channel_map_copy(converterConfig.channelMapIn, pDevice->playback.channelMap, pDevice->playback.channels);
        converterConfig.formatOut                  = pDevice->playback.internalFormat;
        converterConfig.channelsOut                = pDevice->playback.internalChannels;
        converterConfig.sampleRateOut              = pDevice->playback.internalSampleRate;
        ma_channel_map_copy(converterConfig.channelMapOut, pDevice->playback.internalChannelMap, pDevice->playback.internalChannels);
        converterConfig.resampling.allowDynamicSampleRate = MA_FALSE;
        converterConfig.resampling.algorithm       = pDevice->resampling.algorithm;
        converterConfig.resampling.linear.lpfOrder = pDevice->resampling.linear.lpfOrder;
        converterConfig.resampling.speex.quality   = pDevice->resampling.speex.quality;

        result = ma_data_converter_init(&converterConfig, &pDevice->playback.converter);
        if (result != MA_SUCCESS) {
            return result;
        }
    }

    return MA_SUCCESS;
}
ma_bool32 ma_channel_map_valid(ma_uint32 channels, const ma_channel channelMap[MA_MAX_CHANNELS])
{
    if (channelMap == NULL) {
        return MA_FALSE;
    }

    /* A channel count of 0 is invalid. */
    if (channels == 0) {
        return MA_FALSE;
    }

    /* It does not make sense to have a mono channel when there is more than 1 channel. */
    if (channels > 1) {
        ma_uint32 iChannel;
        for (iChannel = 0; iChannel < channels; ++iChannel) {
            if (channelMap[iChannel] == MA_CHANNEL_MONO) {
                return MA_FALSE;
            }
        }
    }

    return MA_TRUE;
}

ma_bool32 ma_channel_map_equal(ma_uint32 channels, const ma_channel channelMapA[MA_MAX_CHANNELS], const ma_channel channelMapB[MA_MAX_CHANNELS])
{
    ma_uint32 iChannel;

    if (channelMapA == channelMapB) {
        return MA_FALSE;
    }

    if (channels == 0 || channels > MA_MAX_CHANNELS) {
        return MA_FALSE;
    }

    for (iChannel = 0; iChannel < channels; ++iChannel) {
        if (channelMapA[iChannel] != channelMapB[iChannel]) {
            return MA_FALSE;
        }
    }

    return MA_TRUE;
}


MA_API ma_bool32 ma_channel_map_contains_channel_position(ma_uint32 channels, const ma_channel channelMap[MA_MAX_CHANNELS], ma_channel channelPosition)
{
    ma_uint32 iChannel;
    for (iChannel = 0; iChannel < channels; ++iChannel) {
        if (channelMap[iChannel] == channelPosition) {
            return MA_TRUE;
        }
    }

    return MA_FALSE;
}


ma_int32 ma_channel_converter_float_to_fixed(float x)
{
    return (ma_int32)(x * (1<<MA_CHANNEL_CONVERTER_FIXED_POINT_SHIFT));
}


MA_API ma_result ma_channel_converter_init(const ma_channel_converter_config* pConfig, ma_channel_converter* pConverter)
{
    ma_uint32 iChannelIn;
    ma_uint32 iChannelOut;

    if (pConverter == NULL) {
        return MA_INVALID_ARGS;
    }

    MA_ZERO_OBJECT(pConverter);

    if (pConfig == NULL) {
        return MA_INVALID_ARGS;
    }

    if (!ma_channel_map_valid(pConfig->channelsIn, pConfig->channelMapIn)) {
        return MA_INVALID_ARGS; /* Invalid input channel map. */
    }
    if (!ma_channel_map_valid(pConfig->channelsOut, pConfig->channelMapOut)) {
        return MA_INVALID_ARGS; /* Invalid output channel map. */
    }

    pConverter->format      = pConfig->format;
    pConverter->channelsIn  = pConfig->channelsIn;
    pConverter->channelsOut = pConfig->channelsOut;
    ma_channel_map_copy(pConverter->channelMapIn,  pConfig->channelMapIn,  pConfig->channelsIn);
    ma_channel_map_copy(pConverter->channelMapOut, pConfig->channelMapOut, pConfig->channelsOut);
    pConverter->mixingMode  = pConfig->mixingMode;

    for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; iChannelIn += 1) {
        for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
            if (pConverter->format == ma_format_f32) {
                pConverter->weights.f32[iChannelIn][iChannelOut] = pConfig->weights[iChannelIn][iChannelOut];
            } else {
                pConverter->weights.s16[iChannelIn][iChannelOut] = ma_channel_converter_float_to_fixed(pConfig->weights[iChannelIn][iChannelOut]);
            }
        }
    }
    


    /* If the input and output channels and channel maps are the same we should use a passthrough. */
    if (pConverter->channelsIn == pConverter->channelsOut) {
        if (ma_channel_map_equal(pConverter->channelsIn, pConverter->channelMapIn, pConverter->channelMapOut)) {
            pConverter->isPassthrough = MA_TRUE;
        }
	
        if (ma_channel_map_blank(pConverter->channelsIn, pConverter->channelMapIn) || ma_channel_map_blank(pConverter->channelsOut, pConverter->channelMapOut)) {
            pConverter->isPassthrough = MA_TRUE;
        }
    }


    /*
    We can use a simple case for expanding the mono channel. This will used when expanding a mono input into any output so long
    as no LFE is present in the output.
    */
    if (!pConverter->isPassthrough) {
        if (pConverter->channelsIn == 1 && pConverter->channelMapIn[0] == MA_CHANNEL_MONO) {
            /* Optimal case if no LFE is in the output channel map. */
            pConverter->isSimpleMonoExpansion = MA_TRUE;
            if (ma_channel_map_contains_channel_position(pConverter->channelsOut, pConverter->channelMapOut, MA_CHANNEL_LFE)) {
                pConverter->isSimpleMonoExpansion = MA_FALSE;
            }
        }
    }

    /* Another optimized case is stereo to mono. */
    if (!pConverter->isPassthrough) {
        if (pConverter->channelsOut == 1 && pConverter->channelMapOut[0] == MA_CHANNEL_MONO && pConverter->channelsIn == 2) {
            /* Optimal case if no LFE is in the input channel map. */
            pConverter->isStereoToMono = MA_TRUE;
            if (ma_channel_map_contains_channel_position(pConverter->channelsIn, pConverter->channelMapIn, MA_CHANNEL_LFE)) {
                pConverter->isStereoToMono = MA_FALSE;
            }
        }
    }


    /*
    Here is where we do a bit of pre-processing to know how each channel should be combined to make up the output. Rules:
    
        1) If it's a passthrough, do nothing - it's just a simple memcpy().
        2) If the channel counts are the same and every channel position in the input map is present in the output map, use a
           simple shuffle. An example might be different 5.1 channel layouts.
        3) Otherwise channels are blended based on spatial locality.
    */
    if (!pConverter->isPassthrough) {
        if (pConverter->channelsIn == pConverter->channelsOut) {
            ma_bool32 areAllChannelPositionsPresent = MA_TRUE;
            for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
                ma_bool32 isInputChannelPositionInOutput = MA_FALSE;
                for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                    if (pConverter->channelMapIn[iChannelIn] == pConverter->channelMapOut[iChannelOut]) {
                        isInputChannelPositionInOutput = MA_TRUE;
                        break;
                    }
                }

                if (!isInputChannelPositionInOutput) {
                    areAllChannelPositionsPresent = MA_FALSE;
                    break;
                }
            }

            if (areAllChannelPositionsPresent) {
                pConverter->isSimpleShuffle = MA_TRUE;

                /*
                All the router will be doing is rearranging channels which means all we need to do is use a shuffling table which is just
                a mapping between the index of the input channel to the index of the output channel.
                */
                for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
                    for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                        if (pConverter->channelMapIn[iChannelIn] == pConverter->channelMapOut[iChannelOut]) {
                            pConverter->shuffleTable[iChannelIn] = (ma_uint8)iChannelOut;
                            break;
                        }
                    }
                }
            }
        }
    }


    /*
    Here is where weights are calculated. Note that we calculate the weights at all times, even when using a passthrough and simple
    shuffling. We use different algorithms for calculating weights depending on our mixing mode.
    
    In simple mode we don't do any blending (except for converting between mono, which is done in a later step). Instead we just
    map 1:1 matching channels. In this mode, if no channels in the input channel map correspond to anything in the output channel
    map, nothing will be heard!
    */

    /* In all cases we need to make sure all channels that are present in both channel maps have a 1:1 mapping. */
    for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
        ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

        for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
            ma_channel channelPosOut = pConverter->channelMapOut[iChannelOut];

            if (channelPosIn == channelPosOut) {
                if (pConverter->format == ma_format_f32) {
                    pConverter->weights.f32[iChannelIn][iChannelOut] = 1;
                } else {
                    pConverter->weights.s16[iChannelIn][iChannelOut] = (1 << MA_CHANNEL_CONVERTER_FIXED_POINT_SHIFT);
                }
            }
        }
    }

    /*
    The mono channel is accumulated on all other channels, except LFE. Make sure in this loop we exclude output mono channels since
    they were handled in the pass above.
    */
    for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
        ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

        if (channelPosIn == MA_CHANNEL_MONO) {
            for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                ma_channel channelPosOut = pConverter->channelMapOut[iChannelOut];

                if (channelPosOut != MA_CHANNEL_NONE && channelPosOut != MA_CHANNEL_MONO && channelPosOut != MA_CHANNEL_LFE) {
                    if (pConverter->format == ma_format_f32) {
                        pConverter->weights.f32[iChannelIn][iChannelOut] = 1;
                    } else {
                        pConverter->weights.s16[iChannelIn][iChannelOut] = (1 << MA_CHANNEL_CONVERTER_FIXED_POINT_SHIFT);
                    }
                }
            }
        }
    }

    /* The output mono channel is the average of all non-none, non-mono and non-lfe input channels. */
    {
        ma_uint32 len = 0;
        for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
            ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

            if (channelPosIn != MA_CHANNEL_NONE && channelPosIn != MA_CHANNEL_MONO && channelPosIn != MA_CHANNEL_LFE) {
                len += 1;
            }
        }

        if (len > 0) {
            float monoWeight = 1.0f / len;

            for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                ma_channel channelPosOut = pConverter->channelMapOut[iChannelOut];

                if (channelPosOut == MA_CHANNEL_MONO) {
                    for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
                        ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

                        if (channelPosIn != MA_CHANNEL_NONE && channelPosIn != MA_CHANNEL_MONO && channelPosIn != MA_CHANNEL_LFE) {
                            if (pConverter->format == ma_format_f32) {
                                pConverter->weights.f32[iChannelIn][iChannelOut] = monoWeight;
                            } else {
                                pConverter->weights.s16[iChannelIn][iChannelOut] = ma_channel_converter_float_to_fixed(monoWeight);
                            }
                        }
                    }
                }
            }
        }
    }


    /* Input and output channels that are not present on the other side need to be blended in based on spatial locality. */
    switch (pConverter->mixingMode)
    {
        case ma_channel_mix_mode_rectangular:
        {
            /* Unmapped input channels. */
            for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
                ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

                if (ma_is_spatial_channel_position(channelPosIn)) {
                    if (!ma_channel_map_contains_channel_position(pConverter->channelsOut, pConverter->channelMapOut, channelPosIn)) {
                        for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                            ma_channel channelPosOut = pConverter->channelMapOut[iChannelOut];

                            if (ma_is_spatial_channel_position(channelPosOut)) {
                                float weight = 0;
                                if (pConverter->mixingMode == ma_channel_mix_mode_rectangular) {
                                    weight = ma_calculate_channel_position_rectangular_weight(channelPosIn, channelPosOut);
                                }

                                /* Only apply the weight if we haven't already got some contribution from the respective channels. */
                                if (pConverter->format == ma_format_f32) {
                                    if (pConverter->weights.f32[iChannelIn][iChannelOut] == 0) {
                                        pConverter->weights.f32[iChannelIn][iChannelOut] = weight;
                                    }
                                } else {
                                    if (pConverter->weights.s16[iChannelIn][iChannelOut] == 0) {
                                        pConverter->weights.s16[iChannelIn][iChannelOut] = ma_channel_converter_float_to_fixed(weight);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            /* Unmapped output channels. */
            for (iChannelOut = 0; iChannelOut < pConverter->channelsOut; ++iChannelOut) {
                ma_channel channelPosOut = pConverter->channelMapOut[iChannelOut];

                if (ma_is_spatial_channel_position(channelPosOut)) {
                    if (!ma_channel_map_contains_channel_position(pConverter->channelsIn, pConverter->channelMapIn, channelPosOut)) {
                        for (iChannelIn = 0; iChannelIn < pConverter->channelsIn; ++iChannelIn) {
                            ma_channel channelPosIn = pConverter->channelMapIn[iChannelIn];

                            if (ma_is_spatial_channel_position(channelPosIn)) {
                                float weight = 0;
                                if (pConverter->mixingMode == ma_channel_mix_mode_rectangular) {
                                    weight = ma_calculate_channel_position_rectangular_weight(channelPosIn, channelPosOut);
                                }

                                /* Only apply the weight if we haven't already got some contribution from the respective channels. */
                                if (pConverter->format == ma_format_f32) {
                                    if (pConverter->weights.f32[iChannelIn][iChannelOut] == 0) {
                                        pConverter->weights.f32[iChannelIn][iChannelOut] = weight;
                                    }
                                } else {
                                    if (pConverter->weights.s16[iChannelIn][iChannelOut] == 0) {
                                        pConverter->weights.s16[iChannelIn][iChannelOut] = ma_channel_converter_float_to_fixed(weight);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } break;

        case ma_channel_mix_mode_custom_weights:
        case ma_channel_mix_mode_simple:
        default:
        {
            /* Fallthrough. */
        } break;
    }


    return MA_SUCCESS;
}


ma_result ma_data_converter_init(const ma_data_converter_config* pConfig, ma_data_converter* pConverter)
{
    ma_result result;
    ma_format midFormat;

    if (pConverter == NULL) {
        return MA_INVALID_ARGS;
    }

    MA_ZERO_OBJECT(pConverter);

    if (pConfig == NULL) {
        return MA_INVALID_ARGS;
    }

    pConverter->config = *pConfig;

    /* Basic validation. */
    if (pConfig->channelsIn < MA_MIN_CHANNELS || pConfig->channelsOut < MA_MIN_CHANNELS ||
        pConfig->channelsIn > MA_MAX_CHANNELS || pConfig->channelsOut > MA_MAX_CHANNELS) {
        return MA_INVALID_ARGS;
    }

    /*
    We want to avoid as much data conversion as possible. The channel converter and resampler both support s16 and f32 natively. We need to decide
    on the format to use for this stage. We call this the mid format because it's used in the middle stage of the conversion pipeline. If the output
    format is either s16 or f32 we use that one. If that is not the case it will do the same thing for the input format. If it's neither we just
    use f32.
    */
    /*  */ if (pConverter->config.formatOut == ma_format_s16 || pConverter->config.formatOut == ma_format_f32) {
        midFormat = pConverter->config.formatOut;
    } else if (pConverter->config.formatIn  == ma_format_s16 || pConverter->config.formatIn  == ma_format_f32) {
        midFormat = pConverter->config.formatIn;
    } else {
        midFormat = ma_format_f32;
    }

    /* Channel converter. We always initialize this, but we check if it configures itself as a passthrough to determine whether or not it's needed. */
    {
        ma_uint32 iChannelIn;
        ma_uint32 iChannelOut;
        ma_channel_converter_config channelConverterConfig;

        channelConverterConfig = ma_channel_converter_config_init(midFormat, pConverter->config.channelsIn, pConverter->config.channelMapIn, pConverter->config.channelsOut, pConverter->config.channelMapOut, pConverter->config.channelMixMode);
        
        /* Channel weights. */
        for (iChannelIn = 0; iChannelIn < pConverter->config.channelsIn; iChannelIn += 1) {
            for (iChannelOut = 0; iChannelOut < pConverter->config.channelsOut; iChannelOut += 1) {
                channelConverterConfig.weights[iChannelIn][iChannelOut] = pConverter->config.channelWeights[iChannelIn][iChannelOut];
            }
        }
        
        result = ma_channel_converter_init(&channelConverterConfig, &pConverter->channelConverter);
        if (result != MA_SUCCESS) {
            return result;
        }

        /* If the channel converter is not a passthrough we need to enable it. Otherwise we can skip it. */
        if (pConverter->channelConverter.isPassthrough == MA_FALSE) {
            pConverter->hasChannelConverter = MA_TRUE;
        }
    }


    /* Always enable dynamic sample rates if the input sample rate is different because we're always going to need a resampler in this case anyway. */
    if (pConverter->config.resampling.allowDynamicSampleRate == MA_FALSE) {
        pConverter->config.resampling.allowDynamicSampleRate = pConverter->config.sampleRateIn != pConverter->config.sampleRateOut;
    }

    /* Resampler. */
    if (pConverter->config.resampling.allowDynamicSampleRate) {
        ma_resampler_config resamplerConfig;
        ma_uint32 resamplerChannels;

        /* The resampler is the most expensive part of the conversion process, so we need to do it at the stage where the channel count is at it's lowest. */
        if (pConverter->config.channelsIn < pConverter->config.channelsOut) {
            resamplerChannels = pConverter->config.channelsIn;
        } else {
            resamplerChannels = pConverter->config.channelsOut;
        }
	assert(0);
        //resamplerConfig = ma_resampler_config_init(midFormat, resamplerChannels, pConverter->config.sampleRateIn, pConverter->config.sampleRateOut, pConverter->config.resampling.algorithm);
        resamplerConfig.linear.lpfOrder         = pConverter->config.resampling.linear.lpfOrder;
        resamplerConfig.linear.lpfNyquistFactor = pConverter->config.resampling.linear.lpfNyquistFactor;
        resamplerConfig.speex.quality           = pConverter->config.resampling.speex.quality;

        result = 0; //ma_resampler_init(&resamplerConfig, &pConverter->resampler);
        if (result != MA_SUCCESS) {
            return result;
        }

        pConverter->hasResampler = MA_TRUE;
    }


    /* We can simplify pre- and post-format conversion if we have neither channel conversion nor resampling. */
    if (pConverter->hasChannelConverter == MA_FALSE && pConverter->hasResampler == MA_FALSE) {
        /* We have neither channel conversion nor resampling so we'll only need one of pre- or post-format conversion, or none if the input and output formats are the same. */
        if (pConverter->config.formatIn == pConverter->config.formatOut) {
            /* The formats are the same so we can just pass through. */
            pConverter->hasPreFormatConversion  = MA_FALSE;
            pConverter->hasPostFormatConversion = MA_FALSE;
        } else {
            /* The formats are different so we need to do either pre- or post-format conversion. It doesn't matter which. */
            pConverter->hasPreFormatConversion  = MA_FALSE;
            pConverter->hasPostFormatConversion = MA_TRUE;
        }
    } else {
        /* We have a channel converter and/or resampler so we'll need channel conversion based on the mid format. */
        if (pConverter->config.formatIn != midFormat) {
            pConverter->hasPreFormatConversion = MA_TRUE;
        }
        if (pConverter->config.formatOut != midFormat) {
            pConverter->hasPostFormatConversion = MA_TRUE;
        }
    }

    /* We can enable passthrough optimizations if applicable. Note that we'll only be able to do this if the sample rate is static. */
    if (pConverter->hasPreFormatConversion  == MA_FALSE &&
        pConverter->hasPostFormatConversion == MA_FALSE &&
        pConverter->hasChannelConverter     == MA_FALSE &&
        pConverter->hasResampler            == MA_FALSE) {
        pConverter->isPassthrough = MA_TRUE;
    }

    return MA_SUCCESS;
}

MA_API ma_result ma_decoder_init_file(const char* pFilePath, const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
  ma_decoder_init_vfs(NULL, pFilePath, pConfig, pDecoder);
}

MA_API ma_result ma_context_uninit(ma_context* pContext)
{
  assert(0);
}

ma_bool32 ma_context_is_device_id_equal__null(ma_context* pContext, const ma_device_id* pID0, const ma_device_id* pID1)
{
    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pID0 != NULL);
    MA_ASSERT(pID1 != NULL);
    (void)pContext;

    return pID0->nullbackend == pID1->nullbackend;
}

ma_result ma_context_uninit__null(ma_context* pContext)
{
    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pContext->backend == ma_backend_null);

    (void)pContext;
    return MA_SUCCESS;
}

static ma_result ma_context_enumerate_devices__null(ma_context* pContext, ma_enum_devices_callback_proc callback, void* pUserData)
{
    ma_bool32 cbResult = MA_TRUE;

    MA_ASSERT(pContext != NULL);
    MA_ASSERT(callback != NULL);

    /* Playback. */
    if (cbResult) {
        ma_device_info deviceInfo;
        MA_ZERO_OBJECT(&deviceInfo);
        ma_strncpy_s(deviceInfo.name, sizeof(deviceInfo.name), "NULL Playback Device", (size_t)-1);
        cbResult = callback(pContext, ma_device_type_playback, &deviceInfo, pUserData);
    }

    /* Capture. */
    if (cbResult) {
        ma_device_info deviceInfo;
        MA_ZERO_OBJECT(&deviceInfo);
        ma_strncpy_s(deviceInfo.name, sizeof(deviceInfo.name), "NULL Capture Device", (size_t)-1);
        cbResult = callback(pContext, ma_device_type_capture, &deviceInfo, pUserData);
    }

    return MA_SUCCESS;
}


ma_result ma_device_do_operation__null(ma_device* pDevice, ma_uint32 operation)
{
    c89atomic_exchange_32(&pDevice->null_device.operation, operation);
    if (ma_event_signal(&pDevice->null_device.operationEvent) != MA_SUCCESS) {
        return MA_ERROR;
    }

    if (ma_event_wait(&pDevice->null_device.operationCompletionEvent) != MA_SUCCESS) {
        return MA_ERROR;
    }

    return pDevice->null_device.operationResult;
}

ma_result ma_device_start__null(ma_device* pDevice)
{
    MA_ASSERT(pDevice != NULL);

    ma_device_do_operation__null(pDevice, MA_DEVICE_OP_START__NULL);

    c89atomic_exchange_32(&pDevice->null_device.isStarted, MA_TRUE);
    return MA_SUCCESS;
}

ma_result ma_device_stop__null(ma_device* pDevice)
{
    MA_ASSERT(pDevice != NULL);

    ma_device_do_operation__null(pDevice, MA_DEVICE_OP_SUSPEND__NULL);

    c89atomic_exchange_32(&pDevice->null_device.isStarted, MA_FALSE);
    return MA_SUCCESS;
}

ma_uint64 ma_device_get_total_run_time_in_frames__null(ma_device* pDevice)
{
    ma_uint32 internalSampleRate;
    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        internalSampleRate = pDevice->capture.internalSampleRate;
    } else {
        internalSampleRate = pDevice->playback.internalSampleRate;
    }


    return (ma_uint64)((pDevice->null_device.priorRunTime + ma_timer_get_time_in_seconds(&pDevice->null_device.timer)) * internalSampleRate);
}


ma_result ma_device_write__null(ma_device* pDevice, const void* pPCMFrames, ma_uint32 frameCount, ma_uint32* pFramesWritten)
{
    ma_result result = MA_SUCCESS;
    ma_uint32 totalPCMFramesProcessed;
    ma_bool32 wasStartedOnEntry;

    if (pFramesWritten != NULL) {
        *pFramesWritten = 0;
    }

    wasStartedOnEntry = pDevice->null_device.isStarted;

    /* Keep going until everything has been read. */
    totalPCMFramesProcessed = 0;
    while (totalPCMFramesProcessed < frameCount) {
        ma_uint64 targetFrame;

        /* If there are any frames remaining in the current period, consume those first. */
        if (pDevice->null_device.currentPeriodFramesRemainingPlayback > 0) {
            ma_uint32 framesRemaining = (frameCount - totalPCMFramesProcessed);
            ma_uint32 framesToProcess = pDevice->null_device.currentPeriodFramesRemainingPlayback;
            if (framesToProcess > framesRemaining) {
                framesToProcess = framesRemaining;
            }

            /* We don't actually do anything with pPCMFrames, so just mark it as unused to prevent a warning. */
            (void)pPCMFrames;

            pDevice->null_device.currentPeriodFramesRemainingPlayback -= framesToProcess;
            totalPCMFramesProcessed += framesToProcess;
        }

        /* If we've consumed the current period we'll need to mark it as such an ensure the device is started if it's not already. */
        if (pDevice->null_device.currentPeriodFramesRemainingPlayback == 0) {
            pDevice->null_device.currentPeriodFramesRemainingPlayback = 0;

            if (!pDevice->null_device.isStarted && !wasStartedOnEntry) {
                result = ma_device_start__null(pDevice);
                if (result != MA_SUCCESS) {
                    break;
                }
            }
        }

        /* If we've consumed the whole buffer we can return now. */
        MA_ASSERT(totalPCMFramesProcessed <= frameCount);
        if (totalPCMFramesProcessed == frameCount) {
            break;
        }

        /* Getting here means we've still got more frames to consume, we but need to wait for it to become available. */
        targetFrame = pDevice->null_device.lastProcessedFramePlayback;
        for (;;) {
            ma_uint64 currentFrame;

            /* Stop waiting if the device has been stopped. */
            if (!pDevice->null_device.isStarted) {
                break;
            }

            currentFrame = ma_device_get_total_run_time_in_frames__null(pDevice);
            if (currentFrame >= targetFrame) {
                break;
            }

            /* Getting here means we haven't yet reached the target sample, so continue waiting. */
            ma_sleep(10);
        }

        pDevice->null_device.lastProcessedFramePlayback          += pDevice->playback.internalPeriodSizeInFrames;
        pDevice->null_device.currentPeriodFramesRemainingPlayback = pDevice->playback.internalPeriodSizeInFrames;
    }

    if (pFramesWritten != NULL) {
        *pFramesWritten = totalPCMFramesProcessed;
    }

    return result;
}

static ma_result ma_device_read__null(ma_device* pDevice, void* pPCMFrames, ma_uint32 frameCount, ma_uint32* pFramesRead)
{
    ma_result result = MA_SUCCESS;
    ma_uint32 totalPCMFramesProcessed;

    if (pFramesRead != NULL) {
        *pFramesRead = 0;
    }

    /* Keep going until everything has been read. */
    totalPCMFramesProcessed = 0;
    while (totalPCMFramesProcessed < frameCount) {
        ma_uint64 targetFrame;

        /* If there are any frames remaining in the current period, consume those first. */
        if (pDevice->null_device.currentPeriodFramesRemainingCapture > 0) {
            ma_uint32 bpf = ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels);
            ma_uint32 framesRemaining = (frameCount - totalPCMFramesProcessed);
            ma_uint32 framesToProcess = pDevice->null_device.currentPeriodFramesRemainingCapture;
            if (framesToProcess > framesRemaining) {
                framesToProcess = framesRemaining;
            }

            /* We need to ensure the output buffer is zeroed. */
            MA_ZERO_MEMORY(ma_offset_ptr(pPCMFrames, totalPCMFramesProcessed*bpf), framesToProcess*bpf);

            pDevice->null_device.currentPeriodFramesRemainingCapture -= framesToProcess;
            totalPCMFramesProcessed += framesToProcess;
        }

        /* If we've consumed the current period we'll need to mark it as such an ensure the device is started if it's not already. */
        if (pDevice->null_device.currentPeriodFramesRemainingCapture == 0) {
            pDevice->null_device.currentPeriodFramesRemainingCapture = 0;
        }

        /* If we've consumed the whole buffer we can return now. */
        MA_ASSERT(totalPCMFramesProcessed <= frameCount);
        if (totalPCMFramesProcessed == frameCount) {
            break;
        }

        /* Getting here means we've still got more frames to consume, we but need to wait for it to become available. */
        targetFrame = pDevice->null_device.lastProcessedFrameCapture + pDevice->capture.internalPeriodSizeInFrames;
        for (;;) {
            ma_uint64 currentFrame;

            /* Stop waiting if the device has been stopped. */
            if (!pDevice->null_device.isStarted) {
                break;
            }

            currentFrame = ma_device_get_total_run_time_in_frames__null(pDevice);
            if (currentFrame >= targetFrame) {
                break;
            }

            /* Getting here means we haven't yet reached the target sample, so continue waiting. */
            ma_sleep(10);
        }

        pDevice->null_device.lastProcessedFrameCapture          += pDevice->capture.internalPeriodSizeInFrames;
        pDevice->null_device.currentPeriodFramesRemainingCapture = pDevice->capture.internalPeriodSizeInFrames;
    }

    if (pFramesRead != NULL) {
        *pFramesRead = totalPCMFramesProcessed;
    }

    return result;
}



static ma_result ma_context_init__null(const ma_context_config* pConfig, ma_context* pContext)
{
}


void ma_device__on_data(ma_device* pDevice, void* pFramesOut, const void* pFramesIn, ma_uint32 frameCount)
{
    float masterVolumeFactor;
    
    masterVolumeFactor = pDevice->masterVolumeFactor;

    if (pDevice->onData) {
        if (!pDevice->noPreZeroedOutputBuffer && pFramesOut != NULL) {
            ma_silence_pcm_frames(pFramesOut, frameCount, pDevice->playback.format, pDevice->playback.channels);
        }

        /* Volume control of input makes things a bit awkward because the input buffer is read-only. We'll need to use a temp buffer and loop in this case. */
        if (pFramesIn != NULL && masterVolumeFactor < 1) {
            ma_uint8 tempFramesIn[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
            ma_uint32 bpfCapture  = ma_get_bytes_per_frame(pDevice->capture.format, pDevice->capture.channels);
            ma_uint32 bpfPlayback = ma_get_bytes_per_frame(pDevice->playback.format, pDevice->playback.channels);
            ma_uint32 totalFramesProcessed = 0;
            while (totalFramesProcessed < frameCount) {
                ma_uint32 framesToProcessThisIteration = frameCount - totalFramesProcessed;
                if (framesToProcessThisIteration > sizeof(tempFramesIn)/bpfCapture) {
                    framesToProcessThisIteration = sizeof(tempFramesIn)/bpfCapture;
                }

                ma_copy_and_apply_volume_factor_pcm_frames(tempFramesIn, ma_offset_ptr(pFramesIn, totalFramesProcessed*bpfCapture), framesToProcessThisIteration, pDevice->capture.format, pDevice->capture.channels, masterVolumeFactor);

                pDevice->onData(pDevice, ma_offset_ptr(pFramesOut, totalFramesProcessed*bpfPlayback), tempFramesIn, framesToProcessThisIteration);

                totalFramesProcessed += framesToProcessThisIteration;
            }
        } else {
            pDevice->onData(pDevice, pFramesOut, pFramesIn, frameCount);
        }

        /* Volume control and clipping for playback devices. */
        if (pFramesOut != NULL) {
            if (masterVolumeFactor < 1) {
                if (pFramesIn == NULL) {    /* <-- In full-duplex situations, the volume will have been applied to the input samples before the data callback. Applying it again post-callback will incorrectly compound it. */
                    ma_apply_volume_factor_pcm_frames(pFramesOut, frameCount, pDevice->playback.format, pDevice->playback.channels, masterVolumeFactor);
                }
            }

            if (!pDevice->noClip && pDevice->playback.format == ma_format_f32) {
                ma_clip_pcm_frames_f32((float*)pFramesOut, frameCount, pDevice->playback.channels);
            }
        }
    }
}

ma_result ma_device_main_loop__null(ma_device* pDevice)
{
    ma_result result = MA_SUCCESS;
    ma_bool32 exitLoop = MA_FALSE;
    
    MA_ASSERT(pDevice != NULL);

    /* The capture device needs to be started immediately. */
    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        result = ma_device_start__null(pDevice);
        if (result != MA_SUCCESS) {
            return result;
        }
    }

    while (ma_device__get_state(pDevice) == MA_STATE_STARTED && !exitLoop) {
        switch (pDevice->type)
        {
            case ma_device_type_duplex:
            {
                /* The process is: device_read -> convert -> callback -> convert -> device_write */
                ma_uint32 totalCapturedDeviceFramesProcessed = 0;
                ma_uint32 capturedDevicePeriodSizeInFrames = ma_min(pDevice->capture.internalPeriodSizeInFrames, pDevice->playback.internalPeriodSizeInFrames);
                    
                while (totalCapturedDeviceFramesProcessed < capturedDevicePeriodSizeInFrames) {
                    ma_uint8  capturedDeviceData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                    ma_uint8  playbackDeviceData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                    ma_uint32 capturedDeviceDataCapInFrames = sizeof(capturedDeviceData) / ma_get_bytes_per_frame(pDevice->capture.internalFormat,  pDevice->capture.internalChannels);
                    ma_uint32 playbackDeviceDataCapInFrames = sizeof(playbackDeviceData) / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
                    ma_uint32 capturedDeviceFramesRemaining;
                    ma_uint32 capturedDeviceFramesProcessed;
                    ma_uint32 capturedDeviceFramesToProcess;
                    ma_uint32 capturedDeviceFramesToTryProcessing = capturedDevicePeriodSizeInFrames - totalCapturedDeviceFramesProcessed;
                    if (capturedDeviceFramesToTryProcessing > capturedDeviceDataCapInFrames) {
                        capturedDeviceFramesToTryProcessing = capturedDeviceDataCapInFrames;
                    }

                    result = ma_device_read__null(pDevice, capturedDeviceData, capturedDeviceFramesToTryProcessing, &capturedDeviceFramesToProcess);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    capturedDeviceFramesRemaining = capturedDeviceFramesToProcess;
                    capturedDeviceFramesProcessed = 0;

                    /* At this point we have our captured data in device format and we now need to convert it to client format. */
                    for (;;) {
                        ma_uint8  capturedClientData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                        ma_uint8  playbackClientData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                        ma_uint32 capturedClientDataCapInFrames = sizeof(capturedClientData) / ma_get_bytes_per_frame(pDevice->capture.format, pDevice->capture.channels);
                        ma_uint32 playbackClientDataCapInFrames = sizeof(playbackClientData) / ma_get_bytes_per_frame(pDevice->playback.format, pDevice->playback.channels);
                        ma_uint64 capturedClientFramesToProcessThisIteration = ma_min(capturedClientDataCapInFrames, playbackClientDataCapInFrames);
                        ma_uint64 capturedDeviceFramesToProcessThisIteration = capturedDeviceFramesRemaining;
                        ma_uint8* pRunningCapturedDeviceFrames = ma_offset_ptr(capturedDeviceData, capturedDeviceFramesProcessed * ma_get_bytes_per_frame(pDevice->capture.internalFormat,  pDevice->capture.internalChannels));
                        
                        /* Convert capture data from device format to client format. */
                        result = ma_data_converter_process_pcm_frames(&pDevice->capture.converter, pRunningCapturedDeviceFrames, &capturedDeviceFramesToProcessThisIteration, capturedClientData, &capturedClientFramesToProcessThisIteration);
                        if (result != MA_SUCCESS) {
                            break;
                        }

                        /*
                        If we weren't able to generate any output frames it must mean we've exhaused all of our input. The only time this would not be the case is if capturedClientData was too small
                        which should never be the case when it's of the size MA_DATA_CONVERTER_STACK_BUFFER_SIZE.
                        */
                        if (capturedClientFramesToProcessThisIteration == 0) {
                            break;
                        }

                        ma_device__on_data(pDevice, playbackClientData, capturedClientData, (ma_uint32)capturedClientFramesToProcessThisIteration);    /* Safe cast .*/

                        capturedDeviceFramesProcessed += (ma_uint32)capturedDeviceFramesToProcessThisIteration; /* Safe cast. */
                        capturedDeviceFramesRemaining -= (ma_uint32)capturedDeviceFramesToProcessThisIteration; /* Safe cast. */

                        /* At this point the playbackClientData buffer should be holding data that needs to be written to the device. */
                        for (;;) {
                            ma_uint64 convertedClientFrameCount = capturedClientFramesToProcessThisIteration;
                            ma_uint64 convertedDeviceFrameCount = playbackDeviceDataCapInFrames;
                            result = ma_data_converter_process_pcm_frames(&pDevice->playback.converter, playbackClientData, &convertedClientFrameCount, playbackDeviceData, &convertedDeviceFrameCount);
                            if (result != MA_SUCCESS) {
                                break;
                            }

                            result = ma_device_write__null(pDevice, playbackDeviceData, (ma_uint32)convertedDeviceFrameCount, NULL);   /* Safe cast. */
                            if (result != MA_SUCCESS) {
                                exitLoop = MA_TRUE;
                                break;
                            }

                            capturedClientFramesToProcessThisIteration -= (ma_uint32)convertedClientFrameCount;  /* Safe cast. */
                            if (capturedClientFramesToProcessThisIteration == 0) {
                                break;
                            }
                        }

                        /* In case an error happened from ma_device_write__null()... */
                        if (result != MA_SUCCESS) {
                            exitLoop = MA_TRUE;
                            break;
                        }
                    }

                    totalCapturedDeviceFramesProcessed += capturedDeviceFramesProcessed;
                }
            } break;

            case ma_device_type_capture:
            {
                /* We read in chunks of the period size, but use a stack allocated buffer for the intermediary. */
                ma_uint8 intermediaryBuffer[8192];
                ma_uint32 intermediaryBufferSizeInFrames = sizeof(intermediaryBuffer) / ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels);
                ma_uint32 periodSizeInFrames = pDevice->capture.internalPeriodSizeInFrames;
                ma_uint32 framesReadThisPeriod = 0;
                while (framesReadThisPeriod < periodSizeInFrames) {
                    ma_uint32 framesRemainingInPeriod = periodSizeInFrames - framesReadThisPeriod;
                    ma_uint32 framesProcessed;
                    ma_uint32 framesToReadThisIteration = framesRemainingInPeriod;
                    if (framesToReadThisIteration > intermediaryBufferSizeInFrames) {
                        framesToReadThisIteration = intermediaryBufferSizeInFrames;
                    }

                    result = ma_device_read__null(pDevice, intermediaryBuffer, framesToReadThisIteration, &framesProcessed);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    ma_device__send_frames_to_client(pDevice, framesProcessed, intermediaryBuffer);

                    framesReadThisPeriod += framesProcessed;
                }
            } break;

            case ma_device_type_playback:
            {
                /* We write in chunks of the period size, but use a stack allocated buffer for the intermediary. */
                ma_uint8 intermediaryBuffer[8192];
                ma_uint32 intermediaryBufferSizeInFrames = sizeof(intermediaryBuffer) / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
                ma_uint32 periodSizeInFrames = pDevice->playback.internalPeriodSizeInFrames;
                ma_uint32 framesWrittenThisPeriod = 0;
                while (framesWrittenThisPeriod < periodSizeInFrames) {
                    ma_uint32 framesRemainingInPeriod = periodSizeInFrames - framesWrittenThisPeriod;
                    ma_uint32 framesProcessed;
                    ma_uint32 framesToWriteThisIteration = framesRemainingInPeriod;
                    if (framesToWriteThisIteration > intermediaryBufferSizeInFrames) {
                        framesToWriteThisIteration = intermediaryBufferSizeInFrames;
                    }

                    ma_device__read_frames_from_client(pDevice, framesToWriteThisIteration, intermediaryBuffer);

                    result = ma_device_write__null(pDevice, intermediaryBuffer, framesToWriteThisIteration, &framesProcessed);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    framesWrittenThisPeriod += framesProcessed;
                }
            } break;

            /* To silence a warning. Will never hit this. */
            case ma_device_type_loopback:
            default: break;
        }
    }


    /* Here is where the device is started. */
    ma_device_stop__null(pDevice);

    return result;
}

ma_context_config ma_context_config_init()
{
    ma_context_config config;
    MA_ZERO_OBJECT(&config);
    return config;
}


MA_API ma_result ma_context_init(const ma_backend backends[], ma_uint32 backendCount, const ma_context_config* pConfig, ma_context* pContext)
{

  MA_ZERO_OBJECT(pContext);
  (void)pConfig;
  ma_context_config config;
      /* Always make sure the config is set first to ensure properties are available as soon as possible. */
    if (pConfig != NULL) {
        config = *pConfig;
    } else {
        config = ma_context_config_init();
    }

  ma_result result = ma_context_init__pulse(&config, pContext);
  printf("\npulse rc=%i\n", result);
  assert(result == MA_SUCCESS);
  //  MA_ASSERT(pContext != NULL);

  pContext->backend = ma_backend_pulseaudio;


#if 0  
  pContext->onUninit         = ma_context_uninit__null;
    pContext->onDeviceIDEqual  = ma_context_is_device_id_equal__null;
    pContext->onEnumDevices    = ma_context_enumerate_devices__null;
    pContext->onGetDeviceInfo  = ma_context_get_device_info__null;
    pContext->onDeviceInit     = ma_device_init__null;
    pContext->onDeviceUninit   = ma_device_uninit__null;
    pContext->onDeviceStart    = NULL; /* Not required for synchronous backends. */
    pContext->onDeviceStop     = NULL; /* Not required for synchronous backends. */
    pContext->onDeviceMainLoop = ma_device_main_loop__null;
#endif
    /* The null backend always works. */
    return MA_SUCCESS;


}

MA_API ma_data_converter_config ma_data_converter_config_init_default()
{
    ma_data_converter_config config;
    MA_ZERO_OBJECT(&config);

    config.ditherMode = ma_dither_mode_none;
    config.resampling.algorithm = ma_resample_algorithm_linear;
    config.resampling.allowDynamicSampleRate = MA_FALSE; /* Disable dynamic sample rates by default because dynamic rate adjustments should be quite rare and it allows an optimization for cases when the in and out sample rates are the same. */

    /* Linear resampling defaults. */
    config.resampling.linear.lpfOrder = 1;
    config.resampling.linear.lpfNyquistFactor = 1;

    /* Speex resampling defaults. */
    config.resampling.speex.quality = 3;

    return config;
}



void ma_pcm_convert(void* pOut, ma_format formatOut, const void* pIn, ma_format formatIn, ma_uint64 sampleCount, ma_dither_mode ditherMode)
{
    printf("Formats %i %i\n", formatIn, formatOut);
  if (formatOut == formatIn) {
        ma_copy_memory_64(pOut, pIn, sampleCount * ma_get_bytes_per_sample(formatOut));
        return;
    }


    switch (formatIn)
    {
        case ma_format_u8:
        {
            switch (formatOut)
            {
                case ma_format_s16: ma_pcm_u8_to_s16(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s24: ma_pcm_u8_to_s24(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s32: ma_pcm_u8_to_s32(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_f32: ma_pcm_u8_to_f32(pOut, pIn, sampleCount, ditherMode); return;
                default: break;
            }
        } break;

        case ma_format_s16:
        {
            switch (formatOut)
            {
                case ma_format_u8:  ma_pcm_s16_to_u8( pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s24: ma_pcm_s16_to_s24(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s32: ma_pcm_s16_to_s32(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_f32: ma_pcm_s16_to_f32(pOut, pIn, sampleCount, ditherMode); return;
                default: break;
            }
        } break;

        case ma_format_s24:
        {
            switch (formatOut)
            {
                case ma_format_u8:  ma_pcm_s24_to_u8( pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s16: ma_pcm_s24_to_s16(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s32: ma_pcm_s24_to_s32(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_f32: ma_pcm_s24_to_f32(pOut, pIn, sampleCount, ditherMode); return;
                default: break;
            }
        } break;

        case ma_format_s32:
        {
            switch (formatOut)
            {
                case ma_format_u8:  ma_pcm_s32_to_u8( pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s16: ma_pcm_s32_to_s16(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s24: ma_pcm_s32_to_s24(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_f32: ma_pcm_s32_to_f32(pOut, pIn, sampleCount, ditherMode); return;
                default: break;
            }
        } break;

        case ma_format_f32:
        {
            switch (formatOut)
            {
                case ma_format_u8:  ma_pcm_f32_to_u8( pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s16: ma_pcm_f32_to_s16(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s24: ma_pcm_f32_to_s24(pOut, pIn, sampleCount, ditherMode); return;
                case ma_format_s32: ma_pcm_f32_to_s32(pOut, pIn, sampleCount, ditherMode); return;
                default: break;
            }
        } break;

        default: break;
    }
}

void ma_convert_pcm_frames_format(void* pOut, ma_format formatOut, const void* pIn, ma_format formatIn, ma_uint64 frameCount, ma_uint32 channels, ma_dither_mode ditherMode)
{
    ma_pcm_convert(pOut, formatOut, pIn, formatIn, frameCount * channels, ditherMode);
}


ma_result ma_data_converter_process_pcm_frames__format_only(ma_data_converter* pConverter, const void* pFramesIn, ma_uint64* pFrameCountIn, void* pFramesOut, ma_uint64* pFrameCountOut)
{
    ma_uint64 frameCountIn;
    ma_uint64 frameCountOut;
    ma_uint64 frameCount;

    MA_ASSERT(pConverter != NULL);
    
    frameCountIn = 0;
    if (pFrameCountIn != NULL) {
        frameCountIn = *pFrameCountIn;
    }

    frameCountOut = 0;
    if (pFrameCountOut != NULL) {
        frameCountOut = *pFrameCountOut;
    }

    frameCount = ma_min(frameCountIn, frameCountOut);

    if (pFramesOut != NULL) {
        if (pFramesIn != NULL) {

	  ma_convert_pcm_frames_format(pFramesOut, pConverter->config.formatOut, pFramesIn, pConverter->config.formatIn, frameCount, pConverter->config.channelsIn, pConverter->config.ditherMode);
        } else {
            ma_zero_memory_64(pFramesOut, frameCount * ma_get_bytes_per_frame(pConverter->config.formatOut, pConverter->config.channelsOut));
        }
    }

    if (pFrameCountIn != NULL) {
        *pFrameCountIn = frameCount;
    }
    if (pFrameCountOut != NULL) {
        *pFrameCountOut = frameCount;
    }

    return MA_SUCCESS;
}


MA_API ma_result ma_data_converter_process_pcm_frames(ma_data_converter* pConverter, const void* pFramesIn, ma_uint64* pFrameCountIn, void* pFramesOut, ma_uint64* pFrameCountOut)
{
  if (pConverter == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pConverter->isPassthrough) {
      assert(0);
      return 0; //ma_data_converter_process_pcm_frames__passthrough(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
    }

    /*
    Here is where the real work is done. Getting here means we're not using a passthrough and we need to move the data through each of the relevant stages. The order
    of our stages depends on the input and output channel count. If the input channels is less than the output channels we want to do sample rate conversion first so
    that it has less work (resampling is the most expensive part of format conversion).
    */
    if (pConverter->config.channelsIn < pConverter->config.channelsOut) {
        /* Do resampling first, if necessary. */
        MA_ASSERT(pConverter->hasChannelConverter == MA_TRUE);

        if (pConverter->hasResampler) {
            /* Resampling first. */
	  assert(0);
	  return 0; //ma_data_converter_process_pcm_frames__resampling_first(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
        } else {
            /* Resampling not required. */
	  assert(0);
	  return 0;// ma_data_converter_process_pcm_frames__channels_only(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
        }
    } else {
        /* Do channel conversion first, if necessary. */
        if (pConverter->hasChannelConverter) {
            if (pConverter->hasResampler) {
                /* Channel routing first. */
	      assert(0);
	      return 0; //ma_data_converter_process_pcm_frames__channels_first(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
            } else {
                /* Resampling not required. */
	      assert(0);
	      return 0; // ma_data_converter_process_pcm_frames__channels_only(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
            }
        } else {
            /* Channel routing not required. */
            if (pConverter->hasResampler) {
                /* Resampling only. */
	      assert(0);
	      return 0; // ma_data_converter_process_pcm_frames__resample_only(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
            } else {
	      
	      /* No channel routing nor resampling required. Just format conversion. */
	      ma_data_converter_process_pcm_frames__format_only(pConverter, pFramesIn, pFrameCountIn, pFramesOut, pFrameCountOut);
            }
        }
    }
}

MA_API ma_uint64 ma_data_converter_get_required_input_frame_count(ma_data_converter* pConverter, ma_uint64 outputFrameCount)
{
    if (pConverter == NULL) {
        return 0;
    }

    if (pConverter->hasResampler) {
      assert(0);
      return 0;      //ma_resampler_get_required_input_frame_count(&pConverter->resampler, outputFrameCount);
    } else {
        return outputFrameCount;    /* 1:1 */
    }
}

MA_API ma_data_converter_config ma_data_converter_config_init(ma_format formatIn, ma_format formatOut, ma_uint32 channelsIn, ma_uint32 channelsOut, ma_uint32 sampleRateIn, ma_uint32 sampleRateOut)
{
    ma_data_converter_config config = ma_data_converter_config_init_default();
    config.formatIn = formatIn;
    config.formatOut = formatOut;
    config.channelsIn = channelsIn;
    config.channelsOut = channelsOut;
    config.sampleRateIn = sampleRateIn;
    config.sampleRateOut = sampleRateOut;
    
    return config;
}

MA_API ma_result ma_decoder_seek_to_pcm_frame(ma_decoder* pDecoder, ma_uint64 frameIndex)
{
    if (pDecoder == NULL) {
        return MA_INVALID_ARGS;
    }

    if (pDecoder->onSeekToPCMFrame) {
        ma_uint64 internalFrameIndex;
        if (pDecoder->internalSampleRate == pDecoder->outputSampleRate) {
            internalFrameIndex = frameIndex;
        } else {
	  assert(0);
	  //internalFrameIndex = ma_calculate_frame_count_after_resampling(pDecoder->internalSampleRate, pDecoder->outputSampleRate, frameIndex);
        }

        return pDecoder->onSeekToPCMFrame(pDecoder, internalFrameIndex);
    }

    /* Should never get here, but if we do it means onSeekToPCMFrame was not set by the backend. */
    return MA_INVALID_ARGS;
}


drmp3_uint64 drmp3_read_pcm_frames_raw(drmp3* pMP3, drmp3_uint64 framesToRead, void* pBufferOut)
{
    drmp3_uint64 totalFramesRead = 0;
    DRMP3_ASSERT(pMP3 != NULL);
    DRMP3_ASSERT(pMP3->onRead != NULL);
    while (framesToRead > 0) {
        drmp3_uint32 framesToConsume = (drmp3_uint32)DRMP3_MIN(pMP3->pcmFramesRemainingInMP3Frame, framesToRead);
        if (pBufferOut != NULL) {
        #if defined(DR_MP3_FLOAT_OUTPUT)
            float* pFramesOutF32 = (float*)DRMP3_OFFSET_PTR(pBufferOut,          sizeof(float) * totalFramesRead                   * pMP3->channels);
            float* pFramesInF32  = (float*)DRMP3_OFFSET_PTR(&pMP3->pcmFrames[0], sizeof(float) * pMP3->pcmFramesConsumedInMP3Frame * pMP3->mp3FrameChannels);
            DRMP3_COPY_MEMORY(pFramesOutF32, pFramesInF32, sizeof(float) * framesToConsume * pMP3->channels);
        #else
            drmp3_int16* pFramesOutS16 = (drmp3_int16*)DRMP3_OFFSET_PTR(pBufferOut,          sizeof(drmp3_int16) * totalFramesRead                   * pMP3->channels);
            drmp3_int16* pFramesInS16  = (drmp3_int16*)DRMP3_OFFSET_PTR(&pMP3->pcmFrames[0], sizeof(drmp3_int16) * pMP3->pcmFramesConsumedInMP3Frame * pMP3->mp3FrameChannels);
            DRMP3_COPY_MEMORY(pFramesOutS16, pFramesInS16, sizeof(drmp3_int16) * framesToConsume * pMP3->channels);
        #endif
        }
        pMP3->currentPCMFrame              += framesToConsume;
        pMP3->pcmFramesConsumedInMP3Frame  += framesToConsume;
        pMP3->pcmFramesRemainingInMP3Frame -= framesToConsume;
        totalFramesRead                    += framesToConsume;
        framesToRead                       -= framesToConsume;
        if (framesToRead == 0) {
            break;
        }
        DRMP3_ASSERT(pMP3->pcmFramesRemainingInMP3Frame == 0);
        if (drmp3_decode_next_frame(pMP3) == 0) {
            break;
        }
    }
    return totalFramesRead;
}

drmp3_uint64 drmp3_read_pcm_frames_s16(drmp3* pMP3, drmp3_uint64 framesToRead, drmp3_int16* pBufferOut)
{
    if (pMP3 == NULL || pMP3->onRead == NULL) {
        return 0;
    }
#if !defined(DR_MP3_FLOAT_OUTPUT)
    return drmp3_read_pcm_frames_raw(pMP3, framesToRead, pBufferOut);
#else
    {
        float pTempF32[4096];
        drmp3_uint64 totalPCMFramesRead = 0;
        while (totalPCMFramesRead < framesToRead) {
            drmp3_uint64 framesJustRead;
            drmp3_uint64 framesRemaining = framesToRead - totalPCMFramesRead;
            drmp3_uint64 framesToReadNow = DRMP3_COUNTOF(pTempF32) / pMP3->channels;
            if (framesToReadNow > framesRemaining) {
                framesToReadNow = framesRemaining;
            }
            framesJustRead = drmp3_read_pcm_frames_raw(pMP3, framesToReadNow, pTempF32);
            if (framesJustRead == 0) {
                break;
            }
            drmp3_f32_to_s16((drmp3_int16*)DRMP3_OFFSET_PTR(pBufferOut, sizeof(drmp3_int16) * totalPCMFramesRead * pMP3->channels), pTempF32, framesJustRead * pMP3->channels);
            totalPCMFramesRead += framesJustRead;
        }
        return totalPCMFramesRead;
    }
#endif
}

drmp3_uint64 drmp3_get_pcm_frame_count(drmp3* pMP3)
{
    drmp3_uint64 totalPCMFrameCount;
    if (!drmp3_get_mp3_and_pcm_frame_count(pMP3, NULL, &totalPCMFrameCount)) {
        return 0;
    }
    return totalPCMFrameCount;
}

static drmp3_bool32 drmp3_init_internal(drmp3* pMP3, drmp3_read_proc onRead, drmp3_seek_proc onSeek, void* pUserData, const drmp3_allocation_callbacks* pAllocationCallbacks)
{
    DRMP3_ASSERT(pMP3 != NULL);
    DRMP3_ASSERT(onRead != NULL);
    drmp3dec_init(&pMP3->decoder);
    pMP3->onRead = onRead;
    pMP3->onSeek = onSeek;
    pMP3->pUserData = pUserData;
    pMP3->allocationCallbacks = drmp3_copy_allocation_callbacks_or_defaults(pAllocationCallbacks);
    if (pMP3->allocationCallbacks.onFree == NULL || (pMP3->allocationCallbacks.onMalloc == NULL && pMP3->allocationCallbacks.onRealloc == NULL)) {
        return DRMP3_FALSE;
    }
    if (!drmp3_decode_next_frame(pMP3)) {
      assert(0);
      //drmp3_uninit(pMP3);
        return DRMP3_FALSE;
    }
    pMP3->channels   = pMP3->mp3FrameChannels;
    pMP3->sampleRate = pMP3->mp3FrameSampleRate;
    return DRMP3_TRUE;
}

DRMP3_API drmp3_bool32 drmp3_init(drmp3* pMP3, drmp3_read_proc onRead, drmp3_seek_proc onSeek, void* pUserData, const drmp3_allocation_callbacks* pAllocationCallbacks)
{
    if (pMP3 == NULL || onRead == NULL) {
        return DRMP3_FALSE;
    }
    DRMP3_ZERO_OBJECT(pMP3);
    return drmp3_init_internal(pMP3, onRead, onSeek, pUserData, pAllocationCallbacks);
}

drmp3_allocation_callbacks drmp3_copy_allocation_callbacks_or_defaults(const drmp3_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks != NULL) {
        return *pAllocationCallbacks;
    } else {
        drmp3_allocation_callbacks allocationCallbacks;
        allocationCallbacks.pUserData = NULL;
        allocationCallbacks.onMalloc  = drmp3__malloc_default;
        allocationCallbacks.onRealloc = drmp3__realloc_default;
        allocationCallbacks.onFree    = drmp3__free_default;
        return allocationCallbacks;
    }
}

drmp3_bool32 drmp3__on_seek(drmp3* pMP3, int offset, drmp3_seek_origin origin)
{
    DRMP3_ASSERT(offset >= 0);
    if (!pMP3->onSeek(pMP3->pUserData, offset, origin)) {
        return DRMP3_FALSE;
    }
    if (origin == drmp3_seek_origin_start) {
        pMP3->streamCursor = (drmp3_uint64)offset;
    } else {
        pMP3->streamCursor += offset;
    }
    return DRMP3_TRUE;
}



void drmp3_reset(drmp3* pMP3)
{
    DRMP3_ASSERT(pMP3 != NULL);
    pMP3->pcmFramesConsumedInMP3Frame = 0;
    pMP3->pcmFramesRemainingInMP3Frame = 0;
    pMP3->currentPCMFrame = 0;
    pMP3->dataSize = 0;
    pMP3->atEnd = DRMP3_FALSE;
    drmp3dec_init(&pMP3->decoder);
}

drmp3_bool32 drmp3_seek_to_start_of_stream(drmp3* pMP3)
{
    DRMP3_ASSERT(pMP3 != NULL);
    DRMP3_ASSERT(pMP3->onSeek != NULL);
    if (!drmp3__on_seek(pMP3, 0, drmp3_seek_origin_start)) {
        return DRMP3_FALSE;
    }
    drmp3_reset(pMP3);
    return DRMP3_TRUE;
}

drmp3_bool32 drmp3_get_mp3_and_pcm_frame_count(drmp3* pMP3, drmp3_uint64* pMP3FrameCount, drmp3_uint64* pPCMFrameCount)
{
    drmp3_uint64 currentPCMFrame;
    drmp3_uint64 totalPCMFrameCount;
    drmp3_uint64 totalMP3FrameCount;
    if (pMP3 == NULL) {
        return DRMP3_FALSE;
    }
    if (pMP3->onSeek == NULL) {
        return DRMP3_FALSE;
    }
    currentPCMFrame = pMP3->currentPCMFrame;
    if (!drmp3_seek_to_start_of_stream(pMP3)) {
        return DRMP3_FALSE;
    }
    totalPCMFrameCount = 0;
    totalMP3FrameCount = 0;
    for (;;) {
        drmp3_uint32 pcmFramesInCurrentMP3Frame;
        pcmFramesInCurrentMP3Frame = drmp3_decode_next_frame_ex(pMP3, NULL);
        if (pcmFramesInCurrentMP3Frame == 0) {
            break;
        }
        totalPCMFrameCount += pcmFramesInCurrentMP3Frame;
        totalMP3FrameCount += 1;
    }
    if (!drmp3_seek_to_start_of_stream(pMP3)) {
        return DRMP3_FALSE;
    }
    assert(0);

    //if (!drmp3_seek_to_pcm_frame(pMP3, currentPCMFrame)) {
    //  return DRMP3_FALSE;
    //}


    if (pMP3FrameCount != NULL) {
        *pMP3FrameCount = totalMP3FrameCount;
    }
    if (pPCMFrameCount != NULL) {
        *pPCMFrameCount = totalPCMFrameCount;
    }
    return DRMP3_TRUE;
}


void drmp3dec_init(drmp3dec *dec)
{
    dec->header[0] = 0;
}


void drmp3_bs_init(drmp3_bs *bs, const drmp3_uint8 *data, int bytes)
{
    bs->buf   = data;
    bs->pos   = 0;
    bs->limit = bytes*8;
}

drmp3_uint32 drmp3_bs_get_bits(drmp3_bs *bs, int n)
{
    drmp3_uint32 next, cache = 0, s = bs->pos & 7;
    int shl = n + s;
    const drmp3_uint8 *p = bs->buf + (bs->pos >> 3);
    if ((bs->pos += n) > bs->limit)
        return 0;
    next = *p++ & (255 >> s);
    while ((shl -= 8) > 0)
    {
        cache |= next << shl;
        next = *p++;
    }
    return cache | (next >> -shl);
}
static int drmp3_hdr_valid(const drmp3_uint8 *h)
{
    return h[0] == 0xff &&
        ((h[1] & 0xF0) == 0xf0 || (h[1] & 0xFE) == 0xe2) &&
        (DRMP3_HDR_GET_LAYER(h) != 0) &&
        (DRMP3_HDR_GET_BITRATE(h) != 15) &&
        (DRMP3_HDR_GET_SAMPLE_RATE(h) != 3);
}

static int drmp3_hdr_compare(const drmp3_uint8 *h1, const drmp3_uint8 *h2)
{
    return drmp3_hdr_valid(h2) &&
        ((h1[1] ^ h2[1]) & 0xFE) == 0 &&
        ((h1[2] ^ h2[2]) & 0x0C) == 0 &&
        !(DRMP3_HDR_IS_FREE_FORMAT(h1) ^ DRMP3_HDR_IS_FREE_FORMAT(h2));
}



static unsigned drmp3_hdr_bitrate_kbps(const drmp3_uint8 *h)
{
    static const drmp3_uint8 halfrate[2][3][15] = {
        { { 0,4,8,12,16,20,24,28,32,40,48,56,64,72,80 }, { 0,4,8,12,16,20,24,28,32,40,48,56,64,72,80 }, { 0,16,24,28,32,40,48,56,64,72,80,88,96,112,128 } },
        { { 0,16,20,24,28,32,40,48,56,64,80,96,112,128,160 }, { 0,16,24,28,32,40,48,56,64,80,96,112,128,160,192 }, { 0,16,32,48,64,80,96,112,128,144,160,176,192,208,224 } },
    };
    return 2*halfrate[!!DRMP3_HDR_TEST_MPEG1(h)][DRMP3_HDR_GET_LAYER(h) - 1][DRMP3_HDR_GET_BITRATE(h)];
}
static unsigned drmp3_hdr_sample_rate_hz(const drmp3_uint8 *h)
{
    static const unsigned g_hz[3] = { 44100, 48000, 32000 };
    return g_hz[DRMP3_HDR_GET_SAMPLE_RATE(h)] >> (int)!DRMP3_HDR_TEST_MPEG1(h) >> (int)!DRMP3_HDR_TEST_NOT_MPEG25(h);
}

static unsigned drmp3_hdr_frame_samples(const drmp3_uint8 *h)
{
    return DRMP3_HDR_IS_LAYER_1(h) ? 384 : (1152 >> (int)DRMP3_HDR_IS_FRAME_576(h));
}

static int drmp3_hdr_frame_bytes(const drmp3_uint8 *h, int free_format_size)
{
    int frame_bytes = drmp3_hdr_frame_samples(h)*drmp3_hdr_bitrate_kbps(h)*125/drmp3_hdr_sample_rate_hz(h);
    if (DRMP3_HDR_IS_LAYER_1(h))
    {
        frame_bytes &= ~3;
    }
    return frame_bytes ? frame_bytes : free_format_size;
}

static int drmp3_hdr_padding(const drmp3_uint8 *h)
{
    return DRMP3_HDR_TEST_PADDING(h) ? (DRMP3_HDR_IS_LAYER_1(h) ? 4 : 1) : 0;
}

static int drmp3d_match_frame(const drmp3_uint8 *hdr, int mp3_bytes, int frame_bytes)
{
    int i, nmatch;
    for (i = 0, nmatch = 0; nmatch < DRMP3_MAX_FRAME_SYNC_MATCHES; nmatch++)
    {
        i += drmp3_hdr_frame_bytes(hdr + i, frame_bytes) + drmp3_hdr_padding(hdr + i);
        if (i + DRMP3_HDR_SIZE > mp3_bytes)
            return nmatch > 0;
        if (!drmp3_hdr_compare(hdr, hdr + i))
            return 0;
    }
    return 1;
}


static int drmp3d_find_frame(const drmp3_uint8 *mp3, int mp3_bytes, int *free_format_bytes, int *ptr_frame_bytes)
{
    int i, k;
    for (i = 0; i < mp3_bytes - DRMP3_HDR_SIZE; i++, mp3++)
    {
        if (drmp3_hdr_valid(mp3))
        {
            int frame_bytes = drmp3_hdr_frame_bytes(mp3, *free_format_bytes);
            int frame_and_padding = frame_bytes + drmp3_hdr_padding(mp3);
            for (k = DRMP3_HDR_SIZE; !frame_bytes && k < DRMP3_MAX_FREE_FORMAT_FRAME_SIZE && i + 2*k < mp3_bytes - DRMP3_HDR_SIZE; k++)
            {
                if (drmp3_hdr_compare(mp3, mp3 + k))
                {
                    int fb = k - drmp3_hdr_padding(mp3);
                    int nextfb = fb + drmp3_hdr_padding(mp3 + k);
                    if (i + k + nextfb + DRMP3_HDR_SIZE > mp3_bytes || !drmp3_hdr_compare(mp3, mp3 + k + nextfb))
                        continue;
                    frame_and_padding = k;
                    frame_bytes = fb;
                    *free_format_bytes = fb;
                }
            }
            if ((frame_bytes && i + frame_and_padding <= mp3_bytes &&
                drmp3d_match_frame(mp3, mp3_bytes - i, frame_bytes)) ||
                (!i && frame_and_padding == mp3_bytes))
            {
                *ptr_frame_bytes = frame_and_padding;
                return i;
            }
            *free_format_bytes = 0;
        }
    }
    *ptr_frame_bytes = 0;
    return mp3_bytes;
}


#ifndef DR_MP3_FLOAT_OUTPUT
typedef drmp3_int16 drmp3d_sample_t;
drmp3_int16 drmp3d_scale_pcm(float sample)
{
    drmp3_int16 s;
#if DRMP3_HAVE_ARMV6
    drmp3_int32 s32 = (drmp3_int32)(sample + .5f);
    s32 -= (s32 < 0);
    s = (drmp3_int16)drmp3_clip_int16_arm(s32);
#else
    if (sample >=  32766.5) return (drmp3_int16) 32767;
    if (sample <= -32767.5) return (drmp3_int16)-32768;
    s = (drmp3_int16)(sample + .5f);
    s -= (s < 0);
#endif
    return s;
}
#else
typedef float drmp3d_sample_t;
static float drmp3d_scale_pcm(float sample)
{
    return sample*(1.f/32768.f);
}
#endif

static void drmp3d_synth_pair(drmp3d_sample_t *pcm, int nch, const float *z)
{
    float a;
    a  = (z[14*64] - z[    0]) * 29;
    a += (z[ 1*64] + z[13*64]) * 213;
    a += (z[12*64] - z[ 2*64]) * 459;
    a += (z[ 3*64] + z[11*64]) * 2037;
    a += (z[10*64] - z[ 4*64]) * 5153;
    a += (z[ 5*64] + z[ 9*64]) * 6574;
    a += (z[ 8*64] - z[ 6*64]) * 37489;
    a +=  z[ 7*64]             * 75038;
    pcm[0] = drmp3d_scale_pcm(a);
    z += 2;
    a  = z[14*64] * 104;
    a += z[12*64] * 1567;
    a += z[10*64] * 9727;
    a += z[ 8*64] * 64019;
    a += z[ 6*64] * -9975;
    a += z[ 4*64] * -45;
    a += z[ 2*64] * 146;
    a += z[ 0*64] * -5;
    pcm[16*nch] = drmp3d_scale_pcm(a);
}



static void drmp3d_synth(float *xl, drmp3d_sample_t *dstl, int nch, float *lins)
{
    int i;
    float *xr = xl + 576*(nch - 1);
    drmp3d_sample_t *dstr = dstl + (nch - 1);
    static const float g_win[] = {
        -1,26,-31,208,218,401,-519,2063,2000,4788,-5517,7134,5959,35640,-39336,74992,
        -1,24,-35,202,222,347,-581,2080,1952,4425,-5879,7640,5288,33791,-41176,74856,
        -1,21,-38,196,225,294,-645,2087,1893,4063,-6237,8092,4561,31947,-43006,74630,
        -1,19,-41,190,227,244,-711,2085,1822,3705,-6589,8492,3776,30112,-44821,74313,
        -1,17,-45,183,228,197,-779,2075,1739,3351,-6935,8840,2935,28289,-46617,73908,
        -1,16,-49,176,228,153,-848,2057,1644,3004,-7271,9139,2037,26482,-48390,73415,
        -2,14,-53,169,227,111,-919,2032,1535,2663,-7597,9389,1082,24694,-50137,72835,
        -2,13,-58,161,224,72,-991,2001,1414,2330,-7910,9592,70,22929,-51853,72169,
        -2,11,-63,154,221,36,-1064,1962,1280,2006,-8209,9750,-998,21189,-53534,71420,
        -2,10,-68,147,215,2,-1137,1919,1131,1692,-8491,9863,-2122,19478,-55178,70590,
        -3,9,-73,139,208,-29,-1210,1870,970,1388,-8755,9935,-3300,17799,-56778,69679,
        -3,8,-79,132,200,-57,-1283,1817,794,1095,-8998,9966,-4533,16155,-58333,68692,
        -4,7,-85,125,189,-83,-1356,1759,605,814,-9219,9959,-5818,14548,-59838,67629,
        -4,7,-91,117,177,-106,-1428,1698,402,545,-9416,9916,-7154,12980,-61289,66494,
        -5,6,-97,111,163,-127,-1498,1634,185,288,-9585,9838,-8540,11455,-62684,65290
    };
    float *zlin = lins + 15*64;
    const float *w = g_win;
    zlin[4*15]     = xl[18*16];
    zlin[4*15 + 1] = xr[18*16];
    zlin[4*15 + 2] = xl[0];
    zlin[4*15 + 3] = xr[0];
    zlin[4*31]     = xl[1 + 18*16];
    zlin[4*31 + 1] = xr[1 + 18*16];
    zlin[4*31 + 2] = xl[1];
    zlin[4*31 + 3] = xr[1];
    drmp3d_synth_pair(dstr, nch, lins + 4*15 + 1);
    drmp3d_synth_pair(dstr + 32*nch, nch, lins + 4*15 + 64 + 1);
    drmp3d_synth_pair(dstl, nch, lins + 4*15);
    drmp3d_synth_pair(dstl + 32*nch, nch, lins + 4*15 + 64);
#if DRMP3_HAVE_SIMD
    if (drmp3_have_simd()) for (i = 14; i >= 0; i--)
    {
#define DRMP3_VLOAD(k) drmp3_f4 w0 = DRMP3_VSET(*w++); drmp3_f4 w1 = DRMP3_VSET(*w++); drmp3_f4 vz = DRMP3_VLD(&zlin[4*i - 64*k]); drmp3_f4 vy = DRMP3_VLD(&zlin[4*i - 64*(15 - k)]);
#define DRMP3_V0(k) { DRMP3_VLOAD(k) b =               DRMP3_VADD(DRMP3_VMUL(vz, w1), DRMP3_VMUL(vy, w0)) ; a =               DRMP3_VSUB(DRMP3_VMUL(vz, w0), DRMP3_VMUL(vy, w1));  }
#define DRMP3_V1(k) { DRMP3_VLOAD(k) b = DRMP3_VADD(b, DRMP3_VADD(DRMP3_VMUL(vz, w1), DRMP3_VMUL(vy, w0))); a = DRMP3_VADD(a, DRMP3_VSUB(DRMP3_VMUL(vz, w0), DRMP3_VMUL(vy, w1))); }
#define DRMP3_V2(k) { DRMP3_VLOAD(k) b = DRMP3_VADD(b, DRMP3_VADD(DRMP3_VMUL(vz, w1), DRMP3_VMUL(vy, w0))); a = DRMP3_VADD(a, DRMP3_VSUB(DRMP3_VMUL(vy, w1), DRMP3_VMUL(vz, w0))); }
        drmp3_f4 a, b;
        zlin[4*i]     = xl[18*(31 - i)];
        zlin[4*i + 1] = xr[18*(31 - i)];
        zlin[4*i + 2] = xl[1 + 18*(31 - i)];
        zlin[4*i + 3] = xr[1 + 18*(31 - i)];
        zlin[4*i + 64] = xl[1 + 18*(1 + i)];
        zlin[4*i + 64 + 1] = xr[1 + 18*(1 + i)];
        zlin[4*i - 64 + 2] = xl[18*(1 + i)];
        zlin[4*i - 64 + 3] = xr[18*(1 + i)];
        DRMP3_V0(0) DRMP3_V2(1) DRMP3_V1(2) DRMP3_V2(3) DRMP3_V1(4) DRMP3_V2(5) DRMP3_V1(6) DRMP3_V2(7)
        {
#ifndef DR_MP3_FLOAT_OUTPUT
#if DRMP3_HAVE_SSE
            static const drmp3_f4 g_max = { 32767.0f, 32767.0f, 32767.0f, 32767.0f };
            static const drmp3_f4 g_min = { -32768.0f, -32768.0f, -32768.0f, -32768.0f };
            __m128i pcm8 = _mm_packs_epi32(_mm_cvtps_epi32(_mm_max_ps(_mm_min_ps(a, g_max), g_min)),
                                           _mm_cvtps_epi32(_mm_max_ps(_mm_min_ps(b, g_max), g_min)));
            dstr[(15 - i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 1);
            dstr[(17 + i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 5);
            dstl[(15 - i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 0);
            dstl[(17 + i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 4);
            dstr[(47 - i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 3);
            dstr[(49 + i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 7);
            dstl[(47 - i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 2);
            dstl[(49 + i)*nch] = (drmp3_int16)_mm_extract_epi16(pcm8, 6);
#else
            int16x4_t pcma, pcmb;
            a = DRMP3_VADD(a, DRMP3_VSET(0.5f));
            b = DRMP3_VADD(b, DRMP3_VSET(0.5f));
            pcma = vqmovn_s32(vqaddq_s32(vcvtq_s32_f32(a), vreinterpretq_s32_u32(vcltq_f32(a, DRMP3_VSET(0)))));
            pcmb = vqmovn_s32(vqaddq_s32(vcvtq_s32_f32(b), vreinterpretq_s32_u32(vcltq_f32(b, DRMP3_VSET(0)))));
            vst1_lane_s16(dstr + (15 - i)*nch, pcma, 1);
            vst1_lane_s16(dstr + (17 + i)*nch, pcmb, 1);
            vst1_lane_s16(dstl + (15 - i)*nch, pcma, 0);
            vst1_lane_s16(dstl + (17 + i)*nch, pcmb, 0);
            vst1_lane_s16(dstr + (47 - i)*nch, pcma, 3);
            vst1_lane_s16(dstr + (49 + i)*nch, pcmb, 3);
            vst1_lane_s16(dstl + (47 - i)*nch, pcma, 2);
            vst1_lane_s16(dstl + (49 + i)*nch, pcmb, 2);
#endif
#else
            static const drmp3_f4 g_scale = { 1.0f/32768.0f, 1.0f/32768.0f, 1.0f/32768.0f, 1.0f/32768.0f };
            a = DRMP3_VMUL(a, g_scale);
            b = DRMP3_VMUL(b, g_scale);
#if DRMP3_HAVE_SSE
            _mm_store_ss(dstr + (15 - i)*nch, _mm_shuffle_ps(a, a, _MM_SHUFFLE(1, 1, 1, 1)));
            _mm_store_ss(dstr + (17 + i)*nch, _mm_shuffle_ps(b, b, _MM_SHUFFLE(1, 1, 1, 1)));
            _mm_store_ss(dstl + (15 - i)*nch, _mm_shuffle_ps(a, a, _MM_SHUFFLE(0, 0, 0, 0)));
            _mm_store_ss(dstl + (17 + i)*nch, _mm_shuffle_ps(b, b, _MM_SHUFFLE(0, 0, 0, 0)));
            _mm_store_ss(dstr + (47 - i)*nch, _mm_shuffle_ps(a, a, _MM_SHUFFLE(3, 3, 3, 3)));
            _mm_store_ss(dstr + (49 + i)*nch, _mm_shuffle_ps(b, b, _MM_SHUFFLE(3, 3, 3, 3)));
            _mm_store_ss(dstl + (47 - i)*nch, _mm_shuffle_ps(a, a, _MM_SHUFFLE(2, 2, 2, 2)));
            _mm_store_ss(dstl + (49 + i)*nch, _mm_shuffle_ps(b, b, _MM_SHUFFLE(2, 2, 2, 2)));
#else
            vst1q_lane_f32(dstr + (15 - i)*nch, a, 1);
            vst1q_lane_f32(dstr + (17 + i)*nch, b, 1);
            vst1q_lane_f32(dstl + (15 - i)*nch, a, 0);
            vst1q_lane_f32(dstl + (17 + i)*nch, b, 0);
            vst1q_lane_f32(dstr + (47 - i)*nch, a, 3);
            vst1q_lane_f32(dstr + (49 + i)*nch, b, 3);
            vst1q_lane_f32(dstl + (47 - i)*nch, a, 2);
            vst1q_lane_f32(dstl + (49 + i)*nch, b, 2);
#endif
#endif
        }
    } else
#endif
#ifdef DR_MP3_ONLY_SIMD
    {}
#else
    for (i = 14; i >= 0; i--)
    {
#define DRMP3_LOAD(k) float w0 = *w++; float w1 = *w++; float *vz = &zlin[4*i - k*64]; float *vy = &zlin[4*i - (15 - k)*64];
#define DRMP3_S0(k) { int j; DRMP3_LOAD(k); for (j = 0; j < 4; j++) b[j]  = vz[j]*w1 + vy[j]*w0, a[j]  = vz[j]*w0 - vy[j]*w1; }
#define DRMP3_S1(k) { int j; DRMP3_LOAD(k); for (j = 0; j < 4; j++) b[j] += vz[j]*w1 + vy[j]*w0, a[j] += vz[j]*w0 - vy[j]*w1; }
#define DRMP3_S2(k) { int j; DRMP3_LOAD(k); for (j = 0; j < 4; j++) b[j] += vz[j]*w1 + vy[j]*w0, a[j] += vy[j]*w1 - vz[j]*w0; }
        float a[4], b[4];
        zlin[4*i]     = xl[18*(31 - i)];
        zlin[4*i + 1] = xr[18*(31 - i)];
        zlin[4*i + 2] = xl[1 + 18*(31 - i)];
        zlin[4*i + 3] = xr[1 + 18*(31 - i)];
        zlin[4*(i + 16)]   = xl[1 + 18*(1 + i)];
        zlin[4*(i + 16) + 1] = xr[1 + 18*(1 + i)];
        zlin[4*(i - 16) + 2] = xl[18*(1 + i)];
        zlin[4*(i - 16) + 3] = xr[18*(1 + i)];
        DRMP3_S0(0) DRMP3_S2(1) DRMP3_S1(2) DRMP3_S2(3) DRMP3_S1(4) DRMP3_S2(5) DRMP3_S1(6) DRMP3_S2(7)
        dstr[(15 - i)*nch] = drmp3d_scale_pcm(a[1]);
        dstr[(17 + i)*nch] = drmp3d_scale_pcm(b[1]);
        dstl[(15 - i)*nch] = drmp3d_scale_pcm(a[0]);
        dstl[(17 + i)*nch] = drmp3d_scale_pcm(b[0]);
        dstr[(47 - i)*nch] = drmp3d_scale_pcm(a[3]);
        dstr[(49 + i)*nch] = drmp3d_scale_pcm(b[3]);
        dstl[(47 - i)*nch] = drmp3d_scale_pcm(a[2]);
        dstl[(49 + i)*nch] = drmp3d_scale_pcm(b[2]);
    }
#endif
}



void drmp3d_synth_granule(float *qmf_state, float *grbuf, int nbands, int nch, drmp3d_sample_t *pcm, float *lins)
{

    int i;
    for (i = 0; i < nch; i++)
    {
      drmp3d_DCT_II(grbuf + 576*i, nbands, i+1000);
    }
    memcpy(lins, qmf_state, sizeof(float)*15*64);
    for (i = 0; i < nbands; i += 2)
    {
        drmp3d_synth(grbuf + i, pcm + 32*nch*i, nch, lins + i*64);
    }
#ifndef DR_MP3_NONSTANDARD_BUT_LOGICAL
    if (nch == 1)
    {
        for (i = 0; i < 15*64; i += 2)
        {
            qmf_state[i] = lins[nbands*64 + i];
        }
    } else
#endif
    {
        memcpy(qmf_state, lins + nbands*64, sizeof(float)*15*64);
    }
}

static const drmp3_L12_subband_alloc *drmp3_L12_subband_alloc_table(const drmp3_uint8 *hdr, drmp3_L12_scale_info *sci)
{
    const drmp3_L12_subband_alloc *alloc;
    int mode = DRMP3_HDR_GET_STEREO_MODE(hdr);
    int nbands, stereo_bands = (mode == DRMP3_MODE_MONO) ? 0 : (mode == DRMP3_MODE_JOINT_STEREO) ? (DRMP3_HDR_GET_STEREO_MODE_EXT(hdr) << 2) + 4 : 32;
    if (DRMP3_HDR_IS_LAYER_1(hdr))
    {
        static const drmp3_L12_subband_alloc g_alloc_L1[] = { { 76, 4, 32 } };
        alloc = g_alloc_L1;
        nbands = 32;
    } else if (!DRMP3_HDR_TEST_MPEG1(hdr))
    {
        static const drmp3_L12_subband_alloc g_alloc_L2M2[] = { { 60, 4, 4 }, { 44, 3, 7 }, { 44, 2, 19 } };
        alloc = g_alloc_L2M2;
        nbands = 30;
    } else
    {
        static const drmp3_L12_subband_alloc g_alloc_L2M1[] = { { 0, 4, 3 }, { 16, 4, 8 }, { 32, 3, 12 }, { 40, 2, 7 } };
        int sample_rate_idx = DRMP3_HDR_GET_SAMPLE_RATE(hdr);
        unsigned kbps = drmp3_hdr_bitrate_kbps(hdr) >> (int)(mode != DRMP3_MODE_MONO);
        if (!kbps)
        {
            kbps = 192;
        }
        alloc = g_alloc_L2M1;
        nbands = 27;
        if (kbps < 56)
        {
            static const drmp3_L12_subband_alloc g_alloc_L2M1_lowrate[] = { { 44, 4, 2 }, { 44, 3, 10 } };
            alloc = g_alloc_L2M1_lowrate;
            nbands = sample_rate_idx == 2 ? 12 : 8;
        } else if (kbps >= 96 && sample_rate_idx != 1)
        {
            nbands = 30;
        }
    }
    sci->total_bands = (drmp3_uint8)nbands;
    sci->stereo_bands = (drmp3_uint8)DRMP3_MIN(stereo_bands, nbands);
    return alloc;
}
static void drmp3_L12_read_scalefactors(drmp3_bs *bs, drmp3_uint8 *pba, drmp3_uint8 *scfcod, int bands, float *scf)
{
    static const float g_deq_L12[18*3] = {
#define DRMP3_DQ(x) 9.53674316e-07f/x, 7.56931807e-07f/x, 6.00777173e-07f/x
        DRMP3_DQ(3),DRMP3_DQ(7),DRMP3_DQ(15),DRMP3_DQ(31),DRMP3_DQ(63),DRMP3_DQ(127),DRMP3_DQ(255),DRMP3_DQ(511),DRMP3_DQ(1023),DRMP3_DQ(2047),DRMP3_DQ(4095),DRMP3_DQ(8191),DRMP3_DQ(16383),DRMP3_DQ(32767),DRMP3_DQ(65535),DRMP3_DQ(3),DRMP3_DQ(5),DRMP3_DQ(9)
    };
    int i, m;
    for (i = 0; i < bands; i++)
    {
        float s = 0;
        int ba = *pba++;
        int mask = ba ? 4 + ((19 >> scfcod[i]) & 3) : 0;
        for (m = 4; m; m >>= 1)
        {
            if (mask & m)
            {
                int b = drmp3_bs_get_bits(bs, 6);
                s = g_deq_L12[ba*3 - 6 + b % 3]*(int)(1 << 21 >> b/3);
            }
            *scf++ = s;
        }
    }
}





int drmp3_L12_dequantize_granule(float *grbuf, drmp3_bs *bs, drmp3_L12_scale_info *sci, int group_size)
{
  assert(0);
    int i, j, k, choff = 576;
    for (j = 0; j < 4; j++)
    {
        float *dst = grbuf + group_size*j;
        for (i = 0; i < 2*sci->total_bands; i++)
        {
            int ba = sci->bitalloc[i];
            if (ba != 0)
            {
                if (ba < 17)
                {
                    int half = (1 << (ba - 1)) - 1;
                    for (k = 0; k < group_size; k++)
                    {
                        dst[k] = (float)((int)drmp3_bs_get_bits(bs, ba) - half);
                    }
                } else
                {
                    unsigned mod = (2 << (ba - 17)) + 1;
                    unsigned code = drmp3_bs_get_bits(bs, mod + 2 - (mod >> 3));
                    for (k = 0; k < group_size; k++, code /= mod)
                    {
                        dst[k] = (float)((int)(code % mod - mod/2));
                    }
                }
            }
            dst += choff;
            choff = 18 - choff;
        }
    }
    return group_size*4;
}

static int n_scf_times = 0;
void drmp3_L12_apply_scf_384(drmp3_L12_scale_info *sci, const float *scf, float *dst)
{
    int i, k;
    memcpy(dst + 576 + sci->stereo_bands*18, dst + sci->stereo_bands*18, (sci->total_bands - sci->stereo_bands)*18*sizeof(float));
    for (i = 0; i < sci->total_bands; i++, dst += 18, scf += 6)
    {
        for (k = 0; k < 12; k++)
        {
            dst[k + 0]   *= scf[0];
            dst[k + 576] *= scf[3];
	    n_scf_times += 2;
        }
    }
    printf("scf n_scf_times %i\n", n_scf_times);
}




static void drmp3_L12_read_scale_info(const drmp3_uint8 *hdr, drmp3_bs *bs, drmp3_L12_scale_info *sci)
{
    static const drmp3_uint8 g_bitalloc_code_tab[] = {
        0,17, 3, 4, 5,6,7, 8,9,10,11,12,13,14,15,16,
        0,17,18, 3,19,4,5, 6,7, 8, 9,10,11,12,13,16,
        0,17,18, 3,19,4,5,16,
        0,17,18,16,
        0,17,18,19, 4,5,6, 7,8, 9,10,11,12,13,14,15,
        0,17,18, 3,19,4,5, 6,7, 8, 9,10,11,12,13,14,
        0, 2, 3, 4, 5,6,7, 8,9,10,11,12,13,14,15,16
    };
    const drmp3_L12_subband_alloc *subband_alloc = drmp3_L12_subband_alloc_table(hdr, sci);
    int i, k = 0, ba_bits = 0;
    const drmp3_uint8 *ba_code_tab = g_bitalloc_code_tab;
    for (i = 0; i < sci->total_bands; i++)
    {
        drmp3_uint8 ba;
        if (i == k)
        {
            k += subband_alloc->band_count;
            ba_bits = subband_alloc->code_tab_width;
            ba_code_tab = g_bitalloc_code_tab + subband_alloc->tab_offset;
            subband_alloc++;
        }
        ba = ba_code_tab[drmp3_bs_get_bits(bs, ba_bits)];
        sci->bitalloc[2*i] = ba;
        if (i < sci->stereo_bands)
        {
            ba = ba_code_tab[drmp3_bs_get_bits(bs, ba_bits)];
        }
        sci->bitalloc[2*i + 1] = sci->stereo_bands ? ba : 0;
    }
    for (i = 0; i < 2*sci->total_bands; i++)
    {
        sci->scfcod[i] = (drmp3_uint8)(sci->bitalloc[i] ? DRMP3_HDR_IS_LAYER_1(hdr) ? 2 : drmp3_bs_get_bits(bs, 2) : 6);
    }
    drmp3_L12_read_scalefactors(bs, sci->bitalloc, sci->scfcod, sci->total_bands*2, sci->scf);
    for (i = sci->stereo_bands; i < sci->total_bands; i++)
    {
        sci->bitalloc[2*i + 1] = 0;
    }
}


int drmp3dec_decode_frame(drmp3dec *dec, const drmp3_uint8 *mp3, int mp3_bytes, void *pcm, drmp3dec_frame_info *info)
{
    int i = 0, igr, frame_size = 0, success = 1;
    const drmp3_uint8 *hdr;
    drmp3_bs bs_frame[1];
    drmp3dec_scratch scratch;
    if (mp3_bytes > 4 && dec->header[0] == 0xff && drmp3_hdr_compare(dec->header, mp3))
    {
        frame_size = drmp3_hdr_frame_bytes(mp3, dec->free_format_bytes) + drmp3_hdr_padding(mp3);
        if (frame_size != mp3_bytes && (frame_size + DRMP3_HDR_SIZE > mp3_bytes || !drmp3_hdr_compare(mp3, mp3 + frame_size)))
        {
            frame_size = 0;
        }
    }
    if (!frame_size)
    {
        memset(dec, 0, sizeof(drmp3dec));
        i = drmp3d_find_frame(mp3, mp3_bytes, &dec->free_format_bytes, &frame_size);
        if (!frame_size || i + frame_size > mp3_bytes)
        {
            info->frame_bytes = i;
            return 0;
        }
    }
    hdr = mp3 + i;
    memcpy(dec->header, hdr, DRMP3_HDR_SIZE);
    info->frame_bytes = i + frame_size;
    info->channels = DRMP3_HDR_IS_MONO(hdr) ? 1 : 2;
    info->hz = drmp3_hdr_sample_rate_hz(hdr);
    info->layer = 4 - DRMP3_HDR_GET_LAYER(hdr);
    info->bitrate_kbps = drmp3_hdr_bitrate_kbps(hdr);
    drmp3_bs_init(bs_frame, hdr + DRMP3_HDR_SIZE, frame_size - DRMP3_HDR_SIZE);
    if (DRMP3_HDR_IS_CRC(hdr))
    {
        drmp3_bs_get_bits(bs_frame, 16);
    }
    if (info->layer == 3)
    {

        int main_data_begin = drmp3_L3_read_side_info(bs_frame, scratch.gr_info, hdr);
        if (main_data_begin < 0 || bs_frame->pos > bs_frame->limit)
        {
            drmp3dec_init(dec);
            return 0;
        }
        success = drmp3_L3_restore_reservoir(dec, bs_frame, &scratch, main_data_begin);
        if (success && pcm != NULL)
        {
            for (igr = 0; igr < (DRMP3_HDR_TEST_MPEG1(hdr) ? 2 : 1); igr++, pcm = DRMP3_OFFSET_PTR(pcm, sizeof(drmp3d_sample_t)*576*info->channels))
            {
                memset(scratch.grbuf[0], 0, 576*2*sizeof(float));
                drmp3_L3_decode(dec, &scratch, scratch.gr_info + igr*info->channels, info->channels);
                drmp3d_synth_granule(dec->qmf_state, scratch.grbuf[0], 18, info->channels, (drmp3d_sample_t*)pcm, scratch.syn[0]);
            }
        }
        drmp3_L3_save_reservoir(dec, &scratch);
    } else
    {
#ifdef DR_MP3_ONLY_MP3
        return 0;
#else
	assert(0);
        drmp3_L12_scale_info sci[1];
        if (pcm == NULL) {
            return drmp3_hdr_frame_samples(hdr);
        }
        drmp3_L12_read_scale_info(hdr, bs_frame, sci);
        memset(scratch.grbuf[0], 0, 576*2*sizeof(float));
        for (i = 0, igr = 0; igr < 3; igr++)
        {
            if (12 == (i += drmp3_L12_dequantize_granule(scratch.grbuf[0] + i, bs_frame, sci, info->layer | 1)))
            {
                i = 0;
                drmp3_L12_apply_scf_384(sci, sci->scf + igr, scratch.grbuf[0]);
                drmp3d_synth_granule(dec->qmf_state, scratch.grbuf[0], 12, info->channels, (drmp3d_sample_t*)pcm, scratch.syn[0]);
                memset(scratch.grbuf[0], 0, 576*2*sizeof(float));
                pcm = DRMP3_OFFSET_PTR(pcm, sizeof(drmp3d_sample_t)*384*info->channels);
            }
            if (bs_frame->pos > bs_frame->limit)
            {
                drmp3dec_init(dec);
                return 0;
            }
        }
#endif
    }
    if (success > 0) n_frames += drmp3_hdr_frame_samples(dec->header);
    return success*drmp3_hdr_frame_samples(dec->header);
}


void* drmp3__realloc_from_callbacks(void* p, size_t szNew, size_t szOld, const drmp3_allocation_callbacks* pAllocationCallbacks)
{
    if (pAllocationCallbacks == NULL) {
        return NULL;
    }
    if (pAllocationCallbacks->onRealloc != NULL) {
        return pAllocationCallbacks->onRealloc(p, szNew, pAllocationCallbacks->pUserData);
    }
    if (pAllocationCallbacks->onMalloc != NULL && pAllocationCallbacks->onFree != NULL) {
        void* p2;
        p2 = pAllocationCallbacks->onMalloc(szNew, pAllocationCallbacks->pUserData);
        if (p2 == NULL) {
            return NULL;
        }
        if (p != NULL) {
            DRMP3_COPY_MEMORY(p2, p, szOld);
            pAllocationCallbacks->onFree(p, pAllocationCallbacks->pUserData);
        }
        return p2;
    }
    return NULL;
}

static size_t drmp3__on_read(drmp3* pMP3, void* pBufferOut, size_t bytesToRead)
{
    size_t bytesRead = pMP3->onRead(pMP3->pUserData, pBufferOut, bytesToRead);
    pMP3->streamCursor += bytesRead;
    return bytesRead;
}

drmp3_uint32 drmp3_decode_next_frame_ex__callbacks(drmp3* pMP3, drmp3d_sample_t* pPCMFrames)
{
    drmp3_uint32 pcmFramesRead = 0;
    DRMP3_ASSERT(pMP3 != NULL);
    DRMP3_ASSERT(pMP3->onRead != NULL);
    if (pMP3->atEnd) {
        return 0;
    }
    for (;;) {
        drmp3dec_frame_info info;
        if (pMP3->dataSize < DRMP3_MIN_DATA_CHUNK_SIZE) {
            size_t bytesRead;
            memmove(pMP3->pData, pMP3->pData + pMP3->dataConsumed, pMP3->dataSize);
            pMP3->dataConsumed = 0;
            if (pMP3->dataCapacity < DRMP3_DATA_CHUNK_SIZE) {
                drmp3_uint8* pNewData;
                size_t newDataCap;
                newDataCap = DRMP3_DATA_CHUNK_SIZE;
                pNewData = (drmp3_uint8*)drmp3__realloc_from_callbacks(pMP3->pData, newDataCap, pMP3->dataCapacity, &pMP3->allocationCallbacks);
                if (pNewData == NULL) {
                    return 0;
                }
                pMP3->pData = pNewData;
                pMP3->dataCapacity = newDataCap;
            }
            bytesRead = drmp3__on_read(pMP3, pMP3->pData + pMP3->dataSize, (pMP3->dataCapacity - pMP3->dataSize));
            if (bytesRead == 0) {
                if (pMP3->dataSize == 0) {
                    pMP3->atEnd = DRMP3_TRUE;
                    return 0;
                }
            }
            pMP3->dataSize += bytesRead;
        }
        if (pMP3->dataSize > INT_MAX) {
            pMP3->atEnd = DRMP3_TRUE;
            return 0;
        }
	//>H
        pcmFramesRead = drmp3dec_decode_frame(&pMP3->decoder, pMP3->pData + pMP3->dataConsumed, (int)pMP3->dataSize, pPCMFrames, &info);
        if (info.frame_bytes > 0) {
            pMP3->dataConsumed += (size_t)info.frame_bytes;
            pMP3->dataSize     -= (size_t)info.frame_bytes;
        }
        if (pcmFramesRead > 0) {
            pcmFramesRead = drmp3_hdr_frame_samples(pMP3->decoder.header);
            pMP3->pcmFramesConsumedInMP3Frame = 0;
            pMP3->pcmFramesRemainingInMP3Frame = pcmFramesRead;
            pMP3->mp3FrameChannels = info.channels;
            pMP3->mp3FrameSampleRate = info.hz;
            break;
        } else if (info.frame_bytes == 0) {
            size_t bytesRead;
            memmove(pMP3->pData, pMP3->pData + pMP3->dataConsumed, pMP3->dataSize);
            pMP3->dataConsumed = 0;
            if (pMP3->dataCapacity == pMP3->dataSize) {
                drmp3_uint8* pNewData;
                size_t newDataCap;
                newDataCap = pMP3->dataCapacity + DRMP3_DATA_CHUNK_SIZE;
                pNewData = (drmp3_uint8*)drmp3__realloc_from_callbacks(pMP3->pData, newDataCap, pMP3->dataCapacity, &pMP3->allocationCallbacks);
                if (pNewData == NULL) {
                    return 0;
                }
                pMP3->pData = pNewData;
                pMP3->dataCapacity = newDataCap;
            }
            bytesRead = drmp3__on_read(pMP3, pMP3->pData + pMP3->dataSize, (pMP3->dataCapacity - pMP3->dataSize));
            if (bytesRead == 0) {
                pMP3->atEnd = DRMP3_TRUE;
                return 0;
            }
            pMP3->dataSize += bytesRead;
        }
    };
    return pcmFramesRead;
}
static drmp3_uint32 drmp3_decode_next_frame_ex__memory(drmp3* pMP3, drmp3d_sample_t* pPCMFrames)
{
    drmp3_uint32 pcmFramesRead = 0;
    drmp3dec_frame_info info;
    DRMP3_ASSERT(pMP3 != NULL);
    DRMP3_ASSERT(pMP3->memory.pData != NULL);
    if (pMP3->atEnd) {
        return 0;
    }
    pcmFramesRead = drmp3dec_decode_frame(&pMP3->decoder, pMP3->memory.pData + pMP3->memory.currentReadPos, (int)(pMP3->memory.dataSize - pMP3->memory.currentReadPos), pPCMFrames, &info);
    if (pcmFramesRead > 0) {
        pMP3->pcmFramesConsumedInMP3Frame  = 0;
        pMP3->pcmFramesRemainingInMP3Frame = pcmFramesRead;
        pMP3->mp3FrameChannels             = info.channels;
        pMP3->mp3FrameSampleRate           = info.hz;
    }
    pMP3->memory.currentReadPos += (size_t)info.frame_bytes;
    return pcmFramesRead;
}

drmp3_uint32 drmp3_decode_next_frame_ex(drmp3* pMP3, drmp3d_sample_t* pPCMFrames)
{
    if (pMP3->memory.pData != NULL && pMP3->memory.dataSize > 0) {
        return drmp3_decode_next_frame_ex__memory(pMP3, pPCMFrames);
    } else {
        return drmp3_decode_next_frame_ex__callbacks(pMP3, pPCMFrames);
    }
}

drmp3_uint32 drmp3_decode_next_frame(drmp3* pMP3)
{
    DRMP3_ASSERT(pMP3 != NULL);
    return drmp3_decode_next_frame_ex(pMP3, (drmp3d_sample_t*)pMP3->pcmFrames);
}

int drmp3_L3_read_side_info(drmp3_bs *bs, drmp3_L3_gr_info *gr, const drmp3_uint8 *hdr)
{
    static const drmp3_uint8 g_scf_long[8][23] = {
        { 6,6,6,6,6,6,8,10,12,14,16,20,24,28,32,38,46,52,60,68,58,54,0 },
        { 12,12,12,12,12,12,16,20,24,28,32,40,48,56,64,76,90,2,2,2,2,2,0 },
        { 6,6,6,6,6,6,8,10,12,14,16,20,24,28,32,38,46,52,60,68,58,54,0 },
        { 6,6,6,6,6,6,8,10,12,14,16,18,22,26,32,38,46,54,62,70,76,36,0 },
        { 6,6,6,6,6,6,8,10,12,14,16,20,24,28,32,38,46,52,60,68,58,54,0 },
        { 4,4,4,4,4,4,6,6,8,8,10,12,16,20,24,28,34,42,50,54,76,158,0 },
        { 4,4,4,4,4,4,6,6,6,8,10,12,16,18,22,28,34,40,46,54,54,192,0 },
        { 4,4,4,4,4,4,6,6,8,10,12,16,20,24,30,38,46,56,68,84,102,26,0 }
    };
    static const drmp3_uint8 g_scf_short[8][40] = {
        { 4,4,4,4,4,4,4,4,4,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,30,30,30,40,40,40,18,18,18,0 },
        { 8,8,8,8,8,8,8,8,8,12,12,12,16,16,16,20,20,20,24,24,24,28,28,28,36,36,36,2,2,2,2,2,2,2,2,2,26,26,26,0 },
        { 4,4,4,4,4,4,4,4,4,6,6,6,6,6,6,8,8,8,10,10,10,14,14,14,18,18,18,26,26,26,32,32,32,42,42,42,18,18,18,0 },
        { 4,4,4,4,4,4,4,4,4,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,32,32,32,44,44,44,12,12,12,0 },
        { 4,4,4,4,4,4,4,4,4,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,30,30,30,40,40,40,18,18,18,0 },
        { 4,4,4,4,4,4,4,4,4,4,4,4,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,22,22,22,30,30,30,56,56,56,0 },
        { 4,4,4,4,4,4,4,4,4,4,4,4,6,6,6,6,6,6,10,10,10,12,12,12,14,14,14,16,16,16,20,20,20,26,26,26,66,66,66,0 },
        { 4,4,4,4,4,4,4,4,4,4,4,4,6,6,6,8,8,8,12,12,12,16,16,16,20,20,20,26,26,26,34,34,34,42,42,42,12,12,12,0 }
    };
    static const drmp3_uint8 g_scf_mixed[8][40] = {
        { 6,6,6,6,6,6,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,30,30,30,40,40,40,18,18,18,0 },
        { 12,12,12,4,4,4,8,8,8,12,12,12,16,16,16,20,20,20,24,24,24,28,28,28,36,36,36,2,2,2,2,2,2,2,2,2,26,26,26,0 },
        { 6,6,6,6,6,6,6,6,6,6,6,6,8,8,8,10,10,10,14,14,14,18,18,18,26,26,26,32,32,32,42,42,42,18,18,18,0 },
        { 6,6,6,6,6,6,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,32,32,32,44,44,44,12,12,12,0 },
        { 6,6,6,6,6,6,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,24,24,24,30,30,30,40,40,40,18,18,18,0 },
        { 4,4,4,4,4,4,6,6,4,4,4,6,6,6,8,8,8,10,10,10,12,12,12,14,14,14,18,18,18,22,22,22,30,30,30,56,56,56,0 },
        { 4,4,4,4,4,4,6,6,4,4,4,6,6,6,6,6,6,10,10,10,12,12,12,14,14,14,16,16,16,20,20,20,26,26,26,66,66,66,0 },
        { 4,4,4,4,4,4,6,6,4,4,4,6,6,6,8,8,8,12,12,12,16,16,16,20,20,20,26,26,26,34,34,34,42,42,42,12,12,12,0 }
    };
    unsigned tables, scfsi = 0;
    int main_data_begin, part_23_sum = 0;
    int gr_count = DRMP3_HDR_IS_MONO(hdr) ? 1 : 2;
    int sr_idx = DRMP3_HDR_GET_MY_SAMPLE_RATE(hdr); sr_idx -= (sr_idx != 0);
    if (DRMP3_HDR_TEST_MPEG1(hdr))
    {
        gr_count *= 2;
        main_data_begin = drmp3_bs_get_bits(bs, 9);
        scfsi = drmp3_bs_get_bits(bs, 7 + gr_count);
    } else
    {
        main_data_begin = drmp3_bs_get_bits(bs, 8 + gr_count) >> gr_count;
    }
    do
    {
        if (DRMP3_HDR_IS_MONO(hdr))
        {
            scfsi <<= 4;
        }
        gr->part_23_length = (drmp3_uint16)drmp3_bs_get_bits(bs, 12);
        part_23_sum += gr->part_23_length;
        gr->big_values = (drmp3_uint16)drmp3_bs_get_bits(bs,  9);
        if (gr->big_values > 288)
        {
            return -1;
        }
        gr->global_gain = (drmp3_uint8)drmp3_bs_get_bits(bs, 8);
        gr->scalefac_compress = (drmp3_uint16)drmp3_bs_get_bits(bs, DRMP3_HDR_TEST_MPEG1(hdr) ? 4 : 9);
        gr->sfbtab = g_scf_long[sr_idx];
        gr->n_long_sfb  = 22;
        gr->n_short_sfb = 0;
        if (drmp3_bs_get_bits(bs, 1))
        {
            gr->block_type = (drmp3_uint8)drmp3_bs_get_bits(bs, 2);
            if (!gr->block_type)
            {
                return -1;
            }
            gr->mixed_block_flag = (drmp3_uint8)drmp3_bs_get_bits(bs, 1);
            gr->region_count[0] = 7;
            gr->region_count[1] = 255;
            if (gr->block_type == DRMP3_SHORT_BLOCK_TYPE)
            {
                scfsi &= 0x0F0F;
                if (!gr->mixed_block_flag)
                {
                    gr->region_count[0] = 8;
                    gr->sfbtab = g_scf_short[sr_idx];
                    gr->n_long_sfb = 0;
                    gr->n_short_sfb = 39;
                } else
                {
                    gr->sfbtab = g_scf_mixed[sr_idx];
                    gr->n_long_sfb = DRMP3_HDR_TEST_MPEG1(hdr) ? 8 : 6;
                    gr->n_short_sfb = 30;
                }
            }
            tables = drmp3_bs_get_bits(bs, 10);
            tables <<= 5;
            gr->subblock_gain[0] = (drmp3_uint8)drmp3_bs_get_bits(bs, 3);
            gr->subblock_gain[1] = (drmp3_uint8)drmp3_bs_get_bits(bs, 3);
            gr->subblock_gain[2] = (drmp3_uint8)drmp3_bs_get_bits(bs, 3);
        } else
        {
            gr->block_type = 0;
            gr->mixed_block_flag = 0;
            tables = drmp3_bs_get_bits(bs, 15);
            gr->region_count[0] = (drmp3_uint8)drmp3_bs_get_bits(bs, 4);
            gr->region_count[1] = (drmp3_uint8)drmp3_bs_get_bits(bs, 3);
            gr->region_count[2] = 255;
        }
        gr->table_select[0] = (drmp3_uint8)(tables >> 10);
        gr->table_select[1] = (drmp3_uint8)((tables >> 5) & 31);
        gr->table_select[2] = (drmp3_uint8)((tables) & 31);
        gr->preflag = (drmp3_uint8)(DRMP3_HDR_TEST_MPEG1(hdr) ? drmp3_bs_get_bits(bs, 1) : (gr->scalefac_compress >= 500));
        gr->scalefac_scale = (drmp3_uint8)drmp3_bs_get_bits(bs, 1);
        gr->count1_table = (drmp3_uint8)drmp3_bs_get_bits(bs, 1);
        gr->scfsi = (drmp3_uint8)((scfsi >> 12) & 15);
        scfsi <<= 4;
        gr++;
    } while(--gr_count);
    if (part_23_sum + bs->pos > bs->limit + main_data_begin*8)
    {
        return -1;
    }
    return main_data_begin;
}

// eof
