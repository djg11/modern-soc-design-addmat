/*
Random Number Generation

miniaudio uses the LCG random number generation algorithm. This is good enough for audio.

Note that miniaudio's global LCG implementation uses global state which is _not_ thread-local. When this is called across
multiple threads, results will be unpredictable. However, it won't crash and results will still be random enough for
miniaudio's purposes.
*/
#ifndef MA_DEFAULT_LCG_SEED
#define MA_DEFAULT_LCG_SEED 4321
#endif

#define MA_LCG_M   2147483647
#define MA_LCG_A   48271
#define MA_LCG_C   0

static ma_lcg g_maLCG = {MA_DEFAULT_LCG_SEED}; /* Non-zero initial seed. Use ma_seed() to use an explicit seed. */

static MA_INLINE void ma_lcg_seed(ma_lcg* pLCG, ma_int32 seed)
{
    MA_ASSERT(pLCG != NULL);
    pLCG->state = seed;
}

static MA_INLINE ma_int32 ma_lcg_rand_s32(ma_lcg* pLCG)
{
    pLCG->state = (MA_LCG_A * pLCG->state + MA_LCG_C) % MA_LCG_M;
    return pLCG->state;
}

static MA_INLINE ma_uint32 ma_lcg_rand_u32(ma_lcg* pLCG)
{
    return (ma_uint32)ma_lcg_rand_s32(pLCG);
}

static MA_INLINE ma_int16 ma_lcg_rand_s16(ma_lcg* pLCG)
{
    return (ma_int16)(ma_lcg_rand_s32(pLCG) & 0xFFFF);
}

static MA_INLINE double ma_lcg_rand_f64(ma_lcg* pLCG)
{
    return ma_lcg_rand_s32(pLCG) / (double)0x7FFFFFFF;
}

static MA_INLINE float ma_lcg_rand_f32(ma_lcg* pLCG)
{
    return (float)ma_lcg_rand_f64(pLCG);
}

static MA_INLINE float ma_lcg_rand_range_f32(ma_lcg* pLCG, float lo, float hi)
{
    return ma_scale_to_range_f32(ma_lcg_rand_f32(pLCG), lo, hi);
}

static MA_INLINE ma_int32 ma_lcg_rand_range_s32(ma_lcg* pLCG, ma_int32 lo, ma_int32 hi)
{
    if (lo == hi) {
        return lo;
    }

    return lo + ma_lcg_rand_u32(pLCG) / (0xFFFFFFFF / (hi - lo + 1) + 1);
}



static MA_INLINE void ma_seed(ma_int32 seed)
{
    ma_lcg_seed(&g_maLCG, seed);
}

static MA_INLINE ma_int32 ma_rand_s32(void)
{
    return ma_lcg_rand_s32(&g_maLCG);
}

static MA_INLINE ma_uint32 ma_rand_u32(void)
{
    return ma_lcg_rand_u32(&g_maLCG);
}

static MA_INLINE double ma_rand_f64(void)
{
    return ma_lcg_rand_f64(&g_maLCG);
}

static MA_INLINE float ma_rand_f32(void)
{
    return ma_lcg_rand_f32(&g_maLCG);
}

static MA_INLINE float ma_rand_range_f32(float lo, float hi)
{
    return ma_lcg_rand_range_f32(&g_maLCG, lo, hi);
}

static MA_INLINE ma_int32 ma_rand_range_s32(ma_int32 lo, ma_int32 hi)
{
    return ma_lcg_rand_range_s32(&g_maLCG, lo, hi);
}


static MA_INLINE float ma_dither_f32_rectangle(float ditherMin, float ditherMax)
{
    return ma_rand_range_f32(ditherMin, ditherMax);
}

static MA_INLINE float ma_dither_f32_triangle(float ditherMin, float ditherMax)
{
    float a = ma_rand_range_f32(ditherMin, 0);
    float b = ma_rand_range_f32(0, ditherMax);
    return a + b;
}

static MA_INLINE float ma_dither_f32(ma_dither_mode ditherMode, float ditherMin, float ditherMax)
{
    if (ditherMode == ma_dither_mode_rectangle) {
        return ma_dither_f32_rectangle(ditherMin, ditherMax);
    }
    if (ditherMode == ma_dither_mode_triangle) {
        return ma_dither_f32_triangle(ditherMin, ditherMax);
    }

    return 0;
}

static MA_INLINE ma_int32 ma_dither_s32(ma_dither_mode ditherMode, ma_int32 ditherMin, ma_int32 ditherMax)
{
    if (ditherMode == ma_dither_mode_rectangle) {
        ma_int32 a = ma_rand_range_s32(ditherMin, ditherMax);
        return a;
    }
    if (ditherMode == ma_dither_mode_triangle) {
        ma_int32 a = ma_rand_range_s32(ditherMin, 0);
        ma_int32 b = ma_rand_range_s32(0, ditherMax);
        return a + b;
    }

    return 0;
}

