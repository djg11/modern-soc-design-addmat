

/* MP3 */
#define MA_HAS_MP3

size_t ma_decoder_internal_on_read__mp3(void* pUserData, void* pBufferOut, size_t bytesToRead)
{
    ma_decoder* pDecoder = (ma_decoder*)pUserData;
    MA_ASSERT(pDecoder != NULL);

    return ma_decoder_read_bytes(pDecoder, pBufferOut, bytesToRead);
}

static drmp3_bool32 ma_decoder_internal_on_seek__mp3(void* pUserData, int offset, drmp3_seek_origin origin)
{
    ma_decoder* pDecoder = (ma_decoder*)pUserData;
    MA_ASSERT(pDecoder != NULL);
    assert(0);
    return 0; //ma_decoder_seek_bytes(pDecoder, offset, (origin == drmp3_seek_origin_start) ? ma_seek_origin_start : ma_seek_origin_current);
}

static ma_uint64 ma_decoder_internal_on_read_pcm_frames__mp3(ma_decoder* pDecoder, void* pFramesOut, ma_uint64 frameCount)
{
    drmp3* pMP3;

    MA_ASSERT(pDecoder   != NULL);
    MA_ASSERT(pFramesOut != NULL);

    pMP3 = (drmp3*)pDecoder->pInternalDecoder;
    MA_ASSERT(pMP3 != NULL);

#if defined(DR_MP3_FLOAT_OUTPUT)
    MA_ASSERT(pDecoder->internalFormat == ma_format_f32);
    return drmp3_read_pcm_frames_f32(pMP3, frameCount, (float*)pFramesOut);
#else
    MA_ASSERT(pDecoder->internalFormat == ma_format_s16);
    return drmp3_read_pcm_frames_s16(pMP3, frameCount, (drmp3_int16*)pFramesOut);
#endif
}

static ma_result ma_decoder_internal_on_seek_to_pcm_frame__mp3(ma_decoder* pDecoder, ma_uint64 frameIndex)
{
    drmp3* pMP3;
    drmp3_bool32 result;

    pMP3 = (drmp3*)pDecoder->pInternalDecoder;
    MA_ASSERT(pMP3 != NULL);

    assert(0);
    //    result = drmp3_seek_to_pcm_frame(pMP3, frameIndex);
    if (result) {
        return MA_SUCCESS;
    } else {
        return MA_ERROR;
    }
}

static ma_result ma_decoder_internal_on_uninit__mp3(ma_decoder* pDecoder)
{
  assert(0);
  //drmp3_uninit((drmp3*)pDecoder->pInternalDecoder);
  //ma__free_from_callbacks(pDecoder->pInternalDecoder, &pDecoder->allocationCallbacks);
  return MA_SUCCESS;
}

static ma_uint64 ma_decoder_internal_on_get_length_in_pcm_frames__mp3(ma_decoder* pDecoder)
{
    return drmp3_get_pcm_frame_count((drmp3*)pDecoder->pInternalDecoder);
}

ma_result ma_decoder_init_mp3__internal(const ma_decoder_config* pConfig, ma_decoder* pDecoder)
{
    drmp3* pMP3;
    drmp3_allocation_callbacks allocationCallbacks;

    MA_ASSERT(pConfig != NULL);
    MA_ASSERT(pDecoder != NULL);

    pMP3 = (drmp3*)ma__malloc_from_callbacks(sizeof(*pMP3), &pDecoder->allocationCallbacks);
    if (pMP3 == NULL) {
        return MA_OUT_OF_MEMORY;
    }

    allocationCallbacks.pUserData = pDecoder->allocationCallbacks.pUserData;
    allocationCallbacks.onMalloc  = pDecoder->allocationCallbacks.onMalloc;
    allocationCallbacks.onRealloc = pDecoder->allocationCallbacks.onRealloc;
    allocationCallbacks.onFree    = pDecoder->allocationCallbacks.onFree;

    /*
    Try opening the decoder first. We always use whatever dr_mp3 reports for channel count and sample rate. The format is determined by
    the presence of DR_MP3_FLOAT_OUTPUT.
    */
    if (!drmp3_init(pMP3, ma_decoder_internal_on_read__mp3, ma_decoder_internal_on_seek__mp3, pDecoder, &allocationCallbacks)) {
        ma__free_from_callbacks(pMP3, &pDecoder->allocationCallbacks);
        return MA_ERROR;
    }

    /* If we get here it means we successfully initialized the MP3 decoder. We can now initialize the rest of the ma_decoder. */
    pDecoder->onReadPCMFrames        = ma_decoder_internal_on_read_pcm_frames__mp3;
    pDecoder->onSeekToPCMFrame       = ma_decoder_internal_on_seek_to_pcm_frame__mp3;
    pDecoder->onUninit               = ma_decoder_internal_on_uninit__mp3;
    pDecoder->onGetLengthInPCMFrames = ma_decoder_internal_on_get_length_in_pcm_frames__mp3;
    pDecoder->pInternalDecoder       = pMP3;

    /* Internal format. */
#if defined(DR_MP3_FLOAT_OUTPUT)
    pDecoder->internalFormat     = ma_format_f32;
#else
    pDecoder->internalFormat     = ma_format_s16;
#endif
    pDecoder->internalChannels   = pMP3->channels;
    pDecoder->internalSampleRate = pMP3->sampleRate;
    ma_get_standard_channel_map(ma_standard_channel_map_default, pDecoder->internalChannels, pDecoder->internalChannelMap);

    return MA_SUCCESS;
}


