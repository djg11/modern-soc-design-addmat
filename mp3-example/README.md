## mp3-example ##
# Contents of the mp3-example additional material for "Modern SoC Design, First Edition" by DJ Greaves. 2021.

For the Design Exploration chapter, the full experimental setup for the MP3 example used to estimate the ALU and memory
requirement for basic audio decoding.



#
# LICENSE NOTICE:
# No waranties are implied or expressed.  The original material in this folder is taken from https://github.com/dr-soft/miniaudio
# which was released with two alternative licenses: 1 - Public Domain (www.unlicense.org) and 2 - MIT No Attribution.
# The additional Makefile, djgmain.c and ancillary instrumentation files are licensed to you by DJ Greaves under the
# same conditions.

In this folder, a freely available MP3 implementation was first fetched using
  git clone https://github.com/dr-soft/miniaudio


That system contains a lot of software that supports a wide variety of compilation modes with different backends and operating systems.
The relevant parts were cut out by hand and placed in a few files for easier navigation.

The selection of the pulseaudio backend was manually hardcoded and the MMX extensions were disabled with additional flags.


total 6624
-rw-rw-r--. 1 djg11 djg11    4140 Jun 27  2020 mp3bimp.h
-rw-rw-r--. 1 djg11 djg11  172212 Jul  4  2020 mp3core.c
-rw-rw-r--. 1 djg11 djg11 1836428 Jun 28  2020 bimp.h
-rw-rw-r--. 1 djg11 djg11   49119 Jun 26  2020 c89atomic.h
-rw-rw-r--. 1 djg11 djg11   75532 Jun 29  2020 formats.h
-rw-rw-r--. 1 djg11 djg11  100380 Jun 27  2020 hacked_audio.h
-rw-rw-r--. 1 djg11 djg11   94242 Jun 28  2020 mytop.h

I then wrote a few testbench files to make it run and report the instrumentation results:
-rw-rw-r--. 1 djg11 djg11  105165 Jun 28  2020 pulsedjg.h
-rw-rw-r--. 1 djg11 djg11  147318 Jun 28  2020 djgmain.c
-rw-rw-r--. 1 djg11 djg11    1239 Jul 27 15:11 Makefile


Rather than running on virtual platform to get ALU counts, the code
was instrumented with a few macros to record the call counts to each
subroutine and with manual annotations of the number of ALU operations
applied to data.  Loop control and array subscription arithmetic was
ignored since it is negligable for VLSI implementation ane likely to
be totally restructured duing any HLS or manual refactoring.


The macros include IADD and FTIMES etc. to keep track of integer and
FP addition (including subtraction) and multiplication.  As is common
in high-performance code, no division is used (except by powers of 2).


// Profile counts added by DJG.
// Subroutine call counts
int n_frames = 0;
int n_drmp3d = 0;

// ALU counts for integer (excluding loop control/array subscription).
int n_IADD = 0;
int n_ITIMES = 0;

// ALU counts for floating-point.
int n_FADD = 0;
int n_FTIMES = 0;  

These are printed at the end of execution after playing a file of known length: e.g. a 3 minute song.


Despite the heavy instrumentation, and being compiled with -O0 and no
MMX support, on my laptop, the mp3 decoder still works in real time,
streaming audio to the sound card.

I disassembled the resulting x86 file so I could count instructions and ROM
size.
  -rw-rw-r--.  1 djg11 djg11 3159443 Jul 27 15:26 disassembly.txt
  


DJG May 2020.
===============================================================================

This software is available as a choice of the following licenses. Choose whichever you prefer.

===============================================================================
ALTERNATIVE 1 - Public Domain (www.unlicense.org)
===============================================================================
This is free and unencumbered software released into the public domain.

Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
software, either in source code form or as a compiled binary, for any purpose,
commercial or non-commercial, and by any means.

In jurisdictions that recognize copyright laws, the author or authors of this
software dedicate any and all copyright interest in the software to the public
domain. We make this dedication for the benefit of the public at large and to
the detriment of our heirs and successors. We intend this dedication to be an
overt act of relinquishment in perpetuity of all present and future rights to
this software under copyright law.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For more information, please refer to <http://unlicense.org/>

===============================================================================
ALTERNATIVE 2 - MIT No Attribution
===============================================================================
Copyright 2020 David Reid

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
