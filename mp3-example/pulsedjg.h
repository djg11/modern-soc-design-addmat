/*
#
# Contents for the mp3-example additional material folder for "Modern SoC Design, First Edition" by DJ Greaves. 2021.
#
# LICENSE NOTICE:
# No waranties are implied or expressed.  The original material in this folder is taken from https://github.com/dr-soft/miniaudio
# which was released with two alternative licenses: 1 - Public Domain (www.unlicense.org) and 2 - MIT No Attribution.
# The additional Makefile, djgmain.c and ancillary instrumentation files are licensed to you by DJ Greaves under the
# same conditions.
#
*/
#define MINIAUDIO_IMPLEMENTATION

/******************************************************************************

PulseAudio Backend

******************************************************************************/

/*
It is assumed pulseaudio.h is available when compile-time linking is being used. We use this for type safety when using
compile time linking (we don't have this luxury when using runtime linking without headers).

When using compile time linking, each of our ma_* equivalents should use the sames types as defined by the header. The
reason for this is that it allow us to take advantage of proper type safety.
*/
#ifdef MA_NO_RUNTIME_LINKING

/* pulseaudio.h marks some functions with "inline" which isn't always supported. Need to emulate it. */
#if !defined(__cplusplus)
    #if defined(__STRICT_ANSI__)
        #if !defined(inline)
            #define inline __inline__ __attribute__((always_inline))
            #define MA_INLINE_DEFINED
        #endif
    #endif
#endif
#include <pulse/pulseaudio.h>
#if defined(MA_INLINE_DEFINED)
    #undef inline
    #undef MA_INLINE_DEFINED
#endif

#define MA_PA_OK                                       PA_OK
#define MA_PA_ERR_ACCESS                               PA_ERR_ACCESS
#define MA_PA_ERR_INVALID                              PA_ERR_INVALID
#define MA_PA_ERR_NOENTITY                             PA_ERR_NOENTITY

#define MA_PA_CHANNELS_MAX                             PA_CHANNELS_MAX
#define MA_PA_RATE_MAX                                 PA_RATE_MAX

typedef pa_context_flags_t ma_pa_context_flags_t;
#define MA_PA_CONTEXT_NOFLAGS                          PA_CONTEXT_NOFLAGS
#define MA_PA_CONTEXT_NOAUTOSPAWN                      PA_CONTEXT_NOAUTOSPAWN
#define MA_PA_CONTEXT_NOFAIL                           PA_CONTEXT_NOFAIL

typedef pa_stream_flags_t ma_pa_stream_flags_t;
#define MA_PA_STREAM_NOFLAGS                           PA_STREAM_NOFLAGS
#define MA_PA_STREAM_START_CORKED                      PA_STREAM_START_CORKED
#define MA_PA_STREAM_INTERPOLATE_TIMING                PA_STREAM_INTERPOLATE_TIMING
#define MA_PA_STREAM_NOT_MONOTONIC                     PA_STREAM_NOT_MONOTONIC
#define MA_PA_STREAM_AUTO_TIMING_UPDATE                PA_STREAM_AUTO_TIMING_UPDATE
#define MA_PA_STREAM_NO_REMAP_CHANNELS                 PA_STREAM_NO_REMAP_CHANNELS
#define MA_PA_STREAM_NO_REMIX_CHANNELS                 PA_STREAM_NO_REMIX_CHANNELS
#define MA_PA_STREAM_FIX_FORMAT                        PA_STREAM_FIX_FORMAT
#define MA_PA_STREAM_FIX_RATE                          PA_STREAM_FIX_RATE
#define MA_PA_STREAM_FIX_CHANNELS                      PA_STREAM_FIX_CHANNELS
#define MA_PA_STREAM_DONT_MOVE                         PA_STREAM_DONT_MOVE
#define MA_PA_STREAM_VARIABLE_RATE                     PA_STREAM_VARIABLE_RATE
#define MA_PA_STREAM_PEAK_DETECT                       PA_STREAM_PEAK_DETECT
#define MA_PA_STREAM_START_MUTED                       PA_STREAM_START_MUTED
#define MA_PA_STREAM_ADJUST_LATENCY                    PA_STREAM_ADJUST_LATENCY
#define MA_PA_STREAM_EARLY_REQUESTS                    PA_STREAM_EARLY_REQUESTS
#define MA_PA_STREAM_DONT_INHIBIT_AUTO_SUSPEND         PA_STREAM_DONT_INHIBIT_AUTO_SUSPEND
#define MA_PA_STREAM_START_UNMUTED                     PA_STREAM_START_UNMUTED
#define MA_PA_STREAM_FAIL_ON_SUSPEND                   PA_STREAM_FAIL_ON_SUSPEND
#define MA_PA_STREAM_RELATIVE_VOLUME                   PA_STREAM_RELATIVE_VOLUME
#define MA_PA_STREAM_PASSTHROUGH                       PA_STREAM_PASSTHROUGH

typedef pa_sink_flags_t ma_pa_sink_flags_t;
#define MA_PA_SINK_NOFLAGS                             PA_SINK_NOFLAGS
#define MA_PA_SINK_HW_VOLUME_CTRL                      PA_SINK_HW_VOLUME_CTRL
#define MA_PA_SINK_LATENCY                             PA_SINK_LATENCY
#define MA_PA_SINK_HARDWARE                            PA_SINK_HARDWARE
#define MA_PA_SINK_NETWORK                             PA_SINK_NETWORK
#define MA_PA_SINK_HW_MUTE_CTRL                        PA_SINK_HW_MUTE_CTRL
#define MA_PA_SINK_DECIBEL_VOLUME                      PA_SINK_DECIBEL_VOLUME
#define MA_PA_SINK_FLAT_VOLUME                         PA_SINK_FLAT_VOLUME
#define MA_PA_SINK_DYNAMIC_LATENCY                     PA_SINK_DYNAMIC_LATENCY
#define MA_PA_SINK_SET_FORMATS                         PA_SINK_SET_FORMATS

typedef pa_source_flags_t ma_pa_source_flags_t;
#define MA_PA_SOURCE_NOFLAGS                           PA_SOURCE_NOFLAGS
#define MA_PA_SOURCE_HW_VOLUME_CTRL                    PA_SOURCE_HW_VOLUME_CTRL
#define MA_PA_SOURCE_LATENCY                           PA_SOURCE_LATENCY
#define MA_PA_SOURCE_HARDWARE                          PA_SOURCE_HARDWARE
#define MA_PA_SOURCE_NETWORK                           PA_SOURCE_NETWORK
#define MA_PA_SOURCE_HW_MUTE_CTRL                      PA_SOURCE_HW_MUTE_CTRL
#define MA_PA_SOURCE_DECIBEL_VOLUME                    PA_SOURCE_DECIBEL_VOLUME
#define MA_PA_SOURCE_DYNAMIC_LATENCY                   PA_SOURCE_DYNAMIC_LATENCY
#define MA_PA_SOURCE_FLAT_VOLUME                       PA_SOURCE_FLAT_VOLUME

typedef pa_context_state_t ma_pa_context_state_t;
#define MA_PA_CONTEXT_UNCONNECTED                      PA_CONTEXT_UNCONNECTED
#define MA_PA_CONTEXT_CONNECTING                       PA_CONTEXT_CONNECTING
#define MA_PA_CONTEXT_AUTHORIZING                      PA_CONTEXT_AUTHORIZING
#define MA_PA_CONTEXT_SETTING_NAME                     PA_CONTEXT_SETTING_NAME
#define MA_PA_CONTEXT_READY                            PA_CONTEXT_READY
#define MA_PA_CONTEXT_FAILED                           PA_CONTEXT_FAILED
#define MA_PA_CONTEXT_TERMINATED                       PA_CONTEXT_TERMINATED

typedef pa_stream_state_t ma_pa_stream_state_t;
#define MA_PA_STREAM_UNCONNECTED                       PA_STREAM_UNCONNECTED
#define MA_PA_STREAM_CREATING                          PA_STREAM_CREATING
#define MA_PA_STREAM_READY                             PA_STREAM_READY
#define MA_PA_STREAM_FAILED                            PA_STREAM_FAILED
#define MA_PA_STREAM_TERMINATED                        PA_STREAM_TERMINATED

typedef pa_operation_state_t ma_pa_operation_state_t;
#define MA_PA_OPERATION_RUNNING                        PA_OPERATION_RUNNING
#define MA_PA_OPERATION_DONE                           PA_OPERATION_DONE
#define MA_PA_OPERATION_CANCELLED                      PA_OPERATION_CANCELLED

typedef pa_sink_state_t ma_pa_sink_state_t;
#define MA_PA_SINK_INVALID_STATE                       PA_SINK_INVALID_STATE
#define MA_PA_SINK_RUNNING                             PA_SINK_RUNNING
#define MA_PA_SINK_IDLE                                PA_SINK_IDLE
#define MA_PA_SINK_SUSPENDED                           PA_SINK_SUSPENDED

typedef pa_source_state_t ma_pa_source_state_t;
#define MA_PA_SOURCE_INVALID_STATE                     PA_SOURCE_INVALID_STATE
#define MA_PA_SOURCE_RUNNING                           PA_SOURCE_RUNNING
#define MA_PA_SOURCE_IDLE                              PA_SOURCE_IDLE
#define MA_PA_SOURCE_SUSPENDED                         PA_SOURCE_SUSPENDED

typedef pa_seek_mode_t ma_pa_seek_mode_t;
#define MA_PA_SEEK_RELATIVE                            PA_SEEK_RELATIVE
#define MA_PA_SEEK_ABSOLUTE                            PA_SEEK_ABSOLUTE
#define MA_PA_SEEK_RELATIVE_ON_READ                    PA_SEEK_RELATIVE_ON_READ
#define MA_PA_SEEK_RELATIVE_END                        PA_SEEK_RELATIVE_END

typedef pa_channel_position_t ma_pa_channel_position_t;
#define MA_PA_CHANNEL_POSITION_INVALID                 PA_CHANNEL_POSITION_INVALID
#define MA_PA_CHANNEL_POSITION_MONO                    PA_CHANNEL_POSITION_MONO
#define MA_PA_CHANNEL_POSITION_FRONT_LEFT              PA_CHANNEL_POSITION_FRONT_LEFT
#define MA_PA_CHANNEL_POSITION_FRONT_RIGHT             PA_CHANNEL_POSITION_FRONT_RIGHT
#define MA_PA_CHANNEL_POSITION_FRONT_CENTER            PA_CHANNEL_POSITION_FRONT_CENTER
#define MA_PA_CHANNEL_POSITION_REAR_CENTER             PA_CHANNEL_POSITION_REAR_CENTER
#define MA_PA_CHANNEL_POSITION_REAR_LEFT               PA_CHANNEL_POSITION_REAR_LEFT
#define MA_PA_CHANNEL_POSITION_REAR_RIGHT              PA_CHANNEL_POSITION_REAR_RIGHT
#define MA_PA_CHANNEL_POSITION_LFE                     PA_CHANNEL_POSITION_LFE
#define MA_PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER    PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER
#define MA_PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER   PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER
#define MA_PA_CHANNEL_POSITION_SIDE_LEFT               PA_CHANNEL_POSITION_SIDE_LEFT
#define MA_PA_CHANNEL_POSITION_SIDE_RIGHT              PA_CHANNEL_POSITION_SIDE_RIGHT
#define MA_PA_CHANNEL_POSITION_AUX0                    PA_CHANNEL_POSITION_AUX0
#define MA_PA_CHANNEL_POSITION_AUX1                    PA_CHANNEL_POSITION_AUX1
#define MA_PA_CHANNEL_POSITION_AUX2                    PA_CHANNEL_POSITION_AUX2
#define MA_PA_CHANNEL_POSITION_AUX3                    PA_CHANNEL_POSITION_AUX3
#define MA_PA_CHANNEL_POSITION_AUX4                    PA_CHANNEL_POSITION_AUX4
#define MA_PA_CHANNEL_POSITION_AUX5                    PA_CHANNEL_POSITION_AUX5
#define MA_PA_CHANNEL_POSITION_AUX6                    PA_CHANNEL_POSITION_AUX6
#define MA_PA_CHANNEL_POSITION_AUX7                    PA_CHANNEL_POSITION_AUX7
#define MA_PA_CHANNEL_POSITION_AUX8                    PA_CHANNEL_POSITION_AUX8
#define MA_PA_CHANNEL_POSITION_AUX9                    PA_CHANNEL_POSITION_AUX9
#define MA_PA_CHANNEL_POSITION_AUX10                   PA_CHANNEL_POSITION_AUX10
#define MA_PA_CHANNEL_POSITION_AUX11                   PA_CHANNEL_POSITION_AUX11
#define MA_PA_CHANNEL_POSITION_AUX12                   PA_CHANNEL_POSITION_AUX12
#define MA_PA_CHANNEL_POSITION_AUX13                   PA_CHANNEL_POSITION_AUX13
#define MA_PA_CHANNEL_POSITION_AUX14                   PA_CHANNEL_POSITION_AUX14
#define MA_PA_CHANNEL_POSITION_AUX15                   PA_CHANNEL_POSITION_AUX15
#define MA_PA_CHANNEL_POSITION_AUX16                   PA_CHANNEL_POSITION_AUX16
#define MA_PA_CHANNEL_POSITION_AUX17                   PA_CHANNEL_POSITION_AUX17
#define MA_PA_CHANNEL_POSITION_AUX18                   PA_CHANNEL_POSITION_AUX18
#define MA_PA_CHANNEL_POSITION_AUX19                   PA_CHANNEL_POSITION_AUX19
#define MA_PA_CHANNEL_POSITION_AUX20                   PA_CHANNEL_POSITION_AUX20
#define MA_PA_CHANNEL_POSITION_AUX21                   PA_CHANNEL_POSITION_AUX21
#define MA_PA_CHANNEL_POSITION_AUX22                   PA_CHANNEL_POSITION_AUX22
#define MA_PA_CHANNEL_POSITION_AUX23                   PA_CHANNEL_POSITION_AUX23
#define MA_PA_CHANNEL_POSITION_AUX24                   PA_CHANNEL_POSITION_AUX24
#define MA_PA_CHANNEL_POSITION_AUX25                   PA_CHANNEL_POSITION_AUX25
#define MA_PA_CHANNEL_POSITION_AUX26                   PA_CHANNEL_POSITION_AUX26
#define MA_PA_CHANNEL_POSITION_AUX27                   PA_CHANNEL_POSITION_AUX27
#define MA_PA_CHANNEL_POSITION_AUX28                   PA_CHANNEL_POSITION_AUX28
#define MA_PA_CHANNEL_POSITION_AUX29                   PA_CHANNEL_POSITION_AUX29
#define MA_PA_CHANNEL_POSITION_AUX30                   PA_CHANNEL_POSITION_AUX30
#define MA_PA_CHANNEL_POSITION_AUX31                   PA_CHANNEL_POSITION_AUX31
#define MA_PA_CHANNEL_POSITION_TOP_CENTER              PA_CHANNEL_POSITION_TOP_CENTER
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_LEFT          PA_CHANNEL_POSITION_TOP_FRONT_LEFT
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_RIGHT         PA_CHANNEL_POSITION_TOP_FRONT_RIGHT
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_CENTER        PA_CHANNEL_POSITION_TOP_FRONT_CENTER
#define MA_PA_CHANNEL_POSITION_TOP_REAR_LEFT           PA_CHANNEL_POSITION_TOP_REAR_LEFT
#define MA_PA_CHANNEL_POSITION_TOP_REAR_RIGHT          PA_CHANNEL_POSITION_TOP_REAR_RIGHT
#define MA_PA_CHANNEL_POSITION_TOP_REAR_CENTER         PA_CHANNEL_POSITION_TOP_REAR_CENTER
#define MA_PA_CHANNEL_POSITION_LEFT                    PA_CHANNEL_POSITION_LEFT
#define MA_PA_CHANNEL_POSITION_RIGHT                   PA_CHANNEL_POSITION_RIGHT
#define MA_PA_CHANNEL_POSITION_CENTER                  PA_CHANNEL_POSITION_CENTER
#define MA_PA_CHANNEL_POSITION_SUBWOOFER               PA_CHANNEL_POSITION_SUBWOOFER

typedef pa_channel_map_def_t ma_pa_channel_map_def_t;
#define MA_PA_CHANNEL_MAP_AIFF                         PA_CHANNEL_MAP_AIFF
#define MA_PA_CHANNEL_MAP_ALSA                         PA_CHANNEL_MAP_ALSA
#define MA_PA_CHANNEL_MAP_AUX                          PA_CHANNEL_MAP_AUX
#define MA_PA_CHANNEL_MAP_WAVEEX                       PA_CHANNEL_MAP_WAVEEX
#define MA_PA_CHANNEL_MAP_OSS                          PA_CHANNEL_MAP_OSS
#define MA_PA_CHANNEL_MAP_DEFAULT                      PA_CHANNEL_MAP_DEFAULT

typedef pa_sample_format_t ma_pa_sample_format_t;
#define MA_PA_SAMPLE_INVALID                           PA_SAMPLE_INVALID
#define MA_PA_SAMPLE_U8                                PA_SAMPLE_U8
#define MA_PA_SAMPLE_ALAW                              PA_SAMPLE_ALAW
#define MA_PA_SAMPLE_ULAW                              PA_SAMPLE_ULAW
#define MA_PA_SAMPLE_S16LE                             PA_SAMPLE_S16LE
#define MA_PA_SAMPLE_S16BE                             PA_SAMPLE_S16BE
#define MA_PA_SAMPLE_FLOAT32LE                         PA_SAMPLE_FLOAT32LE
#define MA_PA_SAMPLE_FLOAT32BE                         PA_SAMPLE_FLOAT32BE
#define MA_PA_SAMPLE_S32LE                             PA_SAMPLE_S32LE
#define MA_PA_SAMPLE_S32BE                             PA_SAMPLE_S32BE
#define MA_PA_SAMPLE_S24LE                             PA_SAMPLE_S24LE
#define MA_PA_SAMPLE_S24BE                             PA_SAMPLE_S24BE
#define MA_PA_SAMPLE_S24_32LE                          PA_SAMPLE_S24_32LE
#define MA_PA_SAMPLE_S24_32BE                          PA_SAMPLE_S24_32BE

typedef pa_mainloop     ma_pa_mainloop;
typedef pa_mainloop_api ma_pa_mainloop_api;
typedef pa_context      ma_pa_context;
typedef pa_operation    ma_pa_operation;
typedef pa_stream       ma_pa_stream;
typedef pa_spawn_api    ma_pa_spawn_api;
typedef pa_buffer_attr  ma_pa_buffer_attr;
typedef pa_channel_map  ma_pa_channel_map;
typedef pa_cvolume      ma_pa_cvolume;
typedef pa_sample_spec  ma_pa_sample_spec;
typedef pa_sink_info    ma_pa_sink_info;
typedef pa_source_info  ma_pa_source_info;

typedef pa_context_notify_cb_t ma_pa_context_notify_cb_t;
typedef pa_sink_info_cb_t ma_pa_sink_info_cb_t;
typedef pa_source_info_cb_t ma_pa_source_info_cb_t;
typedef pa_stream_success_cb_t ma_pa_stream_success_cb_t;
typedef pa_stream_request_cb_t ma_pa_stream_request_cb_t;
typedef pa_free_cb_t ma_pa_free_cb_t;
#else
#define MA_PA_OK                                       0
#define MA_PA_ERR_ACCESS                               1
#define MA_PA_ERR_INVALID                              2
#define MA_PA_ERR_NOENTITY                             5

#define MA_PA_CHANNELS_MAX                             32
#define MA_PA_RATE_MAX                                 384000

typedef int ma_pa_context_flags_t;
#define MA_PA_CONTEXT_NOFLAGS                          0x00000000
#define MA_PA_CONTEXT_NOAUTOSPAWN                      0x00000001
#define MA_PA_CONTEXT_NOFAIL                           0x00000002

typedef int ma_pa_stream_flags_t;
#define MA_PA_STREAM_NOFLAGS                           0x00000000
#define MA_PA_STREAM_START_CORKED                      0x00000001
#define MA_PA_STREAM_INTERPOLATE_TIMING                0x00000002
#define MA_PA_STREAM_NOT_MONOTONIC                     0x00000004
#define MA_PA_STREAM_AUTO_TIMING_UPDATE                0x00000008
#define MA_PA_STREAM_NO_REMAP_CHANNELS                 0x00000010
#define MA_PA_STREAM_NO_REMIX_CHANNELS                 0x00000020
#define MA_PA_STREAM_FIX_FORMAT                        0x00000040
#define MA_PA_STREAM_FIX_RATE                          0x00000080
#define MA_PA_STREAM_FIX_CHANNELS                      0x00000100
#define MA_PA_STREAM_DONT_MOVE                         0x00000200
#define MA_PA_STREAM_VARIABLE_RATE                     0x00000400
#define MA_PA_STREAM_PEAK_DETECT                       0x00000800
#define MA_PA_STREAM_START_MUTED                       0x00001000
#define MA_PA_STREAM_ADJUST_LATENCY                    0x00002000
#define MA_PA_STREAM_EARLY_REQUESTS                    0x00004000
#define MA_PA_STREAM_DONT_INHIBIT_AUTO_SUSPEND         0x00008000
#define MA_PA_STREAM_START_UNMUTED                     0x00010000
#define MA_PA_STREAM_FAIL_ON_SUSPEND                   0x00020000
#define MA_PA_STREAM_RELATIVE_VOLUME                   0x00040000
#define MA_PA_STREAM_PASSTHROUGH                       0x00080000

typedef int ma_pa_sink_flags_t;
#define MA_PA_SINK_NOFLAGS                             0x00000000
#define MA_PA_SINK_HW_VOLUME_CTRL                      0x00000001
#define MA_PA_SINK_LATENCY                             0x00000002
#define MA_PA_SINK_HARDWARE                            0x00000004
#define MA_PA_SINK_NETWORK                             0x00000008
#define MA_PA_SINK_HW_MUTE_CTRL                        0x00000010
#define MA_PA_SINK_DECIBEL_VOLUME                      0x00000020
#define MA_PA_SINK_FLAT_VOLUME                         0x00000040
#define MA_PA_SINK_DYNAMIC_LATENCY                     0x00000080
#define MA_PA_SINK_SET_FORMATS                         0x00000100

typedef int ma_pa_source_flags_t;
#define MA_PA_SOURCE_NOFLAGS                           0x00000000
#define MA_PA_SOURCE_HW_VOLUME_CTRL                    0x00000001
#define MA_PA_SOURCE_LATENCY                           0x00000002
#define MA_PA_SOURCE_HARDWARE                          0x00000004
#define MA_PA_SOURCE_NETWORK                           0x00000008
#define MA_PA_SOURCE_HW_MUTE_CTRL                      0x00000010
#define MA_PA_SOURCE_DECIBEL_VOLUME                    0x00000020
#define MA_PA_SOURCE_DYNAMIC_LATENCY                   0x00000040
#define MA_PA_SOURCE_FLAT_VOLUME                       0x00000080

typedef int ma_pa_context_state_t;
#define MA_PA_CONTEXT_UNCONNECTED                      0
#define MA_PA_CONTEXT_CONNECTING                       1
#define MA_PA_CONTEXT_AUTHORIZING                      2
#define MA_PA_CONTEXT_SETTING_NAME                     3
#define MA_PA_CONTEXT_READY                            4
#define MA_PA_CONTEXT_FAILED                           5
#define MA_PA_CONTEXT_TERMINATED                       6

typedef int ma_pa_stream_state_t;
#define MA_PA_STREAM_UNCONNECTED                       0
#define MA_PA_STREAM_CREATING                          1
#define MA_PA_STREAM_READY                             2
#define MA_PA_STREAM_FAILED                            3
#define MA_PA_STREAM_TERMINATED                        4

typedef int ma_pa_operation_state_t;
#define MA_PA_OPERATION_RUNNING                        0
#define MA_PA_OPERATION_DONE                           1
#define MA_PA_OPERATION_CANCELLED                      2

typedef int ma_pa_sink_state_t;
#define MA_PA_SINK_INVALID_STATE                       -1
#define MA_PA_SINK_RUNNING                             0
#define MA_PA_SINK_IDLE                                1
#define MA_PA_SINK_SUSPENDED                           2

typedef int ma_pa_source_state_t;
#define MA_PA_SOURCE_INVALID_STATE                     -1
#define MA_PA_SOURCE_RUNNING                           0
#define MA_PA_SOURCE_IDLE                              1
#define MA_PA_SOURCE_SUSPENDED                         2

typedef int ma_pa_seek_mode_t;
#define MA_PA_SEEK_RELATIVE                            0
#define MA_PA_SEEK_ABSOLUTE                            1
#define MA_PA_SEEK_RELATIVE_ON_READ                    2
#define MA_PA_SEEK_RELATIVE_END                        3

typedef int ma_pa_channel_position_t;
#define MA_PA_CHANNEL_POSITION_INVALID                 -1
#define MA_PA_CHANNEL_POSITION_MONO                    0
#define MA_PA_CHANNEL_POSITION_FRONT_LEFT              1
#define MA_PA_CHANNEL_POSITION_FRONT_RIGHT             2
#define MA_PA_CHANNEL_POSITION_FRONT_CENTER            3
#define MA_PA_CHANNEL_POSITION_REAR_CENTER             4
#define MA_PA_CHANNEL_POSITION_REAR_LEFT               5
#define MA_PA_CHANNEL_POSITION_REAR_RIGHT              6
#define MA_PA_CHANNEL_POSITION_LFE                     7
#define MA_PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER    8
#define MA_PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER   9
#define MA_PA_CHANNEL_POSITION_SIDE_LEFT               10
#define MA_PA_CHANNEL_POSITION_SIDE_RIGHT              11
#define MA_PA_CHANNEL_POSITION_AUX0                    12
#define MA_PA_CHANNEL_POSITION_AUX1                    13
#define MA_PA_CHANNEL_POSITION_AUX2                    14
#define MA_PA_CHANNEL_POSITION_AUX3                    15
#define MA_PA_CHANNEL_POSITION_AUX4                    16
#define MA_PA_CHANNEL_POSITION_AUX5                    17
#define MA_PA_CHANNEL_POSITION_AUX6                    18
#define MA_PA_CHANNEL_POSITION_AUX7                    19
#define MA_PA_CHANNEL_POSITION_AUX8                    20
#define MA_PA_CHANNEL_POSITION_AUX9                    21
#define MA_PA_CHANNEL_POSITION_AUX10                   22
#define MA_PA_CHANNEL_POSITION_AUX11                   23
#define MA_PA_CHANNEL_POSITION_AUX12                   24
#define MA_PA_CHANNEL_POSITION_AUX13                   25
#define MA_PA_CHANNEL_POSITION_AUX14                   26
#define MA_PA_CHANNEL_POSITION_AUX15                   27
#define MA_PA_CHANNEL_POSITION_AUX16                   28
#define MA_PA_CHANNEL_POSITION_AUX17                   29
#define MA_PA_CHANNEL_POSITION_AUX18                   30
#define MA_PA_CHANNEL_POSITION_AUX19                   31
#define MA_PA_CHANNEL_POSITION_AUX20                   32
#define MA_PA_CHANNEL_POSITION_AUX21                   33
#define MA_PA_CHANNEL_POSITION_AUX22                   34
#define MA_PA_CHANNEL_POSITION_AUX23                   35
#define MA_PA_CHANNEL_POSITION_AUX24                   36
#define MA_PA_CHANNEL_POSITION_AUX25                   37
#define MA_PA_CHANNEL_POSITION_AUX26                   38
#define MA_PA_CHANNEL_POSITION_AUX27                   39
#define MA_PA_CHANNEL_POSITION_AUX28                   40
#define MA_PA_CHANNEL_POSITION_AUX29                   41
#define MA_PA_CHANNEL_POSITION_AUX30                   42
#define MA_PA_CHANNEL_POSITION_AUX31                   43
#define MA_PA_CHANNEL_POSITION_TOP_CENTER              44
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_LEFT          45
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_RIGHT         46
#define MA_PA_CHANNEL_POSITION_TOP_FRONT_CENTER        47
#define MA_PA_CHANNEL_POSITION_TOP_REAR_LEFT           48
#define MA_PA_CHANNEL_POSITION_TOP_REAR_RIGHT          49
#define MA_PA_CHANNEL_POSITION_TOP_REAR_CENTER         50
#define MA_PA_CHANNEL_POSITION_LEFT                    MA_PA_CHANNEL_POSITION_FRONT_LEFT
#define MA_PA_CHANNEL_POSITION_RIGHT                   MA_PA_CHANNEL_POSITION_FRONT_RIGHT
#define MA_PA_CHANNEL_POSITION_CENTER                  MA_PA_CHANNEL_POSITION_FRONT_CENTER
#define MA_PA_CHANNEL_POSITION_SUBWOOFER               MA_PA_CHANNEL_POSITION_LFE

typedef int ma_pa_channel_map_def_t;
#define MA_PA_CHANNEL_MAP_AIFF                         0
#define MA_PA_CHANNEL_MAP_ALSA                         1
#define MA_PA_CHANNEL_MAP_AUX                          2
#define MA_PA_CHANNEL_MAP_WAVEEX                       3
#define MA_PA_CHANNEL_MAP_OSS                          4
#define MA_PA_CHANNEL_MAP_DEFAULT                      MA_PA_CHANNEL_MAP_AIFF

typedef int ma_pa_sample_format_t;
#define MA_PA_SAMPLE_INVALID                           -1
#define MA_PA_SAMPLE_U8                                0
#define MA_PA_SAMPLE_ALAW                              1
#define MA_PA_SAMPLE_ULAW                              2
#define MA_PA_SAMPLE_S16LE                             3
#define MA_PA_SAMPLE_S16BE                             4
#define MA_PA_SAMPLE_FLOAT32LE                         5
#define MA_PA_SAMPLE_FLOAT32BE                         6
#define MA_PA_SAMPLE_S32LE                             7
#define MA_PA_SAMPLE_S32BE                             8
#define MA_PA_SAMPLE_S24LE                             9
#define MA_PA_SAMPLE_S24BE                             10
#define MA_PA_SAMPLE_S24_32LE                          11
#define MA_PA_SAMPLE_S24_32BE                          12

typedef struct ma_pa_mainloop     ma_pa_mainloop;
typedef struct ma_pa_mainloop_api ma_pa_mainloop_api;
typedef struct ma_pa_context      ma_pa_context;
typedef struct ma_pa_operation    ma_pa_operation;
typedef struct ma_pa_stream       ma_pa_stream;
typedef struct ma_pa_spawn_api    ma_pa_spawn_api;

typedef struct
{
    ma_uint32 maxlength;
    ma_uint32 tlength;
    ma_uint32 prebuf;
    ma_uint32 minreq;
    ma_uint32 fragsize;
} ma_pa_buffer_attr;

typedef struct
{
    ma_uint8 channels;
    ma_pa_channel_position_t map[MA_PA_CHANNELS_MAX];
} ma_pa_channel_map;

typedef struct
{
    ma_uint8 channels;
    ma_uint32 values[MA_PA_CHANNELS_MAX];
} ma_pa_cvolume;

typedef struct
{
    ma_pa_sample_format_t format;
    ma_uint32 rate;
    ma_uint8 channels;
} ma_pa_sample_spec;

typedef struct
{
    const char* name;
    ma_uint32 index;
    const char* description;
    ma_pa_sample_spec sample_spec;
    ma_pa_channel_map channel_map;
    ma_uint32 owner_module;
    ma_pa_cvolume volume;
    int mute;
    ma_uint32 monitor_source;
    const char* monitor_source_name;
    ma_uint64 latency;
    const char* driver;
    ma_pa_sink_flags_t flags;
    void* proplist;
    ma_uint64 configured_latency;
    ma_uint32 base_volume;
    ma_pa_sink_state_t state;
    ma_uint32 n_volume_steps;
    ma_uint32 card;
    ma_uint32 n_ports;
    void** ports;
    void* active_port;
    ma_uint8 n_formats;
    void** formats;
} ma_pa_sink_info;

typedef struct
{
    const char *name;
    ma_uint32 index;
    const char *description;
    ma_pa_sample_spec sample_spec;
    ma_pa_channel_map channel_map;
    ma_uint32 owner_module;
    ma_pa_cvolume volume;
    int mute;
    ma_uint32 monitor_of_sink;
    const char *monitor_of_sink_name;
    ma_uint64 latency;
    const char *driver;
    ma_pa_source_flags_t flags;
    void* proplist;
    ma_uint64 configured_latency;
    ma_uint32 base_volume;
    ma_pa_source_state_t state;
    ma_uint32 n_volume_steps;
    ma_uint32 card;
    ma_uint32 n_ports;
    void** ports;
    void* active_port;
    ma_uint8 n_formats;
    void** formats;
} ma_pa_source_info;

typedef void (* ma_pa_context_notify_cb_t)(ma_pa_context* c, void* userdata);
typedef void (* ma_pa_sink_info_cb_t)     (ma_pa_context* c, const ma_pa_sink_info* i, int eol, void* userdata);
typedef void (* ma_pa_source_info_cb_t)   (ma_pa_context* c, const ma_pa_source_info* i, int eol, void* userdata);
typedef void (* ma_pa_stream_success_cb_t)(ma_pa_stream* s, int success, void* userdata);
typedef void (* ma_pa_stream_request_cb_t)(ma_pa_stream* s, size_t nbytes, void* userdata);
typedef void (* ma_pa_free_cb_t)          (void* p);
#endif


typedef ma_pa_mainloop*          (* ma_pa_mainloop_new_proc)                   (void);
typedef void                     (* ma_pa_mainloop_free_proc)                  (ma_pa_mainloop* m);
typedef ma_pa_mainloop_api*      (* ma_pa_mainloop_get_api_proc)               (ma_pa_mainloop* m);
typedef int                      (* ma_pa_mainloop_iterate_proc)               (ma_pa_mainloop* m, int block, int* retval);
typedef void                     (* ma_pa_mainloop_wakeup_proc)                (ma_pa_mainloop* m);
typedef ma_pa_context*           (* ma_pa_context_new_proc)                    (ma_pa_mainloop_api* mainloop, const char* name);
typedef void                     (* ma_pa_context_unref_proc)                  (ma_pa_context* c);
typedef int                      (* ma_pa_context_connect_proc)                (ma_pa_context* c, const char* server, ma_pa_context_flags_t flags, const ma_pa_spawn_api* api);
typedef void                     (* ma_pa_context_disconnect_proc)             (ma_pa_context* c);
typedef void                     (* ma_pa_context_set_state_callback_proc)     (ma_pa_context* c, ma_pa_context_notify_cb_t cb, void* userdata);
typedef ma_pa_context_state_t    (* ma_pa_context_get_state_proc)              (ma_pa_context* c);
typedef ma_pa_operation*         (* ma_pa_context_get_sink_info_list_proc)     (ma_pa_context* c, ma_pa_sink_info_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_context_get_source_info_list_proc)   (ma_pa_context* c, ma_pa_source_info_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_context_get_sink_info_by_name_proc)  (ma_pa_context* c, const char* name, ma_pa_sink_info_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_context_get_source_info_by_name_proc)(ma_pa_context* c, const char* name, ma_pa_source_info_cb_t cb, void* userdata);
typedef void                     (* ma_pa_operation_unref_proc)                (ma_pa_operation* o);
typedef ma_pa_operation_state_t  (* ma_pa_operation_get_state_proc)            (ma_pa_operation* o);
typedef ma_pa_channel_map*       (* ma_pa_channel_map_init_extend_proc)        (ma_pa_channel_map* m, unsigned channels, ma_pa_channel_map_def_t def);
typedef int                      (* ma_pa_channel_map_valid_proc)              (const ma_pa_channel_map* m);
typedef int                      (* ma_pa_channel_map_compatible_proc)         (const ma_pa_channel_map* m, const ma_pa_sample_spec* ss);
typedef ma_pa_stream*            (* ma_pa_stream_new_proc)                     (ma_pa_context* c, const char* name, const ma_pa_sample_spec* ss, const ma_pa_channel_map* map);
typedef void                     (* ma_pa_stream_unref_proc)                   (ma_pa_stream* s);
typedef int                      (* ma_pa_stream_connect_playback_proc)        (ma_pa_stream* s, const char* dev, const ma_pa_buffer_attr* attr, ma_pa_stream_flags_t flags, const ma_pa_cvolume* volume, ma_pa_stream* sync_stream);
typedef int                      (* ma_pa_stream_connect_record_proc)          (ma_pa_stream* s, const char* dev, const ma_pa_buffer_attr* attr, ma_pa_stream_flags_t flags);
typedef int                      (* ma_pa_stream_disconnect_proc)              (ma_pa_stream* s);
typedef ma_pa_stream_state_t     (* ma_pa_stream_get_state_proc)               (ma_pa_stream* s);
typedef const ma_pa_sample_spec* (* ma_pa_stream_get_sample_spec_proc)         (ma_pa_stream* s);
typedef const ma_pa_channel_map* (* ma_pa_stream_get_channel_map_proc)         (ma_pa_stream* s);
typedef const ma_pa_buffer_attr* (* ma_pa_stream_get_buffer_attr_proc)         (ma_pa_stream* s);
typedef ma_pa_operation*         (* ma_pa_stream_set_buffer_attr_proc)         (ma_pa_stream* s, const ma_pa_buffer_attr* attr, ma_pa_stream_success_cb_t cb, void* userdata);
typedef const char*              (* ma_pa_stream_get_device_name_proc)         (ma_pa_stream* s);
typedef void                     (* ma_pa_stream_set_write_callback_proc)      (ma_pa_stream* s, ma_pa_stream_request_cb_t cb, void* userdata);
typedef void                     (* ma_pa_stream_set_read_callback_proc)       (ma_pa_stream* s, ma_pa_stream_request_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_stream_flush_proc)                   (ma_pa_stream* s, ma_pa_stream_success_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_stream_drain_proc)                   (ma_pa_stream* s, ma_pa_stream_success_cb_t cb, void* userdata);
typedef int                      (* ma_pa_stream_is_corked_proc)               (ma_pa_stream* s);
typedef ma_pa_operation*         (* ma_pa_stream_cork_proc)                    (ma_pa_stream* s, int b, ma_pa_stream_success_cb_t cb, void* userdata);
typedef ma_pa_operation*         (* ma_pa_stream_trigger_proc)                 (ma_pa_stream* s, ma_pa_stream_success_cb_t cb, void* userdata);
typedef int                      (* ma_pa_stream_begin_write_proc)             (ma_pa_stream* s, void** data, size_t* nbytes);
typedef int                      (* ma_pa_stream_write_proc)                   (ma_pa_stream* s, const void* data, size_t nbytes, ma_pa_free_cb_t free_cb, int64_t offset, ma_pa_seek_mode_t seek);
typedef int                      (* ma_pa_stream_peek_proc)                    (ma_pa_stream* s, const void** data, size_t* nbytes);
typedef int                      (* ma_pa_stream_drop_proc)                    (ma_pa_stream* s);
typedef size_t                   (* ma_pa_stream_writable_size_proc)           (ma_pa_stream* s);
typedef size_t                   (* ma_pa_stream_readable_size_proc)           (ma_pa_stream* s);

typedef struct
{
    ma_uint32 count;
    ma_uint32 capacity;
    ma_device_info* pInfo;
} ma_pulse_device_enum_data;

ma_result ma_result_from_pulse(int result)
{
    switch (result) {
        case MA_PA_OK:           return MA_SUCCESS;
        case MA_PA_ERR_ACCESS:   return MA_ACCESS_DENIED;
        case MA_PA_ERR_INVALID:  return MA_INVALID_ARGS;
        case MA_PA_ERR_NOENTITY: return MA_NO_DEVICE;
        default:                  return MA_ERROR;
    }
}

#if 0
ma_pa_sample_format_t ma_format_to_pulse(ma_format format)
{
    if (ma_is_little_endian()) {
        switch (format) {
            case ma_format_s16: return MA_PA_SAMPLE_S16LE;
            case ma_format_s24: return MA_PA_SAMPLE_S24LE;
            case ma_format_s32: return MA_PA_SAMPLE_S32LE;
            case ma_format_f32: return MA_PA_SAMPLE_FLOAT32LE;
            default: break;
        }
    } else {
        switch (format) {
            case ma_format_s16: return MA_PA_SAMPLE_S16BE;
            case ma_format_s24: return MA_PA_SAMPLE_S24BE;
            case ma_format_s32: return MA_PA_SAMPLE_S32BE;
            case ma_format_f32: return MA_PA_SAMPLE_FLOAT32BE;
            default: break;
        }
    }

    /* Endian agnostic. */
    switch (format) {
        case ma_format_u8: return MA_PA_SAMPLE_U8;
        default: return MA_PA_SAMPLE_INVALID;
    }
}
#endif

ma_format ma_format_from_pulse(ma_pa_sample_format_t format)
{
    if (ma_is_little_endian()) {
        switch (format) {
            case MA_PA_SAMPLE_S16LE:     return ma_format_s16;
            case MA_PA_SAMPLE_S24LE:     return ma_format_s24;
            case MA_PA_SAMPLE_S32LE:     return ma_format_s32;
            case MA_PA_SAMPLE_FLOAT32LE: return ma_format_f32;
            default: break;
        }
    } else {
        switch (format) {
            case MA_PA_SAMPLE_S16BE:     return ma_format_s16;
            case MA_PA_SAMPLE_S24BE:     return ma_format_s24;
            case MA_PA_SAMPLE_S32BE:     return ma_format_s32;
            case MA_PA_SAMPLE_FLOAT32BE: return ma_format_f32;
            default: break;
        }
    }

    /* Endian agnostic. */
    switch (format) {
        case MA_PA_SAMPLE_U8: return ma_format_u8;
        default: return ma_format_unknown;
    }
}

ma_channel ma_channel_position_from_pulse(ma_pa_channel_position_t position)
{
    switch (position)
    {
        case MA_PA_CHANNEL_POSITION_INVALID:               return MA_CHANNEL_NONE;
        case MA_PA_CHANNEL_POSITION_MONO:                  return MA_CHANNEL_MONO;
        case MA_PA_CHANNEL_POSITION_FRONT_LEFT:            return MA_CHANNEL_FRONT_LEFT;
        case MA_PA_CHANNEL_POSITION_FRONT_RIGHT:           return MA_CHANNEL_FRONT_RIGHT;
        case MA_PA_CHANNEL_POSITION_FRONT_CENTER:          return MA_CHANNEL_FRONT_CENTER;
        case MA_PA_CHANNEL_POSITION_REAR_CENTER:           return MA_CHANNEL_BACK_CENTER;
        case MA_PA_CHANNEL_POSITION_REAR_LEFT:             return MA_CHANNEL_BACK_LEFT;
        case MA_PA_CHANNEL_POSITION_REAR_RIGHT:            return MA_CHANNEL_BACK_RIGHT;
        case MA_PA_CHANNEL_POSITION_LFE:                   return MA_CHANNEL_LFE;
        case MA_PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER:  return MA_CHANNEL_FRONT_LEFT_CENTER;
        case MA_PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER: return MA_CHANNEL_FRONT_RIGHT_CENTER;
        case MA_PA_CHANNEL_POSITION_SIDE_LEFT:             return MA_CHANNEL_SIDE_LEFT;
        case MA_PA_CHANNEL_POSITION_SIDE_RIGHT:            return MA_CHANNEL_SIDE_RIGHT;
        case MA_PA_CHANNEL_POSITION_AUX0:                  return MA_CHANNEL_AUX_0;
        case MA_PA_CHANNEL_POSITION_AUX1:                  return MA_CHANNEL_AUX_1;
        case MA_PA_CHANNEL_POSITION_AUX2:                  return MA_CHANNEL_AUX_2;
        case MA_PA_CHANNEL_POSITION_AUX3:                  return MA_CHANNEL_AUX_3;
        case MA_PA_CHANNEL_POSITION_AUX4:                  return MA_CHANNEL_AUX_4;
        case MA_PA_CHANNEL_POSITION_AUX5:                  return MA_CHANNEL_AUX_5;
        case MA_PA_CHANNEL_POSITION_AUX6:                  return MA_CHANNEL_AUX_6;
        case MA_PA_CHANNEL_POSITION_AUX7:                  return MA_CHANNEL_AUX_7;
        case MA_PA_CHANNEL_POSITION_AUX8:                  return MA_CHANNEL_AUX_8;
        case MA_PA_CHANNEL_POSITION_AUX9:                  return MA_CHANNEL_AUX_9;
        case MA_PA_CHANNEL_POSITION_AUX10:                 return MA_CHANNEL_AUX_10;
        case MA_PA_CHANNEL_POSITION_AUX11:                 return MA_CHANNEL_AUX_11;
        case MA_PA_CHANNEL_POSITION_AUX12:                 return MA_CHANNEL_AUX_12;
        case MA_PA_CHANNEL_POSITION_AUX13:                 return MA_CHANNEL_AUX_13;
        case MA_PA_CHANNEL_POSITION_AUX14:                 return MA_CHANNEL_AUX_14;
        case MA_PA_CHANNEL_POSITION_AUX15:                 return MA_CHANNEL_AUX_15;
        case MA_PA_CHANNEL_POSITION_AUX16:                 return MA_CHANNEL_AUX_16;
        case MA_PA_CHANNEL_POSITION_AUX17:                 return MA_CHANNEL_AUX_17;
        case MA_PA_CHANNEL_POSITION_AUX18:                 return MA_CHANNEL_AUX_18;
        case MA_PA_CHANNEL_POSITION_AUX19:                 return MA_CHANNEL_AUX_19;
        case MA_PA_CHANNEL_POSITION_AUX20:                 return MA_CHANNEL_AUX_20;
        case MA_PA_CHANNEL_POSITION_AUX21:                 return MA_CHANNEL_AUX_21;
        case MA_PA_CHANNEL_POSITION_AUX22:                 return MA_CHANNEL_AUX_22;
        case MA_PA_CHANNEL_POSITION_AUX23:                 return MA_CHANNEL_AUX_23;
        case MA_PA_CHANNEL_POSITION_AUX24:                 return MA_CHANNEL_AUX_24;
        case MA_PA_CHANNEL_POSITION_AUX25:                 return MA_CHANNEL_AUX_25;
        case MA_PA_CHANNEL_POSITION_AUX26:                 return MA_CHANNEL_AUX_26;
        case MA_PA_CHANNEL_POSITION_AUX27:                 return MA_CHANNEL_AUX_27;
        case MA_PA_CHANNEL_POSITION_AUX28:                 return MA_CHANNEL_AUX_28;
        case MA_PA_CHANNEL_POSITION_AUX29:                 return MA_CHANNEL_AUX_29;
        case MA_PA_CHANNEL_POSITION_AUX30:                 return MA_CHANNEL_AUX_30;
        case MA_PA_CHANNEL_POSITION_AUX31:                 return MA_CHANNEL_AUX_31;
        case MA_PA_CHANNEL_POSITION_TOP_CENTER:            return MA_CHANNEL_TOP_CENTER;
        case MA_PA_CHANNEL_POSITION_TOP_FRONT_LEFT:        return MA_CHANNEL_TOP_FRONT_LEFT;
        case MA_PA_CHANNEL_POSITION_TOP_FRONT_RIGHT:       return MA_CHANNEL_TOP_FRONT_RIGHT;
        case MA_PA_CHANNEL_POSITION_TOP_FRONT_CENTER:      return MA_CHANNEL_TOP_FRONT_CENTER;
        case MA_PA_CHANNEL_POSITION_TOP_REAR_LEFT:         return MA_CHANNEL_TOP_BACK_LEFT;
        case MA_PA_CHANNEL_POSITION_TOP_REAR_RIGHT:        return MA_CHANNEL_TOP_BACK_RIGHT;
        case MA_PA_CHANNEL_POSITION_TOP_REAR_CENTER:       return MA_CHANNEL_TOP_BACK_CENTER;
        default: return MA_CHANNEL_NONE;
    }
}

#if 0
ma_pa_channel_position_t ma_channel_position_to_pulse(ma_channel position)
{
    switch (position)
    {
        case MA_CHANNEL_NONE:               return MA_PA_CHANNEL_POSITION_INVALID;
        case MA_CHANNEL_FRONT_LEFT:         return MA_PA_CHANNEL_POSITION_FRONT_LEFT;
        case MA_CHANNEL_FRONT_RIGHT:        return MA_PA_CHANNEL_POSITION_FRONT_RIGHT;
        case MA_CHANNEL_FRONT_CENTER:       return MA_PA_CHANNEL_POSITION_FRONT_CENTER;
        case MA_CHANNEL_LFE:                return MA_PA_CHANNEL_POSITION_LFE;
        case MA_CHANNEL_BACK_LEFT:          return MA_PA_CHANNEL_POSITION_REAR_LEFT;
        case MA_CHANNEL_BACK_RIGHT:         return MA_PA_CHANNEL_POSITION_REAR_RIGHT;
        case MA_CHANNEL_FRONT_LEFT_CENTER:  return MA_PA_CHANNEL_POSITION_FRONT_LEFT_OF_CENTER;
        case MA_CHANNEL_FRONT_RIGHT_CENTER: return MA_PA_CHANNEL_POSITION_FRONT_RIGHT_OF_CENTER;
        case MA_CHANNEL_BACK_CENTER:        return MA_PA_CHANNEL_POSITION_REAR_CENTER;
        case MA_CHANNEL_SIDE_LEFT:          return MA_PA_CHANNEL_POSITION_SIDE_LEFT;
        case MA_CHANNEL_SIDE_RIGHT:         return MA_PA_CHANNEL_POSITION_SIDE_RIGHT;
        case MA_CHANNEL_TOP_CENTER:         return MA_PA_CHANNEL_POSITION_TOP_CENTER;
        case MA_CHANNEL_TOP_FRONT_LEFT:     return MA_PA_CHANNEL_POSITION_TOP_FRONT_LEFT;
        case MA_CHANNEL_TOP_FRONT_CENTER:   return MA_PA_CHANNEL_POSITION_TOP_FRONT_CENTER;
        case MA_CHANNEL_TOP_FRONT_RIGHT:    return MA_PA_CHANNEL_POSITION_TOP_FRONT_RIGHT;
        case MA_CHANNEL_TOP_BACK_LEFT:      return MA_PA_CHANNEL_POSITION_TOP_REAR_LEFT;
        case MA_CHANNEL_TOP_BACK_CENTER:    return MA_PA_CHANNEL_POSITION_TOP_REAR_CENTER;
        case MA_CHANNEL_TOP_BACK_RIGHT:     return MA_PA_CHANNEL_POSITION_TOP_REAR_RIGHT;
        case MA_CHANNEL_19:                 return MA_PA_CHANNEL_POSITION_AUX18;
        case MA_CHANNEL_20:                 return MA_PA_CHANNEL_POSITION_AUX19;
        case MA_CHANNEL_21:                 return MA_PA_CHANNEL_POSITION_AUX20;
        case MA_CHANNEL_22:                 return MA_PA_CHANNEL_POSITION_AUX21;
        case MA_CHANNEL_23:                 return MA_PA_CHANNEL_POSITION_AUX22;
        case MA_CHANNEL_24:                 return MA_PA_CHANNEL_POSITION_AUX23;
        case MA_CHANNEL_25:                 return MA_PA_CHANNEL_POSITION_AUX24;
        case MA_CHANNEL_26:                 return MA_PA_CHANNEL_POSITION_AUX25;
        case MA_CHANNEL_27:                 return MA_PA_CHANNEL_POSITION_AUX26;
        case MA_CHANNEL_28:                 return MA_PA_CHANNEL_POSITION_AUX27;
        case MA_CHANNEL_29:                 return MA_PA_CHANNEL_POSITION_AUX28;
        case MA_CHANNEL_30:                 return MA_PA_CHANNEL_POSITION_AUX29;
        case MA_CHANNEL_31:                 return MA_PA_CHANNEL_POSITION_AUX30;
        case MA_CHANNEL_32:                 return MA_PA_CHANNEL_POSITION_AUX31;
        default: return (ma_pa_channel_position_t)position;
    }
}
#endif

ma_result ma_wait_for_operation__pulse(ma_context* pContext, ma_pa_mainloop* pMainLoop, ma_pa_operation* pOP)
{
    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pMainLoop != NULL);
    MA_ASSERT(pOP != NULL);

    while (((ma_pa_operation_get_state_proc)pContext->pulse.pa_operation_get_state)(pOP) == MA_PA_OPERATION_RUNNING) {
        int error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)(pMainLoop, 1, NULL);
        if (error < 0) {
            return ma_result_from_pulse(error);
        }
    }

    return MA_SUCCESS;
}

ma_result ma_device__wait_for_operation__pulse(ma_device* pDevice, ma_pa_operation* pOP)
{
    MA_ASSERT(pDevice != NULL);
    MA_ASSERT(pOP != NULL);

    return ma_wait_for_operation__pulse(pDevice->pContext, (ma_pa_mainloop*)pDevice->pulse.pMainLoop, pOP);
}


ma_bool32 ma_context_is_device_id_equal__pulse(ma_context* pContext, const ma_device_id* pID0, const ma_device_id* pID1)
{
    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pID0 != NULL);
    MA_ASSERT(pID1 != NULL);
    (void)pContext;

    return ma_strcmp(pID0->pulse, pID1->pulse) == 0;
}


typedef struct
{
    ma_context* pContext;
    ma_enum_devices_callback_proc callback;
    void* pUserData;
    ma_bool32 isTerminated;
} ma_context_enumerate_devices_callback_data__pulse;

void ma_context_enumerate_devices_sink_callback__pulse(ma_pa_context* pPulseContext, const ma_pa_sink_info* pSinkInfo, int endOfList, void* pUserData)
{
    ma_context_enumerate_devices_callback_data__pulse* pData = (ma_context_enumerate_devices_callback_data__pulse*)pUserData;
    ma_device_info deviceInfo;

    MA_ASSERT(pData != NULL);

    if (endOfList || pData->isTerminated) {
        return;
    }

    MA_ZERO_OBJECT(&deviceInfo);

    /* The name from PulseAudio is the ID for miniaudio. */
    if (pSinkInfo->name != NULL) {
        ma_strncpy_s(deviceInfo.id.pulse, sizeof(deviceInfo.id.pulse), pSinkInfo->name, (size_t)-1);
    }

    /* The description from PulseAudio is the name for miniaudio. */
    if (pSinkInfo->description != NULL) {
        ma_strncpy_s(deviceInfo.name, sizeof(deviceInfo.name), pSinkInfo->description, (size_t)-1);
    }

    pData->isTerminated = !pData->callback(pData->pContext, ma_device_type_playback, &deviceInfo, pData->pUserData);

    (void)pPulseContext; /* Unused. */
}

void ma_context_enumerate_devices_source_callback__pulse(ma_pa_context* pPulseContext, const ma_pa_source_info* pSinkInfo, int endOfList, void* pUserData)
{
    ma_context_enumerate_devices_callback_data__pulse* pData = (ma_context_enumerate_devices_callback_data__pulse*)pUserData;
    ma_device_info deviceInfo;

    MA_ASSERT(pData != NULL);

    if (endOfList || pData->isTerminated) {
        return;
    }

    MA_ZERO_OBJECT(&deviceInfo);

    /* The name from PulseAudio is the ID for miniaudio. */
    if (pSinkInfo->name != NULL) {
        ma_strncpy_s(deviceInfo.id.pulse, sizeof(deviceInfo.id.pulse), pSinkInfo->name, (size_t)-1);
    }

    /* The description from PulseAudio is the name for miniaudio. */
    if (pSinkInfo->description != NULL) {
        ma_strncpy_s(deviceInfo.name, sizeof(deviceInfo.name), pSinkInfo->description, (size_t)-1);
    }

    pData->isTerminated = !pData->callback(pData->pContext, ma_device_type_capture, &deviceInfo, pData->pUserData);

    (void)pPulseContext; /* Unused. */
}

ma_result ma_context_enumerate_devices__pulse(ma_context* pContext, ma_enum_devices_callback_proc callback, void* pUserData)
{
    ma_result result = MA_SUCCESS;
    ma_context_enumerate_devices_callback_data__pulse callbackData;
    ma_pa_operation* pOP = NULL;
    ma_pa_mainloop* pMainLoop;
    ma_pa_mainloop_api* pAPI;
    ma_pa_context* pPulseContext;
    int error;

    MA_ASSERT(pContext != NULL);
    MA_ASSERT(callback != NULL);

    callbackData.pContext = pContext;
    callbackData.callback = callback;
    callbackData.pUserData = pUserData;
    callbackData.isTerminated = MA_FALSE;

    pMainLoop = ((ma_pa_mainloop_new_proc)pContext->pulse.pa_mainloop_new)();
    if (pMainLoop == NULL) {
        return MA_FAILED_TO_INIT_BACKEND;
    }

    pAPI = ((ma_pa_mainloop_get_api_proc)pContext->pulse.pa_mainloop_get_api)(pMainLoop);
    if (pAPI == NULL) {
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return MA_FAILED_TO_INIT_BACKEND;
    }

    pPulseContext = ((ma_pa_context_new_proc)pContext->pulse.pa_context_new)(pAPI, pContext->pulse.pApplicationName);
    if (pPulseContext == NULL) {
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return MA_FAILED_TO_INIT_BACKEND;
    }

    error = ((ma_pa_context_connect_proc)pContext->pulse.pa_context_connect)(pPulseContext, pContext->pulse.pServerName, (pContext->pulse.tryAutoSpawn) ? 0 : MA_PA_CONTEXT_NOAUTOSPAWN, NULL);
    if (error != MA_PA_OK) {
        ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return ma_result_from_pulse(error);
    }

    for (;;) {
        ma_pa_context_state_t state = ((ma_pa_context_get_state_proc)pContext->pulse.pa_context_get_state)(pPulseContext);
        if (state == MA_PA_CONTEXT_READY) {
            break;  /* Success. */
        }
        if (state == MA_PA_CONTEXT_CONNECTING || state == MA_PA_CONTEXT_AUTHORIZING || state == MA_PA_CONTEXT_SETTING_NAME) {
            error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)(pMainLoop, 1, NULL);
            if (error < 0) {
                result = ma_result_from_pulse(error);
                goto done;
            }

#ifdef MA_DEBUG_OUTPUT
            printf("[PulseAudio] pa_context_get_state() returned %d. Waiting.\n", state);
#endif
            continue;   /* Keep trying. */
        }
        if (state == MA_PA_CONTEXT_UNCONNECTED || state == MA_PA_CONTEXT_FAILED || state == MA_PA_CONTEXT_TERMINATED) {
#ifdef MA_DEBUG_OUTPUT
            printf("[PulseAudio] pa_context_get_state() returned %d. Failed.\n", state);
#endif
            goto done;  /* Failed. */
        }
    }


    /* Playback. */
    if (!callbackData.isTerminated) {
        pOP = ((ma_pa_context_get_sink_info_list_proc)pContext->pulse.pa_context_get_sink_info_list)(pPulseContext, ma_context_enumerate_devices_sink_callback__pulse, &callbackData);
        if (pOP == NULL) {
            result = MA_ERROR;
            goto done;
        }

        result = ma_wait_for_operation__pulse(pContext, pMainLoop, pOP);
        ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
        if (result != MA_SUCCESS) {
            goto done;
        }
    }


    /* Capture. */
    if (!callbackData.isTerminated) {
        pOP = ((ma_pa_context_get_source_info_list_proc)pContext->pulse.pa_context_get_source_info_list)(pPulseContext, ma_context_enumerate_devices_source_callback__pulse, &callbackData);
        if (pOP == NULL) {
            result = MA_ERROR;
            goto done;
        }

        result = ma_wait_for_operation__pulse(pContext, pMainLoop, pOP);
        ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
        if (result != MA_SUCCESS) {
            goto done;
        }
    }

done:
    ((ma_pa_context_disconnect_proc)pContext->pulse.pa_context_disconnect)(pPulseContext);
    ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
    ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
    return result;
}


typedef struct
{
    ma_device_info* pDeviceInfo;
    ma_bool32 foundDevice;
} ma_context_get_device_info_callback_data__pulse;

void ma_context_get_device_info_sink_callback__pulse(ma_pa_context* pPulseContext, const ma_pa_sink_info* pInfo, int endOfList, void* pUserData)
{
    ma_context_get_device_info_callback_data__pulse* pData = (ma_context_get_device_info_callback_data__pulse*)pUserData;

    if (endOfList > 0) {
        return;
    }

    MA_ASSERT(pData != NULL);
    pData->foundDevice = MA_TRUE;

    if (pInfo->name != NULL) {
        ma_strncpy_s(pData->pDeviceInfo->id.pulse, sizeof(pData->pDeviceInfo->id.pulse), pInfo->name, (size_t)-1);
    }

    if (pInfo->description != NULL) {
        ma_strncpy_s(pData->pDeviceInfo->name, sizeof(pData->pDeviceInfo->name), pInfo->description, (size_t)-1);
    }

    pData->pDeviceInfo->minChannels   = pInfo->sample_spec.channels;
    pData->pDeviceInfo->maxChannels   = pInfo->sample_spec.channels;
    pData->pDeviceInfo->minSampleRate = pInfo->sample_spec.rate;
    pData->pDeviceInfo->maxSampleRate = pInfo->sample_spec.rate;
    pData->pDeviceInfo->formatCount = 1;
    pData->pDeviceInfo->formats[0] = ma_format_from_pulse(pInfo->sample_spec.format);

    (void)pPulseContext; /* Unused. */
}

void ma_context_get_device_info_source_callback__pulse(ma_pa_context* pPulseContext, const ma_pa_source_info* pInfo, int endOfList, void* pUserData)
{
    ma_context_get_device_info_callback_data__pulse* pData = (ma_context_get_device_info_callback_data__pulse*)pUserData;

    if (endOfList > 0) {
        return;
    }

    MA_ASSERT(pData != NULL);
    pData->foundDevice = MA_TRUE;

    if (pInfo->name != NULL) {
        ma_strncpy_s(pData->pDeviceInfo->id.pulse, sizeof(pData->pDeviceInfo->id.pulse), pInfo->name, (size_t)-1);
    }

    if (pInfo->description != NULL) {
        ma_strncpy_s(pData->pDeviceInfo->name, sizeof(pData->pDeviceInfo->name), pInfo->description, (size_t)-1);
    }

    pData->pDeviceInfo->minChannels = pInfo->sample_spec.channels;
    pData->pDeviceInfo->maxChannels = pInfo->sample_spec.channels;
    pData->pDeviceInfo->minSampleRate = pInfo->sample_spec.rate;
    pData->pDeviceInfo->maxSampleRate = pInfo->sample_spec.rate;
    pData->pDeviceInfo->formatCount = 1;
    pData->pDeviceInfo->formats[0] = ma_format_from_pulse(pInfo->sample_spec.format);

    (void)pPulseContext; /* Unused. */
}

ma_result ma_context_get_device_info__pulse(ma_context* pContext, ma_device_type deviceType, const ma_device_id* pDeviceID, ma_share_mode shareMode, ma_device_info* pDeviceInfo)
{
    ma_result result = MA_SUCCESS;
    ma_context_get_device_info_callback_data__pulse callbackData;
    ma_pa_operation* pOP = NULL;
    ma_pa_mainloop* pMainLoop;
    ma_pa_mainloop_api* pAPI;
    ma_pa_context* pPulseContext;
    int error;

    MA_ASSERT(pContext != NULL);

    /* No exclusive mode with the PulseAudio backend. */
    if (shareMode == ma_share_mode_exclusive) {
        return MA_SHARE_MODE_NOT_SUPPORTED;
    }

    callbackData.pDeviceInfo = pDeviceInfo;
    callbackData.foundDevice = MA_FALSE;

    pMainLoop = ((ma_pa_mainloop_new_proc)pContext->pulse.pa_mainloop_new)();
    if (pMainLoop == NULL) {
        return MA_FAILED_TO_INIT_BACKEND;
    }

    pAPI = ((ma_pa_mainloop_get_api_proc)pContext->pulse.pa_mainloop_get_api)(pMainLoop);
    if (pAPI == NULL) {
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return MA_FAILED_TO_INIT_BACKEND;
    }

    pPulseContext = ((ma_pa_context_new_proc)pContext->pulse.pa_context_new)(pAPI, pContext->pulse.pApplicationName);
    if (pPulseContext == NULL) {
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return MA_FAILED_TO_INIT_BACKEND;
    }

    error = ((ma_pa_context_connect_proc)pContext->pulse.pa_context_connect)(pPulseContext, pContext->pulse.pServerName, 0, NULL);
    if (error != MA_PA_OK) {
        ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
        ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
        return ma_result_from_pulse(error);
    }

    for (;;) {
        ma_pa_context_state_t state = ((ma_pa_context_get_state_proc)pContext->pulse.pa_context_get_state)(pPulseContext);
        if (state == MA_PA_CONTEXT_READY) {
            break;  /* Success. */
        }
        if (state == MA_PA_CONTEXT_CONNECTING || state == MA_PA_CONTEXT_AUTHORIZING || state == MA_PA_CONTEXT_SETTING_NAME) {
            error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)(pMainLoop, 1, NULL);
            if (error < 0) {
                result = ma_result_from_pulse(error);
                goto done;
            }

#ifdef MA_DEBUG_OUTPUT
            printf("[PulseAudio] pa_context_get_state() returned %d. Waiting.\n", state);
#endif
            continue;   /* Keep trying. */
        }
        if (state == MA_PA_CONTEXT_UNCONNECTED || state == MA_PA_CONTEXT_FAILED || state == MA_PA_CONTEXT_TERMINATED) {
#ifdef MA_DEBUG_OUTPUT
            printf("[PulseAudio] pa_context_get_state() returned %d. Failed.\n", state);
#endif
            goto done;  /* Failed. */
        }
    }

    if (deviceType == ma_device_type_playback) {
        pOP = ((ma_pa_context_get_sink_info_by_name_proc)pContext->pulse.pa_context_get_sink_info_by_name)(pPulseContext, pDeviceID->pulse, ma_context_get_device_info_sink_callback__pulse, &callbackData);
    } else {
        pOP = ((ma_pa_context_get_source_info_by_name_proc)pContext->pulse.pa_context_get_source_info_by_name)(pPulseContext, pDeviceID->pulse, ma_context_get_device_info_source_callback__pulse, &callbackData);
    }

    if (pOP != NULL) {
        ma_wait_for_operation__pulse(pContext, pMainLoop, pOP);
        ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
    } else {
        result = MA_ERROR;
        goto done;
    }

    if (!callbackData.foundDevice) {
        result = MA_NO_DEVICE;
        goto done;
    }


done:
    ((ma_pa_context_disconnect_proc)pContext->pulse.pa_context_disconnect)(pPulseContext);
    ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)(pPulseContext);
    ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)(pMainLoop);
    return result;
}


void ma_pulse_device_state_callback(ma_pa_context* pPulseContext, void* pUserData)
{
    ma_device* pDevice;
    ma_context* pContext;

    pDevice = (ma_device*)pUserData;
    MA_ASSERT(pDevice != NULL);

    pContext = pDevice->pContext;
    MA_ASSERT(pContext != NULL);

    pDevice->pulse.pulseContextState = ((ma_pa_context_get_state_proc)pContext->pulse.pa_context_get_state)(pPulseContext);
}

void ma_device_sink_info_callback(ma_pa_context* pPulseContext, const ma_pa_sink_info* pInfo, int endOfList, void* pUserData)
{
    ma_pa_sink_info* pInfoOut;

    if (endOfList > 0) {
        return;
    }

    pInfoOut = (ma_pa_sink_info*)pUserData;
    MA_ASSERT(pInfoOut != NULL);

    *pInfoOut = *pInfo;

    (void)pPulseContext; /* Unused. */
}

void ma_device_source_info_callback(ma_pa_context* pPulseContext, const ma_pa_source_info* pInfo, int endOfList, void* pUserData)
{
    ma_pa_source_info* pInfoOut;

    if (endOfList > 0) {
        return;
    }

    pInfoOut = (ma_pa_source_info*)pUserData;
    MA_ASSERT(pInfoOut != NULL);

    *pInfoOut = *pInfo;

    (void)pPulseContext; /* Unused. */
}

void ma_device_sink_name_callback(ma_pa_context* pPulseContext, const ma_pa_sink_info* pInfo, int endOfList, void* pUserData)
{
    ma_device* pDevice;

    if (endOfList > 0) {
        return;
    }

    pDevice = (ma_device*)pUserData;
    MA_ASSERT(pDevice != NULL);

    ma_strncpy_s(pDevice->playback.name, sizeof(pDevice->playback.name), pInfo->description, (size_t)-1);

    (void)pPulseContext; /* Unused. */
}

void ma_device_source_name_callback(ma_pa_context* pPulseContext, const ma_pa_source_info* pInfo, int endOfList, void* pUserData)
{
    ma_device* pDevice;

    if (endOfList > 0) {
        return;
    }

    pDevice = (ma_device*)pUserData;
    MA_ASSERT(pDevice != NULL);

    ma_strncpy_s(pDevice->capture.name, sizeof(pDevice->capture.name), pInfo->description, (size_t)-1);

    (void)pPulseContext; /* Unused. */
}

void ma_device_uninit__pulse(ma_device* pDevice)
{
    ma_context* pContext;

    MA_ASSERT(pDevice != NULL);

    pContext = pDevice->pContext;
    MA_ASSERT(pContext != NULL);

    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        ((ma_pa_stream_disconnect_proc)pContext->pulse.pa_stream_disconnect)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
        ((ma_pa_stream_unref_proc)pContext->pulse.pa_stream_unref)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
    }
    if (pDevice->type == ma_device_type_playback || pDevice->type == ma_device_type_duplex) {
        ((ma_pa_stream_disconnect_proc)pContext->pulse.pa_stream_disconnect)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
        ((ma_pa_stream_unref_proc)pContext->pulse.pa_stream_unref)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
    }

    ((ma_pa_context_disconnect_proc)pContext->pulse.pa_context_disconnect)((ma_pa_context*)pDevice->pulse.pPulseContext);
    ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)((ma_pa_context*)pDevice->pulse.pPulseContext);
    ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)((ma_pa_mainloop*)pDevice->pulse.pMainLoop);
}

ma_pa_buffer_attr ma_device__pa_buffer_attr_new(ma_uint32 periodSizeInFrames, ma_uint32 periods, const ma_pa_sample_spec* ss)
{
    ma_pa_buffer_attr attr;
    attr.maxlength = periodSizeInFrames * periods * ma_get_bytes_per_frame(ma_format_from_pulse(ss->format), ss->channels);
    attr.tlength   = attr.maxlength / periods;
    attr.prebuf    = (ma_uint32)-1;
    attr.minreq    = (ma_uint32)-1;
    attr.fragsize  = attr.maxlength / periods;

    return attr;
}

ma_pa_stream* ma_device__pa_stream_new__pulse(ma_device* pDevice, const char* pStreamName, const ma_pa_sample_spec* ss, const ma_pa_channel_map* cmap)
{
    static int g_StreamCounter = 0;
    char actualStreamName[256];

    if (pStreamName != NULL) {
        ma_strncpy_s(actualStreamName, sizeof(actualStreamName), pStreamName, (size_t)-1);
    } else {
        ma_strcpy_s(actualStreamName, sizeof(actualStreamName), "miniaudio:");
        ma_itoa_s(g_StreamCounter, actualStreamName + 8, sizeof(actualStreamName)-8, 10);  /* 8 = strlen("miniaudio:") */
    }
    g_StreamCounter += 1;

    return ((ma_pa_stream_new_proc)pDevice->pContext->pulse.pa_stream_new)((ma_pa_context*)pDevice->pulse.pPulseContext, actualStreamName, ss, cmap);
}

ma_result ma_device_init__pulse(ma_context* pContext, const ma_device_config* pConfig, ma_device* pDevice)
{
    ma_result result = MA_SUCCESS;
    int error = 0;
    const char* devPlayback = NULL;
    const char* devCapture  = NULL;
    ma_uint32 periodSizeInMilliseconds;
    ma_pa_sink_info sinkInfo;
    ma_pa_source_info sourceInfo;
    ma_pa_operation* pOP = NULL;
    ma_pa_sample_spec ss;
    ma_pa_channel_map cmap;
    ma_pa_buffer_attr attr;
    const ma_pa_sample_spec* pActualSS   = NULL;
    const ma_pa_channel_map* pActualCMap = NULL;
    const ma_pa_buffer_attr* pActualAttr = NULL;
    ma_uint32 iChannel;
    ma_pa_stream_flags_t streamFlags;

    MA_ASSERT(pDevice != NULL);
    MA_ZERO_OBJECT(&pDevice->pulse);

    if (pConfig->deviceType == ma_device_type_loopback) {
        return MA_DEVICE_TYPE_NOT_SUPPORTED;
    }

    /* No exclusive mode with the PulseAudio backend. */
    if (((pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) && pConfig->playback.shareMode == ma_share_mode_exclusive) ||
        ((pConfig->deviceType == ma_device_type_capture  || pConfig->deviceType == ma_device_type_duplex) && pConfig->capture.shareMode  == ma_share_mode_exclusive)) {
        return MA_SHARE_MODE_NOT_SUPPORTED;
    }

    if ((pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) && pConfig->playback.pDeviceID != NULL) {
        devPlayback = pConfig->playback.pDeviceID->pulse;
    }
    if ((pConfig->deviceType == ma_device_type_capture || pConfig->deviceType == ma_device_type_duplex) && pConfig->capture.pDeviceID != NULL) {
        devCapture = pConfig->capture.pDeviceID->pulse;
    }

    periodSizeInMilliseconds = pConfig->periodSizeInMilliseconds;
    if (periodSizeInMilliseconds == 0) {
        periodSizeInMilliseconds = ma_calculate_buffer_size_in_milliseconds_from_frames(pConfig->periodSizeInFrames, pConfig->sampleRate);
    }

    pDevice->pulse.pMainLoop = ((ma_pa_mainloop_new_proc)pContext->pulse.pa_mainloop_new)();
    if (pDevice->pulse.pMainLoop == NULL) {
        result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to create main loop for device.", MA_FAILED_TO_INIT_BACKEND);
        goto on_error0;
    }

    pDevice->pulse.pAPI = ((ma_pa_mainloop_get_api_proc)pContext->pulse.pa_mainloop_get_api)((ma_pa_mainloop*)pDevice->pulse.pMainLoop);
    if (pDevice->pulse.pAPI == NULL) {
        result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to retrieve PulseAudio main loop.", MA_FAILED_TO_INIT_BACKEND);
        goto on_error1;
    }

    pDevice->pulse.pPulseContext = ((ma_pa_context_new_proc)pContext->pulse.pa_context_new)((ma_pa_mainloop_api*)pDevice->pulse.pAPI, pContext->pulse.pApplicationName);
    if (pDevice->pulse.pPulseContext == NULL) {
        result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to create PulseAudio context for device.", MA_FAILED_TO_INIT_BACKEND);
        goto on_error1;
    }

    error = ((ma_pa_context_connect_proc)pContext->pulse.pa_context_connect)((ma_pa_context*)pDevice->pulse.pPulseContext, pContext->pulse.pServerName, (pContext->pulse.tryAutoSpawn) ? 0 : MA_PA_CONTEXT_NOAUTOSPAWN, NULL);
    if (error != MA_PA_OK) {
        result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to connect PulseAudio context.", ma_result_from_pulse(error));
        goto on_error2;
    }


    pDevice->pulse.pulseContextState = MA_PA_CONTEXT_UNCONNECTED;
    ((ma_pa_context_set_state_callback_proc)pContext->pulse.pa_context_set_state_callback)((ma_pa_context*)pDevice->pulse.pPulseContext, ma_pulse_device_state_callback, pDevice);

    /* Wait for PulseAudio to get itself ready before returning. */
    for (;;) {
        if (pDevice->pulse.pulseContextState == MA_PA_CONTEXT_READY) {
            break;
        }

        /* An error may have occurred. */
        if (pDevice->pulse.pulseContextState == MA_PA_CONTEXT_FAILED || pDevice->pulse.pulseContextState == MA_PA_CONTEXT_TERMINATED) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] An error occurred while connecting the PulseAudio context.", MA_ERROR);
            goto on_error3;
        }

        error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)((ma_pa_mainloop*)pDevice->pulse.pMainLoop, 1, NULL);
        if (error < 0) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] The PulseAudio main loop returned an error while connecting the PulseAudio context.", ma_result_from_pulse(error));
            goto on_error3;
        }
    }

    if (pConfig->deviceType == ma_device_type_capture || pConfig->deviceType == ma_device_type_duplex) {
        pOP = ((ma_pa_context_get_source_info_by_name_proc)pContext->pulse.pa_context_get_source_info_by_name)((ma_pa_context*)pDevice->pulse.pPulseContext, devCapture, ma_device_source_info_callback, &sourceInfo);
        if (pOP != NULL) {
            ma_device__wait_for_operation__pulse(pDevice, pOP);
            ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
        } else {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to retrieve source info for capture device.", ma_result_from_pulse(error));
            goto on_error3;
        }

        ss = sourceInfo.sample_spec;
        cmap = sourceInfo.channel_map;

        pDevice->capture.internalPeriodSizeInFrames = ma_calculate_buffer_size_in_frames_from_milliseconds(periodSizeInMilliseconds, ss.rate);
        pDevice->capture.internalPeriods            = pConfig->periods;

        attr = ma_device__pa_buffer_attr_new(pDevice->capture.internalPeriodSizeInFrames, pConfig->periods, &ss);
    #ifdef MA_DEBUG_OUTPUT
        printf("[PulseAudio] Capture attr: maxlength=%d, tlength=%d, prebuf=%d, minreq=%d, fragsize=%d; internalPeriodSizeInFrames=%d\n", attr.maxlength, attr.tlength, attr.prebuf, attr.minreq, attr.fragsize, pDevice->capture.internalPeriodSizeInFrames);
    #endif

        pDevice->pulse.pStreamCapture = ma_device__pa_stream_new__pulse(pDevice, pConfig->pulse.pStreamNameCapture, &ss, &cmap);
        if (pDevice->pulse.pStreamCapture == NULL) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to create PulseAudio capture stream.", MA_FAILED_TO_OPEN_BACKEND_DEVICE);
            goto on_error3;
        }

        streamFlags = MA_PA_STREAM_START_CORKED | MA_PA_STREAM_FIX_FORMAT | MA_PA_STREAM_FIX_RATE | MA_PA_STREAM_FIX_CHANNELS;
        if (devCapture != NULL) {
            streamFlags |= MA_PA_STREAM_DONT_MOVE;
        }

        error = ((ma_pa_stream_connect_record_proc)pContext->pulse.pa_stream_connect_record)((ma_pa_stream*)pDevice->pulse.pStreamCapture, devCapture, &attr, streamFlags);
        if (error != MA_PA_OK) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to connect PulseAudio capture stream.", ma_result_from_pulse(error));
            goto on_error4;
        }

        while (((ma_pa_stream_get_state_proc)pContext->pulse.pa_stream_get_state)((ma_pa_stream*)pDevice->pulse.pStreamCapture) != MA_PA_STREAM_READY) {
            error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)((ma_pa_mainloop*)pDevice->pulse.pMainLoop, 1, NULL);
            if (error < 0) {
                result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] The PulseAudio main loop returned an error while connecting the PulseAudio capture stream.", ma_result_from_pulse(error));
                goto on_error5;
            }
        }

        /* Internal format. */
        pActualSS = ((ma_pa_stream_get_sample_spec_proc)pContext->pulse.pa_stream_get_sample_spec)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
        if (pActualSS != NULL) {
            /* If anything has changed between the requested and the actual sample spec, we need to update the buffer. */
            if (ss.format != pActualSS->format || ss.channels != pActualSS->channels || ss.rate != pActualSS->rate) {
                attr = ma_device__pa_buffer_attr_new(pDevice->capture.internalPeriodSizeInFrames, pConfig->periods, pActualSS);

                pOP = ((ma_pa_stream_set_buffer_attr_proc)pContext->pulse.pa_stream_set_buffer_attr)((ma_pa_stream*)pDevice->pulse.pStreamCapture, &attr, NULL, NULL);
                if (pOP != NULL) {
                    ma_device__wait_for_operation__pulse(pDevice, pOP);
                    ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
                }
            }

            ss = *pActualSS;
        }

        pDevice->capture.internalFormat     = ma_format_from_pulse(ss.format);
        pDevice->capture.internalChannels   = ss.channels;
        pDevice->capture.internalSampleRate = ss.rate;

        /* Internal channel map. */
        pActualCMap = ((ma_pa_stream_get_channel_map_proc)pContext->pulse.pa_stream_get_channel_map)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
        if (pActualCMap != NULL) {
            cmap = *pActualCMap;
        }
        for (iChannel = 0; iChannel < pDevice->capture.internalChannels; ++iChannel) {
            pDevice->capture.internalChannelMap[iChannel] = ma_channel_position_from_pulse(cmap.map[iChannel]);
        }

        /* Buffer. */
        pActualAttr = ((ma_pa_stream_get_buffer_attr_proc)pContext->pulse.pa_stream_get_buffer_attr)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
        if (pActualAttr != NULL) {
            attr = *pActualAttr;
        }
        pDevice->capture.internalPeriods            = attr.maxlength / attr.fragsize;
        pDevice->capture.internalPeriodSizeInFrames = attr.maxlength / ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels) / pDevice->capture.internalPeriods;
    #ifdef MA_DEBUG_OUTPUT
        printf("[PulseAudio] Capture actual attr: maxlength=%d, tlength=%d, prebuf=%d, minreq=%d, fragsize=%d; internalPeriodSizeInFrames=%d\n", attr.maxlength, attr.tlength, attr.prebuf, attr.minreq, attr.fragsize, pDevice->capture.internalPeriodSizeInFrames);
    #endif

        /* Name. */
        devCapture = ((ma_pa_stream_get_device_name_proc)pContext->pulse.pa_stream_get_device_name)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
        if (devCapture != NULL) {
            ma_pa_operation* pOP = ((ma_pa_context_get_source_info_by_name_proc)pContext->pulse.pa_context_get_source_info_by_name)((ma_pa_context*)pDevice->pulse.pPulseContext, devCapture, ma_device_source_name_callback, pDevice);
            if (pOP != NULL) {
                ma_device__wait_for_operation__pulse(pDevice, pOP);
                ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
            }
        }
    }

    if (pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) {
        pOP = ((ma_pa_context_get_sink_info_by_name_proc)pContext->pulse.pa_context_get_sink_info_by_name)((ma_pa_context*)pDevice->pulse.pPulseContext, devPlayback, ma_device_sink_info_callback, &sinkInfo);
        if (pOP != NULL) {
            ma_device__wait_for_operation__pulse(pDevice, pOP);
            ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
        } else {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to retrieve sink info for playback device.", ma_result_from_pulse(error));
            goto on_error3;
        }

        ss = sinkInfo.sample_spec;
        cmap = sinkInfo.channel_map;

        pDevice->playback.internalPeriodSizeInFrames = ma_calculate_buffer_size_in_frames_from_milliseconds(periodSizeInMilliseconds, ss.rate);
        pDevice->playback.internalPeriods            = pConfig->periods;

        attr = ma_device__pa_buffer_attr_new(pDevice->playback.internalPeriodSizeInFrames, pConfig->periods, &ss);
    #ifdef MA_DEBUG_OUTPUT
        printf("[PulseAudio] Playback attr: maxlength=%d, tlength=%d, prebuf=%d, minreq=%d, fragsize=%d; internalPeriodSizeInFrames=%d\n", attr.maxlength, attr.tlength, attr.prebuf, attr.minreq, attr.fragsize, pDevice->playback.internalPeriodSizeInFrames);
    #endif

        pDevice->pulse.pStreamPlayback = ma_device__pa_stream_new__pulse(pDevice, pConfig->pulse.pStreamNamePlayback, &ss, &cmap);
        if (pDevice->pulse.pStreamPlayback == NULL) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to create PulseAudio playback stream.", MA_FAILED_TO_OPEN_BACKEND_DEVICE);
            goto on_error3;
        }

        streamFlags = MA_PA_STREAM_START_CORKED | MA_PA_STREAM_FIX_FORMAT | MA_PA_STREAM_FIX_RATE | MA_PA_STREAM_FIX_CHANNELS;
        if (devPlayback != NULL) {
            streamFlags |= MA_PA_STREAM_DONT_MOVE;
        }

        error = ((ma_pa_stream_connect_playback_proc)pContext->pulse.pa_stream_connect_playback)((ma_pa_stream*)pDevice->pulse.pStreamPlayback, devPlayback, &attr, streamFlags, NULL, NULL);
        if (error != MA_PA_OK) {
            result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to connect PulseAudio playback stream.", ma_result_from_pulse(error));
            goto on_error6;
        }

        while (((ma_pa_stream_get_state_proc)pContext->pulse.pa_stream_get_state)((ma_pa_stream*)pDevice->pulse.pStreamPlayback) != MA_PA_STREAM_READY) {
            error = ((ma_pa_mainloop_iterate_proc)pContext->pulse.pa_mainloop_iterate)((ma_pa_mainloop*)pDevice->pulse.pMainLoop, 1, NULL);
            if (error < 0) {
                result = ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] The PulseAudio main loop returned an error while connecting the PulseAudio playback stream.", ma_result_from_pulse(error));
                goto on_error7;
            }
        }

        /* Internal format. */
        pActualSS = ((ma_pa_stream_get_sample_spec_proc)pContext->pulse.pa_stream_get_sample_spec)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
        if (pActualSS != NULL) {
            /* If anything has changed between the requested and the actual sample spec, we need to update the buffer. */
            if (ss.format != pActualSS->format || ss.channels != pActualSS->channels || ss.rate != pActualSS->rate) {
                attr = ma_device__pa_buffer_attr_new(pDevice->playback.internalPeriodSizeInFrames, pConfig->periods, pActualSS);

                pOP = ((ma_pa_stream_set_buffer_attr_proc)pContext->pulse.pa_stream_set_buffer_attr)((ma_pa_stream*)pDevice->pulse.pStreamPlayback, &attr, NULL, NULL);
                if (pOP != NULL) {
                    ma_device__wait_for_operation__pulse(pDevice, pOP);
                    ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
                }
            }

            ss = *pActualSS;
        }

        pDevice->playback.internalFormat     = ma_format_from_pulse(ss.format);
        pDevice->playback.internalChannels   = ss.channels;
        pDevice->playback.internalSampleRate = ss.rate;

        /* Internal channel map. */
        pActualCMap = ((ma_pa_stream_get_channel_map_proc)pContext->pulse.pa_stream_get_channel_map)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
        if (pActualCMap != NULL) {
            cmap = *pActualCMap;
        }
        for (iChannel = 0; iChannel < pDevice->playback.internalChannels; ++iChannel) {
            pDevice->playback.internalChannelMap[iChannel] = ma_channel_position_from_pulse(cmap.map[iChannel]);
        }

        /* Buffer. */
        pActualAttr = ((ma_pa_stream_get_buffer_attr_proc)pContext->pulse.pa_stream_get_buffer_attr)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
        if (pActualAttr != NULL) {
            attr = *pActualAttr;
        }
        pDevice->playback.internalPeriods            = attr.maxlength / attr.tlength;
        pDevice->playback.internalPeriodSizeInFrames = attr.maxlength / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels) / pDevice->playback.internalPeriods;
    #ifdef MA_DEBUG_OUTPUT
        printf("[PulseAudio] Playback actual attr: maxlength=%d, tlength=%d, prebuf=%d, minreq=%d, fragsize=%d; internalPeriodSizeInFrames=%d\n", attr.maxlength, attr.tlength, attr.prebuf, attr.minreq, attr.fragsize, pDevice->playback.internalPeriodSizeInFrames);
    #endif

        /* Name. */
        devPlayback = ((ma_pa_stream_get_device_name_proc)pContext->pulse.pa_stream_get_device_name)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
        if (devPlayback != NULL) {
            ma_pa_operation* pOP = ((ma_pa_context_get_sink_info_by_name_proc)pContext->pulse.pa_context_get_sink_info_by_name)((ma_pa_context*)pDevice->pulse.pPulseContext, devPlayback, ma_device_sink_name_callback, pDevice);
            if (pOP != NULL) {
                ma_device__wait_for_operation__pulse(pDevice, pOP);
                ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);
            }
        }
    }

    return MA_SUCCESS;


on_error7:
    if (pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) {
        ((ma_pa_stream_disconnect_proc)pContext->pulse.pa_stream_disconnect)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
    }
on_error6:
    if (pConfig->deviceType == ma_device_type_playback || pConfig->deviceType == ma_device_type_duplex) {
        ((ma_pa_stream_unref_proc)pContext->pulse.pa_stream_unref)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
    }
on_error5:
    if (pConfig->deviceType == ma_device_type_capture || pConfig->deviceType == ma_device_type_duplex) {
        ((ma_pa_stream_disconnect_proc)pContext->pulse.pa_stream_disconnect)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
    }
on_error4:
    if (pConfig->deviceType == ma_device_type_capture || pConfig->deviceType == ma_device_type_duplex) {
        ((ma_pa_stream_unref_proc)pContext->pulse.pa_stream_unref)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
    }
on_error3: ((ma_pa_context_disconnect_proc)pContext->pulse.pa_context_disconnect)((ma_pa_context*)pDevice->pulse.pPulseContext);
on_error2: ((ma_pa_context_unref_proc)pContext->pulse.pa_context_unref)((ma_pa_context*)pDevice->pulse.pPulseContext);
on_error1: ((ma_pa_mainloop_free_proc)pContext->pulse.pa_mainloop_free)((ma_pa_mainloop*)pDevice->pulse.pMainLoop);
on_error0:
    return result;
}


void ma_pulse_operation_complete_callback(ma_pa_stream* pStream, int success, void* pUserData)
{
    ma_bool32* pIsSuccessful = (ma_bool32*)pUserData;
    MA_ASSERT(pIsSuccessful != NULL);

    *pIsSuccessful = (ma_bool32)success;

    (void)pStream; /* Unused. */
}

ma_result ma_device__cork_stream__pulse(ma_device* pDevice, ma_device_type deviceType, int cork)
{
    ma_context* pContext = pDevice->pContext;
    ma_bool32 wasSuccessful;
    ma_pa_stream* pStream;
    ma_pa_operation* pOP;
    ma_result result;

    /* This should not be called with a duplex device type. */
    if (deviceType == ma_device_type_duplex) {
        return MA_INVALID_ARGS;
    }

    wasSuccessful = MA_FALSE;

    pStream = (ma_pa_stream*)((deviceType == ma_device_type_capture) ? pDevice->pulse.pStreamCapture : pDevice->pulse.pStreamPlayback);
    MA_ASSERT(pStream != NULL);

    pOP = ((ma_pa_stream_cork_proc)pContext->pulse.pa_stream_cork)(pStream, cork, ma_pulse_operation_complete_callback, &wasSuccessful);
    if (pOP == NULL) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to cork PulseAudio stream.", (cork == 0) ? MA_FAILED_TO_START_BACKEND_DEVICE : MA_FAILED_TO_STOP_BACKEND_DEVICE);
    }

    result = ma_device__wait_for_operation__pulse(pDevice, pOP);
    ((ma_pa_operation_unref_proc)pContext->pulse.pa_operation_unref)(pOP);

    if (result != MA_SUCCESS) {
        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] An error occurred while waiting for the PulseAudio stream to cork.", result);
    }

    if (!wasSuccessful) {
        if (cork) {
            return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to stop PulseAudio stream.", MA_FAILED_TO_STOP_BACKEND_DEVICE);
        } else {
            return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to start PulseAudio stream.", MA_FAILED_TO_START_BACKEND_DEVICE);
        }
    }

    return MA_SUCCESS;
}

ma_result ma_device_stop__pulse(ma_device* pDevice)
{
    ma_result result;
    ma_bool32 wasSuccessful;
    ma_pa_operation* pOP;

    MA_ASSERT(pDevice != NULL);

    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        result = ma_device__cork_stream__pulse(pDevice, ma_device_type_capture, 1);
        if (result != MA_SUCCESS) {
            return result;
        }
    }

    if (pDevice->type == ma_device_type_playback || pDevice->type == ma_device_type_duplex) {
        /* The stream needs to be drained if it's a playback device. */
        pOP = ((ma_pa_stream_drain_proc)pDevice->pContext->pulse.pa_stream_drain)((ma_pa_stream*)pDevice->pulse.pStreamPlayback, ma_pulse_operation_complete_callback, &wasSuccessful);
        if (pOP != NULL) {
            ma_device__wait_for_operation__pulse(pDevice, pOP);
            ((ma_pa_operation_unref_proc)pDevice->pContext->pulse.pa_operation_unref)(pOP);
        }

        result = ma_device__cork_stream__pulse(pDevice, ma_device_type_playback, 1);
        if (result != MA_SUCCESS) {
            return result;
        }
    }

    return MA_SUCCESS;
}

ma_result ma_device_write__pulse(ma_device* pDevice, const void* pPCMFrames, ma_uint32 frameCount, ma_uint32* pFramesWritten)
{
    ma_uint32 totalFramesWritten;

    MA_ASSERT(pDevice != NULL);
    MA_ASSERT(pPCMFrames != NULL);
    MA_ASSERT(frameCount > 0);

    if (pFramesWritten != NULL) {
        *pFramesWritten = 0;
    }

    totalFramesWritten = 0;
    while (totalFramesWritten < frameCount) {
        if (ma_device__get_state(pDevice) != MA_STATE_STARTED) {
            return MA_DEVICE_NOT_STARTED;
        }

        /* Place the data into the mapped buffer if we have one. */
        if (pDevice->pulse.pMappedBufferPlayback != NULL && pDevice->pulse.mappedBufferFramesRemainingPlayback > 0) {
            ma_uint32 bpf = ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
            ma_uint32 mappedBufferFramesConsumed = pDevice->pulse.mappedBufferFramesCapacityPlayback - pDevice->pulse.mappedBufferFramesRemainingPlayback;

            void* pDst = (ma_uint8*)pDevice->pulse.pMappedBufferPlayback + (mappedBufferFramesConsumed * bpf);
            const void* pSrc = (const ma_uint8*)pPCMFrames + (totalFramesWritten * bpf);
            ma_uint32  framesToCopy = ma_min(pDevice->pulse.mappedBufferFramesRemainingPlayback, (frameCount - totalFramesWritten));
            MA_COPY_MEMORY(pDst, pSrc, framesToCopy * bpf);

            pDevice->pulse.mappedBufferFramesRemainingPlayback -= framesToCopy;
            totalFramesWritten += framesToCopy;
        }

        /*
        Getting here means we've run out of data in the currently mapped chunk. We need to write this to the device and then try
        mapping another chunk. If this fails we need to wait for space to become available.
        */
        if (pDevice->pulse.mappedBufferFramesCapacityPlayback > 0 && pDevice->pulse.mappedBufferFramesRemainingPlayback == 0) {
            size_t nbytes = pDevice->pulse.mappedBufferFramesCapacityPlayback * ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);

            int error = ((ma_pa_stream_write_proc)pDevice->pContext->pulse.pa_stream_write)((ma_pa_stream*)pDevice->pulse.pStreamPlayback, pDevice->pulse.pMappedBufferPlayback, nbytes, NULL, 0, MA_PA_SEEK_RELATIVE);
            if (error < 0) {
                return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to write data to the PulseAudio stream.", ma_result_from_pulse(error));
            }

            pDevice->pulse.pMappedBufferPlayback = NULL;
            pDevice->pulse.mappedBufferFramesRemainingPlayback = 0;
            pDevice->pulse.mappedBufferFramesCapacityPlayback = 0;
        }

        MA_ASSERT(totalFramesWritten <= frameCount);
        if (totalFramesWritten == frameCount) {
            break;
        }

        /* Getting here means we need to map a new buffer. If we don't have enough space we need to wait for more. */
        for (;;) {
            size_t writableSizeInBytes;

            /* If the device has been corked, don't try to continue. */
            if (((ma_pa_stream_is_corked_proc)pDevice->pContext->pulse.pa_stream_is_corked)((ma_pa_stream*)pDevice->pulse.pStreamPlayback)) {
                break;
            }

            writableSizeInBytes = ((ma_pa_stream_writable_size_proc)pDevice->pContext->pulse.pa_stream_writable_size)((ma_pa_stream*)pDevice->pulse.pStreamPlayback);
            if (writableSizeInBytes != (size_t)-1) {
                if (writableSizeInBytes > 0) {
                    /* Data is avaialable. */
                    size_t bytesToMap = writableSizeInBytes;
                    int error = ((ma_pa_stream_begin_write_proc)pDevice->pContext->pulse.pa_stream_begin_write)((ma_pa_stream*)pDevice->pulse.pStreamPlayback, &pDevice->pulse.pMappedBufferPlayback, &bytesToMap);
                    if (error < 0) {
                        return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to map write buffer.", ma_result_from_pulse(error));
                    }

                    pDevice->pulse.mappedBufferFramesCapacityPlayback  = bytesToMap / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
                    pDevice->pulse.mappedBufferFramesRemainingPlayback = pDevice->pulse.mappedBufferFramesCapacityPlayback;

                    break;
                } else {
                    /* No data available. Need to wait for more. */
                    int error = ((ma_pa_mainloop_iterate_proc)pDevice->pContext->pulse.pa_mainloop_iterate)((ma_pa_mainloop*)pDevice->pulse.pMainLoop, 1, NULL);
                    if (error < 0) {
                        return ma_result_from_pulse(error);
                    }

                    continue;
                }
            } else {
                return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to query the stream's writable size.", MA_ERROR);
            }
        }
    }

    if (pFramesWritten != NULL) {
        *pFramesWritten = totalFramesWritten;
    }

    return MA_SUCCESS;
}

ma_result ma_device_read__pulse(ma_device* pDevice, void* pPCMFrames, ma_uint32 frameCount, ma_uint32* pFramesRead)
{
    ma_uint32 totalFramesRead;

    MA_ASSERT(pDevice != NULL);
    MA_ASSERT(pPCMFrames != NULL);
    MA_ASSERT(frameCount > 0);

    if (pFramesRead != NULL) {
        *pFramesRead = 0;
    }

    totalFramesRead = 0;
    while (totalFramesRead < frameCount) {
        if (ma_device__get_state(pDevice) != MA_STATE_STARTED) {
            return MA_DEVICE_NOT_STARTED;
        }

        /*
        If a buffer is mapped we need to read from that first. Once it's consumed we need to drop it. Note that pDevice->pulse.pMappedBufferCapture can be null in which
        case it could be a hole. In this case we just write zeros into the output buffer.
        */
        if (pDevice->pulse.mappedBufferFramesRemainingCapture > 0) {
            ma_uint32 bpf = ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels);
            ma_uint32 mappedBufferFramesConsumed = pDevice->pulse.mappedBufferFramesCapacityCapture - pDevice->pulse.mappedBufferFramesRemainingCapture;

            ma_uint32  framesToCopy = ma_min(pDevice->pulse.mappedBufferFramesRemainingCapture, (frameCount - totalFramesRead));
            void* pDst = (ma_uint8*)pPCMFrames + (totalFramesRead * bpf);

            /*
            This little bit of logic here is specifically for PulseAudio and it's hole management. The buffer pointer will be set to NULL
            when the current fragment is a hole. For a hole we just output silence.
            */
            if (pDevice->pulse.pMappedBufferCapture != NULL) {
                const void* pSrc = (const ma_uint8*)pDevice->pulse.pMappedBufferCapture + (mappedBufferFramesConsumed * bpf);
                MA_COPY_MEMORY(pDst, pSrc, framesToCopy * bpf);
            } else {
                MA_ZERO_MEMORY(pDst, framesToCopy * bpf);
            #if defined(MA_DEBUG_OUTPUT)
                printf("[PulseAudio] ma_device_read__pulse: Filling hole with silence.\n");
            #endif
            }

            pDevice->pulse.mappedBufferFramesRemainingCapture -= framesToCopy;
            totalFramesRead += framesToCopy;
        }

        /*
        Getting here means we've run out of data in the currently mapped chunk. We need to drop this from the device and then try
        mapping another chunk. If this fails we need to wait for data to become available.
        */
        if (pDevice->pulse.mappedBufferFramesCapacityCapture > 0 && pDevice->pulse.mappedBufferFramesRemainingCapture == 0) {
            int error;

        #if defined(MA_DEBUG_OUTPUT)
            printf("[PulseAudio] ma_device_read__pulse: Call pa_stream_drop()\n");
        #endif

            error = ((ma_pa_stream_drop_proc)pDevice->pContext->pulse.pa_stream_drop)((ma_pa_stream*)pDevice->pulse.pStreamCapture);
            if (error != 0) {
                return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to drop fragment.", ma_result_from_pulse(error));
            }

            pDevice->pulse.pMappedBufferCapture = NULL;
            pDevice->pulse.mappedBufferFramesRemainingCapture = 0;
            pDevice->pulse.mappedBufferFramesCapacityCapture = 0;
        }

        MA_ASSERT(totalFramesRead <= frameCount);
        if (totalFramesRead == frameCount) {
            break;
        }

        /* Getting here means we need to map a new buffer. If we don't have enough data we wait for more. */
        for (;;) {
            int error;
            size_t bytesMapped;

            if (ma_device__get_state(pDevice) != MA_STATE_STARTED) {
                break;
            }

            /* If the device has been corked, don't try to continue. */
            if (((ma_pa_stream_is_corked_proc)pDevice->pContext->pulse.pa_stream_is_corked)((ma_pa_stream*)pDevice->pulse.pStreamCapture)) {
            #if defined(MA_DEBUG_OUTPUT)
                printf("[PulseAudio] ma_device_read__pulse: Corked.\n");
            #endif
                break;
            }

            MA_ASSERT(pDevice->pulse.pMappedBufferCapture == NULL); /* <-- We're about to map a buffer which means we shouldn't have an existing mapping. */

            error = ((ma_pa_stream_peek_proc)pDevice->pContext->pulse.pa_stream_peek)((ma_pa_stream*)pDevice->pulse.pStreamCapture, &pDevice->pulse.pMappedBufferCapture, &bytesMapped);
            if (error < 0) {
                return ma_post_error(pDevice, MA_LOG_LEVEL_ERROR, "[PulseAudio] Failed to peek capture buffer.", ma_result_from_pulse(error));
            }

            if (bytesMapped > 0) {
                pDevice->pulse.mappedBufferFramesCapacityCapture  = bytesMapped / ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels);
                pDevice->pulse.mappedBufferFramesRemainingCapture = pDevice->pulse.mappedBufferFramesCapacityCapture;

                #if defined(MA_DEBUG_OUTPUT)
                    printf("[PulseAudio] ma_device_read__pulse: Mapped. mappedBufferFramesCapacityCapture=%d, mappedBufferFramesRemainingCapture=%d\n", pDevice->pulse.mappedBufferFramesCapacityCapture, pDevice->pulse.mappedBufferFramesRemainingCapture);
                #endif

                if (pDevice->pulse.pMappedBufferCapture == NULL) {
                    /* It's a hole. */
                    #if defined(MA_DEBUG_OUTPUT)
                        printf("[PulseAudio] ma_device_read__pulse: Call pa_stream_peek(). Hole.\n");
                    #endif
                }

                break;
            } else {
                if (pDevice->pulse.pMappedBufferCapture == NULL) {
                    /* Nothing available yet. Need to wait for more. */

                    /*
                    I have had reports of a deadlock in this part of the code. I have reproduced this when using the "Built-in Audio Analogue Stereo" device without
                    an actual microphone connected. I'm experimenting here by not blocking in pa_mainloop_iterate() and instead sleep for a bit when there are no
                    dispatches.
                    */
                    error = ((ma_pa_mainloop_iterate_proc)pDevice->pContext->pulse.pa_mainloop_iterate)((ma_pa_mainloop*)pDevice->pulse.pMainLoop, 0, NULL);
                    if (error < 0) {
                        return ma_result_from_pulse(error);
                    }

                    /* Sleep for a bit if nothing was dispatched. */
                    if (error == 0) {
                        ma_sleep(1);
                    }

                #if defined(MA_DEBUG_OUTPUT)
                    printf("[PulseAudio] ma_device_read__pulse: No data available. Waiting. mappedBufferFramesCapacityCapture=%d, mappedBufferFramesRemainingCapture=%d\n", pDevice->pulse.mappedBufferFramesCapacityCapture, pDevice->pulse.mappedBufferFramesRemainingCapture);
                #endif
                } else {
                    /* Getting here means we mapped 0 bytes, but have a non-NULL buffer. I don't think this should ever happen. */
                    MA_ASSERT(MA_FALSE);
                }
            }
        }
    }

    if (pFramesRead != NULL) {
        *pFramesRead = totalFramesRead;
    }

    return MA_SUCCESS;
}

ma_result ma_device_main_loop__pulse(ma_device* pDevice)
{
    ma_result result = MA_SUCCESS;
    ma_bool32 exitLoop = MA_FALSE;

    MA_ASSERT(pDevice != NULL);

    /* The stream needs to be uncorked first. We do this at the top for both capture and playback for PulseAudio. */
    if (pDevice->type == ma_device_type_capture || pDevice->type == ma_device_type_duplex) {
        result = ma_device__cork_stream__pulse(pDevice, ma_device_type_capture, 0);
        if (result != MA_SUCCESS) {
            return result;
        }
    }
    if (pDevice->type == ma_device_type_playback || pDevice->type == ma_device_type_duplex) {
        result = ma_device__cork_stream__pulse(pDevice, ma_device_type_playback, 0);
        if (result != MA_SUCCESS) {
            return result;
        }
    }


    while (ma_device__get_state(pDevice) == MA_STATE_STARTED && !exitLoop) {
        switch (pDevice->type)
        {
            case ma_device_type_duplex:
            {
                /* The process is: device_read -> convert -> callback -> convert -> device_write */
                ma_uint32 totalCapturedDeviceFramesProcessed = 0;
                ma_uint32 capturedDevicePeriodSizeInFrames = ma_min(pDevice->capture.internalPeriodSizeInFrames, pDevice->playback.internalPeriodSizeInFrames);
                    
                while (totalCapturedDeviceFramesProcessed < capturedDevicePeriodSizeInFrames) {
                    ma_uint8  capturedDeviceData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                    ma_uint8  playbackDeviceData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                    ma_uint32 capturedDeviceDataCapInFrames = sizeof(capturedDeviceData) / ma_get_bytes_per_frame(pDevice->capture.internalFormat,  pDevice->capture.internalChannels);
                    ma_uint32 playbackDeviceDataCapInFrames = sizeof(playbackDeviceData) / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
                    ma_uint32 capturedDeviceFramesRemaining;
                    ma_uint32 capturedDeviceFramesProcessed;
                    ma_uint32 capturedDeviceFramesToProcess;
                    ma_uint32 capturedDeviceFramesToTryProcessing = capturedDevicePeriodSizeInFrames - totalCapturedDeviceFramesProcessed;
                    if (capturedDeviceFramesToTryProcessing > capturedDeviceDataCapInFrames) {
                        capturedDeviceFramesToTryProcessing = capturedDeviceDataCapInFrames;
                    }

                    result = ma_device_read__pulse(pDevice, capturedDeviceData, capturedDeviceFramesToTryProcessing, &capturedDeviceFramesToProcess);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    capturedDeviceFramesRemaining = capturedDeviceFramesToProcess;
                    capturedDeviceFramesProcessed = 0;

                    for (;;) {
                        ma_uint8  capturedClientData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                        ma_uint8  playbackClientData[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                        ma_uint32 capturedClientDataCapInFrames = sizeof(capturedClientData) / ma_get_bytes_per_frame(pDevice->capture.format, pDevice->capture.channels);
                        ma_uint32 playbackClientDataCapInFrames = sizeof(playbackClientData) / ma_get_bytes_per_frame(pDevice->playback.format, pDevice->playback.channels);
                        ma_uint64 capturedClientFramesToProcessThisIteration = ma_min(capturedClientDataCapInFrames, playbackClientDataCapInFrames);
                        ma_uint64 capturedDeviceFramesToProcessThisIteration = capturedDeviceFramesRemaining;
                        ma_uint8* pRunningCapturedDeviceFrames = ma_offset_ptr(capturedDeviceData, capturedDeviceFramesProcessed * ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels));

                        /* Convert capture data from device format to client format. */
                        result = ma_data_converter_process_pcm_frames(&pDevice->capture.converter, pRunningCapturedDeviceFrames, &capturedDeviceFramesToProcessThisIteration, capturedClientData, &capturedClientFramesToProcessThisIteration);
                        if (result != MA_SUCCESS) {
                            break;
                        }

                        /*
                        If we weren't able to generate any output frames it must mean we've exhaused all of our input. The only time this would not be the case is if capturedClientData was too small
                        which should never be the case when it's of the size MA_DATA_CONVERTER_STACK_BUFFER_SIZE.
                        */
                        if (capturedClientFramesToProcessThisIteration == 0) {
                            break;
                        }

                        ma_device__on_data(pDevice, playbackClientData, capturedClientData, (ma_uint32)capturedClientFramesToProcessThisIteration);    /* Safe cast .*/

                        capturedDeviceFramesProcessed += (ma_uint32)capturedDeviceFramesToProcessThisIteration; /* Safe cast. */
                        capturedDeviceFramesRemaining -= (ma_uint32)capturedDeviceFramesToProcessThisIteration; /* Safe cast. */

                        /* At this point the playbackClientData buffer should be holding data that needs to be written to the device. */
                        for (;;) {
                            ma_uint64 convertedClientFrameCount = capturedClientFramesToProcessThisIteration;
                            ma_uint64 convertedDeviceFrameCount = playbackDeviceDataCapInFrames;
                            result = ma_data_converter_process_pcm_frames(&pDevice->playback.converter, playbackClientData, &convertedClientFrameCount, playbackDeviceData, &convertedDeviceFrameCount);
                            if (result != MA_SUCCESS) {
                                break;
                            }

                            result = ma_device_write__pulse(pDevice, playbackDeviceData, (ma_uint32)convertedDeviceFrameCount, NULL);   /* Safe cast. */
                            if (result != MA_SUCCESS) {
                                exitLoop = MA_TRUE;
                                break;
                            }

                            capturedClientFramesToProcessThisIteration -= (ma_uint32)convertedClientFrameCount;  /* Safe cast. */
                            if (capturedClientFramesToProcessThisIteration == 0) {
                                break;
                            }
                        }

                        /* In case an error happened from ma_device_write__pulse()... */
                        if (result != MA_SUCCESS) {
                            exitLoop = MA_TRUE;
                            break;
                        }
                    }

                    totalCapturedDeviceFramesProcessed += capturedDeviceFramesProcessed;
                }
            } break;

            case ma_device_type_capture:
            {
                ma_uint8 intermediaryBuffer[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                ma_uint32 intermediaryBufferSizeInFrames = sizeof(intermediaryBuffer) / ma_get_bytes_per_frame(pDevice->capture.internalFormat, pDevice->capture.internalChannels);
                ma_uint32 periodSizeInFrames = pDevice->capture.internalPeriodSizeInFrames;
                ma_uint32 framesReadThisPeriod = 0;
                while (framesReadThisPeriod < periodSizeInFrames) {
                    ma_uint32 framesRemainingInPeriod = periodSizeInFrames - framesReadThisPeriod;
                    ma_uint32 framesProcessed;
                    ma_uint32 framesToReadThisIteration = framesRemainingInPeriod;
                    if (framesToReadThisIteration > intermediaryBufferSizeInFrames) {
                        framesToReadThisIteration = intermediaryBufferSizeInFrames;
                    }

                    result = ma_device_read__pulse(pDevice, intermediaryBuffer, framesToReadThisIteration, &framesProcessed);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    ma_device__send_frames_to_client(pDevice, framesProcessed, intermediaryBuffer);

                    framesReadThisPeriod += framesProcessed;
                }
            } break;

            case ma_device_type_playback:
            {
                ma_uint8 intermediaryBuffer[MA_DATA_CONVERTER_STACK_BUFFER_SIZE];
                ma_uint32 intermediaryBufferSizeInFrames = sizeof(intermediaryBuffer) / ma_get_bytes_per_frame(pDevice->playback.internalFormat, pDevice->playback.internalChannels);
                ma_uint32 periodSizeInFrames = pDevice->playback.internalPeriodSizeInFrames;
                ma_uint32 framesWrittenThisPeriod = 0;
                while (framesWrittenThisPeriod < periodSizeInFrames) {
                    ma_uint32 framesRemainingInPeriod = periodSizeInFrames - framesWrittenThisPeriod;
                    ma_uint32 framesProcessed;
                    ma_uint32 framesToWriteThisIteration = framesRemainingInPeriod;
                    if (framesToWriteThisIteration > intermediaryBufferSizeInFrames) {
                        framesToWriteThisIteration = intermediaryBufferSizeInFrames;
                    }

                    ma_device__read_frames_from_client(pDevice, framesToWriteThisIteration, intermediaryBuffer);

                    result = ma_device_write__pulse(pDevice, intermediaryBuffer, framesToWriteThisIteration, &framesProcessed);
                    if (result != MA_SUCCESS) {
                        exitLoop = MA_TRUE;
                        break;
                    }

                    framesWrittenThisPeriod += framesProcessed;
                }
            } break;

            /* To silence a warning. Will never hit this. */
            case ma_device_type_loopback:
            default: break;
        }
    }

    /* Here is where the device needs to be stopped. */
    ma_device_stop__pulse(pDevice);

    return result;
}


ma_result ma_context_uninit__pulse(ma_context* pContext)
{
    MA_ASSERT(pContext != NULL);
    MA_ASSERT(pContext->backend == ma_backend_pulseaudio);

    ma_free(pContext->pulse.pServerName, &pContext->allocationCallbacks);
    pContext->pulse.pServerName = NULL;

    ma_free(pContext->pulse.pApplicationName, &pContext->allocationCallbacks);
    pContext->pulse.pApplicationName = NULL;

#ifndef MA_NO_RUNTIME_LINKING
    ma_dlclose(pContext, pContext->pulse.pulseSO);
#endif

    return MA_SUCCESS;
}

