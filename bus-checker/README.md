# bus-checker #

This folder contains material for the first edition of the book Modern SoC Design on Arm by DJ Greaves.

## Exercise ##

Chapter 6, Formal Methods, of the book, contains the following exercise which is a pointer to this current folder.

Q.6. Implement the checker described in the bus-checker folder of the additional material.

There are no supplementary files for this question. Everything is in the README file.


## Tooling ##

To do this exercise, you will need access to an RTL simulator supporting assertions.  

This exercise can be performed using SVA or PSL, according to whichever tools you have to hand.

Modelsim supports assertions and is free to students (but not on linux
sadly) but perhaps the Windows version can be run under wine.  If you
do not have access to local System Verilog or PSL tools that support
assertions, you can use an online service, such as www.edaplayground.com.


There are a number of stages to this assignment.  The first three
steps call for 'gate-level' designs.  Feel free to present your
'gate-level' designs using structural RTL (Verilog or VHDL) or structural
SystemC or using circuit diagrams.


## Step 1 An RTL Implementation of an Arbiter ##


An arbiter makes a decision about which requester for a shared
resource will next be granted service. 

Arbiters are introduced in section 4.2.1 "Contention and Arbitration"
There are a number of possible policies that an arbiter can use
and their implementation can be synchronous or asynchronous. You must
first select an arbitration policy and implement an arbiter component.
Your arbiter should have three inputs and outputs.

It is perhaps easiest to use the following simple synchronous, static
priority arbiter for your study, but choose whatever you want.  Here
is suitable Verilog source code that has had one line removed for you
to recreate.

    module arbiter(input clk, input reset, input [2:0] reqs, output reg [2:0] grants);


      always @(posedge clk) if (reset) grants <= 0;
	  else begin
	     grants[0] <= reqs[0]; // Highest static priority
	     grants[1] <=  ... fill in this gap ..,
	     grants[2] <= reqs[2] && !(reqs[0] || reqs[1]);
	  end



1a: Design at the gate-level an arbiter for three customers and a
single resource and say what basic type of arbiter it is. (You do not
need to include details of the resource or customers.)

1b: Each customer will interact with the arbiter using a protocol,
typically using one request and one grant signal. Give a formal
specification of this protocol using a state transition diagram.
For a synchronous protocol, explain how the concept of the clock
is embodied in the diagram.


## Step 2 - Simple protocol safety checking using hardware monitors ##

2a: (easy) Give a completely separate RTL or gate-level design that is
a monitor (or checker) for the following safety property: "At all
times, never is the resource granted to more than one requester".
This checker should be a component (eg separate RTL module) that has as
many inputs as is needed to monitor the necessary nets in the system
(e.g. all connections to the arbiter) and an output that is asserted
in any state where the assertion is violated.


2b: (harder) Similarly, design an RTL or gate-level protocol checker
that could be instantiated for each connection between a customer and
the arbiter that checks each instance of the request/grant protocol is
being properly followed.  Do you have, or can you envision, a
request/grant hardware protocol that has no illegal behaviours?  What
is allowed to happen in you system if a customer wants to give up
waiting for the resource (known as 'balking')?

2c: For your particular request/grant protocol design, if you extended 2a to also check that
no grant is issued with a request would this be a state or path property checker?

## Step 3 - Liveness Checking Machine ##

3. Give a completely separate RTL or gate-level design that is a monitor/checker
of the following liveness property: "Whenever reset is not asserted,
when a request is made for the resource, it will eventually be granted".


## Step 4 - Formal Logic Implementations ##

4a. Using PSL, SVA or a similar assertion language, give an assertion that checks
the safety property of step 2a above.

4b. Give a similar temporal logic assertion that asserts that
the liveness property of step 3 above is never violated.


## Step 5 Questions ##

5. Answer three of the questions below.

NOTE: Your arbiter design does not necessarily need to fulfil
all the properties being checked.

Questions for discussion with your supervisor?

 1. Explain whether and why your gate-level checkers check exactly what
you have specified in PSL or SVA.

 2. What problem arises when trying to check so-called 'strong'
properties using monitoring checkers and how did you handle this in
your design ?

 3. Arbiters can be classified as synchronous or
asynchronous. Consider an arbiter that is the opposite to yours in
this respect and show any corresponding modifications needed to your
temporal logic expressions.

END

