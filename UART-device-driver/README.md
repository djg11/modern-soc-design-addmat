
(C) 1995 DJ Greaves, University of Cambridge, Computer Laboratory.


# UART Device Driver #

Chapter 2 of Modern SoC design by DJ Greaves  presents the basic code for an interrupt-driven device driver.
The full code for a UART serial port  is given here.  The structure is essentially similar for block and streaming audio
devices.



  -rw-rw-r--.  1 djg11 djg11 2765 Aug 11 17:01 uart-driver.txt

Also present are polled I/O routines that can be used early in the boot sequence
before the interrupt system of the SoC has been enabled.


The C code assumes a separate interrupt vector for transmit interrupts:
Typically the transmit and receive ISRs are not separate.  Instead,
all forms of service request are checked by one entry point in the
C device driver code. That would then branch to one of these service
routines.



Many UARTs have integral input and output FIFOs.  These implement enable data to be bulked up and for the
the interrupt load on the host to be reduced.  For transmit, the space available interrupt
may only be raised when the TX FIFO is nearly empty.  For receive, the interrupt may be delayed
in case more data arrives shortly afterwards, enabling the majority of the RC FIFO content to be
drained using a single interrupt.  The presented code can be used for such devices without modification.

EOF
